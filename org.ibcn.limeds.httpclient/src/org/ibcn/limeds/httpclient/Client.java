/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.httpclient;

import java.util.Optional;

import org.ibcn.commons.restclient.api.ClientContext;
import org.ibcn.commons.restclient.api.ClientResponse;
import org.ibcn.commons.restclient.api.Headers;
import org.ibcn.limeds.ServiceSegment;
import org.ibcn.limeds.annotations.HttpMethod;
import org.ibcn.limeds.annotations.InputDoc;
import org.ibcn.limeds.annotations.OutputDoc;
import org.ibcn.limeds.annotations.Segment;
import org.ibcn.limeds.annotations.Service;
import org.ibcn.limeds.annotations.ServiceOperation;
import org.ibcn.limeds.annotations.Validated;
import org.ibcn.limeds.httpclient.api.HttpClient;
import org.ibcn.limeds.httpclient.api.RequestContext;
import org.ibcn.limeds.httpclient.api.Response;
import org.ibcn.limeds.json.Json;
import org.ibcn.limeds.json.JsonObject;
import org.ibcn.limeds.json.JsonValue;

@Segment(id = "limeds.http.Client", description = "This Segment exposes a HTTP client that allows you to perform basic HTTP operations.")
@Validated(input = true, output = false)
public class Client extends ServiceSegment implements HttpClient {

	public static final String LIBRARY_VERSION = "3.0.0";

	@Service
	private org.ibcn.commons.restclient.api.Client httpClient;

	@Override
	@ServiceOperation(description = "Excecute an HTTP get request.")
	@InputDoc(label = "targetURL", schema = "\"String (The request URL.)\"")
	@InputDoc(label = "headers", schema = "{\"@typeInfo\" : \"Object (The optional HTTP request headers map.)\"}")
	public JsonValue get(String url, JsonObject headers) throws Exception {
		ClientContext clientContext = httpClient.target(url);
		applyHeaders(clientContext, headers);
		return outputToJson(clientContext.get().returnString());
	}

	@Override
	@ServiceOperation(description = "Excecute an HTTP put request.")
	@InputDoc(label = "targetURL", schema = "\"String (The request URL.)\"")
	@InputDoc(label = "requestJson", schema = "{\"@typeInfo\" : \"Object * (Any valid JSON data instance to send as body for the request.)\"}")
	@InputDoc(label = "headers", schema = "{\"@typeInfo\" : \"Object (The optional HTTP request headers map.)\"}")
	public JsonValue put(String url, JsonValue requestJson, JsonObject headers) throws Exception {
		ClientContext clientContext = httpClient.target(url);
		applyHeaders(clientContext, headers);
		return outputToJson(clientContext.putJson(requestJson.toString()).returnString());
	}

	@Override
	@ServiceOperation(description = "Excecute an HTTP post request.")
	@InputDoc(label = "targetURL", schema = "\"String (The request URL.)\"")
	@InputDoc(label = "requestJson", schema = "{\"@typeInfo\" : \"Object * (Any valid JSON data instance to send as body for the request.)\"}")
	@InputDoc(label = "headers", schema = "{\"@typeInfo\" : \"Object (The optional HTTP request headers map.)\"}")
	public JsonValue post(String url, JsonValue requestJson, JsonObject headers) throws Exception {
		ClientContext clientContext = httpClient.target(url);
		applyHeaders(clientContext, headers);
		return outputToJson(clientContext.postJson(requestJson.toString()).returnString());
	}

	@Override
	@ServiceOperation(description = "Excecute an HTTP delete request.")
	@InputDoc(label = "targetURL", schema = "\"String (The request URL.)\"")
	@InputDoc(label = "headers", schema = "{\"@typeInfo\" : \"Object (The optional HTTP request headers map.)\"}")
	public JsonValue delete(String url, JsonObject headers) throws Exception {
		ClientContext clientContext = httpClient.target(url);
		applyHeaders(clientContext, headers);
		clientContext.delete().returnNoResult();
		return null;
	}

	@SuppressWarnings("incomplete-switch")
	@Override
	@ServiceOperation(description = "Excecute an advanced HTTP request.")
	@InputDoc(label = "requestContext", schemaClass = RequestContext.class)
	@OutputDoc(schemaClass = Response.class)
	public JsonObject doRequest(JsonObject requestContext) throws Exception {

		HttpMethod method = HttpMethod.valueOf(requestContext.getString("method"));
		ClientContext clientContext = httpClient.target(requestContext.getString("targetUrl"));
		String requestBody = null;

		if (requestContext.has("headers")) {
			applyHeaders(clientContext, requestContext.get("headers").asObject());
		}

		Optional<String> contentType = requestContext.has("headers") ? requestContext.get("headers").asObject()
				.entrySet().stream().filter(e -> e.getKey().toLowerCase().equals("content-type"))
				.map(e -> e.getValue().asString()).findAny() : Optional.empty();
		clientContext.withHeader("Content-Type", contentType.isPresent() ? contentType.get() : "text/plain");
		requestBody = requestContext.getString("body");

		ClientResponse<String> response = null;

		switch (method) {
		case HEAD:
			response = clientContext.head().returnString();
			break;
		case GET:
			response = clientContext.get().returnString();
			break;
		case PUT:
			response = clientContext.put(requestBody).returnString();
			break;
		case POST:
			response = clientContext.post(requestBody).returnString();
			break;
		case DELETE:
			response = clientContext.delete().returnString();
		}

		JsonObject output = new JsonObject();
		Headers origHeaders = response.getHeaders();
		JsonObject headers = new JsonObject();
		origHeaders.keySet().forEach(key -> headers.put(key, origHeaders.getValueAsString(key)));
		output.put("headers", headers);
		output.put("status", response.status());
		if (response.getHeaders().containsKey("content-type")
				&& response.getHeaders().getFirst("content-type").startsWith("application/json")) {
			output.put("body", (JsonValue) Json.from(response.getBody()));
		} else {
			output.put("body", response.getBody());
		}

		return output;
	}

	private JsonValue outputToJson(ClientResponse<String> response) {
		if (response.status() == 200) {
			try {
				return Json.from(response.getBody());
			} catch (Exception e) {
				// The response is no valid JSON
			}
		}
		return null;
	}

	private void applyHeaders(ClientContext clientContext, JsonObject headers) {
		headers.forEach((k, v) -> clientContext.withHeader(k, v.asString()));
	}

	@Override
	@ServiceOperation(description = "Excecute an HTTP get request.")
	public JsonValue get(String url) throws Exception {
		return get(url, new JsonObject());
	}

	@Override
	@ServiceOperation(description = "Excecute an HTTP put request.")
	public JsonValue put(String url, JsonValue requestJson) throws Exception {
		return put(url, requestJson, new JsonObject());
	}

	@Override
	@ServiceOperation(description = "Excecute an HTTP post request.")
	public JsonValue post(String url, JsonValue requestJson) throws Exception {
		return post(url, requestJson, new JsonObject());
	}

	@Override
	@ServiceOperation(description = "Excecute an HTTP delete request.")
	public JsonValue delete(String url) throws Exception {
		return delete(url, new JsonObject());
	}

}
