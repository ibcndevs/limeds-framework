/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.httpclient.api;

import org.ibcn.limeds.json.JsonObject;
import org.ibcn.limeds.json.JsonValue;

public interface HttpClient {

	/**
	 * Simple get to the specified url, returns the response body interpreted as
	 * JSON.
	 */
	JsonValue get(String url) throws Exception;

	JsonValue get(String url, JsonObject headers) throws Exception;

	/**
	 * Simple put to the specified url with the specified request body, returns
	 * the response body interpreted as JSON.
	 */
	JsonValue put(String url, JsonValue requestJson) throws Exception;

	JsonValue put(String url, JsonValue requestJson, JsonObject headers) throws Exception;

	/**
	 * Simple post to the specified url with the specified request body, returns
	 * the response body interpreted as JSON.
	 */
	JsonValue post(String url, JsonValue requestJson) throws Exception;

	JsonValue post(String url, JsonValue requestJson, JsonObject headers) throws Exception;

	/**
	 * Simple delete to the specified url, returns the response body interpreted
	 * as JSON.
	 */
	JsonValue delete(String url) throws Exception;

	JsonValue delete(String url, JsonObject headers) throws Exception;

	/**
	 * Generic request operation for more advanced HTTP calls. The context
	 * parameter is a JSON representation of the RequestContext class. The
	 * returned object is a JSON representation of the Response class.
	 * 
	 * @param context
	 * @return
	 */
	JsonObject doRequest(JsonObject context) throws Exception;

}
