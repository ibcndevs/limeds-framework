/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.httpclient.api;

import org.ibcn.limeds.annotations.HttpMethod;
import org.ibcn.limeds.annotations.JsonAttribute;
import org.ibcn.limeds.annotations.JsonType;
import org.ibcn.limeds.json.JsonObject;
import org.ibcn.limeds.json.JsonValue;

@JsonType
public class RequestContext {

	@JsonAttribute(desc = "The HTTP method for the request.")
	private HttpMethod method = HttpMethod.GET;

	@JsonAttribute(desc = "The target URL for the request.")
	private String targetUrl;

	@JsonAttribute(desc = "The HTTP headers for the request.", optional = true)
	private JsonObject headers = new JsonObject();

	@JsonAttribute(desc = "The body that is sent with the request with the content-type specified using headers. For non-JSON content-types the JSON is sent as text")
	private JsonValue body;

}
