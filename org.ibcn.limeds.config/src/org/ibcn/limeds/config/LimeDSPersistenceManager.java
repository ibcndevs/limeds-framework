/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.config;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.felix.cm.file.ConfigurationHandler;
import org.osgi.service.cm.Configuration;
import org.osgi.service.cm.ConfigurationAdmin;
import org.osgi.service.cm.ConfigurationEvent;
import org.osgi.service.cm.ConfigurationListener;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;

@Component(service = { LimeDSPersistenceManager.class, ConfigurationListener.class }, immediate = true)
public class LimeDSPersistenceManager implements ConfigurationListener {

	private static final String CONFIG_ID_PROP = "limeds.configId";
	private static ExecutorService taskRunner = Executors.newSingleThreadExecutor();

	@FunctionalInterface
	public interface Supplier<T, E extends Throwable> {
		T get() throws E;
	}

	public static <T> java.util.function.Supplier<T> wrap(Supplier<T, Throwable> supplier) {
		return () -> {
			try {
				return supplier.get();
			} catch (Throwable e) {
				throw new RuntimeException(e);
			}
		};
	}

	@Reference(cardinality = ReferenceCardinality.MANDATORY)
	private volatile ConfigurationAdmin configAdmin;

	// Map pid to filename
	private Map<String, String> fileMapping = new HashMap<>();

	private File getStorage() {
		File storage = new File("./config");
		if (!storage.exists()) {
			storage.mkdirs();
		}
		return storage;
	}

	private Configuration findConfig(Dictionary<String, Object> dict,
			java.util.function.Supplier<Configuration> altSource) throws Exception {
		String configId = (String) dict.get(CONFIG_ID_PROP);
		if (configId != null) {
			Configuration[] result = configAdmin.listConfigurations("(" + CONFIG_ID_PROP + "=" + configId + ")");
			if (result != null && result.length > 0) {
				return result[0];
			}
		} else {
			// Generate LimeDS config id
			dict.put(CONFIG_ID_PROP, UUID.randomUUID().toString());
		}
		return altSource.get();
	}

	@SuppressWarnings({ "unchecked" })
	@Activate
	private void activate() throws Exception {
		taskRunner.execute(() -> {
			// Init settings and communicate with config admin
			Arrays.stream(getStorage().listFiles()).forEach(file -> {
				try (FileInputStream in = new FileInputStream(file)) {
					Dictionary<String, Object> dict = ConfigurationHandler.read(in);

					if (dict.get("service.factoryPid") == null) {
						// Load and register regular config
						String pid = (String) dict.get("service.pid");
						Configuration existingConfig = findConfig(dict,
								wrap(() -> configAdmin.getConfiguration(pid, null)));
						existingConfig.update(dict);
						fileMapping.put(pid, file.getName());
						store(existingConfig.getPid(), existingConfig.getProperties());
					} else {
						// Load and register factory config
						Configuration newInstanceConfig = findConfig(dict, wrap(() -> configAdmin
								.createFactoryConfiguration((String) dict.get("service.factoryPid"), null)));
						newInstanceConfig.update(dict);
						fileMapping.put(newInstanceConfig.getPid(), file.getName());
						store(newInstanceConfig.getPid(), newInstanceConfig.getProperties());
					}
				} catch (Exception e) {
					new RuntimeException(e);
				}
			});
		});
	}

	private File getFile(String pid) {
		return fileMapping.containsKey(pid) ? new File(getStorage(), fileMapping.get(pid)) : null;

	}

	private void delete(String pid) throws IOException {
		File configFile = getFile(pid);
		if (configFile != null && configFile.exists()) {
			getFile(pid).delete();
		}
	}

	@SuppressWarnings("rawtypes")
	private void store(String pid, Dictionary properties) throws IOException {
		// Ignore storing factory and transient configurations (need not be
		// persisted)
		if (properties.get("factory.pidList") == null
				&& (properties.get("_transient") == null || !((Boolean) properties.get("_transient")))) {

			if (properties.get("service.factoryPid") == null) {
				// Write regular config

				if (fileMapping.containsKey(pid)) {
					// Config exists
					try (FileOutputStream out = new FileOutputStream(getFile(pid))) {
						ConfigurationHandler.write(out, properties);
					}
				} else {
					// Write new config file
					String filename = pid + ".cfg";
					try (FileOutputStream out = new FileOutputStream(new File(getStorage(), filename))) {
						ConfigurationHandler.write(out, properties);
						fileMapping.put(pid, filename);
					}
				}
			} else {
				// Write factory config

				// Remove runtime specific pid
				properties.remove("service.pid");

				String factoryPid = (String) properties.get("service.factoryPid");
				if (fileMapping.containsKey(pid)) {
					// Update of existing factory config
					try (FileOutputStream out = new FileOutputStream(getFile(pid))) {
						ConfigurationHandler.write(out, properties);
					}
				} else {
					// Persist new factory config
					String filename = factoryPid + "-" + UUID.randomUUID().toString() + ".cfg";
					try (FileOutputStream out = new FileOutputStream(new File(getStorage(), filename))) {
						ConfigurationHandler.write(out, properties);
						fileMapping.put(pid, filename);
					}
				}
			}
		}
	}

	@Override
	public void configurationEvent(ConfigurationEvent event) {
		taskRunner.execute(() -> {
			try {
				if (ConfigurationEvent.CM_UPDATED == event.getType()) {

					store(event.getPid(), configAdmin.getConfiguration(event.getPid(), null).getProperties());

				} else if (ConfigurationEvent.CM_DELETED == event.getType()) {
					delete(event.getPid());
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		});
	}

}
