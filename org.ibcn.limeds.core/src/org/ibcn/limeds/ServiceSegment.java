/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.ibcn.limeds.annotations.ServiceOperation;
import org.ibcn.limeds.json.JsonPrimitive;
import org.ibcn.limeds.json.JsonValue;

public abstract class ServiceSegment implements FunctionalSegment {

	private final Set<Method> operations;

	public ServiceSegment() {
		// Build the method mapping
		operations = Arrays.stream(this.getClass().getDeclaredMethods())
				.filter(f -> f.isAnnotationPresent(ServiceOperation.class)).collect(Collectors.toSet());
	}

	public Set<Method> getSupportedOperations() {
		return operations;
	}

	@Override
	public JsonValue apply(JsonValue... input) throws Exception {
		if (input.length > 0) {
			// First arg determines operation to call
			String operation = input[0].asString();
			Optional<Method> targetMethod = operations.stream().filter(m -> operation.equals(m.getName()))
					.filter(m -> m.getParameterCount() == input.length - 1).findFirst();
			if (targetMethod.isPresent()) {
				int nrOfArgs = targetMethod.get().getParameters().length;

				// Box arguments
				List<Object> args = new LinkedList<>();
				Arrays.stream(input).skip(1).map(ServiceSegment::unbox).forEach(args::add);

				// Pad list with nulls until we have the correct number
				for (int i = 0; i < (nrOfArgs - args.size()); i++) {
					args.add(null);
				}

				// Call using reflection & (un)box i/o
				return box(targetMethod.get().invoke(this, args.toArray()));
			} else {
				throw new UnsupportedOperationException("The operation '" + operation + "' with " + (input.length - 1)
						+ " arguments cannot be called on this segment.");
			}
		} else {
			throw new UnsupportedOperationException("Segment is a ServiceSegment but no operation was specified...");
		}
	}

	public static Object unbox(JsonValue value) {
		if (value instanceof JsonPrimitive) {
			return ((JsonPrimitive) value).getValue();
		} else {
			return value;
		}
	}

	public static JsonValue box(Object value) {
		if (value instanceof JsonValue) {
			return (JsonValue) value;
		} else {
			return new JsonPrimitive(value);
		}
	}

}
