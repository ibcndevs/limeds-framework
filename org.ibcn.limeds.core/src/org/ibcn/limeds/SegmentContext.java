/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds;

import org.ibcn.limeds.descriptors.SegmentDescriptor;

/**
 * Represents the context of a Segment component.
 * 
 * @author wkerckho
 *
 */
public class SegmentContext implements Comparable<SegmentContext> {

	private final SegmentDescriptor descriptor;
	private final FunctionalSegment instance;

	/**
	 * Creates a new SegmentContext from a descriptor and instance reference.
	 * 
	 * @param descriptor
	 *            The SegmentDescriptor that contains all the meta-data for the
	 *            component.
	 * @param intance
	 *            The instance that is currently backing the component.
	 */
	public SegmentContext(SegmentDescriptor descriptor, FunctionalSegment intance) {
		this.descriptor = descriptor;
		this.instance = intance;
	}

	/**
	 * Gets the SegmentDescriptor (meta-data) for this context.
	 * 
	 * @return A SegmentDescriptor instance.
	 */
	public SegmentDescriptor getDescriptor() {
		return descriptor;
	}

	/**
	 * Gets the instance of the component.
	 * 
	 * @return A FunctionalSegment instance
	 */
	public FunctionalSegment getInstance() {
		return instance;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((descriptor == null) ? 0 : descriptor.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SegmentContext other = (SegmentContext) obj;
		if (descriptor == null) {
			if (other.descriptor != null)
				return false;
		} else if (!descriptor.equals(other.descriptor))
			return false;
		return true;
	}

	/**
	 * The order of SegmentContext instances is based on the component id and
	 * version.
	 */
	@Override
	public int compareTo(SegmentContext o) {
		return (descriptor.getId() + "_" + descriptor.getVersion())
				.compareTo(o.getDescriptor().getId() + "_" + o.getDescriptor().getVersion());
	}

}
