/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.exceptions;

import org.ibcn.limeds.json.JsonValue;

/**
 * An exception that can be thrown by an aspect when executing the doBefore
 * method. This way, it lets the framework know that the actual
 * FunctionalSegment call (apply) should not be performed, as the result was
 * already obtained by the aspect.
 * 
 * This system is used for aspects that implement the framework's load balancing
 * and caching features.
 * 
 * @author wkerckho
 *
 */
@SuppressWarnings("serial")
public class HandledByAspect extends Exception {

	private final JsonValue content;

	public HandledByAspect(JsonValue content) {
		this.content = content;
	}

	/**
	 * Get the content produced by the aspect that would otherwise have been
	 * retrieved by calling the flow component.
	 * 
	 * @return
	 */
	public JsonValue getContent() {
		return content;
	}

}
