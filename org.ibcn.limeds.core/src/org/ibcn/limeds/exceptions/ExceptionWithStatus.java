/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.exceptions;

/**
 * Can be used by Segments to throw Exceptions that contain a HTTP status code.
 * This can be very useful if the FunctionalSegment apply-method is exposed as
 * an HTTP endpoint.
 * 
 * @author wkerckho
 *
 */
public class ExceptionWithStatus extends Exception {

	private static final long serialVersionUID = 5817761254544188697L;
	private final int status;
	private final String message;

	/**
	 * Creates an exception with a HTTP error code and an explanatory message.
	 */
	public ExceptionWithStatus(int status, String message) {
		this.message = message;
		this.status = status;
	}

	/**
	 * Creates an exception only containing a HTTP error code.
	 */
	public ExceptionWithStatus(int status) {
		this.status = status;
		this.message = "";
	}

	/**
	 * Get the HTTP error code.
	 */
	public int getStatus() {
		return status;
	}

	/**
	 * Get the accompanying message for the status code (empty string if no
	 * message was provided).
	 */
	public String getMessage() {
		return message;
	}

}
