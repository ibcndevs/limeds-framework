/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.exceptions;

import java.util.Arrays;
import java.util.Optional;

import javax.script.ScriptException;

import org.ibcn.limeds.SegmentContext;

@SuppressWarnings("serial")
public class SegmentExecutionException extends Exception {

	private SegmentContext context;

	public SegmentExecutionException(SegmentContext context, Throwable parent) {
		super(parent);
		this.context = context;
	}

	@Override
	public String getMessage() {
		return "[" + context.getDescriptor() + ".apply(..)] " + super.getMessage();
	}

	public SegmentContext getContext() {
		return context;
	}

	public String getLocation() {
		if (getCause() instanceof ScriptException) {
			int delimIndex = getCause().getMessage().lastIndexOf(' ');
			return context.getDescriptor().getId() + " (<eval>:" + getCause().getMessage().substring(delimIndex + 1)
					+ ")";
		} else if (getCause() instanceof RuntimeException
				&& getCause().getStackTrace()[0].getClassName().equals("jdk.nashorn.internal.runtime.ScriptRuntime")) {
			Optional<StackTraceElement> jsError = Arrays.stream(getCause().getCause().getStackTrace())
					.filter(el -> el.getClassName().startsWith("jdk.nashorn.internal.scripts.Script$Recompilation"))
					.findFirst();
			if (jsError.isPresent()) {
				return context.getDescriptor().getId() + " (<eval>:" + jsError.get().getLineNumber() + ")";
			}
		} else {
			Optional<StackTraceElement> errorSource = Arrays.stream(getCause().getStackTrace())
					.filter(el -> el.getClassName().equals(context.getDescriptor().getHandlerName())).findFirst();
			if (errorSource.isPresent()) {
				return errorSource.get().toString();
			}
		}
		return context.getDescriptor().getHandlerName() + " (Unknown Source)";
	}

}
