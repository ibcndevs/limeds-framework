/**
 * This package contains the LimeDS exception definitions.
 */
@org.osgi.annotation.versioning.Version("1.0.0")
package org.ibcn.limeds.exceptions;
