/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * This interface provides a hook for monitoring the LimeDS HTTP server.
 * 
 * @author wkerckho
 */
public interface HttpMonitor {

	public static abstract class Tracker implements AutoCloseable {

		protected final long startTS;
		protected final HttpServletRequest request;
		protected final HttpServletResponse response;

		protected Tracker(HttpServletRequest request, HttpServletResponse response) {
			startTS = System.currentTimeMillis();
			this.request = request;
			this.response = response;
		}

		@Override
		public void close() {
			onStop(System.currentTimeMillis() - startTS);
		}

		/**
		 * Implement this method to specify what should happen when a tracker is
		 * stopped. Note that this call blocks the HTTP operation, so be careful
		 * not to impact performance!
		 * 
		 * @param executionTime
		 *            The execution time of the tracked HTTP method in
		 *            milliseconds.
		 */
		protected abstract void onStop(long executionTime);

	}

	/**
	 * Creates a tracker for monitoring a HTTP method call.
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	Tracker startTracking(HttpServletRequest request, HttpServletResponse response);

}
