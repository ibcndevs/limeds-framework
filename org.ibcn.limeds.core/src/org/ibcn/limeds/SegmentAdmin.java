/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds;

import java.util.Optional;
import java.util.Set;

import org.ibcn.limeds.aspects.AspectFactory;
import org.ibcn.limeds.descriptors.SegmentDescriptor;
import org.ibcn.limeds.json.JsonArray;
import org.ibcn.limeds.json.JsonObject;
import org.osgi.framework.BundleContext;

/**
 * This interface defines the SegmentAdmin service. Using this service clients
 * can register new Components and Component Aspects with LimeDS. It also
 * provides an overview of the registered components allowing clients to gain
 * insight in the current status of the LimeDS platform.
 * 
 * @author wkerckho
 *
 */
public interface SegmentAdmin {

	/**
	 * Register a new AspectFactory
	 */
	void addAspectFactory(AspectFactory aspectFactory) throws Exception;

	/**
	 * Unregister the specified AspectFactory
	 */
	void removeAspectFactory(AspectFactory aspectFactory) throws Exception;

	/**
	 * Return the registered Aspect Factories.
	 */
	Set<AspectFactory> getRegisteredAspects();

	/**
	 * Register a new component by specifying a descriptor, the instance to host
	 * and the bundle context of the component.
	 * 
	 * @param descriptor
	 *            An instance of SegmentDescriptor that fully describes the
	 *            properties of the component, such as its dependencies,
	 *            documentation, etc...
	 * @param instance
	 *            The object instance that will back this component. At the
	 *            moment only instances that implement FunctionalSegment are
	 *            supported.
	 * @param context
	 *            The OSGi BundleContext to register this component with. This
	 *            allows the framework to preserve the link between an OSGi
	 *            service (which is used to implement the component) and the
	 *            source bundle that has registered the service.
	 * @throws Exception
	 */
	void register(SegmentDescriptor descriptor, Object instance, BundleContext context) throws Exception;

	/**
	 * Unregister a component by specifying a descriptor and the bundle context
	 * of the component.
	 * 
	 * @param descriptor
	 *            An instance of SegmentDescriptor that uniquely identifies the
	 *            component.
	 * @param context
	 *            The OSGi BundleContext to unregister this component with.
	 * @throws Exception
	 */
	void unregister(SegmentDescriptor descriptor, BundleContext context) throws Exception;

	/**
	 * List the meta-data of the registered LimeDS components.
	 * 
	 * @return A list of SegmentDescriptor instances
	 */
	Set<SegmentDescriptor> getRegisteredSegments();
	
	SegmentDescriptor findDescriptor(FunctionalSegment segment);

	Optional<SegmentDescriptor> findRegisteredSegment(String componentId, String versionExpr);

	/**
	 * Get live information about the specified component (uniquely defined by
	 * id and version) as a JSON object.
	 * 
	 * @param componentId
	 *            The id of the component
	 * @param version
	 *            The version of the component
	 * @return A JsonObject instance containing dynamic information about the
	 *         component
	 */
	Optional<JsonObject> getSegmentInfo(String componentId, String version);

	/**
	 * Get live information about the registered components as a JSON array.
	 * 
	 * @return A JsonArray instance containing dynamic information about the
	 *         registered components
	 */
	JsonArray getSegmentsInfo();

	/**
	 * Get information about the available component factories as a JSON array.
	 * 
	 * @return A JsonArray instance containing information about the registered
	 *         factories.
	 */
	JsonArray getSegmentFactoriesInfo();
	
	/**
	 * Get information about the specified factory (uniquely defined by
	 * id and version) as a JSON object.
	 * 
	 * @param componentId
	 *            The id of the factory
	 * @param version
	 *            The version of the factory
	 * @return A JsonObject instance containing information about the
	 *         factory
	 */
	JsonObject getSegmentFactoryInfo(String componentId, String version);

	/**
	 * List the currently registered HTTP operations as an ArrayNode
	 * 
	 * @return A JsonArray instance containing information about the HTTP
	 *         endpoints that have been setup by the currently active LimeDS
	 *         components.
	 */
	JsonArray getRegisteredHttpOperations();


}
