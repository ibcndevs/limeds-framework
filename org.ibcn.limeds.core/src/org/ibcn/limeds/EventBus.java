/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds;

import org.ibcn.limeds.json.JsonValue;

/**
 * This interface specifies the LimeDS EventBus, which is provided as an
 * alternative way of communication between Segments as opposed to the
 * synchronous service-oriented approach provided by links.
 * 
 * @author wkerckho
 *
 */
public interface EventBus {

	/**
	 * Broadcasts the specified event (JSON) on the specified channel.
	 * <p>
	 * The method returns immediately (doesn't wait for the event to be
	 * processed by the registered handles).
	 */
	void broadcast(String channelID, JsonValue event);

}
