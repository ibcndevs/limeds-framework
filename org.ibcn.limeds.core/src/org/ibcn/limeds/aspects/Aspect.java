/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.aspects;

import java.util.Optional;

import org.ibcn.limeds.json.JsonValue;

/**
 * Interface defining a Aspect, a framework mechanism to extend upon component
 * handling by allowing functionality to be wrapped around component
 * interactions, i.e. before and after the apply method of a FunctionalSegment.
 * 
 * @author wkerckho
 *
 */
public interface Aspect {

	/**
	 * This method is called by LimeDS for each component that is activated.
	 * <p>
	 * This allows the aspect to configure fields or setup data-structures or
	 * objects that are required for the doBefore and doAfter functionality.
	 */
	void init();

	/**
	 * This method allows an aspect to hook into the execution logic before the
	 * component is called.
	 * 
	 * @param input
	 *            The arguments that will be passed to the component
	 * @throws Exception
	 *             An exception is thrown if the Aspect cannot approve the call
	 *             to the component (e.g. when validation is performed on the
	 *             input data). In this case, the Segment will not be executed
	 *             and the error will be rethrown up the call stack.
	 */
	void doBefore(JsonValue... input) throws Exception;

	/**
	 * This method allows an aspect to hook into the execution logic after the
	 * component is called.
	 * 
	 * @param output
	 *            The value that is returned from the component
	 * @param error
	 *            An optional indicating whether the component executed
	 *            successfully. If this is not the case, the optional will
	 *            contain the error that was thrown.
	 * @throws Exception
	 *             An exception is thrown if the Aspect cannot approve the call
	 *             to the component (e.g. when validation is performed on the
	 *             output data). In this case, the output of the Segment will be
	 *             discarded and the error will be rethrown up the call stack.
	 */
	void doAfter(JsonValue[] input, JsonValue output, Optional<Throwable> error) throws Exception;

	/**
	 * This method is called by LimeDS for each component that is deactivated.
	 * <p>
	 * This allows cleaning up the fields or objects that were created when
	 * calling init().
	 */
	void destroy();

}
