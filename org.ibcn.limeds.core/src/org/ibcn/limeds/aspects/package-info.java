/**
 * This package specifies the Aspect feature of LimeDS.
 */
@org.osgi.annotation.versioning.Version("1.1.0")
package org.ibcn.limeds.aspects;
