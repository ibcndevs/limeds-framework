/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.aspects;

import java.util.Optional;

import org.ibcn.limeds.SegmentContext;

/**
 * This interface defines an AspectFactory. AspectFactory implementations can be
 * registered as OSGi services by developers in order to extend the LimeDS
 * framework with new aspects which can be applied to FunctionalSegments.
 * <p>
 * The registered AspectFactory instances are then used by the system to setup
 * aspect chains which are called before and after the execution of
 * FunctionalSegment apply methods (if the resp. Segment supports the resp.
 * Aspect).
 * 
 * @author wkerckho
 *
 */
public interface AspectFactory {

	/**
	 * Aspects with a lower priority value are executed first. Aspects such as
	 * Validation should be executed at the beginning of the chain, in order to
	 * avoid it being skipped due to aspects that can interrupt the chain, such
	 * as Caching.
	 * 
	 * @return
	 */
	int getPriority();

	/**
	 * Returns an optional instance of a Aspect for the specified component
	 * context. The optional will be empty if the aspect does not apply for this
	 * context.
	 * 
	 * @param context
	 * @return
	 */
	Optional<Aspect> createInstance(SegmentContext context);
}
