/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.descriptors;

import java.util.function.Predicate;

import org.ibcn.limeds.annotations.HttpMethod;
import org.ibcn.limeds.json.JsonValue;

/**
 * Describes a HTTP assertion, which can be used to couple the life cycle of a
 * Segment to the availability of a specified HTTP resource.
 * <p>
 * E.g. when an input adapter component depends on a Web API for data retrieval,
 * a HTTP assertion should be added so the framework can automatically disable
 * the component when the Web API cannot be reached.
 * 
 * @author wkerckho
 *
 */
public class HttpAssertionDescriptor {

	protected String target;
	protected HttpMethod method;
	protected String request;
	protected String requestMimeType;
	protected String responseValidatorScript;
	protected transient Predicate<JsonValue> responseValidator;

	/**
	 * Get the target URL to check.
	 */
	public String getTarget() {
		return target;
	}

	/**
	 * Set the target URL to check.
	 */
	public void setTarget(String target) {
		this.target = target;
	}

	/**
	 * Get the HTTP method to use.
	 */
	public HttpMethod getMethod() {
		return method;
	}

	/**
	 * Set the HTTP method to use.
	 */
	public void setMethod(HttpMethod method) {
		this.method = method;
	}

	/**
	 * Get the body that should be sent with the request (optional).
	 */
	public String getRequest() {
		return request;
	}

	/**
	 * Set the body that should be sent with the request (optional).
	 */
	public void setRequest(String request) {
		this.request = request;
	}

	public String getRequestMimeType() {
		return requestMimeType;
	}

	public void setRequestMimeType(String requestMimeType) {
		this.requestMimeType = requestMimeType;
	}

	/**
	 * Get the validation Javascript that should be applied to the HTTP response
	 * (optional).
	 */
	public String getResponseValidatorScript() {
		return responseValidatorScript;
	}

	/**
	 * Set the validation Javascript that should be applied to the HTTP response
	 * (optional).
	 */
	public void setResponseValidatorScript(String script) {
		this.responseValidatorScript = script;
	}

	/**
	 * Get the predicate that should be applied to the HTTP response (optional).
	 */
	public Predicate<JsonValue> getResponseValidator() {
		return responseValidator;
	}

	/**
	 * Set the predicate that should be applied to the HTTP response (optional).
	 */
	public void setResponseValidator(Predicate<JsonValue> responseValidator) {
		this.responseValidator = responseValidator;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((method == null) ? 0 : method.hashCode());
		result = prime * result + ((request == null) ? 0 : request.hashCode());
		result = prime * result + ((responseValidatorScript == null) ? 0 : responseValidatorScript.hashCode());
		result = prime * result + ((target == null) ? 0 : target.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		HttpAssertionDescriptor other = (HttpAssertionDescriptor) obj;
		if (method != other.method)
			return false;
		if (request == null) {
			if (other.request != null)
				return false;
		} else if (!request.equals(other.request))
			return false;
		if (responseValidatorScript == null) {
			if (other.responseValidatorScript != null)
				return false;
		} else if (!responseValidatorScript.equals(other.responseValidatorScript))
			return false;
		if (target == null) {
			if (other.target != null)
				return false;
		} else if (!target.equals(other.target))
			return false;
		return true;
	}

}
