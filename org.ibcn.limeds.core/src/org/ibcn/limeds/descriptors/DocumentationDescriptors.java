/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.descriptors;

import java.util.LinkedList;
import java.util.List;

import org.ibcn.limeds.annotations.JsonAttribute;
import org.ibcn.limeds.json.JsonValue;

public class DocumentationDescriptors {

	public static class Input {
		@JsonAttribute(optional = true)
		private String label;
		private JsonValue schema;

		public String getLabel() {
			return label;
		}

		public void setLabel(String id) {
			this.label = id;
		}

		public JsonValue getSchema() {
			return schema;
		}

		public void setSchema(JsonValue schema) {
			this.schema = schema;
		}
	}

	public static class Output {

		private JsonValue schema;

		public JsonValue getSchema() {
			return schema;
		}

		public void setSchema(JsonValue schema) {
			this.schema = schema;
		}

	}

	public static class Operation {

		private String id;

		@JsonAttribute(optional = true)
		private String description;

		@JsonAttribute(optional = true)
		private List<Input> in;
		@JsonAttribute(optional = true)
		private Output out;

		public Operation() {
			in = new LinkedList<>();
		}

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public String getDescription() {
			return description;
		}

		public void setDescription(String description) {
			this.description = description;
		}

		public List<Input> getIn() {
			return in;
		}

		public void setIn(List<Input> in) {
			this.in = in;
		}

		public Output getOut() {
			return out;
		}

		public void setOut(Output out) {
			this.out = out;
		}

	}

	public static class Http {

		@JsonAttribute(optional = true)
		private JsonValue querySchema;
		private String[] pathInfo;

		public JsonValue getQuerySchema() {
			return querySchema;
		}

		public void setQuerySchema(JsonValue querySchema) {
			this.querySchema = querySchema;
		}

		public String[] getPathInfo() {
			return pathInfo;
		}

		public void setPathInfo(String[] pathInfo) {
			this.pathInfo = pathInfo;
		}

	}

}
