/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.descriptors;

/**
 * This enum lists the programming languages LimeDS wishes to support
 * out-of-the-box.
 * <p>
 * Python support is currently pending (though a proof-of-concept implementation
 * has been realized on top of Jython).
 * 
 * @author wkerckho
 *
 */
public enum SegmentLanguage {
	Java, Javascript, Python;

	public boolean isEqual(String languageId) {
		return toString().toLowerCase().equals(languageId.toLowerCase());
	}
}
