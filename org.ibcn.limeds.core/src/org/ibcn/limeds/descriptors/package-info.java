/**
 * This package contains the classes used to model the LimeDS component
 * meta-data.
 */
@org.osgi.annotation.versioning.Version("2.0.0")
package org.ibcn.limeds.descriptors;
