/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.descriptors;

import java.util.concurrent.TimeUnit;

import org.ibcn.limeds.annotations.Cached;

/**
 * This class is used to describe a Segment cache configuration.
 * 
 * @author wkerckho
 *
 */
public class CacheDescriptor {

	public static final int DEFAULT_SIZE = 512;

	private int size = DEFAULT_SIZE;
	private long retentionDuration = 5;
	private TimeUnit retentionDurationUnit = TimeUnit.MINUTES;

	/**
	 * Creates a new CacheDescriptor
	 */
	public CacheDescriptor() {

	}

	/**
	 * Creates a CacheDescriptor from a @Cached annotation instance.
	 * 
	 * @param cached
	 */
	public CacheDescriptor(Cached cached) {
		size = cached.size();
		retentionDuration = cached.retentionDuration();
		retentionDurationUnit = cached.retentionDurationUnit();
	}

	/**
	 * Get the maximum size of the cache (how many output objects it can hold).
	 */
	public int getSize() {
		return size;
	}

	/**
	 * Set the maximum size of the cache (how many output objects it can hold).
	 */
	public void setSize(int size) {
		this.size = size;
	}

	/**
	 * Get the maximum time an object can be stored in the cache (in
	 * milliseconds, unless otherwise specified using retentionDurationUnit).
	 */
	public long getRetentionDuration() {
		return retentionDuration;
	}

	/**
	 * Set the maximum time an object can be stored in the cache (in
	 * milliseconds, unless otherwise specified using retentionDurationUnit).
	 */
	public void setRetentionDuration(long retentionDuration) {
		this.retentionDuration = retentionDuration;
	}

	/**
	 * Get the time unit for the retention duration (default is milliseconds).
	 */
	public TimeUnit getRetentionDurationUnit() {
		return retentionDurationUnit;
	}

	/**
	 * Set the time unit for the retention duration (default is milliseconds).
	 */
	public void setRetentionDurationUnit(TimeUnit retentionDurationUnit) {
		this.retentionDurationUnit = retentionDurationUnit;
	}

}
