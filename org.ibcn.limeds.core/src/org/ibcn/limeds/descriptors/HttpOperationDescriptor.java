/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.descriptors;

import org.ibcn.limeds.annotations.AuthMode;
import org.ibcn.limeds.annotations.HttpMethod;
import org.ibcn.limeds.annotations.HttpOperation;
import org.ibcn.limeds.annotations.JsonAttribute;
import org.ibcn.limeds.annotations.LoadBalancingStrategy;

/**
 * Describes how a FunctionalSegment should be exposed as an HTTP endpoint.
 * 
 * @author wkerckho
 *
 */
public class HttpOperationDescriptor {

	@JsonAttribute(optional = true)
	protected String groupId;
	@JsonAttribute(optional = true)
	protected HttpMethod method;
	@JsonAttribute(optional = true)
	protected String path;
	@JsonAttribute(optional = true)
	protected String[] authorityRequired;
	@JsonAttribute(optional = true)
	protected AuthMode authMode = AuthMode.NONE;
	@JsonAttribute(optional = true)
	protected String loadBalancing = LoadBalancingStrategy.NONE;

	public HttpOperationDescriptor() {
	}

	public HttpOperationDescriptor(HttpMethod method, String path) {
		this.method = method;
		this.path = path;
	}

	public HttpOperationDescriptor(HttpOperation operationAnnotation) {
		this.method = operationAnnotation.method();
		this.path = operationAnnotation.path();
		this.authMode = operationAnnotation.authMode();
		this.authorityRequired = operationAnnotation.authorityRequired();
		this.loadBalancing = operationAnnotation.loadBalancing();
	}

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	/**
	 * Get the HTTP method of the endpoint.
	 */
	public HttpMethod getMethod() {
		return method;
	}

	/**
	 * Set the HTTP method of the endpoint.
	 */
	public void setMethod(HttpMethod method) {
		this.method = method;
	}

	/**
	 * Get the path of the endpoint.
	 */
	public String getPath() {
		return path;
	}

	/**
	 * Set the path of the endpoint.
	 * <p>
	 * It can contains path parameters using the {} syntax (e.g.
	 * /authors/{authorId}/books/{bookId}) that will be inserted in the flow
	 * function apply through the httpContext JSON object that is passed as an
	 * additional argument.
	 */
	public void setPath(String path) {
		this.path = path;
	}

	/**
	 * Get the array of authorities that are required for this operation
	 * (depends on the configured AuthMode).
	 */
	public String[] getAuthorityRequired() {
		return authorityRequired;
	}

	/**
	 * Sets the array of authorities that are required for this operation.
	 */
	public void setAuthorityRequired(String[] authorityRequired) {
		this.authorityRequired = authorityRequired;
	}

	/**
	 * Get the authMode for this operation, the default behaviour is that no
	 * authorization is required.
	 */
	public AuthMode getAuthMode() {
		return authMode;
	}

	/**
	 * Set the authMode for this operation
	 */
	public void setAuthMode(AuthMode authMode) {
		this.authMode = authMode;
	}

	/**
	 * Get the load balancing mode for this operation
	 */
	public String getLoadBalancing() {
		return loadBalancing;
	}

	/**
	 * Set the load balancing mode for this operation
	 */
	public void setLoadBalancing(String loadBalancing) {
		this.loadBalancing = loadBalancing;
	}

}
