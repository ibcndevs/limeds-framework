/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.descriptors;

import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import org.ibcn.limeds.annotations.JsonAttribute;
import org.ibcn.limeds.json.JsonObject;
import org.ibcn.limeds.json.JsonValue;

/**
 * This class represents the meta-data of a LimeDS component.
 * 
 * @author wkerckho
 *
 */
public class SegmentDescriptor {

	protected String id;
	protected String version = "0.0.0";
	@JsonAttribute(optional = true, desc = "Optional description of the Segment.")
	protected String description = "";
	protected boolean export = false;
	protected String language;
	@JsonAttribute(optional = true)
	protected Set<String> tags;
	@JsonAttribute(optional = true)
	protected Set<String> serviceProperties;
	protected Set<String> interfaces;
	@JsonAttribute(optional = true)
	protected Set<String> subscriptions;
	@JsonAttribute(optional = true)
	protected TemplateDescriptor templateInfo;
	@JsonAttribute(optional = true)
	protected Set<DocumentationDescriptors.Operation> documentation;
	@JsonAttribute(optional = true)
	protected DocumentationDescriptors.Http httpDocumentation;
	@JsonAttribute(optional = true)
	protected HttpOperationDescriptor httpOperation;
	@JsonAttribute(optional = true)
	protected String schedule;
	@JsonAttribute(optional = true)
	protected Map<String, DependencyDescriptor> dependencies;
	@JsonAttribute(optional = true)
	protected Set<HttpAssertionDescriptor> httpAssertions;
	@JsonAttribute(optional = true)
	protected Map<String, JsonValue> configureAspects;

	protected transient String handlerName = null;

	public SegmentDescriptor() {
		tags = new LinkedHashSet<>();
		serviceProperties = new LinkedHashSet<>();
		subscriptions = new LinkedHashSet<>();
		interfaces = new LinkedHashSet<>();
		documentation = new LinkedHashSet<>();
		dependencies = new LinkedHashMap<>();
		httpAssertions = new LinkedHashSet<>();
		configureAspects = new JsonObject();
	}

	/**
	 * Returns the id of the component.
	 */
	public String getId() {
		return id;
	}

	/**
	 * Sets the id of the component.
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Get the id of the programming language used to implement this component.
	 * <p>
	 * If the used language is something other than Java, the system will use
	 * this id to lookup the binding to interface with this language through the
	 * LanguageAdapterFactory interface.
	 */
	public String getLanguage() {
		return language;
	}

	/**
	 * Set the id of the programming language used to implement this component.
	 * 
	 * @param language
	 */
	public void setLanguage(String language) {
		this.language = language;
	}

	/**
	 * Return the descriptor of this component.
	 * 
	 * @return A brief statement that describes the purpose of the component,
	 *         can be empty.
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Set the descriptor of this component.
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Returns the tags of the component.
	 */
	public Set<String> getTags() {
		return tags;
	}

	/**
	 * Sets the tags of the component.
	 */
	public void setTags(Set<String> tags) {
		this.tags = tags;
	}

	/**
	 * Returns the service properties of the component.
	 */
	public Set<String> getServiceProperties() {
		return serviceProperties;
	}

	/**
	 * Sets the service properties of the component.
	 */
	public void setServiceProperties(Set<String> serviceProperties) {
		this.serviceProperties = serviceProperties;
	}

	/**
	 * Returns the event channel subscriptions (a set of target channel IDs) for
	 * this component.
	 */
	public Set<String> getSubscriptions() {
		return subscriptions;
	}

	/**
	 * Sets the event channel subscriptions (a set of target channel IDs) for
	 * this component.
	 */
	public void setSubscriptions(Set<String> subscriptions) {
		this.subscriptions = subscriptions;
	}

	/**
	 * Returns the interfaces implemented by the component.
	 */
	public Set<String> getInterfaces() {
		return interfaces;
	}

	/**
	 * Sets the interfaces implemented by the component.
	 */
	public void setInterfaces(Set<String> interfaces) {
		this.interfaces = interfaces;
	}

	/**
	 * Returns the template descriptor for this component.
	 * <p>
	 * A TemplateDescriptor instance is only returned if the component defines
	 * configuration properties or if it is a component factory. Otherwise this
	 * method always returns null.
	 */
	public TemplateDescriptor getTemplateInfo() {
		return templateInfo;
	}

	/**
	 * Sets the template descriptor for this component.
	 */
	public void setTemplateInfo(TemplateDescriptor templateInfo) {
		this.templateInfo = templateInfo;
	}

	public Set<DocumentationDescriptors.Operation> getDocumentation() {
		return documentation;
	}

	public void setDocumentation(Set<DocumentationDescriptors.Operation> documentation) {
		this.documentation = documentation;
	}

	/**
	 * Gets the http documentation for this component.
	 */
	public DocumentationDescriptors.Http getHttpDocumentation() {
		return httpDocumentation;
	}

	/**
	 * Sets the http documentation for this component.
	 */
	public void setHttpDocumentation(DocumentationDescriptors.Http httpDocumentation) {
		this.httpDocumentation = httpDocumentation;
	}

	/**
	 * Returns the HttpOperationDescriptor of the component. If no http endpoint
	 * was configured, this method returns null.
	 */
	public HttpOperationDescriptor getHttpOperation() {
		return httpOperation;
	}

	/**
	 * Sets the HttpOperationDescriptor of the component.
	 */
	public void setHttpOperation(HttpOperationDescriptor httpOperation) {
		this.httpOperation = httpOperation;
	}

	/**
	 * Returns the schedule of the component. If no schedule was configured,
	 * this method returns null.
	 */
	public String getSchedule() {
		return schedule;
	}

	/**
	 * Sets the schedule of the component.
	 */
	public void setSchedule(String schedule) {
		this.schedule = schedule;
	}

	/**
	 * Returns the http assertions of the component.
	 */
	public Set<HttpAssertionDescriptor> getHttpAssertions() {
		return httpAssertions;
	}

	/**
	 * Sets the http assertions of the component.
	 */
	public void setHttpAssertions(Set<HttpAssertionDescriptor> httpAssertions) {
		this.httpAssertions = httpAssertions;
	}

	/**
	 * Returns the dependencies of the component.
	 */
	public Map<String, DependencyDescriptor> getDependencies() {
		return dependencies;
	}

	/**
	 * Sets the dependencies of the component.
	 */
	public void setDependencies(Map<String, DependencyDescriptor> dependencies) {
		this.dependencies = dependencies;
	}

	public Set<JsonValue> getDocReferenceTypes() {
		Set<JsonValue> result = new HashSet<>();
		/*
		 * If there is documentation for the apply function or other operations,
		 * filter all type references and add these to the result.
		 */
		if (documentation != null) {
			documentation.stream().forEach(op -> {
				if (op.getIn() != null) {
					op.getIn().stream()
							.filter(in -> in.getSchema() instanceof JsonObject && in.getSchema().has("@typeRef"))
							.forEach(in -> result.add(in.getSchema()));
				}
				if (op.getOut() != null && op.getOut().getSchema() instanceof JsonObject
						&& op.getOut().getSchema().has("@typeRef")) {
					result.add(op.getOut().getSchema());
				}
			});
		}

		/*
		 * If there is HTTP documentation for which the query schema contains a
		 * type reference, add it to the result.
		 */
		if (httpDocumentation != null && httpDocumentation.getQuerySchema() instanceof JsonObject
				&& httpDocumentation.getQuerySchema().has("@typeRef")) {
			result.add(httpDocumentation.getQuerySchema());
		}
		return result;
	}

	/**
	 * Get the version of the component.
	 * <p>
	 * Component versions are automatically derived from the version of the Java
	 * package or the Data Flow that encapsulates the function. Support for
	 * manually setting the version might be added in the future.
	 * <p>
	 * Versioning for the package or Data Flow should follow the SemVer
	 * specification: http://semver.org/
	 */
	public String getVersion() {
		return version;
	}

	/**
	 * Set the version of the component.
	 */
	public void setVersion(String version) {
		this.version = version;
	}

	/**
	 * Get if the component should be publicly visible to other components.
	 * <p>
	 * Components for which the export attribute is set to false, will not end
	 * up in the LimeDS component index, meaning they will not show up when
	 * building Flows using the Visual Editor. Note that this is not a security
	 * measure, but a way to reduce function cluttering and enable some form of
	 * encapsulation when designing data flows.
	 */
	public boolean isExport() {
		return export;
	}

	/**
	 * Set if the component should be publicly visible to other components.
	 */
	public void setExport(boolean export) {
		this.export = export;
	}

	public Map<String, JsonValue> getConfigureAspects() {
		return configureAspects;
	}

	public void setConfigureAspects(Map<String, JsonValue> configureAspects) {
		this.configureAspects = configureAspects;
	}

	public String getHandlerName() {
		return handlerName != null ? handlerName : id;
	}

	public void setHandlerName(String handlerName) {
		this.handlerName = handlerName;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((version == null) ? 0 : version.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SegmentDescriptor other = (SegmentDescriptor) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (version == null) {
			if (other.version != null)
				return false;
		} else if (!version.equals(other.version))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return id + "_" + version;
	}

}
