/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.descriptors;

import java.lang.reflect.Field;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.ibcn.limeds.FunctionalSegment;
import org.ibcn.limeds.ServicePropertyNames;
import org.ibcn.limeds.annotations.JsonAttribute;
import org.ibcn.limeds.annotations.Link;
import org.ibcn.limeds.util.FilterUtil;
import org.ibcn.limeds.versioning.VersionUtil;

/**
 * Describes a dependency of a Segment.
 */
public class DependencyDescriptor {

	protected boolean required;
	protected boolean collection;
	protected String type;
	@JsonAttribute(optional = true)
	protected String propertyFilter;
	@JsonAttribute(optional = true)
	protected Map<String, String> functionTargets;
	@JsonAttribute(optional = true)
	protected String preferredId;
	protected transient Object instance;
	@JsonAttribute(optional = true)
	protected Set<Long> boundServiceIds = new HashSet<>();
	@JsonAttribute(optional = true)
	protected Set<Long> alternativeServiceIds = new HashSet<>();
	protected transient Field instanceMirror;

	/**
	 * Get if the dependency is required.
	 */
	public boolean isRequired() {
		return required;
	}

	/**
	 * Set if the dependency is required.
	 */
	public void setRequired(boolean required) {
		this.required = required;
	}

	/**
	 * Get if the dependency is a collection (?..n) dependency or a single
	 * (?..1) dependency.
	 */
	public boolean isCollection() {
		return collection;
	}

	/**
	 * Set if the dependency is a collection (?..n) dependency or a single
	 * (?..1) dependency.
	 */
	public void setCollection(boolean collection) {
		this.collection = collection;
	}

	/**
	 * Get the type of the dependency.
	 * <p>
	 * The type is represented as a String containing a fully qualified class
	 * name.
	 */
	public String getType() {
		return type;
	}

	/**
	 * Set the type of the dependency.
	 * <p>
	 * The type is represented as a String containing a fully qualified class
	 * name.
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * Get the property filter (LDAP filter) of the dependency.
	 */
	public String getPropertyFilter() {
		return propertyFilter;
	}

	/**
	 * Set the property filter (LDAP filter) of the dependency.
	 */
	public void setPropertyFilter(String propertyFilter) {
		this.propertyFilter = propertyFilter;
	}

	/**
	 * The dependency can describe one-to-one or one-to-may links on Segments.
	 * This method returns a mapping of the id of the linked Functions to the
	 * required versions (or a wildcard if there is no version requirements).
	 */
	public Map<String, String> getFunctionTargets() {
		return functionTargets;
	}

	/**
	 * Sets the function targets.
	 */
	public void setFunctionTargets(Map<String, String> functionTargets) {
		this.functionTargets = functionTargets;
	}

	/**
	 * Returns true when the dependency has a preferred binding (and thus is a
	 * special dependency type that allows e.g. fallback mechanisms) or false
	 * when the dependency is a regular one-to-one or one-to-many dependency.
	 */
	public boolean hasPreferredBinding() {
		return !isCollection() && getPreferredId() != null;
	}

	/**
	 * If the dependency binds to multiple targets, but it is not a collection
	 * type, this field specifies the id of the target that is preferred to be
	 * bound.
	 */
	public String getPreferredId() {
		return preferredId;
	}

	/**
	 * If the dependency binds to multiple targets, but it is not a collection
	 * type, this method allows setting the id of the target that is preferred
	 * to be bound.
	 */
	public void setPreferredId(String preferredId) {
		this.preferredId = preferredId;
	}

	/**
	 * Get the service filter of the dependency.
	 * <p>
	 * This is the filter that will be used by OSGi to resolve the service
	 * lookup.
	 */
	public String getServiceFilter() {
		String filter = FilterUtil.isType(type);

		if (propertyFilter != null && !propertyFilter.isEmpty()) {
			filter = FilterUtil.and(filter, propertyFilter);
		} else if (FunctionalSegment.class.getName().equals(type)) {
			filter = FilterUtil.and(filter, getFunctionFilter());
		}

		return filter;
	}

	/**
	 * Returns the part of the service filter that applies to LimeDS-specific
	 * properties.
	 * 
	 * @return
	 */
	public String getFunctionFilter() {
		if (functionTargets != null && !functionTargets.isEmpty()) {
			if (functionTargets.size() == 1) {
				Map.Entry<String, String> e = functionTargets.entrySet().iterator().next();
				return FilterUtil.and(FilterUtil.equals(ServicePropertyNames.SEGMENT_ID, e.getKey()),
						VersionUtil.createFilter(ServicePropertyNames.SEGMENT_VERSION, e.getValue()));
			} else {
				return FilterUtil.or(functionTargets.entrySet().stream()
						.map(e -> FilterUtil.and(FilterUtil.equals(ServicePropertyNames.SEGMENT_ID, e.getKey()),
								VersionUtil.createFilter(ServicePropertyNames.SEGMENT_VERSION, e.getValue())))
						.toArray(size -> new String[size]));
			}
		} else {
			return FilterUtil.equals(ServicePropertyNames.SEGMENT_ID, Link.VOID_LINK_TARGET);
		}
	}

	/**
	 * Get the object that represents the bound service(s).
	 */
	public Object getInstance() {
		return instance;
	}

	/**
	 * Set the object that represents the bound service(s).
	 */
	public void setInstance(Object instance) {
		this.instance = instance;
	}

	/**
	 * Get the (OSGi) ids of the bound services.
	 */
	public Set<Long> getBoundServiceIds() {
		return boundServiceIds;
	}

	/**
	 * Set the (OSGi) ids of the bound services.
	 */
	public void setBoundServiceIds(Set<Long> boundServiceIds) {
		this.boundServiceIds = boundServiceIds;
	}

	/**
	 * If a preferred service id is configured for the dependency, alternative
	 * service ids can be retrieved using this function.
	 */
	public Set<Long> getAlternativeServiceIds() {
		return alternativeServiceIds;
	}

	/**
	 * This method sets the alternative service ids (if applicable).
	 */
	public void setAlternativeServiceIds(Set<Long> alternativeServiceIds) {
		this.alternativeServiceIds = alternativeServiceIds;
	}

	/**
	 * Get the instance mirror field.
	 * <p>
	 * If the dependency annotation (@Link or @Service) is placed on top of a
	 * field that directly represents the service or component type instead of a
	 * DependencyDescriptor type, an instance mirror is creates that is used to
	 * keep the contents of this field in sync with the internal state (the
	 * field instance) of this DependencyDescriptor.
	 */
	public Field getInstanceMirror() {
		return instanceMirror;
	}

	/**
	 * Set the instance mirror field.
	 * <p>
	 * If the dependency annotation (@Link or @Service) is placed on top of a
	 * field that directly represents the service or component type instead of a
	 * DependencyDescriptor type, an instance mirror is creates that is used to
	 * keep the contents of this field in sync with the internal state (the
	 * field instance) of this DependencyDescriptor.
	 */
	public void setInstanceMirror(Field instanceMirror) {
		this.instanceMirror = instanceMirror;
	}

	@Override
	public String toString() {
		return "limeDS dependency: " + getServiceFilter();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (collection ? 1231 : 1237);
		result = prime * result + ((propertyFilter == null) ? 0 : propertyFilter.hashCode());
		result = prime * result + (required ? 1231 : 1237);
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DependencyDescriptor other = (DependencyDescriptor) obj;
		if (collection != other.collection)
			return false;
		if (propertyFilter == null) {
			if (other.propertyFilter != null)
				return false;
		} else if (!propertyFilter.equals(other.propertyFilter))
			return false;
		if (required != other.required)
			return false;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		return true;
	}

}
