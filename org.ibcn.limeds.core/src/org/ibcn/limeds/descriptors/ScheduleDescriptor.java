/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.descriptors;

/**
 * Represents an execution schedule for LimeDS components.
 * 
 * @author wkerckho
 *
 */
@FunctionalInterface
public interface ScheduleDescriptor {

	public enum ScheduleMode {
		every, delay;
	}

	public enum ScheduleUnit {
		milliseconds, seconds, minutes, hours, days;
	}

	/**
	 * Get the descriptor as a LimeDS schedule expression.
	 * <p>
	 * format: [command: delay/every] [n] [timeUnit: milliseconds, seconds,
	 * minutes, hours, days] [offset (optional)] [n] [timeUnit]
	 * <p>
	 * Example1: schedule every day at 1 am &gt; "every 1 days offset 1 hour" <br>
	 * Example2: schedule at framework start &gt; "delay 0 seconds" <br>
	 * Example2: schedule every hour, after 20 minutes =&gt;
	 * "every 1 hours offset 20 minutes"
	 */
	String asExpression();

}
