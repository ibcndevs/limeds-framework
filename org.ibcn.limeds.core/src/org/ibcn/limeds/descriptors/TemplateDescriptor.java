/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.descriptors;

import java.util.LinkedList;
import java.util.List;

import org.ibcn.limeds.annotations.JsonAttribute;

/**
 * When defining config properties or providing a factory instantiation, a
 * Component will need to provide template information. This is done using this
 * TemplateDescriptor class.
 * 
 * @author wkerckho
 *
 */
public class TemplateDescriptor {

	public static final String FUNCTION_ID_PROPERTY = "$.id";
	public static final String DYNAMIC_DEPENDENCY_PREFIX = "$.dependencies.";
	public static final String EXPORT_PROPERTY = "$.export";
	public static final String SLICE_LOCAL_PROPERTY = "_sliceLocal";

	public static class ConfigProperty {

		public String name;
		public String type;
		@JsonAttribute(optional = true)
		public String value;
		@JsonAttribute(optional = true)
		public String[] possibleValues;
		@JsonAttribute(optional = true)
		public String description;

		public ConfigProperty() {
		}

		public ConfigProperty(String name, String type, String value, String description, String... possibleValues) {
			this.name = name;
			this.type = type;
			this.value = value;
			this.description = description;
			this.possibleValues = possibleValues;
		}

	}

	/**
	 * This enum list the various ways of how the template info should be used.
	 */
	public enum Type {
		FACTORY, FACTORY_INSTANCE, CONFIGURABLE_INSTANCE;
	}

	private Type type;
	@JsonAttribute(optional = true)
	private String factoryId;
	@JsonAttribute(optional = true)
	private transient String instanceId;
	private List<ConfigProperty> configuration = new LinkedList<>();

	/**
	 * Get the type that defines how the template should be used.
	 */
	public Type getType() {
		return type;
	}

	/**
	 * Set the type that defines how the template should be used.
	 */
	public void setType(Type type) {
		this.type = type;
	}

	/**
	 * If the template type is a FACTORY, then this method returns the id of the
	 * factory. If the template type is a FACTORY_INSTANCE, this method returns
	 * the id of the factory that was used to instantiate the factory instance.
	 */
	public String getFactoryId() {
		return factoryId;
	}

	/**
	 * Set the factory id.
	 */
	public void setFactoryId(String factoryId) {
		this.factoryId = factoryId;
	}

	/**
	 * When in FACTORY or CONFIGURABLE_INSTANCE mode, the template configuration
	 * defines the instantiation parameters for factory or configurable function
	 * instances. When in FACTORY_INSTANCE mode, the template configuration
	 * contains the instantiated parameters for a factory instance.
	 */
	public List<ConfigProperty> getConfiguration() {
		return configuration;
	}

	/**
	 * Set the template configuration.
	 */
	public void setConfiguration(List<ConfigProperty> configuration) {
		this.configuration = configuration;
	}

	/**
	 * Get the id of the configuration (used if the template type is
	 * FACTORY_INSTANCE)
	 */
	public String getInstanceId() {
		return instanceId;
	}

	/**
	 * Set the id for the configuration.
	 */
	public void setInstanceId(String instanceId) {
		this.instanceId = instanceId;
	}

}
