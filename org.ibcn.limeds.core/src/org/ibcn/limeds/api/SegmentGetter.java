/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.api;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.function.Supplier;

import org.ibcn.limeds.HttpEndpointSegment;
import org.ibcn.limeds.SegmentAdmin;
import org.ibcn.limeds.ServicePropertyNames;
import org.ibcn.limeds.annotations.AuthMode;
import org.ibcn.limeds.annotations.HttpDoc;
import org.ibcn.limeds.annotations.HttpMethod;
import org.ibcn.limeds.annotations.HttpOperation;
import org.ibcn.limeds.annotations.OutputDoc;
import org.ibcn.limeds.annotations.Segment;
import org.ibcn.limeds.annotations.Service;
import org.ibcn.limeds.annotations.Validated;
import org.ibcn.limeds.descriptors.SegmentDescriptor;
import org.ibcn.limeds.exceptions.ExceptionWithStatus;
import org.ibcn.limeds.json.JsonValue;
import org.ibcn.limeds.security.SystemRoles;
import org.ibcn.limeds.versioning.VersionUtil;
import org.osgi.framework.Filter;
import org.osgi.framework.FrameworkUtil;

/**
 * This Segment sets up a HTTP endpoint for retrieving live component data.
 * 
 * @author wkerckho
 *
 */
@Validated(input = false)
@Segment(id = "limeds.segments.Getter", description = "This function returns the meta-data for a specified Segment.")
public class SegmentGetter extends HttpEndpointSegment {

	private static Comparator<JsonValue> segmentSortVersionDescending = (s1, s2) -> VersionUtil
			.createVersionPropertyValue(s2.getString("version"))
			.compareTo(VersionUtil.createVersionPropertyValue(s1.getString("version")));
	private static Supplier<Exception> notFound = () -> new ExceptionWithStatus(404,
			"The requested Segment cannot be found!");

	@Service
	private SegmentAdmin manager;

	@Override
	@HttpDoc(pathInfo = { "The id of the Segment.",
			"The version of the Segment (or 'latest' for the latest available Segment)" })
	@OutputDoc(schemaClass = SegmentDescriptor.class)
	@HttpOperation(groupId = API_Info.API_GROUP_NAME, method = HttpMethod.GET, path = "/_limeds/segments/{id}/{version}", authMode = AuthMode.AUTHORIZE_DISJUNCTIVE, authorityRequired = {
			SystemRoles.LIMEDS_ADMIN, SystemRoles.LIMEDS_VIEW_ONLY })
	public JsonValue apply(JsonValue input, HttpContext context) throws Exception {
		String versionExpr = context.pathParameters().getString("version");
		if ("latest".equals(versionExpr)) {
			Optional<JsonValue> result = manager.getSegmentsInfo().stream()
					.filter(segment -> context.pathParameters().getString("id").equals(segment.getString("id")))
					.sorted(segmentSortVersionDescending).findFirst();
			return result.orElseThrow(notFound);
		} else if (versionExpr.contains("*") || versionExpr.contains("^")) {
			Filter versionFilter = FrameworkUtil
					.createFilter(VersionUtil.createFilter(ServicePropertyNames.SEGMENT_VERSION, versionExpr));
			Optional<JsonValue> result = manager.getSegmentsInfo().stream()
					.filter(segment -> context.pathParameters().getString("id").equals(segment.getString("id")))
					.filter(segment -> {
						Map<String, String> versionProperty = new HashMap<>();
						versionProperty.put(ServicePropertyNames.SEGMENT_VERSION,
								VersionUtil.createVersionPropertyValue(segment.getString("version")));
						return versionFilter.matches(versionProperty);
					}).sorted(segmentSortVersionDescending).findFirst();
			return result.orElseThrow(notFound);
		} else {
			return manager.getSegmentInfo(context.pathParameters().getString("id"), versionExpr).orElseThrow(notFound);
		}
	}

}
