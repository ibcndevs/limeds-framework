/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.api;

import java.util.NoSuchElementException;

import org.ibcn.limeds.HttpEndpointSegment;
import org.ibcn.limeds.TypeAdmin;
import org.ibcn.limeds.annotations.AuthMode;
import org.ibcn.limeds.annotations.HttpMethod;
import org.ibcn.limeds.annotations.HttpOperation;
import org.ibcn.limeds.annotations.Segment;
import org.ibcn.limeds.annotations.Service;
import org.ibcn.limeds.exceptions.ExceptionWithStatus;
import org.ibcn.limeds.json.Json;
import org.ibcn.limeds.json.JsonObject;
import org.ibcn.limeds.json.JsonValue;
import org.ibcn.limeds.security.SystemRoles;
import org.ibcn.limeds.util.Identifier;

/**
 * This Segment sets up an HTTP endpoint for validating JSON objects according
 * to a specified JSON type definition.
 * 
 * @author wkerckho
 *
 */
@Segment(id = "limeds.types.Validator", description = "This function validates the submitted JSON instance using the specified schema.")
public class TypeValidator extends HttpEndpointSegment {

	@Service
	private TypeAdmin typeAdmin;

	@Override
	@HttpOperation(groupId = API_Info.API_GROUP_NAME, method = HttpMethod.POST, path = "/_limeds/types/{ref}/validator", authMode = AuthMode.AUTHORIZE_CONJUNCTIVE, authorityRequired = SystemRoles.LIMEDS_ADMIN)
	public JsonValue apply(JsonValue input, HttpContext context) throws Exception {
		try {
			Identifier identifier = Identifier.parse(context.pathParameters().getString("ref"));
			JsonObject schema = typeAdmin.getRegisteredType(identifier).get();

			try {
				typeAdmin.validate(schema, input);
				return null;
			} catch (Exception e) {
				return Json.objectBuilder().add("error", e.getMessage()).build();
			}
		} catch (NoSuchElementException e) {
			throw new ExceptionWithStatus(404, "The requested type cannot by found");
		} catch (Exception e) {
			throw new ExceptionWithStatus(500, "The format of a schema reference must be: [schemaId]_[schemaVersion]");
		}
	}

}
