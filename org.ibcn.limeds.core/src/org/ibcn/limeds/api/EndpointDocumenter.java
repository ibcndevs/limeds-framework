/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.api;

import java.util.Arrays;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.ibcn.limeds.HttpEndpointSegment;
import org.ibcn.limeds.SegmentAdmin;
import org.ibcn.limeds.TypeAdmin;
import org.ibcn.limeds.annotations.HttpMethod;
import org.ibcn.limeds.annotations.HttpOperation;
import org.ibcn.limeds.annotations.Segment;
import org.ibcn.limeds.annotations.Service;
import org.ibcn.limeds.descriptors.DocumentationDescriptors;
import org.ibcn.limeds.descriptors.HttpOperationDescriptor;
import org.ibcn.limeds.descriptors.SegmentDescriptor;
import org.ibcn.limeds.impl.http.HttpManager;
import org.ibcn.limeds.json.Json;
import org.ibcn.limeds.json.JsonArray;
import org.ibcn.limeds.json.JsonObject;
import org.ibcn.limeds.json.JsonPrimitive;
import org.ibcn.limeds.json.JsonValue;
import org.ibcn.limeds.json.model.JsonSchemaConvertor;
import org.ibcn.limeds.json.model.JsonValidator;
import org.ibcn.limeds.json.model.JsonValidator.FieldDescriptor;
import org.ibcn.limeds.util.Errors;
import org.ibcn.limeds.util.FilterUtil;
import org.osgi.service.cm.Configuration;
import org.osgi.service.cm.ConfigurationAdmin;

@Segment(id = "limeds.http.Documenter", description = "This function produces Swagger Documentation data based on the available LimeDS meta-data.", export = false)
public class EndpointDocumenter extends HttpEndpointSegment {

	private static final String GENERIC_TYPE = "_UnknownType";

	@Service
	private SegmentAdmin segmentAdmin;

	@Service
	private TypeAdmin typeAdmin;

	@Service
	private ConfigurationAdmin configAdmin;

	@HttpOperation(groupId = API_Info.API_GROUP_NAME, method = HttpMethod.GET, path = "/_limeds/httpInfo")
	@Override
	public JsonValue apply(JsonValue input, HttpContext context) throws Exception {
		Set<String> tags = new HashSet<>();
		JsonObject paths = new JsonObject();
		JsonObject definitions = new JsonObject();

		// Add catch-all definition for undefined input and outputs
		definitions.put(GENERIC_TYPE,
				Json.objectBuilder().add("type", "object").add("additionalProperties", true).build());

		// Filter the available endpoints and generate swagger properties
		segmentAdmin.getRegisteredSegments().stream()
				.filter(desc -> desc.getHttpOperation() != null && desc.getHttpOperation().getPath() != null
						&& !desc.getHttpOperation().getPath().isEmpty()
						&& segmentAdmin.getSegmentInfo(desc.getId(), desc.getVersion()).get().getBool("active"))
				.sorted((desc1, desc2) -> desc1.getHttpOperation().getPath()
						.compareTo(desc2.getHttpOperation().getPath()))
				.forEach(Errors.wrapConsumer(desc -> {
					process(paths, definitions, desc.getHttpOperation(), desc);
					if (desc.getHttpOperation().getGroupId() != null) {
						tags.add(desc.getHttpOperation().getGroupId());
					}
				}));

		return Json.objectBuilder().add("swagger", "2.0")
				.add("info", Json.objectBuilder()
						.add("description", "This page documents the HTTP endpoints available for your LimeDS instance")
						.add("title", "LimeDS HTTP Documentation").add("version", API_Info.VERSION))
				.add("basePath", getHttpRoot()).add("tags", tags.stream()
						.map(name -> Json.objectBuilder().add("name", name).build()).collect(Json.toArray()))
				.add("paths", paths).add("definitions", definitions).build();
	}

	private String getHttpRoot() {
		Configuration[] configs = null;
		try {
			configs = configAdmin.listConfigurations(FilterUtil.equals("service.pid", HttpManager.class.getName()));
		} catch (Exception e) {

		}
		if (configs != null && configs.length > 0) {
			return "" + configs[0].getProperties().get("alias");
		} else {
			return HttpManager.DEFAULT_HTTP_ROOT;
		}
	}

	private String getTypeId(SegmentDescriptor desc, JsonValue schema, String typeId) {
		return (schema instanceof JsonObject && schema.has("@typeRef")) ? schema.getString("@typeRef")
				: desc.getId() + ":" + typeId + "_" + desc.getVersion();
	}

	private JsonObject typeToSchema(JsonValue typeDef) throws Exception {
		return JsonSchemaConvertor.convert(typeAdmin.resolve(typeDef)).asObject();
	}

	private JsonObject queryDocEntryToParam(String name, String definition) throws Exception {
		FieldDescriptor desc = new FieldDescriptor(name, definition);
		return Json.objectBuilder().add("name", name).add("in", "query").add("description", desc.description)
				.add("required", !desc.optional).add("type", JsonSchemaConvertor.mapType(desc.type)).build();
	}

	private List<String> listPathParams(String path) {
		List<String> result = Arrays.stream(path.split("/")).filter(s -> s.startsWith("{") && s.endsWith("}"))
				.map(s -> s.substring(1, s.length() - 1)).collect(Collectors.toList());
		return result;
	}

	private void process(JsonObject paths, JsonObject definitions, HttpOperationDescriptor operation,
			SegmentDescriptor desc) throws Exception {
		if (!paths.has(operation.getPath())) {
			paths.put(operation.getPath(), new JsonObject());
		}
		JsonObject pathInfo = new JsonObject();

		if (operation.getGroupId() != null) {
			pathInfo.put("tags", Json.arrayOf(operation.getGroupId()));
		}

		pathInfo.put("summary", desc.getId() + "_" + desc.getVersion());
		pathInfo.put("description", desc.getDescription());
		pathInfo.put("operationId", desc.getId().substring(desc.getId().lastIndexOf('.') + 1));
		if (EnumSet.of(HttpMethod.PUT, HttpMethod.POST).contains(operation.getMethod())) {
			pathInfo.put("consumes", Json.arrayOf("application/json"));
		}
		pathInfo.put("produces", Json.arrayOf("application/json"));

		JsonArray parameters = new JsonArray();
		JsonObject responses = new JsonObject();

		DocumentationDescriptors.Operation applyDoc = desc.getDocumentation() != null
				? desc.getDocumentation().stream().filter(doc -> doc.getId().equals("apply")).findFirst().orElse(null)
				: null;
		Optional<DocumentationDescriptors.Input> inputType = (applyDoc != null && applyDoc.getIn() != null)
				? applyDoc.getIn().stream().findFirst() : Optional.empty();
		if (inputType.isPresent() && isValidSchema(inputType.get().getSchema())) {
			String ref = getTypeId(desc, inputType.get().getSchema(), "input");
			boolean collectionType = inputType.get().getSchema() instanceof JsonArray;
			boolean primitiveType = inputType.get().getSchema() instanceof JsonPrimitive;
			JsonValue typeDef = collectionType ? inputType.get().getSchema().get(0).asObject()
					: inputType.get().getSchema();
			JsonObject param = new JsonObject();
			param.put("in", "body");
			param.put("name", "body");
			param.put("required", "true");
			if (primitiveType) {
				// Handle primitive type
				param.put("schema", typeToSchema(typeDef));
				JsonValidator.FieldDescriptor primDesc = new JsonValidator.FieldDescriptor("body", typeDef.asString());
				if (primDesc.description != null && !primDesc.description.isEmpty()) {
					param.put("description", primDesc.description);
				}
			} else {
				definitions.put(ref, typeToSchema(typeDef));
				if (collectionType) {
					param.put("schema", Json.objectBuilder().add("type", "array")
							.add("items", Json.objectBuilder().add("$ref", "#/definitions/" + ref)).build());
				} else {
					param.put("schema", Json.objectBuilder().add("$ref", "#/definitions/" + ref).build());
				}
				if (typeDef.has("@typeInfo")) {
					FieldDescriptor objectInfo = new FieldDescriptor("", typeDef.getString("@typeInfo"));
					param.put("description", objectInfo.description);
				}
			}
			parameters.add(param);
		} else if (EnumSet.of(HttpMethod.POST, HttpMethod.PUT).contains(operation.getMethod())) {
			parameters.add(Json.objectBuilder().add("in", "body").add("name", "body").add("required", "false")
					.add("schema", Json.objectBuilder().add("$ref", "#/definitions/" + GENERIC_TYPE)).build());
		}

		if (applyDoc != null && applyDoc.getOut() != null && isValidSchema(applyDoc.getOut().getSchema())) {
			String ref = getTypeId(desc, applyDoc.getOut().getSchema(), "output");
			definitions.put(ref, typeToSchema(applyDoc.getOut().getSchema()));
			responses.put("200", Json.objectBuilder()
					.add("schema", Json.objectBuilder().add("$ref", "#/definitions/" + ref)).build());
		}

		if (desc.getHttpDocumentation() != null && isValidSchema(desc.getHttpDocumentation().getQuerySchema())) {
			typeAdmin.resolve(desc.getHttpDocumentation().getQuerySchema()).asObject()
					.forEach(Errors.wrapBiConsumer((k, v) -> parameters.add(queryDocEntryToParam(k, v.asString()))));
		}

		if (desc.getHttpDocumentation() != null && desc.getHttpDocumentation().getPathInfo().length > 0) {
			List<String> pathParams = listPathParams(operation.getPath());
			for (int i = 0; i < pathParams.size(); i++) {
				String pathDesc = (i < desc.getHttpDocumentation().getPathInfo().length)
						? desc.getHttpDocumentation().getPathInfo()[i] : "";
				parameters.add(Json.objectBuilder().add("in", "path").add("name", pathParams.get(i))
						.add("required", true).add("description", pathDesc).add("type", "string").build());
			}
		} else {
			listPathParams(operation.getPath()).forEach(s -> parameters.add(Json.objectBuilder().add("in", "path")
					.add("name", s).add("required", true).add("type", "string").build()));
		}

		if (parameters.size() > 0) {
			pathInfo.put("parameters", parameters);
		}

		if (responses.size() > 0) {
			pathInfo.put("responses", responses);
		}

		paths.get(operation.getPath()).asObject().put(operation.getMethod().toString().toLowerCase(), pathInfo);
	}

	private boolean isValidSchema(JsonValue schema) {
		return schema != null && !schema.toString().isEmpty() && !("\"\"".equals(schema.toString()));
	}

}
