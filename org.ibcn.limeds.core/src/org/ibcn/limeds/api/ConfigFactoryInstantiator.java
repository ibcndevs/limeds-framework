/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.api;

import java.util.Dictionary;
import java.util.Hashtable;

import org.ibcn.limeds.HttpEndpointSegment;
import org.ibcn.limeds.annotations.AuthMode;
import org.ibcn.limeds.annotations.HttpMethod;
import org.ibcn.limeds.annotations.HttpOperation;
import org.ibcn.limeds.annotations.InputDoc;
import org.ibcn.limeds.annotations.Segment;
import org.ibcn.limeds.annotations.Service;
import org.ibcn.limeds.annotations.Validated;
import org.ibcn.limeds.json.JsonPrimitive;
import org.ibcn.limeds.json.JsonValue;
import org.ibcn.limeds.security.SystemRoles;
import org.osgi.service.cm.Configuration;
import org.osgi.service.cm.ConfigurationAdmin;

@Validated(output = false)
@Segment(id = "limeds.config.FactoryInstantiator")
public class ConfigFactoryInstantiator extends HttpEndpointSegment {

	@Service
	private ConfigurationAdmin configAdmin;

	@Override
	@InputDoc(schemaRef = "org.ibcn.limeds.api.types.ConfigProperty_${sliceVersion}", collection = true)
	@HttpOperation(groupId = API_Info.API_GROUP_NAME, method = HttpMethod.POST, path = "/_limeds/config/{factoryId}", authMode = AuthMode.AUTHORIZE_DISJUNCTIVE, authorityRequired = SystemRoles.LIMEDS_ADMIN)
	public JsonValue apply(JsonValue input, HttpContext context) throws Exception {
		Configuration config = configAdmin.createFactoryConfiguration(context.pathParameters().getString("factoryId"),
				null);
		Dictionary<String, Object> properties = new Hashtable<>();
		ConfigUpdater.applyChanges(properties, input.asArray(), false);
		config.update(properties);
		return new JsonPrimitive(config.getPid());
	}

}
