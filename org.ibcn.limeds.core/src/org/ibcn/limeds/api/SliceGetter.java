/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.api;

import org.ibcn.limeds.HttpEndpointSegment;
import org.ibcn.limeds.SliceAdmin;
import org.ibcn.limeds.annotations.AuthMode;
import org.ibcn.limeds.annotations.HttpDoc;
import org.ibcn.limeds.annotations.HttpMethod;
import org.ibcn.limeds.annotations.HttpOperation;
import org.ibcn.limeds.annotations.OutputDoc;
import org.ibcn.limeds.annotations.Segment;
import org.ibcn.limeds.annotations.Service;
import org.ibcn.limeds.annotations.Validated;
import org.ibcn.limeds.json.JsonValue;
import org.ibcn.limeds.security.SystemRoles;
import org.ibcn.limeds.util.Identifier;
import org.ibcn.limeds.versioning.VersionUtil;

/**
 * This Segment sets up a HTTP endpoint for retrieving data about specific Slice
 * instances that are editable using the Visual Editor.
 * 
 * @author wkerckho
 *
 */
@Validated(input = false)
@Segment(id = "limeds.slices.Getter", description = "This function returns the meta-data for a specified Slice.")
public class SliceGetter extends HttpEndpointSegment {

	@Service
	private SliceAdmin manager;

	@Override
	@HttpDoc(pathInfo = { "The id of the Slice.",
			"The version of the Slice (or 'latest' for the latest available Slice)" })
	@OutputDoc(schemaRef = "org.ibcn.limeds.api.types.Slice_${sliceVersion}")
	@HttpOperation(groupId = API_Info.API_GROUP_NAME, method = HttpMethod.GET, path = "/_limeds/slices/{id}/{version}", authMode = AuthMode.AUTHORIZE_DISJUNCTIVE, authorityRequired = {
			SystemRoles.LIMEDS_ADMIN, SystemRoles.LIMEDS_VIEW_ONLY })
	public JsonValue apply(JsonValue input, HttpContext context) throws Exception {
		if ("latest".equals(context.pathParameters().getString("version"))) {
			return findLatest(manager, context.pathParameters().getString("id"));
		}
		return manager.get(new Identifier(context.pathParameters().getString("id"),
				context.pathParameters().getString("version")));
	}

	static JsonValue findLatest(SliceAdmin manager, String id) {
		return manager.list().stream().filter(f -> id.equals(f.getString("id")))
				.sorted((f1, f2) -> VersionUtil.createVersionPropertyValue(f2.getString("version"))
						.compareTo(VersionUtil.createVersionPropertyValue(f1.getString("version"))))
				.findFirst().get();
	}

}
