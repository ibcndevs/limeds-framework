/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.api;

import java.util.Arrays;

import org.ibcn.limeds.EventBus;
import org.ibcn.limeds.HttpEndpointSegment;
import org.ibcn.limeds.IssueAdmin;
import org.ibcn.limeds.SegmentAdmin;
import org.ibcn.limeds.SliceAdmin;
import org.ibcn.limeds.TypeAdmin;
import org.ibcn.limeds.annotations.HttpMethod;
import org.ibcn.limeds.annotations.HttpOperation;
import org.ibcn.limeds.annotations.Segment;
import org.ibcn.limeds.annotations.Service;
import org.ibcn.limeds.json.Json;
import org.ibcn.limeds.json.JsonObject;
import org.ibcn.limeds.json.JsonValue;
import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;

@Segment(id = "limeds.system.HostInfoProvider")
public class HostInfoProvider extends HttpEndpointSegment {

	private static final String RELEASE_HEADER = "LimeDS-Release";
	private static final Bundle BUNDLE = FrameworkUtil.getBundle(HostInfoProvider.class);

	@Service
	private EventBus eventBus;

	@Service
	private IssueAdmin issueAdmin;

	@Service
	private SegmentAdmin segmentAdmin;

	@Service
	private SliceAdmin sliceAdmin;

	@Service
	private TypeAdmin typeAdmin;

	@Override
	@HttpOperation(groupId = API_Info.API_GROUP_NAME, method = HttpMethod.GET, path = "/_limeds/hostinfo")
	public JsonValue apply(JsonValue input, HttpContext context) throws Exception {
		JsonObject result = new JsonObject();
		result.put("releaseVersion",
				BUNDLE.getHeaders().get(RELEASE_HEADER) != null ? BUNDLE.getHeaders().get(RELEASE_HEADER) : "custom");
		result.put("modules",
				Arrays.stream(BUNDLE.getBundleContext().getBundles()).map(this::extractInfo).collect(Json.toArray()));
		return result;
	}

	private JsonObject extractInfo(Bundle bundle) {
		JsonObject info = new JsonObject();
		info.put("id", bundle.getSymbolicName());
		info.put("version", bundle.getVersion().toString());
		info.put("status", fromState(bundle.getState()));
		return info;
	}

	private String fromState(int state) {
		switch (state) {
		case Bundle.ACTIVE:
			return "active";
		case Bundle.INSTALLED:
			return "installed";
		case Bundle.RESOLVED:
			return "resolved";
		default:
			return "processing";
		}
	}

}
