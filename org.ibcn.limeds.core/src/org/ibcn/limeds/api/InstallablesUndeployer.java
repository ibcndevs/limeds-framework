/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.api;

import java.util.Arrays;
import java.util.Optional;

import org.ibcn.limeds.HttpEndpointSegment;
import org.ibcn.limeds.annotations.AuthMode;
import org.ibcn.limeds.annotations.HttpDoc;
import org.ibcn.limeds.annotations.HttpMethod;
import org.ibcn.limeds.annotations.HttpOperation;
import org.ibcn.limeds.annotations.Segment;
import org.ibcn.limeds.json.JsonValue;
import org.ibcn.limeds.security.SystemRoles;
import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;

@Segment(id = "limeds.installables.Undeployer", description = "This function can be used to undeploy an installed module.")
public class InstallablesUndeployer extends HttpEndpointSegment {

	private static final Bundle BUNDLE_REF = FrameworkUtil.getBundle(InstallablesLister.class);

	@Override
	@HttpDoc(pathInfo = { "The id of the module to be uninstalled.", "The version of the module to be uninstalled." })
	@HttpOperation(groupId = API_Info.API_GROUP_NAME, method = HttpMethod.DELETE, path = "/_limeds/installables/{id}/{version}", authMode = AuthMode.AUTHORIZE_CONJUNCTIVE, authorityRequired = SystemRoles.LIMEDS_ADMIN)
	public JsonValue apply(JsonValue input, HttpContext context) throws Exception {
		if ("latest".equals(context.pathParameters().getString("version"))) {
			Optional<Bundle> target = Arrays.stream(BUNDLE_REF.getBundleContext().getBundles())
					.filter(b -> b.getSymbolicName().equals(context.pathParameters().getString("id")))
					.sorted((b1, b2) -> b2.getVersion().compareTo(b1.getVersion())).findFirst();
			if (target.isPresent()) {
				target.get().uninstall();
			} else {
				context.respondWith(404);
			}
			return null;
		} else {
			Optional<Bundle> target = Arrays.stream(BUNDLE_REF.getBundleContext().getBundles())
					.filter(b -> b.getSymbolicName().equals(context.pathParameters().getString("id"))
							&& b.getVersion().toString().equals(context.pathParameters().getString("version")))
					.findFirst();
			if (target.isPresent()) {
				target.get().uninstall();
			} else {
				context.respondWith(404);
			}
			return null;
		}
	}

}
