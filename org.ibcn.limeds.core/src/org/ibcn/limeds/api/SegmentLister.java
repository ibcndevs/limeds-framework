/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.api;

import java.util.function.Predicate;

import org.ibcn.limeds.HttpEndpointSegment;
import org.ibcn.limeds.SegmentAdmin;
import org.ibcn.limeds.annotations.AuthMode;
import org.ibcn.limeds.annotations.HttpDoc;
import org.ibcn.limeds.annotations.HttpMethod;
import org.ibcn.limeds.annotations.HttpOperation;
import org.ibcn.limeds.annotations.OutputDoc;
import org.ibcn.limeds.annotations.Segment;
import org.ibcn.limeds.annotations.Service;
import org.ibcn.limeds.annotations.Validated;
import org.ibcn.limeds.descriptors.SegmentDescriptor;
import org.ibcn.limeds.json.Json;
import org.ibcn.limeds.json.JsonValue;
import org.ibcn.limeds.security.SystemRoles;

/**
 * This Segment sets up a HTTP endpoint for listing live data for all the
 * registered components.
 * 
 * @author wkerckho
 *
 */
@Validated(input = false)
@Segment(id = "limeds.segments.Lister", description = "This function returns all Segment meta-data.")
public class SegmentLister extends HttpEndpointSegment {

	@Service
	private SegmentAdmin manager;

	@Override
	@OutputDoc(schemaClass = SegmentDescriptor.class, collection = true)
	@HttpDoc(queryRef = "org.ibcn.limeds.api.types.SegmentListerQuery_${sliceVersion}")
	@HttpOperation(groupId = API_Info.API_GROUP_NAME, method = HttpMethod.GET, path = "/_limeds/segments", authMode = AuthMode.AUTHORIZE_DISJUNCTIVE, authorityRequired = {
			SystemRoles.LIMEDS_ADMIN, SystemRoles.LIMEDS_VIEW_ONLY })
	public JsonValue apply(JsonValue input, HttpContext context) throws Exception {
		Predicate<JsonValue> filter = v -> true;
		if (!context.isActiveQueryFlag("showPrivate")) {
			filter = filter.and(v -> v.getBool("export"));
		}
		if (!context.isActiveQueryFlag("showSystem")) {
			filter = filter.and(v -> !v.getBool("systemSegment"));
		}
		return manager.getSegmentsInfo().stream().filter(filter).collect(Json.toArray());
	}

}
