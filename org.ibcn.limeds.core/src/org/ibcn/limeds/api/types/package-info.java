/**
 * This package contains the type definitions for the LimeDS REST API.
 */
@org.osgi.annotation.versioning.Version(org.ibcn.limeds.api.API_Info.VERSION)
@org.ibcn.limeds.annotations.IncludeTypes({ "SegmentListerQuery.json", "TypeGetterQuery.json", "ConfigList.json",
		"ConfigProperty.json", "Slice.json", "Installable.json" })
package org.ibcn.limeds.api.types;