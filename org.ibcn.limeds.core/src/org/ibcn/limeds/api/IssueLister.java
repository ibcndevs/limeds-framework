/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.api;

import java.util.Collection;

import org.ibcn.limeds.HttpEndpointSegment;
import org.ibcn.limeds.IssueAdmin;
import org.ibcn.limeds.SegmentAdmin;
import org.ibcn.limeds.annotations.HttpDoc;
import org.ibcn.limeds.annotations.HttpMethod;
import org.ibcn.limeds.annotations.HttpOperation;
import org.ibcn.limeds.annotations.Segment;
import org.ibcn.limeds.annotations.Service;
import org.ibcn.limeds.json.JsonArray;
import org.ibcn.limeds.json.JsonObject;
import org.ibcn.limeds.json.JsonValue;

@Segment
public class IssueLister extends HttpEndpointSegment {

	@Service
	private SegmentAdmin segmentAdmin;

	@Service
	private IssueAdmin issueAdmin;

	@HttpDoc(querySchema = "{ \"limit\" : \"IntNumber * (If present, limits output to the given number of issues last appended to the log file.)\"}")
	@HttpOperation(groupId = API_Info.API_GROUP_NAME, method = HttpMethod.GET, path = "/_limeds/issues")
	@Override
	public JsonValue apply(JsonValue input, HttpContext context) throws Exception {
		JsonObject result = new JsonObject();
		boolean limited = context.queryParameters().has("limit");
		int amount = 0;
		try {
			amount = context.queryParameters().getInt("limit");
		} catch (RuntimeException ex) {
			limited = false;
		}
		Collection<JsonObject> issues = limited ? issueAdmin.getLastReportedIssues(amount)
				: issueAdmin.getReportedIssues();
		issues.stream().forEach(e -> {
			if (!e.asObject().isEmpty()) {
				String compId = e.getString("source");
				if (!result.has(compId)) {
					result.put(compId, new JsonArray());
				}
				result.get(compId).asArray().add(e);
			}
		});
		return result;
	}

}
