/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.api;

import java.util.Comparator;

import org.ibcn.limeds.HttpEndpointSegment;
import org.ibcn.limeds.SegmentAdmin;
import org.ibcn.limeds.annotations.AuthMode;
import org.ibcn.limeds.annotations.HttpDoc;
import org.ibcn.limeds.annotations.HttpMethod;
import org.ibcn.limeds.annotations.HttpOperation;
import org.ibcn.limeds.annotations.OutputDoc;
import org.ibcn.limeds.annotations.Segment;
import org.ibcn.limeds.annotations.Service;
import org.ibcn.limeds.annotations.Validated;
import org.ibcn.limeds.descriptors.SegmentDescriptor;
import org.ibcn.limeds.json.JsonValue;
import org.ibcn.limeds.security.SystemRoles;
import org.ibcn.limeds.versioning.VersionUtil;

/**
 * This Segments sets up a HTTP endpoint for getting one of the registered
 * component factories.
 * 
 * @author wkerckho
 *
 */
@Validated(input = false)
@Segment(id = "limeds.segments.FactoryGetter", description = "This function returns details of one of the available factory templates.")
public class SegmentFactoryGetter extends HttpEndpointSegment {
	private static Comparator<JsonValue> segmentSortVersionDescending = (s1, s2) -> VersionUtil
			.createVersionPropertyValue(s2.getString("version"))
			.compareTo(VersionUtil.createVersionPropertyValue(s1.getString("version")));

	@Service
	private SegmentAdmin manager;

	@Override
	@HttpDoc(pathInfo = { "The id of the Factory.",
			"The version of the Factory (or 'latest' for the latest available Factory)" })
	@OutputDoc(schemaClass = SegmentDescriptor.class)
	@HttpOperation(groupId = API_Info.API_GROUP_NAME, method = HttpMethod.GET, path = "/_limeds/factories/{id}/{version}", authMode = AuthMode.AUTHORIZE_DISJUNCTIVE, authorityRequired = {
			SystemRoles.LIMEDS_ADMIN, SystemRoles.LIMEDS_VIEW_ONLY })
	public JsonValue apply(JsonValue input, HttpContext context) throws Exception {
		String versionExpr = context.pathParameters().getString("version");
		if ("latest".equals(versionExpr)) {
			return manager.getSegmentFactoriesInfo().stream()
					.filter(segment -> context.pathParameters().getString("id").equals(segment.getString("id")))
					.sorted(segmentSortVersionDescending).findFirst().get();
		} else {
			return manager.getSegmentFactoryInfo(context.pathParameters().getString("id"), versionExpr);
		}
	}

}
