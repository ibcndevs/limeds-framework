/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.api;

import java.util.Optional;

import org.ibcn.limeds.HttpEndpointSegment;
import org.ibcn.limeds.SegmentAdmin;
import org.ibcn.limeds.annotations.AuthMode;
import org.ibcn.limeds.annotations.HttpMethod;
import org.ibcn.limeds.annotations.HttpOperation;
import org.ibcn.limeds.annotations.Segment;
import org.ibcn.limeds.annotations.Service;
import org.ibcn.limeds.descriptors.SegmentDescriptor;
import org.ibcn.limeds.descriptors.TemplateDescriptor;
import org.ibcn.limeds.exceptions.ExceptionWithStatus;
import org.ibcn.limeds.json.JsonValue;
import org.ibcn.limeds.security.SystemRoles;
import org.ibcn.limeds.util.ConfigUtil;
import org.ibcn.limeds.util.FilterUtil;
import org.osgi.service.cm.Configuration;
import org.osgi.service.cm.ConfigurationAdmin;

@Segment(id = "limeds.config.Deleter", description = "This function allows removing a specified config instance.")
public class ConfigDeleter extends HttpEndpointSegment {

	@Service
	private ConfigurationAdmin configAdmin;

	@Service
	private SegmentAdmin segmentAdmin;

	@Override
	@HttpOperation(groupId = API_Info.API_GROUP_NAME, method = HttpMethod.DELETE, path = "/_limeds/config/{id}", authMode = AuthMode.AUTHORIZE_DISJUNCTIVE, authorityRequired = SystemRoles.LIMEDS_ADMIN)
	public JsonValue apply(JsonValue input, HttpContext context) throws Exception {
		String pid = context.pathParameters().getString("id");
		Configuration[] matches = configAdmin.listConfigurations(FilterUtil.equals("service.pid", pid));
		if (matches != null) {
			matches[0].delete();

			/*
			 * If the deleted config belonged to a Segment, we need to check if
			 * a default config should be created based on its descriptor
			 */
			Optional<SegmentDescriptor> segment = segmentAdmin.findRegisteredSegment(pid, "latest");
			if (segment.isPresent()) {
				SegmentDescriptor descriptor = segment.get();
				if (descriptor.getTemplateInfo() != null && descriptor.getTemplateInfo().getType()
						.equals(TemplateDescriptor.Type.CONFIGURABLE_INSTANCE)) {
					/*
					 * If configurable props are complete, create default
					 * transient config entry
					 */
					if (descriptor.getTemplateInfo().getConfiguration().stream().allMatch(cp -> cp.value != null)) {
						ConfigUtil.createDefaultConfig(configAdmin, descriptor);
					}
				}
			}

			return null;
		}

		matches = configAdmin.listConfigurations(FilterUtil.equals("$.id", pid));
		if (matches != null) {
			matches[0].delete();
			return null;
		}

		throw new ExceptionWithStatus(404, "The specified config cannot be found!");
	}

}
