/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.api;

import org.ibcn.limeds.SliceAdmin;
import org.ibcn.limeds.HttpEndpointSegment;
import org.ibcn.limeds.annotations.AuthMode;
import org.ibcn.limeds.annotations.HttpDoc;
import org.ibcn.limeds.annotations.HttpMethod;
import org.ibcn.limeds.annotations.HttpOperation;
import org.ibcn.limeds.annotations.Segment;
import org.ibcn.limeds.annotations.Service;
import org.ibcn.limeds.json.JsonValue;
import org.ibcn.limeds.security.SystemRoles;
import org.ibcn.limeds.util.Identifier;

/**
 * This Segment sets up an HTTP endpoint for publishing Slices. The result is a
 * deployable module that can be found in a generated folder in the Slices
 * directory.
 * 
 * @author wkerckho
 *
 */
@Segment(id = "limeds.slices.Publisher", description = "This function publishes a specified Slice to a deployable module.")
public class SlicePublisher extends HttpEndpointSegment {

	@Service
	private SliceAdmin manager;

	@Override
	@HttpDoc(pathInfo = { "The id of the Slice.",
			"The version of the Slice (or 'latest' for the latest available Slice)" })
	@HttpOperation(groupId = API_Info.API_GROUP_NAME, method = HttpMethod.GET, path = "/_limeds/slices/{id}/{version}/publish", authMode = AuthMode.AUTHORIZE_CONJUNCTIVE, authorityRequired = SystemRoles.LIMEDS_ADMIN)
	public JsonValue apply(JsonValue input, HttpContext context) throws Exception {
		Identifier sliceId = null;

		if ("latest".equals(context.pathParameters().getString("version"))) {
			sliceId = new Identifier(context.pathParameters().getString("id"),
					SliceGetter.findLatest(manager, context.pathParameters().getString("id")).getString("version"));
		} else {
			sliceId = new Identifier(context.pathParameters().getString("id"),
					context.pathParameters().getString("version"));
		}

		manager.publish(sliceId);
		return null;
	}

}
