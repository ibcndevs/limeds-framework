/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.api;

import java.util.Arrays;
import java.util.Collections;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.List;

import org.ibcn.limeds.HttpEndpointSegment;
import org.ibcn.limeds.annotations.AuthMode;
import org.ibcn.limeds.annotations.HttpMethod;
import org.ibcn.limeds.annotations.HttpOperation;
import org.ibcn.limeds.annotations.InputDoc;
import org.ibcn.limeds.annotations.Segment;
import org.ibcn.limeds.annotations.Service;
import org.ibcn.limeds.annotations.Validated;
import org.ibcn.limeds.json.JsonArray;
import org.ibcn.limeds.json.JsonValue;
import org.ibcn.limeds.security.SystemRoles;
import org.ibcn.limeds.util.ConfigUtil;
import org.ibcn.limeds.util.FilterUtil;
import org.osgi.service.cm.Configuration;
import org.osgi.service.cm.ConfigurationAdmin;

@Validated(output = false)
@Segment(id = "limeds.config.Updater", description = "This function allows updating a specified config instance. If the instance does not exist, it is created.")
public class ConfigUpdater extends HttpEndpointSegment {

	private static List<String> RETAIN_LIST = Arrays.asList("$.id", "_transient", "limeds.configId");

	@Service
	private ConfigurationAdmin configAdmin;

	@Override
	@InputDoc(schemaRef = "org.ibcn.limeds.api.types.ConfigProperty_${sliceVersion}", collection = true)
	@HttpOperation(groupId = API_Info.API_GROUP_NAME, method = HttpMethod.PUT, path = "/_limeds/config/{id}", authMode = AuthMode.AUTHORIZE_CONJUNCTIVE, authorityRequired = SystemRoles.LIMEDS_ADMIN)
	public JsonValue apply(JsonValue input, HttpContext context) throws Exception {
		Configuration[] matches = configAdmin
				.listConfigurations(FilterUtil.equals("service.pid", context.pathParameters().getString("id")));
		if (matches != null) {
			// Match is not a LimeDS factory config
			Dictionary<String, Object> properties = matches[0].getProperties();
			/*
			 * Apply changes. Force persist is set to true because we want to
			 * persist configs previously defined as transient when an update is
			 * done (= overriding defaults).
			 */
			applyChanges(properties, input.asArray(), true);
			matches[0].update(properties);
			return null;
		}

		matches = configAdmin.listConfigurations(FilterUtil.equals("$.id", context.pathParameters().getString("id")));
		if (matches != null) {
			// Match is a LimeDS factory config
			Dictionary<String, Object> properties = matches[0].getProperties();
			/*
			 * Apply changes. Force persist is set to false because we don't
			 * want to create a config file for factory instances that are
			 * defined in the Slices. Changes to these kind of configs are
			 * temporary until the Slice or Framework is restarted.
			 */
			applyChanges(properties, input.asArray(), false);
			matches[0].update(properties);
			return null;
		}

		// The specified config cannot be found, create one:
		Configuration config = configAdmin.getConfiguration(context.pathParameters().getString("id"), null);
		Dictionary<String, Object> properties = new Hashtable<>();
		applyChanges(properties, input.asArray(), false);
		config.update(properties);

		return null;
	}

	static void applyChanges(Dictionary<String, Object> properties, JsonArray input, boolean forcePersist) {
		Collections.list(properties.keys()).stream().filter(key -> !RETAIN_LIST.contains(key))
				.forEach(properties::remove);

		properties.remove("_transient");

		input.forEach(entry -> properties.put(entry.getString("name"),
				ConfigUtil.convertValue(entry.getString("value"), entry.getString("type"))));
	}

}
