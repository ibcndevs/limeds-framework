/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.api;

import org.ibcn.limeds.FunctionalSegment;
import org.ibcn.limeds.SliceAdmin;
import org.ibcn.limeds.annotations.AuthMode;
import org.ibcn.limeds.annotations.HttpMethod;
import org.ibcn.limeds.annotations.HttpOperation;
import org.ibcn.limeds.annotations.InputDoc;
import org.ibcn.limeds.annotations.Segment;
import org.ibcn.limeds.annotations.Service;
import org.ibcn.limeds.annotations.Validated;
import org.ibcn.limeds.json.JsonValue;
import org.ibcn.limeds.security.SystemRoles;

/**
 * This Segment sets up a HTTP endpoint for the creation of new Slices.
 * 
 * @author wkerckho
 *
 */
@Validated(output = false)
@Segment(id = "limeds.slices.Creator", description = "This function allows the creation of new Slices.")
public class SliceCreator implements FunctionalSegment {

	@Service
	private SliceAdmin manager;

	@Override
	@InputDoc(schemaRef = "org.ibcn.limeds.api.types.Slice_${sliceVersion}")
	@HttpOperation(groupId = API_Info.API_GROUP_NAME, method = HttpMethod.POST, path = "/_limeds/slices", authMode = AuthMode.AUTHORIZE_CONJUNCTIVE, authorityRequired = SystemRoles.LIMEDS_ADMIN)
	public JsonValue apply(JsonValue... input) throws Exception {
		manager.sync(input[0].asObject());
		return null;
	}

}
