/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.api;

import java.net.URI;
import java.util.Arrays;

import org.apache.felix.bundlerepository.RepositoryAdmin;
import org.apache.felix.bundlerepository.Resource;
import org.ibcn.limeds.HttpEndpointSegment;
import org.ibcn.limeds.annotations.AuthMode;
import org.ibcn.limeds.annotations.HttpDoc;
import org.ibcn.limeds.annotations.HttpMethod;
import org.ibcn.limeds.annotations.HttpOperation;
import org.ibcn.limeds.annotations.OutputDoc;
import org.ibcn.limeds.annotations.Segment;
import org.ibcn.limeds.annotations.Service;
import org.ibcn.limeds.annotations.Validated;
import org.ibcn.limeds.json.Json;
import org.ibcn.limeds.json.JsonValue;
import org.ibcn.limeds.security.SystemRoles;
import org.ibcn.limeds.util.Errors;
import org.ibcn.limeds.util.JarProcessor;

/**
 * This Segment sets up an HTTP endpoint for retrieving information about an
 * installable module (by downloading manifest information from the repository).
 * 
 * @author wkerckho
 *
 */
@Validated(input = false)
@Segment(id = "limeds.installables.Getter", description = "This function returns meta-data for a specified module that can be installed from a connected repository.")
public class InstallablesGetter extends HttpEndpointSegment {

	@Service
	private RepositoryAdmin repoAdmin;

	@Override
	@HttpDoc(pathInfo = { "The id of the installable module.",
			"The version of the installable module (or 'latest' for the latest available module)" })
	@OutputDoc(schemaRef = "org.ibcn.limeds.api.types.Slice_${sliceVersion}", collection = true)
	@HttpOperation(groupId = API_Info.API_GROUP_NAME, method = HttpMethod.GET, path = "/_limeds/installables/{id}/{version}", authMode = AuthMode.AUTHORIZE_DISJUNCTIVE, authorityRequired = {
			SystemRoles.LIMEDS_ADMIN, SystemRoles.LIMEDS_VIEW_ONLY })
	public JsonValue apply(JsonValue input, HttpContext context) throws Exception {
		Resource[] resources = repoAdmin.discoverResources("(symbolicname=*)");
		return Arrays.stream(resources)
				.sorted((res1, res2) -> res2.getVersion().toString().compareTo(res1.getVersion().toString()))
				.filter(res -> matchesRequest(res, context))
				.map(Errors.wrapFunction(res -> downloadSliceDescriptor(res, context))).findFirst().get();
	}

	private boolean matchesRequest(Resource res, HttpContext context) {
		return context.pathParameters().getString("id").equals(res.getSymbolicName())
				&& (context.pathParameters().getString("version").equals(res.getVersion().toString())
						|| context.pathParameters().getString("version").equals("latest"));
	}

	private JsonValue downloadSliceDescriptor(Resource res, HttpContext context) throws Exception {
		JarProcessor processor = new JarProcessor(URI.create(res.getURI()).toURL().openStream());
		return processor.getSlicePaths().stream().map(Errors.wrapFunction(path -> processor.readDescriptor(path)))
				.collect(Json.toArray());
	}

}
