/**
 * This package contains the public Segments that together form the LimeDS REST
 * API.
 */
@org.osgi.annotation.versioning.Version(API_Info.VERSION)
package org.ibcn.limeds.api;