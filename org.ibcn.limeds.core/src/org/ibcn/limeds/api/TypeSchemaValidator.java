/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.api;

import org.ibcn.limeds.HttpEndpointSegment;
import org.ibcn.limeds.TypeAdmin;
import org.ibcn.limeds.annotations.AuthMode;
import org.ibcn.limeds.annotations.HttpMethod;
import org.ibcn.limeds.annotations.HttpOperation;
import org.ibcn.limeds.annotations.Segment;
import org.ibcn.limeds.annotations.Service;
import org.ibcn.limeds.json.Json;
import org.ibcn.limeds.json.JsonValue;
import org.ibcn.limeds.security.SystemRoles;

/**
 * This Segment sets up an HTTP endpoint for validating JSON type definitions.
 * 
 * @author wkerckho
 *
 */
@Segment(id = "limeds.types.SchemaValidator", description = "This function validates the submitted schema.")
public class TypeSchemaValidator extends HttpEndpointSegment {

	@Service
	private TypeAdmin typeAdmin;

	@Override
	@HttpOperation(groupId = API_Info.API_GROUP_NAME, method = HttpMethod.POST, path = "/_limeds/types/schemavalidator", authMode = AuthMode.AUTHORIZE_CONJUNCTIVE, authorityRequired = SystemRoles.LIMEDS_ADMIN)
	public JsonValue apply(JsonValue input, HttpContext context) throws Exception {
		try {
			typeAdmin.validateSchema(input);
			return null;
		} catch (Exception e) {
			return Json.objectBuilder().add("error", e.getMessage()).build();
		}
	}

}
