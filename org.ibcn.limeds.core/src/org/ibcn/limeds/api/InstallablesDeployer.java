/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.api;

import java.util.Arrays;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.felix.bundlerepository.RepositoryAdmin;
import org.apache.felix.bundlerepository.Resolver;
import org.apache.felix.bundlerepository.Resource;
import org.ibcn.limeds.HttpEndpointSegment;
import org.ibcn.limeds.annotations.AuthMode;
import org.ibcn.limeds.annotations.HttpDoc;
import org.ibcn.limeds.annotations.HttpMethod;
import org.ibcn.limeds.annotations.HttpOperation;
import org.ibcn.limeds.annotations.Segment;
import org.ibcn.limeds.annotations.Service;
import org.ibcn.limeds.json.Json;
import org.ibcn.limeds.json.JsonArray;
import org.ibcn.limeds.json.JsonObject;
import org.ibcn.limeds.json.JsonPrimitive;
import org.ibcn.limeds.json.JsonValue;
import org.ibcn.limeds.security.SystemRoles;
import org.ibcn.limeds.util.FilterUtil;
import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.Version;
import org.slf4j.LoggerFactory;

/**
 * This Segment sets up an HTTP endpoint for downloading and installing
 * installable modules on this LimeDS instance (including transitive
 * dependencies).
 * 
 * @author wkerckho
 *
 */
@Segment(id = "limeds.installables.Deployer", description = "This function performs the installation of a specified module.")
public class InstallablesDeployer extends HttpEndpointSegment {

	private static final Bundle BUNDLE = FrameworkUtil.getBundle(InstallablesDeployer.class);

	@Service
	private RepositoryAdmin repoAdmin;

	@Override
	@HttpDoc(pathInfo = { "The id of the installable module.",
			"The version of the installable module (or 'latest' for the latest available module)" }, querySchema = "{\"requiredOnly\" : \"Boolean * (Optional boolean indicating whether the deploy process should stick to required only dependencies, default is true.)\"}")
	@HttpOperation(groupId = API_Info.API_GROUP_NAME, method = HttpMethod.PUT, path = "/_limeds/installables/{id}/{version}", authMode = AuthMode.AUTHORIZE_CONJUNCTIVE, authorityRequired = SystemRoles.LIMEDS_ADMIN)
	public JsonValue apply(JsonValue input, HttpContext context) throws Exception {
		String target = FilterUtil.equals("symbolicname", context.pathParameters().getString("id"));
		boolean requiredOnly = context.queryParameters().has("requiredOnly")
				? context.queryParameters().getBool("requiredOnly") : true;

		Resolver resolver = repoAdmin.resolver();
		Optional<Resource> resource = Arrays.stream(repoAdmin.discoverResources(target))
				.sorted((res1, res2) -> res2.getVersion().toString().compareTo(res1.getVersion().toString()))
				.filter(res -> context.pathParameters().getString("version").equals("latest")
						|| res.getVersion().toString().equals(context.pathParameters().getString("version")))
				.findFirst();
		if (resource.isPresent()) {
			resolver.add(resource.get());

			if (isResolvable(resolver, requiredOnly)) {
				JsonObject report = new JsonObject();
				JsonArray installedModules = new JsonArray();
				int deployTries = 3;
				while (deployTries > 0) {
					try {
						resolver.deploy(0);
						deployTries = 0;
					} catch (IllegalStateException e) {
						// Retry
						deployTries--;
						LoggerFactory.getLogger(getClass()).warn("Encountered " + e
								+ " while resolving installables, will retry " + deployTries + " more time(s)...");
						Thread.sleep(10L);
					}
				}
				// Start all deployed bundles
				for (Resource res : resolver.getRequiredResources()) {
					if (BUNDLE.getSymbolicName().equals(res.getSymbolicName())) {
						// A new core was installed, remove
						LoggerFactory.getLogger(getClass()).warn("Resolver deployed a new Core module "
								+ res.getSymbolicName() + "_" + res.getVersion() + ", undoing this change!");
						findBundle(res.getSymbolicName(), res.getVersion()).uninstall();
						if (res.getVersion().getMajor() != BUNDLE.getVersion().getMajor()) {
							report.put("warning",
									"The installable depends on an incompatible version of the LimeDS Core and might not work properly (required version is "
											+ res.getVersion() + ").");
						}
					} else {
						// Start the bundle
						findBundle(res.getSymbolicName(), res.getVersion()).start();
						installedModules.add(getResourceJson(res, resolver));
					}
				}

				findBundle(resource.get().getSymbolicName(), resource.get().getVersion()).start();
				installedModules.add(getResourceJson(resource.get(), resolver));

				report.put("installedModules", installedModules);

				// Return report
				return report;
			} else {
				context.respondWith(500);
				return Arrays.stream(resolver.getUnsatisfiedRequirements())
						.map(req -> new JsonPrimitive("Unable to resolve: " + req.getRequirement()))
						.collect(Json.toArray());
			}
		}
		return null;
	}

	private JsonObject getResourceJson(Resource r, Resolver resolver) {
		JsonObject result = new JsonObject();
		result.put("name", r.getSymbolicName());
		result.put("version", r.getVersion().toString());
		result.put("reason", resolver.getReason(r) == null ? new JsonArray() // getReason can return null, without it being wrong see: http://grepcode.com/file/repo1.maven.org/maven2/org.apache.felix/org.apache.felix.bundlerepository/1.6.0/org/apache/felix/bundlerepository/impl/ResolverImpl.java#ResolverImpl.getReason%28org.apache.felix.bundlerepository.Resource%29
				: Arrays.stream(resolver.getReason(r))
						.map(res -> new JsonPrimitive(res.getResource().getSymbolicName() + "_"
								+ res.getResource().getVersion().toString() + ": " + res.getRequirement().getFilter()))
						.collect(Json.toArray()));
		return result;
	}

	private boolean isResolvable(Resolver resolver, boolean requiredOnly) {
		if (requiredOnly) {
			LoggerFactory.getLogger(getClass())
					.warn("Resolving " + Arrays.stream(resolver.getAddedResources())
							.map(res -> res.getSymbolicName() + "_" + res.getVersion())
							.collect(Collectors.joining(", ")) + " (required only)");
			return resolver.resolve(Resolver.NO_OPTIONAL_RESOURCES);
		} else {
			LoggerFactory.getLogger(getClass()).warn("Resolving " + Arrays.stream(resolver.getAddedResources())
					.map(res -> res.getSymbolicName() + "_" + res.getVersion()).collect(Collectors.joining(", ")));
			return resolver.resolve();
		}
	}

	private Bundle findBundle(String name, Version version) {
		return Arrays.stream(BUNDLE.getBundleContext().getBundles())
				.filter(b -> b.getSymbolicName().equals(name) && b.getVersion().equals(version)).findFirst().get();
	}

}
