/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.api;

import java.util.Arrays;
import java.util.Dictionary;
import java.util.Enumeration;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.ibcn.limeds.HttpEndpointSegment;
import org.ibcn.limeds.SegmentAdmin;
import org.ibcn.limeds.annotations.AuthMode;
import org.ibcn.limeds.annotations.HttpDoc;
import org.ibcn.limeds.annotations.HttpMethod;
import org.ibcn.limeds.annotations.HttpOperation;
import org.ibcn.limeds.annotations.OutputDoc;
import org.ibcn.limeds.annotations.Segment;
import org.ibcn.limeds.annotations.Service;
import org.ibcn.limeds.annotations.Validated;
import org.ibcn.limeds.cfg.ConfigHelper;
import org.ibcn.limeds.descriptors.TemplateDescriptor;
import org.ibcn.limeds.json.Json;
import org.ibcn.limeds.json.JsonArray;
import org.ibcn.limeds.json.JsonObject;
import org.ibcn.limeds.json.JsonValue;
import org.ibcn.limeds.security.SystemRoles;
import org.ibcn.limeds.util.ConfigUtil;
import org.osgi.service.cm.Configuration;
import org.osgi.service.cm.ConfigurationAdmin;

@Validated(input = false)
@Segment(id = "limeds.config.Lister", description = "This function returns all available config information including non-instantiated instances such as default configurations and factory templates.")
public class ConfigLister extends HttpEndpointSegment {

	@Service
	private ConfigurationAdmin configAdmin;

	@Service
	private ConfigHelper configHelper;

	@Service
	private SegmentAdmin segmentAdmin;

	@Override
	@OutputDoc(schemaRef = "org.ibcn.limeds.api.types.ConfigList_${sliceVersion}")
	@HttpDoc(querySchema = "{\"showSliceLocal\" : \"Boolean * (If present or true, the configurations for local slice factory instantiations will also be shown.)\"}")
	@HttpOperation(groupId = API_Info.API_GROUP_NAME, method = HttpMethod.GET, path = "/_limeds/config", authMode = AuthMode.AUTHORIZE_DISJUNCTIVE, authorityRequired = {
			SystemRoles.LIMEDS_ADMIN, SystemRoles.LIMEDS_VIEW_ONLY })
	public JsonValue apply(JsonValue input, HttpContext context) throws Exception {
		JsonObject result = new JsonObject();
		// Add instanceable configurations
		configHelper.getInstanceables().forEach((id, config) -> result.put(id, config));

		// Add LimeDS factories
		segmentAdmin.getSegmentFactoriesInfo().stream().map(factory -> {
			factory.get("templateInfo").asObject().put("sliceConfig", true);
			factory.get("templateInfo").asObject().get("configuration").asArray()
					.add(Json.objectBuilder().add("name", TemplateDescriptor.EXPORT_PROPERTY).add("type", "boolean")
							.add("value", "true").build());
			return factory;
		}).forEach(factory -> result.put(factory.getString("id"), factory.get("templateInfo")));

		Configuration[] configs = configAdmin.listConfigurations(null);
		// Add available configurations
		Predicate<Configuration> filter = context.isActiveQueryFlag("showSliceLocal") ? cfg -> true
				: cfg -> cfg.getProperties().get(TemplateDescriptor.SLICE_LOCAL_PROPERTY) == null
						|| !((boolean) cfg.getProperties().get(TemplateDescriptor.SLICE_LOCAL_PROPERTY));

		if (configs != null) {
			Arrays.stream(configs).filter(filter).forEach(cfg -> result.put(extractKey(cfg), transform(cfg)));
		}
		return result;
	}

	private JsonObject transform(Configuration config) {
		Set<String> configurableIds = segmentAdmin.getRegisteredSegments().stream()
				.filter(desc -> desc.getTemplateInfo() != null
						&& desc.getTemplateInfo().getType().equals(TemplateDescriptor.Type.CONFIGURABLE_INSTANCE))
				.map(desc -> desc.getId()).collect(Collectors.toSet());
		Set<String> factoryIds = segmentAdmin.getSegmentFactoriesInfo().stream().map(desc -> desc.getString("id"))
				.collect(Collectors.toSet());

		if (configurableIds.contains(config.getPid())) {
			return Json.objectBuilder().add("type", TemplateDescriptor.Type.CONFIGURABLE_INSTANCE.toString())
					.add("configuration", propsToJson(config.getProperties())).add("sliceConfig", true).build();
		} else if (config.getFactoryPid() != null && factoryIds.contains(config.getFactoryPid())) {
			return Json.objectBuilder().add("type", TemplateDescriptor.Type.FACTORY_INSTANCE.toString())
					.add("factoryId", config.getFactoryPid()).add("configuration", propsToJson(config.getProperties()))
					.add("sliceConfig", true).build();
		} else {
			JsonObject result = new JsonObject();
			if (config.getFactoryPid() != null) {
				result.put("type", TemplateDescriptor.Type.FACTORY_INSTANCE.toString());
				result.put("factoryId", config.getFactoryPid());
			} else {
				result.put("type", TemplateDescriptor.Type.CONFIGURABLE_INSTANCE.toString());
			}
			result.put("configuration", propsToJson(config.getProperties()));
			result.put("sliceConfig", false);
			return result;
		}
	}

	private String extractKey(Configuration config) {
		if (config.getProperties().get("$.id") != null) {
			return "" + config.getProperties().get("$.id");
		} else {
			return config.getPid();
		}
	}

	private JsonArray propsToJson(Dictionary<String, Object> props) {
		JsonArray result = new JsonArray();
		Enumeration<String> keys = props.keys();
		while (keys.hasMoreElements()) {
			String key = keys.nextElement();
			if (!ConfigUtil.IGNORE_LIST.contains(key)) {
				Object value = props.get(key);
				result.add(Json.objectBuilder().add("name", key).add("type", value.getClass().getTypeName()).add(
						"value",
						value.getClass().isArray() ? stripBrackets(Arrays.toString((Object[]) value)) : "" + value)
						.build());
			}
		}
		return result;
	}

	private String stripBrackets(String arrayRepr) {
		return arrayRepr.substring(1, arrayRepr.length() - 1);
	}

}
