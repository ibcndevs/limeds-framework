/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.api;

import java.util.Optional;

import org.ibcn.limeds.HttpEndpointSegment;
import org.ibcn.limeds.TypeAdmin;
import org.ibcn.limeds.annotations.AuthMode;
import org.ibcn.limeds.annotations.HttpDoc;
import org.ibcn.limeds.annotations.HttpMethod;
import org.ibcn.limeds.annotations.HttpOperation;
import org.ibcn.limeds.annotations.Segment;
import org.ibcn.limeds.annotations.Service;
import org.ibcn.limeds.exceptions.ExceptionWithStatus;
import org.ibcn.limeds.json.JsonObject;
import org.ibcn.limeds.json.JsonValue;
import org.ibcn.limeds.security.SystemRoles;
import org.ibcn.limeds.util.Identifier;

/**
 * This Segment sets up an HTTP endpoint for retrieving JSON type information.
 * 
 * @author wkerckho
 *
 */
@Segment(id = "limeds.types.Getter", description = "This function returns the LimeDS schema of a specified type.")
public class TypeGetter extends HttpEndpointSegment {

	@Service
	private TypeAdmin typeAdmin;

	@Override
	@HttpDoc(queryRef = "org.ibcn.limeds.api.types.TypeGetterQuery_${sliceVersion}")
	@HttpOperation(groupId = API_Info.API_GROUP_NAME, method = HttpMethod.GET, path = "/_limeds/types/{ref}", authMode = AuthMode.AUTHORIZE_DISJUNCTIVE, authorityRequired = {
			SystemRoles.LIMEDS_ADMIN, SystemRoles.LIMEDS_VIEW_ONLY })
	public JsonValue apply(JsonValue input, HttpContext context) throws Exception {
		boolean resolve = context.isActiveQueryFlag("resolve");

		try {
			Identifier identifier = Identifier.parse(context.pathParameters().getString("ref"));

			Optional<JsonObject> type = typeAdmin.getRegisteredType(identifier);
			if (type.isPresent()) {
				return resolve ? typeAdmin.resolve(type.get()) : type.get();
			} else {
				context.respondWith(404);
				return null;
			}
		} catch (Exception e) {
			throw new ExceptionWithStatus(500, "The format of a schema reference must be: [schemaId]_[schemaVersion]");
		}
	}

}
