/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.api;

import org.ibcn.limeds.HttpEndpointSegment;
import org.ibcn.limeds.SliceAdmin;
import org.ibcn.limeds.annotations.AuthMode;
import org.ibcn.limeds.annotations.HttpDoc;
import org.ibcn.limeds.annotations.HttpMethod;
import org.ibcn.limeds.annotations.HttpOperation;
import org.ibcn.limeds.annotations.InputDoc;
import org.ibcn.limeds.annotations.Segment;
import org.ibcn.limeds.annotations.Service;
import org.ibcn.limeds.annotations.Validated;
import org.ibcn.limeds.json.JsonObject;
import org.ibcn.limeds.json.JsonValue;
import org.ibcn.limeds.security.SystemRoles;

/**
 * This Segment sets up a HTTP endpoint for synchronizing the state of specific
 * Slice instances.
 * 
 * @author wkerckho
 *
 */
@Validated(output = false)
@Segment(id = "limeds.slices.Updater", description = "This function updates a specified Slice.")
public class SliceUpdater extends HttpEndpointSegment {

	@Service
	private SliceAdmin manager;

	@Override
	@InputDoc(schemaRef = "org.ibcn.limeds.api.types.Slice_${sliceVersion}")
	@HttpDoc(pathInfo = { "The id of the Slice.",
			"The version of the Slice (or 'latest' for the latest available Slice)" })
	@HttpOperation(groupId = API_Info.API_GROUP_NAME, method = HttpMethod.PUT, path = "/_limeds/slices/{id}/{version}", authMode = AuthMode.AUTHORIZE_CONJUNCTIVE, authorityRequired = SystemRoles.LIMEDS_ADMIN)
	public JsonValue apply(JsonValue input, HttpContext context) throws Exception {
		String version = context.pathParameters().getString("version");
		if ("latest".equals(version)) {
			version = SliceGetter.findLatest(manager, context.pathParameters().getString("id")).getString("version");
		}

		JsonObject flow = input.asObject();
		flow.put("id", context.pathParameters().get("id"));
		flow.put("version", version);
		manager.sync(flow);
		return null;
	}

}
