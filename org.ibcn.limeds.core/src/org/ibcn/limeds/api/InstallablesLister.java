/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.api;

import java.util.Arrays;

import org.apache.felix.bundlerepository.RepositoryAdmin;
import org.apache.felix.bundlerepository.Resource;
import org.ibcn.limeds.HttpEndpointSegment;
import org.ibcn.limeds.ServicePropertyNames;
import org.ibcn.limeds.annotations.AuthMode;
import org.ibcn.limeds.annotations.Configurable;
import org.ibcn.limeds.annotations.HttpDoc;
import org.ibcn.limeds.annotations.HttpMethod;
import org.ibcn.limeds.annotations.HttpOperation;
import org.ibcn.limeds.annotations.OutputDoc;
import org.ibcn.limeds.annotations.Segment;
import org.ibcn.limeds.annotations.Service;
import org.ibcn.limeds.annotations.Validated;
import org.ibcn.limeds.json.Json;
import org.ibcn.limeds.json.JsonArray;
import org.ibcn.limeds.json.JsonValue;
import org.ibcn.limeds.security.SystemRoles;
import org.ibcn.limeds.util.Errors;
import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This Segment sets up an HTTP endpoint for listing all the modules that can be
 * installed from the repositories that were registered for this LimeDS
 * instance.
 * 
 * @author wkerckho
 *
 */
@Validated(input = false)
@Segment(id = "limeds.installables.Lister", description = "This function returns all the modules that can be installed from the connected repositories.")
public class InstallablesLister extends HttpEndpointSegment {

	private static final Logger LOGGER = LoggerFactory.getLogger(InstallablesLister.class);
	private static final Bundle BUNDLE_REF = FrameworkUtil.getBundle(InstallablesLister.class);

	@Service
	private RepositoryAdmin repoAdmin;

	@Configurable(description = "The remote OSGi R5/OBR repositories to check for LimeDS installable modules and dependencies.", defaultValue = "http://limeds.be:8081/r5/limeds-main, http://limeds.be:8081/r5/limeds-modules, http://limeds.be:8081/r5/limeds-dep, http://repository.amdatu.org/release/index.xml.gz, http://repository.amdatu.org/dependencies/index.xml.gz, https://raw.githubusercontent.com/bndtools/bundle-hub/master/index.xml.gz")
	private String[] repositories;

	@Override
	public void started() throws Exception {
		// Register all configured repositories
		Arrays.stream(repositories).forEach(Errors.wrapConsumer(repo -> repoAdmin.addRepository(repo)));
	}

	@Override
	public void stopped() throws Exception {
		// Unregister the configured repositories
		Arrays.stream(repositories).forEach(Errors.wrapConsumer(repo -> repoAdmin.removeRepository(repo)));
	}

	@Override
	@OutputDoc(schemaRef = "org.ibcn.limeds.api.types.Installable_${sliceVersion}", collection = true)
	@HttpDoc(querySchema = "{\"refresh\" : \"Boolean * (Empty query parameter, if present -or when true- indicates that the connected repositories should be refreshed.)\"}")
	@HttpOperation(groupId = API_Info.API_GROUP_NAME, method = HttpMethod.GET, path = "/_limeds/installables", authMode = AuthMode.AUTHORIZE_DISJUNCTIVE, authorityRequired = {
			SystemRoles.LIMEDS_ADMIN, SystemRoles.LIMEDS_VIEW_ONLY })
	public JsonValue apply(JsonValue input, HttpContext context) throws Exception {
		if (context.isActiveQueryFlag("refresh")) {
			Arrays.stream(repoAdmin.listRepositories()).forEach(Errors.wrapConsumer(repo -> {
				LOGGER.info("Refreshing R5 repo {}", repo.getURI());
				repoAdmin.addRepository(repo.getURI());
			}));
		}

		Resource[] resources = repoAdmin.discoverResources("(symbolicname=*)");
		boolean showAll = context.isActiveQueryFlag("showAll");

		return Arrays.stream(resources).filter(res -> showAll || isSliceModule(res))
				.map(res -> Json.objectBuilder().add("id", res.getSymbolicName())
						.add("version", res.getVersion().toString()).add("installed", isInstalled(res))
						.add("segments", getSegments(res)).add("types", getTypes(res)).build())
				.collect(Json.toArray());
	}

	private boolean isInstalled(Resource res) {
		return Arrays.stream(BUNDLE_REF.getBundleContext().getBundles()).anyMatch(
				b -> b.getSymbolicName().equals(res.getSymbolicName()) && b.getVersion().equals(res.getVersion()));
	}

	private JsonValue getSegments(Resource res) {
		JsonArray result = new JsonArray();
		if (res.getCapabilities() != null) {
			Arrays.stream(res.getCapabilities()).filter(cap -> "limeds.segment".equals(cap.getName()))
					.forEach(cap -> result.add("" + cap.getPropertiesAsMap().get(ServicePropertyNames.SEGMENT_ID)));
		}
		return result;
	}

	private JsonValue getTypes(Resource res) {
		JsonArray result = new JsonArray();
		if (res.getCapabilities() != null) {
			Arrays.stream(res.getCapabilities()).filter(cap -> "limeds.type".equals(cap.getName()))
					.forEach(cap -> result.add("" + cap.getPropertiesAsMap().get(ServicePropertyNames.TYPE_ID)));
		}
		return result;
	}

	private boolean isSliceModule(Resource resource) {
		return (resource.getCapabilities() != null
				&& Arrays.stream(resource.getCapabilities()).anyMatch(cap -> "limeds.segment".equals(cap.getName())))
				|| (resource.getRequirements() != null && Arrays.stream(resource.getRequirements())
						.anyMatch(req -> "limeds.segment".equals(req.getName())));
	}

}
