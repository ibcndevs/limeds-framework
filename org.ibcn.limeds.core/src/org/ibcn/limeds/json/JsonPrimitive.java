/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.json;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.List;

import org.json.simple.JSONValue;

/**
 * Subclass of JsonValue for representing primitive types.
 * 
 * @author wkerckho
 *
 */
public class JsonPrimitive implements JsonValue {

	private static final List<Class<?>> supportedTypes = Arrays.asList(Integer.class, Double.class, Float.class,
			Long.class, Boolean.class, String.class);
	private final Object value;

	public JsonPrimitive(Object value) {
		if (value != null && !value.getClass().isPrimitive() && !supportedTypes.contains(value.getClass())) {
			throw new RuntimeException(
					"JsonPrimitive can only contain primitive types or String, not instances of " + value.getClass());
		}
		this.value = value;
	}

	public Object getValue() {
		return value;
	}

	/**
	 * By overriding this method we can convert a JsonValue that is actually a
	 * JSON primitive to JsonPrimitive without using a cast.
	 */
	public JsonPrimitive asPrimitive() {
		return this;
	}

	@Override
	public boolean asBoolean() {
		if (value instanceof Boolean) {
			return (Boolean) value;
		} else if (value instanceof String) {
			return Boolean.parseBoolean((String) value);
		} else {
			throw new RuntimeException("Cannot return value as the requested type!");
		}
	}

	@Override
	public int asInt() {
		if (value instanceof Integer) {
			return (Integer) value;
		} else if (value instanceof Long) {
			return ((Long) value).intValue();
		} else if (value instanceof Double) {
			return ((Double) value).intValue();
		} else if (value instanceof Float) {
			return ((Float) value).intValue();
		} else if (value instanceof String) {
			return Integer.parseInt((String) value);
		} else {
			throw new RuntimeException("Cannot return value as the requested type!");
		}
	}

	@Override
	public double asDouble() {
		if (value instanceof Double) {
			return (Double) value;
		} else if (value instanceof String) {
			return Double.parseDouble((String) value);
		} else if (value instanceof Float) {
			return ((Float) value).doubleValue();
		} else if (value instanceof Long) {
			return ((Long) value).doubleValue();
		} else if (value instanceof Integer) {
			return ((Integer) value).doubleValue();
		} else {
			throw new RuntimeException("Cannot return value as the requested type!");
		}
	}

	@Override
	public long asLong() {
		if (value instanceof Long) {
			return (Long) value;
		} else if (value instanceof Integer) {
			return ((Integer) value).longValue();
		} else if (value instanceof Double) {
			return ((Double) value).longValue();
		} else if (value instanceof Float) {
			return ((Float) value).longValue();
		} else if (value instanceof String) {
			return Long.parseLong((String) value);
		} else {
			throw new RuntimeException("Cannot return value as the requested type!");
		}
	}

	@Override
	public String asString() {
		if (value instanceof String) {
			return (String) value;
		} else {
			return value.toString();
		}
	}

	@Override
	public String toString() {
		if (value == null) {
			return "null";
		}

		if (value instanceof String) {
			return "\"" + JSONValue.escape((String) value) + "\"";
		} else {
			return value.toString();
		}
	}

	@Override
	public JsonValue get(String key) {
		throw new UnsupportedOperationException("JsonPrimitive cannot be indexed by key!");
	}

	@Override
	public JsonValue get(int index) {
		throw new UnsupportedOperationException("JsonPrimitive cannot be accessed by index!");
	}

	@Override
	public boolean has(String key) {
		throw new UnsupportedOperationException("JsonPrimitive cannot be indexed by key!");
	}

	@Override
	public String toPrettyString(int baseIndent, int indentIncrement) {
		return toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		JsonPrimitive other = (JsonPrimitive) obj;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		return true;
	}

	@Override
	public void write(OutputStream out, String charSet) throws IOException {
		out.write(toString().getBytes(charSet));
	}

	@Override
	public void write(PrintWriter out) {
		out.print(toString());
	}

}
