/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.json.impl;

import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import org.ibcn.limeds.javascript.ScriptObjectMirror;
import org.ibcn.limeds.json.Json;
import org.ibcn.limeds.json.JsonObject;
import org.ibcn.limeds.json.JsonPrimitive;
import org.ibcn.limeds.json.JsonValue;
import org.ibcn.limeds.util.Errors;

/**
 * Alternative JSON parser that creates LimeDS JsonValues from Strings by
 * calling the internal Nashorn parser.
 * 
 * @author wkerckho
 *
 */
public class JsonParser {

	public static final JsonParser INSTANCE = new JsonParser();

	private ScriptEngine engine = new ScriptEngineManager().getEngineByName("nashorn");
	private Object json;

	private JsonParser() {
		try {
			json = engine.eval("JSON");
		} catch (ScriptException e) {
			// Should not happen
			e.printStackTrace();
		}
	}

	public JsonValue from(String jsonString) throws Exception {
		return nashornToJson(((Invocable) engine).invokeMethod(json, "parse", jsonString));
	}

	public JsonValue nashornToJson(Object output) {
		if (ScriptObjectMirror.isInstance(output)) {
			try {
				ScriptObjectMirror mirror = new ScriptObjectMirror(output);
				if (mirror.isArray()) {
					// Parse array
					return mirror.values().stream().map(this::nashornToJson).collect(Json.toArray());
				} else {
					// Parse object
					JsonObject object = new JsonObject();
					mirror.keySet().forEach(Errors.wrapConsumer(k -> object.put(k, nashornToJson(mirror.get(k)))));
					return object;
				}
			} catch (Exception e) {
				return null;
			}
		} else {
			return new JsonPrimitive(output);
		}
	}

}
