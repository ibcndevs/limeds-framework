/**
 * (Internal) This package implements some JSON utility classes.
 */
@org.osgi.annotation.versioning.Version("1.0.0")
package org.ibcn.limeds.json.impl;
