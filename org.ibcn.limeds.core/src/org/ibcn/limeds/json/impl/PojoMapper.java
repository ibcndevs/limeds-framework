/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.json.impl;

import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.ibcn.limeds.json.Json;
import org.ibcn.limeds.json.JsonObject;
import org.ibcn.limeds.json.JsonPrimitive;
import org.ibcn.limeds.json.JsonValue;
import org.ibcn.limeds.util.Errors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Utility class to convert between Java POJOs and LimeDS JsonValue instances.
 * 
 * @author wkerckho
 *
 */
public class PojoMapper {

	public static final PojoMapper INSTANCE = new PojoMapper();
	private static final Logger LOGGER = LoggerFactory.getLogger(PojoMapper.class);

	private PojoMapper() {
	}

	/**
	 * Converts a POJO instance to a JSON object using the field information.
	 * Fields marked with the transient modifier are ignored.
	 * 
	 * @param pojo
	 * @return
	 */
	public JsonObject toJSON(Object pojo) {
		JsonObject object = new JsonObject();
		List<Field> fields = new LinkedList<>();
		Class<?> currentClass = pojo.getClass();
		fields.addAll(Arrays.asList(currentClass.getDeclaredFields()));
		while (currentClass.getSuperclass() != null) {
			currentClass = currentClass.getSuperclass();
			fields.addAll(Arrays.asList(currentClass.getDeclaredFields()));
		}

		fields.stream().filter(f -> !f.isSynthetic()).forEach(f -> {
			f.setAccessible(true);
			try {
				Object value = f.get(pojo);

				// Ignore null, static and transient fields
				if (value != null && !Modifier.isTransient(f.getModifiers()) && !Modifier.isStatic(f.getModifiers())) {
					object.put(f.getName(), convert(value));
				}
			} catch (Exception e) {
				// Should not happen
				LOGGER.warn("Error while converting POJO to JSON for field " + f.getName() + " : " + e.toString());
			}
		});
		return object;
	}

	private JsonValue convert(Object value) {
		if (isJsonPrimitive(value)) {
			return new JsonPrimitive(value);
		} else if (value.getClass().isEnum() || value instanceof Enum) {
			return new JsonPrimitive(value.toString());
		} else if (value.getClass().isArray()) {
			return processArray(value).map(this::convert).collect(Json.toArray());
		} else if (value instanceof JsonValue) {
			return (JsonValue) value;
		} else if (value instanceof Map) {
			JsonObject childObject = new JsonObject();
			((Map<?, ?>) value)
					.forEach((k, v) -> childObject.put(k instanceof String ? (String) k : "" + k, convert(v)));
			return childObject;
		} else if (value instanceof Collection) {
			return ((Collection<?>) value).stream().map(this::convert).collect(Json.toArray());
		} else {
			// Value is a POJO
			return toJSON(value);
		}
	}

	private Stream<?> processArray(Object value) {
		if (value instanceof int[]) {
			return Arrays.stream((int[]) value).boxed();
		} else if (value instanceof double[]) {
			return Arrays.stream((double[]) value).boxed();
		} else if (value instanceof long[]) {
			return Arrays.stream((long[]) value).boxed();
		} else if (value instanceof boolean[]) {
			return Arrays.asList((boolean[]) value).stream();
		} else {
			return Arrays.stream((Object[]) value);
		}
	}

	private boolean isJsonPrimitive(Object value) {
		return value instanceof String || value instanceof Long || value instanceof Integer || value instanceof Double
				|| value instanceof Boolean || value.getClass().isPrimitive();
	}

	/**
	 * 
	 * @param clazz
	 * @param json
	 * @return
	 */
	public <T> T fromJSON(Class<T> clazz, JsonObject json) throws Exception {
		T object = clazz.newInstance();
		Arrays.stream(clazz.getDeclaredFields()).forEach(f -> {
			f.setAccessible(true);
			if (!Modifier.isTransient(f.getModifiers()) && !Modifier.isStatic(f.getModifiers())) {
				try {
					Object value = deconvert(f, f.getType(), json.get(f.getName()));
					if (value != null) {
						f.set(object, value);
					}
				} catch (Exception e) {
					// Should not happen
					LOGGER.warn("Error while converting JSON to POJO for field " + f.getName() + ": " + e.toString());
				}
			}
		});
		return object;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Object deconvert(Field f, Class<?> type, JsonValue value) throws Exception {
		if (value == null) {
			return null;
		}

		if (JsonValue.class.isAssignableFrom(type)) {
			return value;
		} else if (type.isEnum() || Enum.class.isAssignableFrom(type)) {
			return Enum.valueOf((Class<Enum>) type, value.asString());
		} else if (type.isPrimitive()
				|| Arrays.asList(String.class, Double.class, Integer.class, Long.class, Boolean.class, Float.class)
						.contains(type)) {
			if (type.equals(Boolean.TYPE) || type.equals(Boolean.class)) {
				return value.asBoolean();
			} else if (type.equals(Double.TYPE) || type.equals(Double.class)) {
				return value.asDouble();
			} else if (type.equals(Integer.TYPE) || type.equals(Integer.class)) {
				return value.asInt();
			} else if (type.equals(Long.TYPE) || type.equals(Long.class)) {
				return value.asLong();
			} else if (type.equals(Float.TYPE) || type.equals(Float.class)) {
				return value.asDouble();
			} else {
				return value.asString();
			}
		} else if (type.isArray()) {
			Object array = Array.newInstance(type.getComponentType(), value.asArray().size());
			for (int i = 0; i < value.asArray().size(); i++) {
				Array.set(array, i, deconvert(f, type.getComponentType(), value.asArray().get(i)));
			}
			return array;
		} else if (Map.class.isAssignableFrom(type)) {
			Map<Object, Object> tmp = new HashMap<>();
			value.asObject().forEach(Errors
					.wrapBiConsumer((k, v) -> tmp.put(processKey(f, k), deconvert(f, getParameterType(f, 1), v))));
			return tmp;
		} else if (Collection.class.isAssignableFrom(type)) {
			Collector<Object, ?, ?> collector;
			if (Set.class.isAssignableFrom(type)) {
				collector = Collectors.toSet();
			} else if (List.class.isAssignableFrom(type)) {
				collector = Collectors.toList();
			} else {
				throw new RuntimeException("JSON to POJO does not support collection types other then Set and List!");
			}
			return value.asArray().stream().map(Errors.wrapFunction(v -> deconvert(f, getParameterType(f, 0), v)))
					.collect(collector);
		} else {
			// We assume the type to return is another POJO
			try {
				return fromJSON((Class<Object>) type, value.asObject());
			} catch (Exception e) {
				throw new Exception("Cannot populate POJO field " + f.getName() + " with " + value);
			}
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private Object processKey(Field f, String k) {
		Class<?> type = getParameterType(f, 0);
		if (type.isEnum() || Enum.class.isAssignableFrom(type)) {
			return Enum.valueOf((Class<Enum>) type, k);
		} else {
			return k;
		}
	}

	private Class<?> getParameterType(Field f, int index) {
		ParameterizedType listType = (ParameterizedType) f.getGenericType();
		return (Class<?>) listType.getActualTypeArguments()[index];
	}

}
