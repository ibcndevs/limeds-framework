/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.json.impl;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.AbstractList;
import java.util.AbstractMap;
import java.util.AbstractSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.ibcn.limeds.exceptions.NoContentException;
import org.ibcn.limeds.json.JsonArray;
import org.ibcn.limeds.json.JsonObject;
import org.ibcn.limeds.json.JsonPrimitive;
import org.ibcn.limeds.json.JsonValue;
import org.json.simple.parser.ContainerFactory;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.json.simple.parser.Yytoken;

/**
 * This utility class implements json-simple ContainerFactory for reading LimeDS
 * JsonValues from streams/strings/etc...
 * 
 * @author wkerckho
 *
 */
public class JsonInputHelper implements ContainerFactory {

	public static final JsonInputHelper INSTANCE = new JsonInputHelper();

	@SuppressWarnings("rawtypes")
	@Override
	public List creatArrayContainer() {
		return new RawJsonList(new JsonArray());
	}

	@SuppressWarnings("rawtypes")
	@Override
	public Map createObjectContainer() {
		return new RawJsonMap(new JsonObject());
	}

	public JsonValue from(InputStream jsonStream) throws Exception {
		try {
			return fromRaw(new JSONParser().parse(new InputStreamReader(jsonStream, "UTF-8"), this));
		} catch (ParseException e) {
			if (e.getUnexpectedObject() instanceof Yytoken
					&& ((Yytoken) e.getUnexpectedObject()).type == Yytoken.TYPE_EOF && e.getPosition() == 0) {
				throw new NoContentException();
			} else {
				throw e;
			}
		}
	}

	private static Object toRaw(JsonValue json) {
		if (json instanceof JsonArray) {
			return new RawJsonList(json.asArray());
		} else if (json instanceof JsonObject) {
			return new RawJsonMap(json.asObject());
		} else {
			return json.asPrimitive().getValue();
		}
	}

	private static JsonValue fromRaw(Object value) {
		if (value instanceof RawType) {
			return ((RawType) value).getJson();
		} else {
			return new JsonPrimitive(value);
		}
	}

	private static interface RawType {

		JsonValue getJson();

	}

	private static class RawJsonList extends AbstractList<Object> implements RawType {

		private final JsonArray array;

		public RawJsonList(JsonArray array) {
			this.array = array;
		}

		@Override
		public Object get(int index) {
			if (index >= 0 && index < array.size()) {
				return toRaw(array.get(index));
			} else {
				throw new IndexOutOfBoundsException();
			}
		}

		@Override
		public Object set(int index, Object element) {
			JsonValue previous = array.get(index);
			array.set(index, fromRaw(element));
			return toRaw(previous);
		}

		@Override
		public void add(int index, Object element) {
			array.add(index, fromRaw(element));
		}

		@Override
		public Object remove(int index) {
			return toRaw(array.remove(index));
		}

		@Override
		public int size() {
			return array.size();
		}

		@Override
		public int hashCode() {
			return array.hashCode();
		}

		@Override
		public boolean equals(Object other) {
			return array.equals(other);
		}

		@Override
		public JsonValue getJson() {
			return array;
		}

	}

	private static class RawJsonMap extends AbstractMap<String, Object> implements RawType {

		private final JsonObject object;

		public RawJsonMap(JsonObject object) {
			this.object = object;
		}

		@Override
		public Set<Map.Entry<String, Object>> entrySet() {
			return new AbstractSet<Map.Entry<String, Object>>() {

				@Override
				public Iterator<Map.Entry<String, Object>> iterator() {
					return new Iterator<java.util.Map.Entry<String, Object>>() {

						private Iterator<String> keyIter = object.keySet().iterator();
						private Map.Entry<String, Object> removable = null;

						@Override
						public boolean hasNext() {
							return keyIter.hasNext();
						}

						@Override
						public void remove() {
							if (removable != null) {
								object.remove(removable.getKey());
								removable = null;
							}
						}

						@Override
						public Map.Entry<String, Object> next() {
							final String key = keyIter.next();
							removable = new Map.Entry<String, Object>() {

								@Override
								public String getKey() {
									return key;
								}

								@Override
								public Object getValue() {
									return toRaw(object.get(key));
								}

								@Override
								public JsonValue setValue(Object value) {
									throw new UnsupportedOperationException("Cannot set JSON property using iterator!");
								}

							};
							return removable;
						}

					};
				}

				@Override
				public int size() {
					return object.size();
				}

			};
		}

		@Override
		public Object put(String key, Object value) {
			JsonValue previous = object.put(key, (JsonValue) fromRaw(value));
			if (previous != null) {
				return toRaw(previous);
			} else {
				return null;
			}
		}

		@Override
		public int hashCode() {
			return object.hashCode();
		}

		@Override
		public boolean equals(Object other) {
			return object.equals(other);
		}

		public JsonValue getJson() {
			return object;
		}

	}

}
