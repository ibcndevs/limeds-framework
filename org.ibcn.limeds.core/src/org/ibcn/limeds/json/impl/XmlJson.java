/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.json.impl;

import java.io.InputStream;
import java.io.StringWriter;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.ibcn.limeds.json.Json;
import org.ibcn.limeds.json.JsonArray;
import org.ibcn.limeds.json.JsonObject;
import org.ibcn.limeds.json.JsonValue;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

/**
 * Experimental utility class that converts between XML and JSON.
 * 
 * @author wkerckho
 *
 */
public class XmlJson {

	public static JsonObject parse(InputStream xml) throws Exception {
		DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		Document document = builder.parse(xml);
		return Json.objectBuilder()
				.add(document.getDocumentElement().getNodeName(), parse(document.getDocumentElement())).build();
	}

	public static String xmlFrom(JsonObject json) throws Exception {
		DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		Document document = builder.newDocument();

		json.forEach((key, val) -> {
			Element el = document.createElement(key);
			add(key, val, el);
		});

		TransformerFactory tf = TransformerFactory.newInstance();
		Transformer transformer = tf.newTransformer();
		transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
		StringWriter writer = new StringWriter();
		transformer.transform(new DOMSource(document), new StreamResult(writer));
		return writer.getBuffer().toString().replaceAll("\n|\r", "");
	}

	private static JsonValue parse(Element element) {
		JsonObject content = new JsonObject();

		// Parse attributes
		for (int i = 0; i < element.getAttributes().getLength(); i++) {
			Attr attribute = (Attr) element.getAttributes().item(i);
			content.put("@" + attribute.getName(), attribute.getValue());
		}

		// Parse children
		NodeList children = element.getChildNodes();
		for (int i = 0; i < children.getLength(); i++) {
			Node child = children.item(i);
			if (child instanceof Element) {
				if (!content.has(child.getNodeName())) {
					content.put(child.getNodeName(), new JsonArray());
				}
				content.get(child.getNodeName()).asArray().add(parse((Element) child));
			} else if (child instanceof Text && child.getNodeValue() != null
					&& !child.getNodeValue().trim().isEmpty()) {
				content.put("#text", child.getTextContent());
			}
		}
		return content;
	}

	private static void add(String elementName, JsonValue elementValue, Element parent) {
		if ("#text".equals(elementName)) {
			Text text = parent.getOwnerDocument().createTextNode(elementValue.asPrimitive().asString());
			parent.appendChild(text);
		} else {
			if (elementValue instanceof JsonObject) {
				Element xmlElement = parent.getOwnerDocument().createElement(elementName);
				if (parent.getOwnerDocument().getDocumentElement() == null) {
					parent.getOwnerDocument().appendChild(xmlElement);
				} else {
					parent.appendChild(xmlElement);
				}
				elementValue.asObject().forEach((key, val) -> {
					if (key.startsWith("@")) {
						// Set as attribute
						xmlElement.setAttribute(key.substring(1), val.asString());
					} else {
						// Set as child
						add(key, val, xmlElement);
					}
				});
			} else if (elementValue instanceof JsonArray) {
				elementValue.asArray().forEach(val -> {
					add(elementName, val, parent);
				});
			} else {
				Element xmlElement = parent.getOwnerDocument().createElement(elementName);
				xmlElement.setTextContent(elementValue.asString());
				parent.appendChild(xmlElement);
			}
		}
	}

}
