/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.json;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Arrays;
import java.util.function.Function;
import java.util.stream.Collector;

import org.ibcn.limeds.json.impl.JsonInputHelper;
import org.ibcn.limeds.json.impl.JsonParser;
import org.ibcn.limeds.json.impl.PojoMapper;
import org.ibcn.limeds.json.impl.XmlJson;
import org.ibcn.limeds.util.Errors;

/**
 * Utility class that provides operations for (de)serializing JSON values and
 * building JsonValue instances.
 * 
 * @author wkerckho
 *
 */
public class Json {

	/**
	 * Create a JsonObjectBuilder instance for building a JsonObject.
	 * 
	 * @return
	 */
	public static JsonObjectBuilder objectBuilder() {
		return new JsonObjectBuilder();
	}

	/**
	 * Creates a JsonValue (or subtype) object from a String.
	 * 
	 * @param json
	 *            A String representing the JSON type.
	 * @return an instance of JsonValue
	 * @throws Exception
	 *             An exception is thrown when the supplied String does not
	 *             represent a valid JSON type.
	 */
	@SuppressWarnings("unchecked")
	public static <T extends JsonValue> T from(String json) throws Exception {
		return (T) JsonParser.INSTANCE.from(json);
	}

	/**
	 * Creates a JsonObject from an XML string. This object contains one
	 * key-value mapping which represents the root element of the XML document.
	 * In order to allow generic conversion of XML to JSON, some assumptions are
	 * made:
	 * <p>
	 * Without meta-data, XML offers no distinction between array and object
	 * types, so each element below the root is represented as an array-type
	 * containing zero or one JsonObject in case the element is an object type
	 * or zero or multiple JsonObjects in case the element is an array type.
	 * <p>
	 * Attributes are represented as key-value pairs in the JsonObject with
	 * an @-prefix.
	 * <p>
	 * When the xml element is a value-node e.g. ..&lt;name&gt;Jane Doe&lt;/name&gt;.., the
	 * value of this element can be retrieved as ...name[0].#value.
	 * <p>
	 * Convert some XML documents yourselves for more info on this function.
	 * 
	 * @param xml
	 * @return
	 * @throws Exception
	 */
	public static JsonObject fromXML(String xml) throws Exception {
		return XmlJson.parse(new ByteArrayInputStream(xml.getBytes("UTF-8")));
	}

	/**
	 * Creates a JsonObject from an XML inputstream. This object contains one
	 * key-value mapping which represents the root element of the XML document.
	 * In order to allow generic conversion of XML to JSON, some assumptions are
	 * made:
	 * <p>
	 * Without meta-data, XML offers no distinction between array and object
	 * types, so each element below the root is represented as an array-type
	 * containing zero or one JsonObject in case the element is an object type
	 * or zero or multiple JsonObjects in case the element is an array type.
	 * <p>
	 * Attributes are represented as key-value pairs in the JsonObject with
	 * an @-prefix.
	 * <p>
	 * When the xml element contains text e.g. ..&lt;name&gt;Jane Doe&lt;/name&gt;.., the
	 * text of this element can be retrieved as ...name[0].#text.
	 * <p>
	 * Convert some XML documents yourselves for more info on this function.
	 * 
	 * @param xml
	 * @return
	 * @throws Exception
	 */
	public static JsonObject fromXML(InputStream xml) throws Exception {
		return XmlJson.parse(xml);
	}

	/**
	 * Converts a JsonObject that was created using fromXML back to a XML
	 * document. Note that you can use this to convert your own JsonObjects to
	 * XML documents, but there is a learning curve as we follow a very specific
	 * structuring in order for the approach to remain generic.
	 * 
	 * @param json
	 * @return
	 * @throws Exception
	 */
	public static String toXML(JsonObject json) throws Exception {
		return XmlJson.xmlFrom(json);
	}

	/**
	 * Creates a JsonValue (or subtype) object from an input stream.
	 * 
	 * @param jsonStream
	 *            A stream respresenting a JSON type.
	 * @return an instance of JsonValue
	 * @throws Exception
	 *             An exception is thrown when the supplied stream does not
	 *             represent a valid JSON type.
	 */
	@SuppressWarnings("unchecked")
	public static <T extends JsonValue> T from(InputStream jsonStream) throws Exception {
		return (T) JsonInputHelper.INSTANCE.from(jsonStream);
	}

	/**
	 * Creates an JsonObject instance from the specified POJO
	 * 
	 * @param pojo
	 *            A POJO instance
	 * @return an instance of JsonObject
	 */
	public static JsonObject from(Object pojo) {
		return PojoMapper.INSTANCE.toJSON(pojo);
	}

	/**
	 * Converts a JsonObject representing a JSON object to an instance of the
	 * specified class.
	 * 
	 * @param type
	 *            The class to which the JSON should be deserialized
	 * @param json
	 *            A JsonObject instance
	 * @return An instance of the specified type, derived from the specified
	 *         JSON object
	 */
	public static <T> T to(Class<T> type, JsonObject json) throws Exception {
		return PojoMapper.INSTANCE.fromJSON(type, json);
	}

	/**
	 * Creates a JSON-array of JsonValue instances represented by an JsonArray.
	 * 
	 * @param elements
	 *            Zero or more instances of JsonNode
	 * @return an JsonArray instance
	 */
	public static JsonArray arrayOf(JsonValue... elements) {
		return Arrays.stream(elements).collect(Json.toArray());
	}

	/**
	 * Creates a JSON-array of integers represented by an JsonArray.
	 * 
	 * @param elements
	 *            Zero or more integers
	 * @return an JsonArray instance
	 */
	public static JsonArray arrayOf(Integer... elements) {
		return Arrays.stream(elements).map(JsonPrimitive::new).collect(Json.toArray());
	}

	/**
	 * Creates a JSON-array of longs represented by an JsonArray.
	 * 
	 * @param elements
	 *            Zero or more longs
	 * @return an JsonArray instance
	 */
	public static JsonArray arrayOf(Long... elements) {
		return Arrays.stream(elements).map(JsonPrimitive::new).collect(Json.toArray());
	}

	/**
	 * Creates a JSON-array of doubles represented by an JsonArray.
	 * 
	 * @param elements
	 *            Zero or more doubles
	 * @return an JsonArray instance
	 */
	public static JsonArray arrayOf(Double... elements) {
		return Arrays.stream(elements).map(JsonPrimitive::new).collect(Json.toArray());
	}

	/**
	 * Creates a JSON-array of booleans represented by an JsonArray.
	 * 
	 * @param elements
	 *            Zero or more booleans
	 * @return an JsonArray instance
	 */
	public static JsonArray arrayOf(Boolean... elements) {
		return Arrays.stream(elements).map(JsonPrimitive::new).collect(Json.toArray());
	}

	/**
	 * Creates a JSON-array of Strings represented by an JsonArray.
	 * 
	 * @param elements
	 *            Zero or more Strings
	 * @return an JsonArray instance
	 */
	public static JsonArray arrayOf(String... elements) {
		return Arrays.stream(elements).map(JsonPrimitive::new).collect(Json.toArray());
	}

	/**
	 * A collector utility method to be used in conjunction with Java 8 stream
	 * expression.
	 * 
	 * E.g. collect a List of String as an ArrayNode:
	 * 
	 * <pre>
	 * {
	 * 	&#064;code
	 * 	List&lt;String&gt; response = retrieveSomeList();
	 * 	ArrayNode array = response.stream().map(JsonNodeFactory.instance::textNode).collect(Json.toArray());
	 * }
	 * </pre>
	 * 
	 * @return An ArrayNode instance
	 */
	public static Collector<JsonValue, ?, JsonArray> toArray() {
		return Collector.of(JsonArray::new, JsonArray::add, (left, right) -> {
			left.addAll(right);
			return left;
		});
	}

	/**
	 * Get a Deserializer function to convert an input stream to a JsonValue.
	 * E.g. used in combination with the IBCN RestClient when converting an HTTP
	 * response body to a JsonValue.
	 */
	public static <T extends JsonValue> Function<InputStream, T> getDeserializer() {
		return Errors.wrapFunction(stream -> Json.from(stream));
	}

}
