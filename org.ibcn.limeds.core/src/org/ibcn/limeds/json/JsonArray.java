/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.json;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Subclass of JsonValue for representing JSON arrays. It is based on a Java
 * List implementation for maximum ease-of-use.
 * 
 * @author wkerckho
 *
 */
@SuppressWarnings("serial")
public class JsonArray extends LinkedList<JsonValue> implements JsonValue {

	/**
	 * By overriding this method we can convert a JsonValue that is actually an
	 * array to JsonArray without using a cast.
	 */
	@Override
	public JsonArray asArray() {
		return this;
	}

	/**
	 * Add a boolean value to the JSON array.
	 */
	public void add(boolean value) {
		add(new JsonPrimitive(value));
	}

	/**
	 * Add an integer value to the JSON array.
	 */
	public void add(int value) {
		add(new JsonPrimitive(value));
	}

	/**
	 * Add a long value to the JSON array.
	 */
	public void add(long value) {
		add(new JsonPrimitive(value));
	}

	/**
	 * Add a double value to the JSON array.
	 */
	public void add(double value) {
		add(new JsonPrimitive(value));
	}

	/**
	 * Add a String object to the JSON array.
	 */
	public void add(String value) {
		add(new JsonPrimitive(value));
	}

	/**
	 * This JsonValue method is not supported by JsonArray!
	 */
	@Override
	public JsonValue get(String key) {
		throw new UnsupportedOperationException("Cannot index a JsonArray by key!");
	}

	/**
	 * This JsonValue method is not supported by JsonArray!
	 */
	@Override
	public boolean has(String key) {
		throw new UnsupportedOperationException("Cannot index a JsonArray by key!");
	}

	private String generateSpacing(int indent) {
		return Stream.generate(String::new).limit(indent).collect(Collectors.joining(" "));
	}

	/**
	 * Prints the JsonArray as a formatted string with the specified
	 * indentation.
	 */
	@Override
	public String toPrettyString(int baseIndent, int indentIncrement) {
		String result = "[\n";
		result += stream()
				.map(val -> generateSpacing(baseIndent + indentIncrement)
						+ val.toPrettyString(baseIndent + indentIncrement, indentIncrement))
				.collect(Collectors.joining(",\n", "", "\n"));
		return result + generateSpacing(baseIndent) + "]";
	}

	/**
	 * Writes the JsonArray to the specified output stream using the specified
	 * character set.
	 */
	@Override
	public void write(OutputStream out, String charSet) throws IOException {
		out.write("[".getBytes(charSet));
		byte[] comma = ",".getBytes(charSet);
		if (size() > 0) {
			for (int i = 0; i < size() - 1; i++) {
				get(i).write(out, charSet);
				out.write(comma);
			}
			get(size() - 1).write(out, charSet);
		}
		out.write("]".getBytes(charSet));
	}

	@Override
	public void write(PrintWriter out) {
		out.print("[");
		if (size() > 0) {
			JsonValue tmp = null;
			for (int i = 0; i < size() - 1; i++) {
				tmp = get(i);
				if (tmp instanceof JsonPrimitive) {
					out.print(tmp.toString() + ",");
				} else {
					tmp.write(out);
					out.print(",");
				}
			}
			tmp = get(size() - 1);

			if (tmp instanceof JsonPrimitive) {
				out.print(tmp.toString());
			} else {
				tmp.write(out);
			}
		}
		out.print("]");
	}

}
