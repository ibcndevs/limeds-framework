/**
 * This package specifies and implements the LimeDS JSON model.
 */
@org.osgi.annotation.versioning.Version("1.0.1")
package org.ibcn.limeds.json;
