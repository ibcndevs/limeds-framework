/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.json.model;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;

import org.ibcn.limeds.TypeAdmin;
import org.ibcn.limeds.json.Json;
import org.ibcn.limeds.json.JsonArray;
import org.ibcn.limeds.json.JsonObject;
import org.ibcn.limeds.json.JsonPrimitive;
import org.ibcn.limeds.json.JsonValue;
import org.ibcn.limeds.util.Errors;
import org.ibcn.limeds.util.Identifier;

/**
 * Utility class to validate JSON documents based on the limeDS schema format
 * (as explained here:
 * https://bitbucket.org/ibcndevs/limeds-framework/wiki/Guide%20to%20JSON%
 * 20Validation).
 * 
 * @author wkerckho
 *
 */
public class JsonValidator {

	private static final Set<String> TYPES = Arrays
			.stream(new String[] { "String", "Boolean", "IntNumber", "FloatNumber", "Object" })
			.collect(Collectors.toSet());

	public static class FieldDescriptor {

		public String fieldName;
		public String type = null;
		public boolean optional = false;
		public String description;
		public Set<Object> values = new HashSet<>();
		public String valuesString;

		public FieldDescriptor(String fieldName, String descriptor) throws Exception {
			this.fieldName = fieldName;
			for (String part : descriptor.split(" (?=[\\[\\(\\*])")) {
				part = part.trim();
				if ("*".equals(part)) {
					optional = true;
				} else if (part.startsWith("[") && part.endsWith("]")) {
					if (type == null) {
						throw new Exception("The field type must be defined first in the descriptor of " + fieldName);
					}
					valuesString = part;
					Arrays.stream(part.substring(1, part.length() - 1).split(",")).map(s -> s.trim())
							.forEach(s -> values.add(convertValue(type, s)));
				} else if (part.startsWith("(") && part.endsWith(")")) {
					description = part.substring(1, part.length() - 1);
				} else if (TYPES.contains(part)) {
					type = part;
				}
			}

			if (type == null) {
				throw new Exception("The field descriptor for " + fieldName + " doesn't specify a valid type " + TYPES);
			}
		}

	}

	private TypeAdmin typeAdmin;

	public JsonValidator(TypeAdmin typeAdmin) {
		this.typeAdmin = typeAdmin;
	}

	private static Object convertValue(String type, String value) {
		switch (type) {
		case "Boolean":
			return Boolean.parseBoolean(value);
		case "IntNumber":
			try {
				return Integer.parseInt(value);
			} catch (NumberFormatException e) {
				return Long.parseLong(value);
			}
		case "FloatNumber":
			return Float.parseFloat(value);
		default:
			return value;
		}
	}

	private static boolean isType(JsonValue node, String type) {
		switch (type) {
		case "Boolean":
			return node instanceof JsonPrimitive && node.asPrimitive().getValue() instanceof Boolean;
		case "IntNumber":
			return node instanceof JsonPrimitive && (node.asPrimitive().getValue() instanceof Integer
					|| node.asPrimitive().getValue() instanceof Long);
		case "FloatNumber":
			return node instanceof JsonPrimitive && (node.asPrimitive().getValue() instanceof Double
					|| node.asPrimitive().getValue() instanceof Float);
		case "String":
			return node instanceof JsonPrimitive && node.asPrimitive().getValue() instanceof String;
		case "Object":
			return node instanceof JsonObject;
		}
		return false;
	}

	/**
	 * Validates a JSON document according to the specified schema.
	 * 
	 * @param schema
	 *            A limeDS JSON schema.
	 * @param document
	 *            A JSON document.
	 */
	public void validate(JsonValue schema, JsonValue document) throws Exception {
		validate(typeAdmin.resolve(schema), document, null, "/");
	}

	// TODO needs a rework: a lot changed since the conception of this class
	private static void validate(JsonValue schema, JsonValue document, JsonValue parent, String path) throws Exception {
		boolean arraySchema = false;
		if (schema instanceof JsonArray) {
			// Convert to an object with a single attribute that contains the
			// array
			schema = Json.objectBuilder().add("array", schema).build();
			document = Json.objectBuilder().add("array", document).build();
			arraySchema = true;
		} else if (schema instanceof JsonPrimitive) {
			// Convert to an object with a single attribute containing the
			// primitive
			schema = Json.objectBuilder().add("primitive", schema).build();
			document = Json.objectBuilder().add("primitive", document).build();
		}

		if (schema.has(JsonModel.MAPPING)) {
			if (document instanceof JsonObject) {
				for (String fieldName : document.asObject().keySet()) {
					JsonValue mappingSchema = schema.get(JsonModel.MAPPING);
					if (mappingSchema instanceof JsonPrimitive) {
						FieldDescriptor fieldDesc = new FieldDescriptor(fieldName, mappingSchema.asString());
						checkPrimitiveField(fieldDesc, document, path + fieldName + "/");
					} else {
						validate(mappingSchema, document.get(fieldName), document, path + fieldName + "/");
					}
				}
			} else {
				throw new Exception("Expected " + path + " to be a JSON object!");
			}
		} else {

			for (String fieldName : schema.asObject().keySet()) {
				if (JsonModel.TYPE_INFO.equals(fieldName)) {
					continue;
				}

				JsonValue value = schema.get(fieldName);

				if (value instanceof JsonObject) {
					FieldDescriptor fieldDesc = null;
					if (schema.get(fieldName).has(JsonModel.TYPE_INFO)) {
						fieldDesc = new FieldDescriptor(fieldName,
								schema.get(fieldName).get(JsonModel.TYPE_INFO).asString());
					}
					if (!document.has(fieldName) && (fieldDesc == null || !fieldDesc.optional)) {
						throw new Exception(
								"The required object field '" + fieldName + "' is not present (at " + path + ")");
					} else if (document.has(fieldName)) {
						if (value.has(JsonModel.MAPPING)) {
							JsonValue mappingSchema = value.get(JsonModel.MAPPING);
							JsonObject mapping = document.get(fieldName).asObject();
							for (Entry<String, JsonValue> e : mapping.entrySet()) {
								if (mappingSchema instanceof JsonPrimitive) {
									FieldDescriptor mappingDesc = new FieldDescriptor(e.getKey(),
											mappingSchema.asString());
									checkPrimitiveField(mappingDesc, mapping,
											path + fieldName + "/" + e.getKey() + "/");
								} else {
									validate(mappingSchema, e.getValue(), document,
											path + fieldName + "/" + e.getKey() + "/");
								}
							}
						} else {
							validate(value, document.get(fieldName), document, path + fieldName + "/");
						}
					}
				} else if (value instanceof JsonArray) {
					JsonValue arrayType = schema.get(fieldName).get(0);
					if (arrayType == null) {
						throw new Exception("The array field '" + fieldName + "' does not have a valid descriptor (at "
								+ path + ")");
					} else if (arrayType instanceof JsonArray) {
						throw new Exception(
								"'" + fieldName + "' : nested arrays are currently not supported (at " + path + ")");
					} else if (arrayType instanceof JsonObject) {
						FieldDescriptor fieldDesc = null;
						if (schema.get(fieldName).get(0).has(JsonModel.TYPE_INFO)) {
							fieldDesc = new FieldDescriptor(fieldName,
									schema.get(fieldName).get(0).get(JsonModel.TYPE_INFO).asString());
						}
						JsonValue array = getCheckedArray(fieldName, document,
								fieldDesc != null ? fieldDesc.optional : false, path, arraySchema);
						for (int index = 0; index < array.asArray().size(); index++) {
							validate(arrayType, array.asArray().get(index), document,
									path + fieldName + "/" + index + "/");
						}
					} else if (arrayType instanceof JsonPrimitive
							&& arrayType.asPrimitive().getValue() instanceof String) {
						FieldDescriptor fieldDesc = new FieldDescriptor(fieldName,
								schema.get(fieldName).get(0).asString());
						JsonValue array = getCheckedArray(fieldName, document, fieldDesc.optional, path, arraySchema);
						for (JsonValue item : array.asArray()) {
							if (!isType(item, fieldDesc.type)) {
								throw new Exception("Each item in the array field '" + fieldName + "' must be of type "
										+ fieldDesc.type + " (at " + path + ")");
							}
						}
					}
				} else if (value instanceof JsonPrimitive && value.asPrimitive().getValue() instanceof String) {
					FieldDescriptor fieldDesc = new FieldDescriptor(fieldName, schema.get(fieldName).asString());
					checkPrimitiveField(fieldDesc, document, path);
				}
			}
		}
	}

	private static JsonValue getCheckedArray(String fieldName, JsonValue document, boolean optional, String path,
			boolean schemaIsArray) throws Exception {
		JsonValue array = document.get(fieldName);
		if (array == null) {
			if (optional) {
				// Return empty array so it is ignored by the validator
				return new JsonArray();
			} else {
				throw new Exception("The required array field '" + fieldName + "' is not present (at " + path + ")");
			}
		} else if (array instanceof JsonArray) {
			return array;
		} else {
			if (schemaIsArray) {
				throw new Exception("The provided JSON (at " + path + ") must be an array!");
			} else {
				throw new Exception("The field '" + fieldName + "' should be an array (at " + path + ")");
			}
		}
	}

	private static void checkPrimitiveField(FieldDescriptor fieldDesc, JsonValue document, String path)
			throws Exception {
		JsonValue value = document.get(fieldDesc.fieldName);
		if (value == null && !fieldDesc.optional) {
			throw new Exception("The required field '" + fieldDesc.fieldName + "' of type " + fieldDesc.type
					+ " is not present (at " + path + ")");
		} else {
			if (value != null && !isType(value, fieldDesc.type)) {
				throw new Exception("The field '" + fieldDesc.fieldName + "' must be of type " + fieldDesc.type
						+ " (at " + path + ")");
			}
			if (value != null && !fieldDesc.values.isEmpty()
					&& !fieldDesc.values.contains(convertValue(fieldDesc.type, value.asString()))) {
				throw new Exception("The field '" + fieldDesc.fieldName + "' must have an element of "
						+ fieldDesc.valuesString + " as value, (at " + path + ")");
			}
		}
	}

	public void validateSchema(String field, JsonValue schema) throws Exception {
		if (schema instanceof JsonObject) {
			if (schema.has("@typeInfo")) {
				JsonValue typeInfo = schema.get("@typeInfo");
				if (!(typeInfo instanceof JsonPrimitive)
						|| !(((JsonPrimitive) typeInfo).getValue() instanceof String)) {
					throw new Exception("Invalid value for " + field
							+ ".@typeInfo, expected a String containing a field descriptor");
				}

				FieldDescriptor desc = null;
				try {
					desc = new FieldDescriptor(field, typeInfo.asString());
				} catch (Exception e) {
					throw new Exception("The typeInfo attribute for " + field + " does not contain a valid descriptor");
				}

				if (!desc.type.equals("Object")) {
					throw new Exception("The type of the typeInfo descriptor for " + field + " must be Object");
				}
				schema.asObject().remove("@typeInfo");
			}
			if (schema.has("@typeRef")) {
				if (schema.asObject().size() != 1) {
					throw new Exception(field + " is a typeRef object and cannot contain the attributes "
							+ schema.asObject().keySet().stream()
									.filter(k -> !k.equals("@typeRef") && !k.equals("@typeInfo"))
									.collect(Collectors.toList()));
				}

				try {
					Identifier.parse(schema.getString("@typeRef"));
				} catch (Exception e) {
					throw new Exception("The typeRef attribute is not correctly formatted for " + field
							+ ".@typeRef, expected: [FQ typeName]_[version}");
				}
			} else if (schema.has("@mapping")) {
				if (schema.asObject().size() != 1) {
					throw new Exception(field + " is a mapping definition and cannot contain the attributes "
							+ schema.asObject().keySet().stream()
									.filter(k -> !k.equals("@mapping") && !k.equals("@typeInfo"))
									.collect(Collectors.toList()));
				}

				validateSchema(field + ".@mapping", schema.get("@mapping"));
			} else {
				if (schema.has("@typeParent")) {
					try {
						Identifier.parse(schema.getString("@typeParent"));
					} catch (Exception e) {
						throw new Exception("The typeParent attribute is not correctly formatted for " + field
								+ ".@typeParent, expected: [FQ typeName]_[version}");
					}
					schema.asObject().remove("@typeParent");
				}
				schema.asObject().forEach(Errors.wrapBiConsumer((k, v) -> validateSchema(field + "." + k, v)));
			}
		} else if (schema instanceof JsonArray) {
			if (schema.asArray().size() != 1) {
				throw new Exception("The array type " + field + " does not specify a descriptor");
			}
			if (!(schema.get(0) instanceof JsonObject) && !(schema.get(0) instanceof JsonPrimitive)) {
				throw new Exception(
						"Only a descriptor String or object are valid type descriptor for the array type " + field);
			}
			schema.asArray().forEach(Errors.wrapConsumer(e -> validateSchema(field + "[]", e)

			));
		} else if (schema instanceof JsonPrimitive) {
			if (!(((JsonPrimitive) schema).getValue() instanceof String)) {
				throw new Exception(
						"Invalid attribute value for " + field + ", expected a String containing a field descriptor");
			}
			FieldDescriptor desc = new FieldDescriptor(field, schema.asString());
			if (desc.type.equals("Object")) {
				throw new Exception("The type of the non-collection field descriptor " + field + " cannot be Object");
			}
		}
	}

}
