/**
 * This package specifies and implements a JSON schema for LimeDS.
 */
@org.osgi.annotation.versioning.Version("1.1.0")
package org.ibcn.limeds.json.model;
