/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.json.model;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.ibcn.limeds.annotations.JsonAttribute;
import org.ibcn.limeds.annotations.JsonType;
import org.ibcn.limeds.descriptors.SegmentDescriptor;
import org.ibcn.limeds.json.JsonArray;
import org.ibcn.limeds.json.JsonObject;
import org.ibcn.limeds.util.Errors;

public class ClassToType {

	private static final Set<Class<?>> PRIMITIVE_TYPES = Arrays.stream(new Class<?>[] { String.class, Double.class,
			Long.class, Integer.class, Float.class, Short.class, Boolean.class }).collect(Collectors.toSet());
	private static final Set<Class<?>> COLLECTION_TYPES = Arrays
			.stream(new Class<?>[] { Collection.class, List.class, Set.class }).collect(Collectors.toSet());

	private Class<?> clazz;
	private JsonObject result = new JsonObject();

	public ClassToType(Class<?> clazz) {
		this.clazz = clazz;
	}

	public boolean isPublicType() {
		return clazz.isAnnotationPresent(JsonType.class);
	}

	public String getTypeName() {
		return clazz.getName();
	}

	public JsonObject parseType() throws Exception {
		Arrays.stream(clazz.getDeclaredFields()).filter(f -> !Modifier.isStatic(f.getModifiers()) && !f.isSynthetic()
				&& !Modifier.isTransient(f.getModifiers())).forEach(Errors.wrapConsumer(this::processField));
		return result;
	}

	private void processField(Field field) throws Exception {
		// Check for optional annotation
		JsonAttribute attrInfo = field.getAnnotation(JsonAttribute.class);

		if (field.getType().isPrimitive() || PRIMITIVE_TYPES.contains(field.getType())) {
			String typeDef = getPrimitiveTypeDef(field.getType(), attrInfo);
			if (typeDef != null) {
				result.put(field.getName(), typeDef);
			}
		} else if (field.getType().isEnum()) {
			result.put(field.getName(), "String " + Arrays.stream(field.getType().getEnumConstants())
					.map(e -> e.toString()).collect(Collectors.joining(",", "[", "]")) + getPrimitiveExtra(attrInfo));
		} else if (field.getType().isArray() || COLLECTION_TYPES.contains(field.getType())) {
			Class<?> entryType = field.getType().isArray() ? field.getType().getComponentType()
					: getCollectionComponentTypes(field)[0];
			if (entryType != null) {
				JsonArray arrayRepr = new JsonArray();
				if (entryType.isPrimitive() || PRIMITIVE_TYPES.contains(entryType)) {
					arrayRepr.add(getPrimitiveTypeDef(entryType, attrInfo));
				} else {
					arrayRepr.add(getObjectTypeDef(entryType, attrInfo));
				}
				result.put(field.getName(), arrayRepr);
			}
		} else if (Map.class.equals(field.getType())) {
			Class<?>[] types = getCollectionComponentTypes(field);
			if (types.length == 2 && String.class.equals(types[0])) {
				JsonObject mappingEntry = new JsonObject();
				if (attrInfo != null) {
					mappingEntry.put(JsonModel.TYPE_INFO, "Object" + getPrimitiveExtra(attrInfo));
				}
				if (types[1].isPrimitive() || PRIMITIVE_TYPES.contains(types[1])) {
					mappingEntry.put(JsonModel.MAPPING, getPrimitiveTypeDef(types[1], null));
				} else {
					mappingEntry.put(JsonModel.MAPPING, getObjectTypeDef(types[1], null));
				}
				result.put(field.getName(), mappingEntry);
			} else {
				throw new Exception("JSON type only supports Map<String, ?>!");
			}
		} else {
			result.put(field.getName(), getObjectTypeDef(field.getType(), attrInfo));
		}
	}

	private JsonObject getObjectTypeDef(Class<?> type, JsonAttribute attr) throws Exception {
		JsonObject typeDef = new JsonObject();
		if (attr != null) {
			typeDef.put(JsonModel.TYPE_INFO, "Object" + getPrimitiveExtra(attr));
		}
		typeDef.putAll(new ClassToType(type).parseType());
		return typeDef;
	}

	private String getPrimitiveTypeDef(Class<?> type, JsonAttribute attr) {
		String baseDef = mapType(type);
		if (baseDef != null) {
			return baseDef + getPrimitiveExtra(attr);
		} else {
			return null;
		}
	}

	private String getPrimitiveExtra(JsonAttribute attr) {
		if (attr != null) {
			return " " + (attr.optional() ? "*" : "") + (!attr.desc().isEmpty() ? " (" + attr.desc() + ")" : "");
		} else {
			return "";
		}
	}

	private Class<?>[] getCollectionComponentTypes(Field field) {
		Type genericFieldType = field.getGenericType();

		if (genericFieldType instanceof ParameterizedType) {
			ParameterizedType aType = (ParameterizedType) genericFieldType;
			Type[] fieldArgTypes = aType.getActualTypeArguments();
			return Arrays.stream(fieldArgTypes).map(t -> (Class<?>) t).toArray(size -> new Class<?>[size]);

		}
		return new Class<?>[] { null, null };
	}

	private String mapType(Class<?> type) {
		if (Boolean.class.equals(type) || boolean.class.equals(type)) {
			return "Boolean";
		} else if (Long.class.equals(type) || long.class.equals(type) || Integer.class.equals(type)
				|| int.class.equals(type)) {
			return "IntNumber";
		} else if (Float.class.equals(type) || float.class.equals(type) || Double.class.equals(type)
				|| double.class.equals(type)) {
			return "FloatNumber";
		} else if (String.class.equals(type)) {
			return "String";
		}
		return null;
	}

	public static void main(String[] args) throws Exception {
		System.out.println(new ClassToType(SegmentDescriptor.class).parseType().toPrettyString());
	}

}
