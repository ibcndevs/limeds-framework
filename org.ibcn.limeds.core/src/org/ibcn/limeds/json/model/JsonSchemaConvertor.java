/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.json.model;

import java.util.Map.Entry;

import org.ibcn.limeds.descriptors.DependencyDescriptor;
import org.ibcn.limeds.json.JsonArray;
import org.ibcn.limeds.json.JsonObject;
import org.ibcn.limeds.json.JsonValue;
import org.ibcn.limeds.json.model.JsonValidator.FieldDescriptor;

/**
 * Utility class that converts IBCN/limeDS style JSON schema's to an equivalent
 * instance that adheres to the official JSON-schema spec:
 * http://json-schema.org/
 * 
 * Handy tool for validation (of both schema and json):
 * http://jsonschemalint.com/draft4/
 * 
 * @author wkerckho
 *
 */
public class JsonSchemaConvertor {

	public static JsonValue convert(JsonValue schema) throws Exception {
		return convertImpl(schema);
	}

	private static JsonValue convertImpl(JsonValue schema) throws Exception {
		if (schema instanceof JsonObject) {
			JsonObject schemaNode = new JsonObject();
			schemaNode.put("type", "object");
			if (schema.has(JsonModel.MAPPING)) {
				if (schema.get(JsonModel.MAPPING) instanceof JsonObject
						&& schema.get(JsonModel.MAPPING).has(JsonModel.TYPE_INFO)) {
					FieldDescriptor objectDesc = new FieldDescriptor("",
							schema.get(JsonModel.MAPPING).get(JsonModel.TYPE_INFO).asString());
					if (objectDesc.description != null && !objectDesc.description.isEmpty()) {
						schemaNode.put("description", objectDesc.description);
					}
				}
				schemaNode.put("additionalProperties", convertImpl(schema.get(JsonModel.MAPPING)));
			} else {
				if (schema.has(JsonModel.TYPE_INFO)) {
					FieldDescriptor objectDesc = new FieldDescriptor("", schema.get(JsonModel.TYPE_INFO).asString());
					if (objectDesc.description != null && !objectDesc.description.isEmpty()) {
						schemaNode.put("description", objectDesc.description);
					}
				}

				// Parse properties
				JsonObject properties = new JsonObject();
				JsonArray required = new JsonArray();
				for (Entry<String, JsonValue> entry : schema.asObject().entrySet()) {
					if (!entry.getKey().equals(JsonModel.TYPE_INFO)) {
						if (isRequired(entry.getValue())) {
							required.add(entry.getKey());
						}
						properties.put(entry.getKey(), convertImpl(entry.getValue()));
					}
				}
				schemaNode.put("properties", properties);

				if (required.size() > 0) {
					schemaNode.put("required", required);
				}
			}
			return schemaNode;
		} else if (schema instanceof JsonArray) {
			JsonObject schemaNode = new JsonObject();
			schemaNode.put("type", "array");
			if (schema.asArray().size() > 0) {
				schemaNode.put("items", convertImpl(schema.get(0)));
				if (schemaNode.get("items").has("description")) {
					schemaNode.put("description", schemaNode.get("items").get("description"));
					schemaNode.get("items").asObject().remove("description");
				}
			}
			return schemaNode;
		} else {
			JsonObject schemaNode = new JsonObject();
			FieldDescriptor desc = new FieldDescriptor("", schema.asString());
			if (desc.description != null && !desc.description.isEmpty()) {
				schemaNode.put("description", desc.description);
			}
			if (desc.values != null && desc.values.size() > 0) {
				JsonArray enumArray = new JsonArray();
				desc.values.forEach(e -> {
					if (e instanceof Long) {
						enumArray.add((Long) e);
					} else if (e instanceof Boolean) {
						enumArray.add((Boolean) e);
					} else if (e instanceof Integer) {
						enumArray.add((Integer) e);
					} else if (e instanceof Float) {
						enumArray.add((Float) e);
					} else {
						enumArray.add("" + e);
					}
				});
				schemaNode.put("enum", enumArray);
			}
			schemaNode.put("type", mapType(desc.type));
			return schemaNode;
		}
	}

	private static boolean isRequired(JsonValue value) {
		try {
			if (value instanceof JsonObject) {
				return isRequired(value.get(JsonModel.TYPE_INFO));
			} else if (value instanceof JsonArray) {
				return isRequired(value.get(0));
			} else if (new FieldDescriptor("", value.asString()).optional) {
				return false;
			}
		} catch (Exception e) {
		}
		return true;
	}

	public static String mapType(String type) {
		switch (type) {
		case "String":
			return "string";
		case "Boolean":
			return "boolean";
		case "IntNumber":
			return "integer";
		case "FloatNumber":
			return "number";
		default:
			return "null";
		}
	}

	public static void main(String[] args) throws Exception {
		System.out.println(convert(new ClassToType(DependencyDescriptor.class).parseType()).toPrettyString());
	}
}
