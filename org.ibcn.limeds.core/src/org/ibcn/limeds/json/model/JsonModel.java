/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.json.model;

import org.ibcn.limeds.TypeAdmin;
import org.ibcn.limeds.json.JsonArray;
import org.ibcn.limeds.json.JsonObject;
import org.ibcn.limeds.json.JsonValue;
import org.ibcn.limeds.util.Identifier;

/**
 * Utility class that allows fully described limeDS JSON schema documents to be
 * derived from a document that contains @typeRef or @typeParent references to
 * remote or local limeDS JSON schema resources.
 * 
 * @author wkerckho
 *
 */
public class JsonModel {

	public static final String TYPE_PARENT = "@typeParent";
	public static final String TYPE_REF = "@typeRef";
	public static final String MAPPING = "@mapping";
	public static final String TYPE_INFO = "@typeInfo";

	private TypeAdmin typeAdmin;

	public JsonModel(TypeAdmin typeAdmin) {
		this.typeAdmin = typeAdmin;
	}

	/**
	 * Returns a fully described limeDS JSON schema document (containing
	 * no @typeRef elements) based on the target document.
	 * 
	 * @param target
	 *            The JSON document to evaluate
	 * @return
	 */
	public JsonValue evalReferences(JsonValue target) throws Exception {
		if (target instanceof JsonArray) {
			JsonArray array = target.asArray();
			for (int i = 0; i < array.size(); i++) {
				array.set(i, evalReferences(array.get(i)));
			}
			return array;
		} else if (target instanceof JsonObject) {
			if (target.has(TYPE_REF)) {
				JsonValue refDoc = getRefSchema(target.getString(TYPE_REF));
				if (target.has(JsonModel.TYPE_INFO)) {
					refDoc.asObject().put(JsonModel.TYPE_INFO, target.get(JsonModel.TYPE_INFO));
				}
				return evalReferences(refDoc);
			} else {
				JsonObject object = new JsonObject();
				for (String fieldName : target.asObject().keySet()) {
					if (fieldName.equals(TYPE_PARENT)) {
						JsonValue parent = evalReferences(getRefSchema(target.getString(TYPE_PARENT)));
						if (parent instanceof JsonObject) {
							for (String parentFieldName : parent.asObject().keySet()) {
								object.put(parentFieldName, parent.get(parentFieldName));
							}
						}
					} else {
						object.put(fieldName, evalReferences(target.get(fieldName)));
					}
				}
				return object;
			}
		} else {
			return target;
		}
	}

	private JsonValue getRefSchema(String schemaRef) throws Exception {
		try {
			Identifier refId = Identifier.parse(schemaRef);

			return typeAdmin.getRegisteredType(refId).get();
		} catch (Exception e) {
			throw new Exception("Could not resolve limeDS JSON schema: " + schemaRef, e);
		}
	}

}
