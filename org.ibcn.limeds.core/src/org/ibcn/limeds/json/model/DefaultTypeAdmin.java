/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.json.model;

import java.util.Map;
import java.util.Optional;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentSkipListMap;

import org.ibcn.limeds.TypeAdmin;
import org.ibcn.limeds.annotations.Link;
import org.ibcn.limeds.json.JsonArray;
import org.ibcn.limeds.json.JsonObject;
import org.ibcn.limeds.json.JsonPrimitive;
import org.ibcn.limeds.json.JsonValue;
import org.ibcn.limeds.json.model.JsonValidator.FieldDescriptor;
import org.ibcn.limeds.util.Identifier;
import org.ibcn.limeds.util.Pair;
import org.ibcn.limeds.versioning.VersionUtil;
import org.osgi.service.component.annotations.Component;

@Component
public class DefaultTypeAdmin implements TypeAdmin {

	private JsonModel model = new JsonModel(this);
	private JsonValidator validator = new JsonValidator(this);
	private Map<Identifier, JsonObject> registry = new ConcurrentSkipListMap<>();

	@Override
	public void register(Identifier typeId, JsonObject schema) {
		if (schema.has("@typeVersion")) {
			typeId = new Identifier(typeId.getName(), schema.getString("@typeVersion"));
			schema.remove("@typeVersion");
		}
		registry.put(typeId, schema);
	}

	@Override
	public void unregister(Identifier typeId) {
		registry.remove(typeId);
	}

	@Override
	public JsonObject getRegisterdTypes() {
		JsonObject result = new JsonObject();
		registry.forEach((k, v) -> result.put(k.toString(), v));
		return result;
	}

	@Override
	public Optional<JsonObject> getRegisteredType(Identifier typeId) {
		if (typeId.getVersion().startsWith("^") || typeId.getVersion().contains("*")) {
			typeId = resolveVersionExpr(typeId);
		}

		if (registry.containsKey(typeId)) {
			return Optional.of(registry.get(typeId));
		} else {
			return Optional.empty();
		}
	}

	@Override
	public JsonValue resolve(JsonValue schema) throws Exception {
		return model.evalReferences(schema);
	}

	@Override
	public void validate(JsonValue schema, JsonValue document) throws Exception {
		validator.validate(schema, document);
	}

	@Override
	public void validateSchema(JsonValue schema) throws Exception {
		validator.validateSchema("$", schema);
	}

	@Override
	public void replaceVariables(JsonValue schema, JsonObject context) {
		if (schema instanceof JsonArray) {
			schema.asArray().forEach(entry -> replaceVariables(entry, context));
		} else if (schema instanceof JsonObject) {
			JsonObject tmp = schema.asObject();
			for (String key : tmp.keySet()) {
				if (tmp.get(key) instanceof JsonPrimitive) {
					if (tmp.get(key).asPrimitive().getValue() instanceof String
							&& tmp.getString(key).contains(Link.USE_PARENT_VERSION)) {
						tmp.put(key, tmp.getString(key).replace(Link.USE_PARENT_VERSION, context.getString("version")));
					}
				} else {
					replaceVariables(tmp.get(key), context);
				}
			}
		}
	}

	private Identifier resolveVersionExpr(Identifier typeId) {
		SortedMap<String, Identifier> mapping = new TreeMap<>();
		registry.keySet().stream().filter(key -> key.getName().equals(typeId.getName()))
				.forEach(key -> mapping.put(VersionUtil.createVersionPropertyValue(key.getVersion()), key));

		if (mapping.size() > 0) {
			if ("*".equals(typeId.getVersion())) {
				return mapping.get(mapping.lastKey());
			} else if (typeId.getVersion().startsWith("^")) {
				Pair<String, String> interval = VersionUtil.getInterval(typeId.getVersion());
				SortedMap<String, Identifier> matchingEntries = mapping.subMap(interval.left(), interval.right());
				if (!matchingEntries.isEmpty()) {
					return mapping.get(matchingEntries.lastKey());
				}
			}
		}

		return typeId;
	}

	@Override
	public boolean isOptional(JsonValue schema) throws Exception {
		if (schema != null) {
			if (schema instanceof JsonArray && schema.asArray().size() > 0) {
				return isOptional(schema.asArray().get(0));
			} else if (schema instanceof JsonObject) {
				return isOptional(schema.asObject().get(JsonModel.TYPE_INFO));
			} else if (schema instanceof JsonPrimitive) {
				FieldDescriptor desc = new FieldDescriptor("", schema.asString());
				return desc.optional;
			}
		}
		return true;
	}

}
