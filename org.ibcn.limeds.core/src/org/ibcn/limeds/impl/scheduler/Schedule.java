/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.impl.scheduler;

import org.ibcn.limeds.descriptors.ScheduleDescriptor.ScheduleMode;
import org.ibcn.limeds.descriptors.ScheduleDescriptor.ScheduleUnit;

/**
 * Utility class to parse schedule expressions.
 * 
 * @author wkerckho
 *
 */
public class Schedule {

	private final ScheduleMode mode;
	private final int mainValue;
	private final ScheduleUnit mainUnit;
	private final int offset;
	private final ScheduleUnit offsetUnit;

	public Schedule(String scheduleExpression) throws Exception {
		try {
			String[] parts = scheduleExpression.split(" ");
			mode = ScheduleMode.valueOf(parts[0]);
			mainValue = Integer.parseInt(parts[1]);
			mainUnit = ScheduleUnit.valueOf(parts[2]);

			if (parts.length == 6 && "offset".equals(parts[3])) {
				offset = Integer.parseInt(parts[4]);
				offsetUnit = ScheduleUnit.valueOf(parts[5]);
			} else {
				offset = 0;
				offsetUnit = ScheduleUnit.milliseconds;
			}

		} catch (Exception e) {
			throw new Exception("Could not parse scheduleExpression", e);
		}
	}

	public ScheduleMode getMode() {
		return mode;
	}

	public int getMainValue() {
		return mainValue;
	}

	public long getMainValueInMs() {
		return applyUnit(mainValue, mainUnit);
	}

	private long applyUnit(int value, ScheduleUnit unit) {
		switch (unit) {
		case milliseconds:
			return value;
		case seconds:
			return value * 1000;
		case minutes:
			return value * 60 * 1000;
		case hours:
			return value * 60 * 60 * 1000;
		case days:
			return value * 24 * 60 * 60 * 1000l;
		default:
			return value;
		}
	}

	public ScheduleUnit getMainUnit() {
		return mainUnit;
	}

	public boolean hasOffset() {
		return offset > 0;
	}

	public int getOffsetValue() {
		return offset;
	}

	public long getOffsetValueInMs() {
		return applyUnit(offset, offsetUnit);
	}

	public ScheduleUnit getOffsetUnit() {
		return offsetUnit;
	}

}
