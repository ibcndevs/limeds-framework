/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.impl.scheduler;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import org.ibcn.limeds.FunctionalSegment;
import org.ibcn.limeds.IssueAdmin;
import org.ibcn.limeds.ServicePropertyNames;
import org.ibcn.limeds.descriptors.ScheduleDescriptor.ScheduleMode;
import org.ibcn.limeds.descriptors.ScheduleDescriptor.ScheduleUnit;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.component.annotations.ReferencePolicy;

/**
 * The ProcessScheduler listens for active Flow Functions that have configured a
 * schedule and then makes sure that these functions are called at the
 * appropriate times. This implementation is based on the
 * ScheduledExecutorService of the Java threadpool APIs.
 * 
 * @author wkerckho
 *
 */
@Component(immediate = true)
public class ProcessScheduler {

	private static final int THREAD_POOL_SIZE = 4;
	private ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(THREAD_POOL_SIZE);;

	// Maps flowfunction id to scheduler entry
	private Map<String, ScheduledFuture<?>> registry = new HashMap<>();
	private IssueAdmin issueAdmin;

	@Reference(cardinality = ReferenceCardinality.MANDATORY, policy = ReferencePolicy.DYNAMIC)
	public void bindIssueAdmin(IssueAdmin issueAdmin) {
		this.issueAdmin = issueAdmin;
	}

	public void unbindIssueAdmin(IssueAdmin issueAdmin) {
		if (this.issueAdmin == issueAdmin) {
			this.issueAdmin = null;
		}
	}

	@Reference(policy = ReferencePolicy.DYNAMIC, cardinality = ReferenceCardinality.MULTIPLE)
	public synchronized void scheduleFunction(FunctionalSegment process, Map<String, Object> properties)
			throws Exception {
		if (properties.containsKey(ServicePropertyNames.SEGMENT_SCHEDULE)) {
			Schedule schedule = new Schedule((String) properties.get(ServicePropertyNames.SEGMENT_SCHEDULE));
			ScheduledFuture<?> entry = null;
			if (ScheduleMode.delay.equals(schedule.getMode())) {
				entry = scheduler.schedule(getHandler(process), schedule.getMainValueInMs(), TimeUnit.MILLISECONDS);
			} else if (ScheduleMode.every.equals(schedule.getMode())) {
				entry = scheduler.scheduleAtFixedRate(getHandler(process),
						calculateDelay(schedule.getMainUnit(), schedule.getOffsetValueInMs()),
						schedule.getMainValueInMs(), TimeUnit.MILLISECONDS);
			}

			if (entry != null) {
				registry.put((String) properties.get(ServicePropertyNames.SEGMENT_ID), entry);
			}
		}
	}

	public synchronized void unscheduleFunction(FunctionalSegment process, Map<String, Object> properties)
			throws Exception {
		if (properties.containsKey(ServicePropertyNames.SEGMENT_SCHEDULE)) {
			ScheduledFuture<?> entry = registry.get(properties.get(ServicePropertyNames.SEGMENT_ID));
			if (entry != null) {
				entry.cancel(false);
			}
		}
	}

	/*
	 * Offset only matters for every hours/days, otherwise we trigger
	 * straightaway
	 */
	private long calculateDelay(ScheduleUnit unit, long offsetValueMs) {
		switch (unit) {
		case days:
			// TODO
		case hours:
			// TODO
		default:
			return 0;
		}
	}

	private Runnable getHandler(FunctionalSegment process) {
		return () -> {
			try {
				process.apply();
			} catch (Exception e) {
				issueAdmin.report(e, ProcessScheduler.class);
			}
		};
	}

}
