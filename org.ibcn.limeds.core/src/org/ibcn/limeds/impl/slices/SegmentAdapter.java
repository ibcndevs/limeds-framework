/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.impl.slices;

import java.lang.reflect.Proxy;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Set;
import java.util.stream.Collectors;

import org.ibcn.limeds.FunctionalSegment;
import org.ibcn.limeds.ServiceSegment;
import org.ibcn.limeds.bindings.LanguageAdapter;
import org.ibcn.limeds.descriptors.DependencyDescriptor;
import org.ibcn.limeds.descriptors.SegmentDescriptor;
import org.ibcn.limeds.impl.segments.AspectsHandler;
import org.ibcn.limeds.json.Json;
import org.ibcn.limeds.json.JsonValue;
import org.ibcn.limeds.util.ConfigUtil;
import org.ibcn.limeds.util.Errors;
import org.slf4j.LoggerFactory;

/**
 * SegmentAdapter bridges non-Java Flow Function implementations and the Java
 * FunctionalSegment interface, so these can be used interchangeably with the
 * Java implementations. To do this, a LanguageAdapter instance of the specified
 * language is used.
 * 
 * @author wkerckho
 *
 */
public class SegmentAdapter implements FunctionalSegment {

	private SegmentDescriptor descriptor;
	private LanguageAdapter scriptWrapper;

	public SegmentAdapter(SegmentDescriptor descriptor, LanguageAdapter scriptWrapper) {
		this.scriptWrapper = scriptWrapper;
		this.descriptor = descriptor;

		resetConfig();
	}

	public LanguageAdapter getScriptWrapper() {
		return scriptWrapper;
	}

	@Override
	public void started() {
		// Try to executed JS _started function as lifecycle start handler.
		try {
			scriptWrapper.call("_started", scriptWrapper.wrap(Json.from(descriptor)));
		} catch (Exception e) {
			LoggerFactory.getLogger(SegmentAdapter.class).debug("Could not call _started handler for " + descriptor);
		}
	}

	@Override
	public void stopped() {
		// Try to execute JS _stopped function as lifecycle stop handler
		try {
			scriptWrapper.call("_stopped", scriptWrapper.wrap(Json.from(descriptor)));
		} catch (Exception e) {
			LoggerFactory.getLogger(SegmentAdapter.class).debug("Could not call _stopped handler for " + descriptor);
		}
	}

	@Override
	public void configUpdated() throws Exception {
		// Try to execute JS _configUpdated as handler for config updates
		try {
			scriptWrapper.call("_configUpdated", scriptWrapper.wrap(Json.from(descriptor)));
		} catch (Exception e) {
			LoggerFactory.getLogger(SegmentAdapter.class)
					.debug("Could not call _configUpdated handler for " + descriptor);
		}
	}

	@Override
	public void bind(String dependencyId, DependencyDescriptor desc, Object instance) {
		setDependency(dependencyId, desc);
	}

	@Override
	public void unbind(String dependencyId, DependencyDescriptor desc, Object instance) {
		scriptWrapper.setGlobal(dependencyId, null);
	}

	@Override
	public JsonValue apply(JsonValue... input) throws Exception {
		return scriptWrapper.unwrap(scriptWrapper.call("apply", scriptWrapper.wrap(input)));
	}

	public void setConfigProperty(String name, Object value) {
		if (descriptor.getTemplateInfo() != null
				&& descriptor.getTemplateInfo().getConfiguration().stream().anyMatch(prop -> prop.name.equals(name))) {
			scriptWrapper.setGlobal(name, value);
		}
	}

	public void resetConfig() {
		if (descriptor.getTemplateInfo() != null) {
			descriptor.getTemplateInfo().getConfiguration().forEach(Errors.wrapConsumer(prop -> {
				scriptWrapper.setGlobal(prop.name, prop.value != null ? ConfigUtil.convertValue(prop.value, prop.type)
						: ConfigUtil.getDefault(prop.type));
			}));
		}
	}

	@SuppressWarnings("unchecked")
	protected void setDependency(String variable, DependencyDescriptor dependency) {
		if (dependency.isCollection()) {
			if (FunctionalSegment.class.getName().equals(dependency.getType())) {
				scriptWrapper.setGlobal(variable, ((Set<Object>) dependency.getInstance()).stream()
						.map(o -> wrapFunction(variable, (FunctionalSegment) o, dependency)).toArray());
			} else {
				scriptWrapper.setGlobal(variable, ((Set<Object>) dependency.getInstance()).stream().toArray());
			}
		} else {
			scriptWrapper.setGlobal(variable,
					FunctionalSegment.class.getName().equals(dependency.getType())
							? wrapFunction(variable, (FunctionalSegment) dependency.getInstance(), dependency)
							: dependency.getInstance());
		}
	}

	private Object wrapFunction(String variableID, FunctionalSegment functionInstance,
			DependencyDescriptor dependency) {
		Object wrappedSegment = new WrappedSegment() {

			@Override
			public Object apply(Object arg) throws Exception {
				return apply(new Object[] { arg });
			}

			@Override
			public Object apply() throws Exception {
				return apply(new Object[] {});
			}

			@Override
			public Object apply(String alias, Object... args) throws Exception {
				LinkedList<Object> arguments = new LinkedList<>();
				arguments.add(alias);
				Arrays.stream(args).forEach(arguments::add);
				return apply(arguments.toArray());
			}

			@Override
			public Object expandArgsThenApply(Object[] args) throws Exception {
				return apply(args);
			}

			@Override
			public Object apply(Object... args) throws Exception {
				if (functionInstance == null) {
					throw new Exception("No function is bound for dependency '" + variableID + "'!");
				}
				JsonValue[] input = Arrays.stream(args).map(o -> scriptWrapper.unwrap(o))
						.toArray(size -> new JsonValue[size]);
				JsonValue output = functionInstance.apply(input);
				if (output != null) {
					return scriptWrapper.wrap(output)[0];
				} else {
					return null;
				}
			}

			@Override
			public Object getDescriptor() {
				SegmentDescriptor descriptor = functionInstance.getDescriptor();
				return scriptWrapper.wrap(Json.from(descriptor))[0];
			}

		};

		AspectsHandler h = (AspectsHandler) Proxy.getInvocationHandler(functionInstance);
		FunctionalSegment actualInstance = h.getContext().getInstance();
		if (actualInstance instanceof ServiceSegment) {
			try {
				Object aliasRouter = scriptWrapper.getAliasRouter(wrappedSegment, ((ServiceSegment) actualInstance)
						.getSupportedOperations().stream().map(m -> m.getName()).collect(Collectors.toSet()));
				return aliasRouter;
			} catch (Exception e) {
				LoggerFactory.getLogger(SegmentAdapter.class)
						.warn("Could not setup alias router, reverting to normal instance...");
			}
		}
		return wrappedSegment;
	}

}
