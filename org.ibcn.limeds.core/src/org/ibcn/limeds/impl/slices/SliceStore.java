/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.impl.slices;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Optional;

import org.ibcn.limeds.json.Json;
import org.ibcn.limeds.json.JsonArray;
import org.ibcn.limeds.json.JsonObject;

/**
 * (Currently all Data Flows are stored as individual files on the file system).
 * 
 * @author wkerckho
 *
 */
public class SliceStore {

	private File flowRoot;

	public SliceStore(String flowLocation) {
		flowRoot = new File(flowLocation);
		if (!flowRoot.exists()) {
			flowRoot.mkdirs();
		}
	}

	public synchronized Optional<JsonObject> findByKey(String key) {
		Optional<File> result = findFile(key);
		if (result.isPresent()) {
			try (InputStream is = new FileInputStream(result.get())) {
				return Optional.of(Json.from(is));
			} catch (Exception e) {
			}
		}
		return Optional.empty();

	}

	private Optional<File> findFile(String key) {
		File file = new File(flowRoot, key + ".json");
		if (file.exists() && file.isFile()) {
			return Optional.of(file);
		} else {
			return Optional.empty();
		}
	}

	public synchronized JsonArray findAll() {
		return Arrays.stream(flowRoot.listFiles()).filter(f -> f.isFile()).map(f -> {
			try (InputStream is = new FileInputStream(f)) {
				return Json.from(is);
			} catch (Exception e) {
				return new JsonObject();
			}
		}).collect(Json.toArray());
	}

	public synchronized void save(String key, JsonObject flow) {
		try (FileOutputStream out = new FileOutputStream(new File(flowRoot, key + ".json"))) {
			byte[] strToBytes = flow.toPrettyString().getBytes();
			out.write(strToBytes);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public synchronized void delete(String key) {
		Optional<File> result = findFile(key);
		if (result.isPresent()) {
			result.get().delete();
		}
	}
}
