/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.impl.slices.publishing;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.ibcn.limeds.json.JsonArray;
import org.ibcn.limeds.json.JsonObject;
import org.ibcn.limeds.json.JsonPrimitive;
import org.ibcn.limeds.json.JsonValue;
import org.ibcn.limeds.json.model.JsonModel;

public interface TypeDependencyUtil {

	static Collection<? extends String> eval(JsonObject sliceContext, JsonValue schema) {
		Set<String> result = new HashSet<>();
		if (schema instanceof JsonArray) {
			schema.asArray().forEach(attrSchema -> result.addAll(eval(sliceContext, attrSchema)));
		} else if (schema instanceof JsonObject) {
			if (schema.has(JsonModel.TYPE_REF)) {
				String typeRef = schema.getString(JsonModel.TYPE_REF);
				if (!isLocalType(sliceContext, typeRef)) {
					result.add(typeRef);
				} else {
					if (sliceContext.get("typeDefinitions").has(getId(typeRef))) {
						JsonValue localSchema = sliceContext.get("typeDefinitions").get(getId(typeRef));
						result.addAll(eval(sliceContext, localSchema));
					}
				}
			} else {
				schema.asObject().forEach((attrName, attrSchema) -> {
					if (!(attrSchema instanceof JsonPrimitive)) {
						result.addAll(eval(sliceContext, attrSchema));
					}
				});
			}
		}
		return result;
	}

	static boolean isLocalType(JsonObject sliceContext, String typeRef) {
		return sliceContext.get("typeDefinitions").asObject().keySet().stream().map(ref -> getId(ref))
				.anyMatch(id -> id.equals(getId(typeRef)));
	}

	static String getId(String typeRef) {
		int delimIndex = typeRef.lastIndexOf('_');
		return typeRef.substring(0, delimIndex);
	}

}
