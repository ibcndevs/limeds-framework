/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.impl.slices.publishing;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.jar.Manifest;
import java.util.stream.Collectors;

import org.ibcn.limeds.ServicePropertyNames;
import org.ibcn.limeds.descriptors.SegmentDescriptor;
import org.ibcn.limeds.json.Json;
import org.ibcn.limeds.json.JsonObject;
import org.ibcn.limeds.util.Errors;
import org.ibcn.limeds.util.FilterUtil;
import org.ibcn.limeds.versioning.VersionUtil;
import org.osgi.framework.Constants;

public class SliceManifest {

	private JsonObject descriptor;
	private Manifest manifest = new Manifest();
	private String manifestStr = "Manifest-Version: 1.0\n";
	private Collection<SegmentDescriptor> components;

	public SliceManifest(JsonObject descriptor) {
		this.descriptor = descriptor;
	}

	public Manifest generate() {
		components = descriptor.get("segments").asArray().stream().map(Errors.wrapFunction(json -> {
			SegmentDescriptor comp = Json.to(SegmentDescriptor.class, json.asObject());
			return comp;
		})).collect(Collectors.toList());

		writeBasicMetaData();
		writeProvidedCapabilities();
		writeRequiredCapabilities();

		try {
			manifest.read(new ByteArrayInputStream(manifestStr.getBytes("UTF-8")));
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return manifest;
	}

	private void writeBasicMetaData() {
		add(Constants.BUNDLE_NAME, descriptor.getString("id"));
		add(Constants.BUNDLE_SYMBOLICNAME, descriptor.getString("id"));
		add(Constants.BUNDLE_VERSION, descriptor.getString("version"));

		add("LimeDS-Slices", JarWriter.SLICE_DESCRIPTOR_FILE);
	}

	private void writeProvidedCapabilities() {
		List<String> capabilities = new LinkedList<>();
		components.stream()
				.filter(comp -> comp.isExport()).map(
						comp -> "limeds.segment;" + stringProp(ServicePropertyNames.SEGMENT_ID, comp.getId()) + ";"
								+ stringProp(ServicePropertyNames.SEGMENT_VERSION,
										VersionUtil.createVersionPropertyValue(comp.getVersion())))
				.forEach(capabilities::add);

		descriptor.get("typeDefinitions").asObject()
				.forEach((typeId,
						schema) -> capabilities.add("limeds.type;"
								+ stringProp(ServicePropertyNames.TYPE_ID, TypeDependencyUtil.getId(typeId)) + ";"
								+ stringProp(ServicePropertyNames.TYPE_VERSION, descriptor.getString("version"))));

		if (capabilities.size() > 0) {
			add(Constants.PROVIDE_CAPABILITY, capabilities.toArray(new String[] {}));
		}
	}

	private String stringProp(String name, String value) {
		return name + ":String=\"" + value + "\"";
	}

	private void writeRequiredCapabilities() {
		List<String> requires = new LinkedList<>();
		components.stream().flatMap(comp -> comp.getDependencies().values().stream())
				.map(dep -> "limeds.segment;filter:=\"" + dep.getFunctionFilter() + "\";effective:=resolve")
				.forEach(requires::add);

		descriptor.get("typeDependencies").asArray().stream()
				.map(dep -> "limeds.type;filter:=\"" + getTypeFilter(dep.asString()) + "\";effective:=resolve")
				.forEach(requires::add);

		if (requires.size() > 0) {
			add(Constants.REQUIRE_CAPABILITY, requires.toArray(new String[] {}));
		}
	}

	private String getTypeFilter(String dep) {
		int delimIndex = dep.lastIndexOf('_');
		return FilterUtil.and(FilterUtil.equals(ServicePropertyNames.TYPE_ID, dep.substring(0, delimIndex)), VersionUtil
				.createFilter(ServicePropertyNames.TYPE_VERSION, dep.substring(delimIndex + 1, dep.length())));
	}

	private void add(String key, String... values) {
		manifestStr += key + ": " + Arrays.stream(values).collect(Collectors.joining(",")) + "\n";
	}

}
