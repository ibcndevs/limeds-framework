/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.impl.slices.publishing;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.jar.JarEntry;
import java.util.jar.JarOutputStream;
import java.util.jar.Manifest;

public class JarWriter implements AutoCloseable {

	public static final String SLICE_DESCRIPTOR_FILE = "descriptor.json";
	
	private JarOutputStream out;

	public JarWriter(Manifest manifest, File target) throws FileNotFoundException, IOException {
		out = new JarOutputStream(new FileOutputStream(target), manifest);
	}

	public void addFile(String name, String content) throws IOException {
		JarEntry entry = new JarEntry(name);
		entry.setTime(System.currentTimeMillis());
		out.putNextEntry(entry);

		out.write(content.getBytes("UTF-8"));
		out.closeEntry();
	}

	@Override
	public void close() throws IOException {
		out.close();
	}

}
