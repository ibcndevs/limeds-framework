/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.impl.slices;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Dictionary;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.jar.Manifest;

import org.ibcn.limeds.FunctionalSegment;
import org.ibcn.limeds.SliceAdmin.SliceState;
import org.ibcn.limeds.TypeAdmin;
import org.ibcn.limeds.annotations.Link;
import org.ibcn.limeds.annotations.ModelInfo;
import org.ibcn.limeds.bindings.LanguageAdapter;
import org.ibcn.limeds.descriptors.SegmentDescriptor;
import org.ibcn.limeds.descriptors.TemplateDescriptor;
import org.ibcn.limeds.impl.slices.publishing.JarWriter;
import org.ibcn.limeds.impl.slices.publishing.SliceManifest;
import org.ibcn.limeds.impl.slices.publishing.TypeDependencyUtil;
import org.ibcn.limeds.json.Json;
import org.ibcn.limeds.json.JsonArray;
import org.ibcn.limeds.json.JsonObject;
import org.ibcn.limeds.json.JsonPrimitive;
import org.ibcn.limeds.json.JsonValue;
import org.ibcn.limeds.util.ConfigUtil;
import org.ibcn.limeds.util.ErrorUtil;
import org.ibcn.limeds.util.Errors;
import org.ibcn.limeds.util.Identifier;
import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;
import org.osgi.service.cm.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class encapsulates the various Flow operations for LimeDS.
 * 
 * @author wkerckho
 *
 */
public class SliceOperations {

	private static final Logger LOGGER = LoggerFactory.getLogger(SliceOperations.class);
	private static final BundleContext bundleContext = FrameworkUtil.getBundle(SliceOperations.class)
			.getBundleContext();

	private static boolean isDefined(SegmentDescriptor component, JsonObject flow) {
		return flow.get("codeAttachments").has(component.getId());
	}

	private static void addError(Throwable error, String phase, JsonObject slice, DefaultSliceAdmin context) {
		if (!slice.has("errors")) {
			slice.put("errors", new JsonArray());
		}
		Optional<StackTraceElement> location = ErrorUtil.findFirstMentionOf(SliceOperations.class.getName(), error);
		slice.get("errors").asArray()
				.add(Json.objectBuilder().add("phase", phase).add("type", error.getClass().getName())
						.add("message", error.getMessage())
						.add("location", location.isPresent() ? location.get().toString() : "n/a").build());
	}

	private static void resetError(JsonObject slice, DefaultSliceAdmin context) {
		slice.remove("errors");
		context.getFlowStore().save(new Identifier(slice).toString(), slice);
	}

	/**
	 * Creates a job that attempts to deploy the specified flow to the LimeDS
	 * runtime.
	 */
	public static Runnable createDeployJob(final Identifier flowKey, final DefaultSliceAdmin manager) {
		return () -> {
			LinkedList<SegmentDescriptor> processing = new LinkedList<>();
			JsonObject flow = null;
			String phase = "init";
			try {
				flow = manager.getFlowStore().findByKey(flowKey.toString())
						.orElseThrow(() -> new Exception("Could not find a flow with the specified key: " + flowKey));
				String flowVersion = flow.getString("version");

				// 1. Get the components for this flow
				JsonArray components = flow.get("segments").asArray();

				// 2. Register the types
				if (flow.has("typeDefinitions")) {
					for (Map.Entry<String, JsonValue> typeDef : flow.get("typeDefinitions").asObject().entrySet()) {
						manager.getTypeAdmin().replaceVariables(typeDef.getValue(), flow);
						manager.getTypeAdmin().register(Identifier.parse(typeDef.getKey(), flowVersion),
								typeDef.getValue().asObject());
					}
				}

				// 3. For each flow component in the definition
				for (JsonValue component : components) {
					phase = component.getString("id");

					/*
					 * Prevent empty cache objects from triggering the cache
					 * aspect
					 */
					if (component.has("cache") && component.get("cache").asObject().isEmpty()) {
						component.asObject().remove("cache");
					}

					// Parse the descriptor
					SegmentDescriptor descriptor = Json.to(SegmentDescriptor.class, component.asObject());
					processing.add(descriptor);

					assignLocalVersions(flow, descriptor);

					// Only deploy if the component was defined in this flow.
					if (isDefined(descriptor, flow)) {
						// ... deploy a regular component:

						// Create matching adapter for the attached source code
						FunctionalSegment instance = null;
						if (manager.getLanguageBindings().containsKey(descriptor.getLanguage().toUpperCase())) {
							String script = flow.get("codeAttachments").get(descriptor.getId()).asString();
							LanguageAdapter scriptWrapper = manager.getLanguageBindings()
									.get(descriptor.getLanguage().toUpperCase()).createAdapter(script);
							instance = new SegmentAdapter(descriptor, scriptWrapper);
						} else {
							throw new Exception("This runtime has no active support for the requested language "
									+ descriptor.getLanguage());
						}

						// Register the adapter instance
						manager.getComponentManager().register(descriptor, instance, bundleContext);

					}

					// If this is a factory instance...
					else if (descriptor.getTemplateInfo() != null && TemplateDescriptor.Type.FACTORY_INSTANCE
							.equals(descriptor.getTemplateInfo().getType())) {
						// ... create an entry in the Config admin
						Configuration config = manager.getConfigAdmin()
								.createFactoryConfiguration(descriptor.getTemplateInfo().getFactoryId(), null);
						Dictionary<String, Object> dict = new Hashtable<>();

						// Add template instantiated properties to config
						descriptor.getTemplateInfo().getConfiguration().forEach(cp -> {
							dict.put(cp.name, ConfigUtil.convertValue(cp.value, cp.type));
						});

						// Add the function id to the config
						dict.put(TemplateDescriptor.FUNCTION_ID_PROPERTY, descriptor.getId());
						dict.put(TemplateDescriptor.EXPORT_PROPERTY, descriptor.isExport());

						/*
						 * Indicate that the LimeDS config manager should not
						 * persist this config
						 */
						dict.put("_transient", true);
						dict.put(TemplateDescriptor.SLICE_LOCAL_PROPERTY, true);

						// Add dynamic links to the config
						descriptor.getDependencies()
								.forEach((variable, depDesc) -> dict.put(
										TemplateDescriptor.DYNAMIC_DEPENDENCY_PREFIX + variable,
										depDesc.getFunctionFilter()));

						// Register the config
						config.update(dict);
					}
				}

				// 4. State change + persist
				flow.put("modelVersion", ModelInfo.VERSION);
				flow.put("status", SliceState.deployed.toString());

				manager.getFlowStore().save(flowKey.toString(), flow);
				manager.getActiveFlows().put(flowKey.toString(), flow);

				resetError(flow, manager);
				LOGGER.info("DeployJob-" + flowKey + ": Sucessfully deployed " + processing.size() + " segments!");
			} catch (Exception e) {
				addError(e, phase, flow, manager);

				if (processing.size() > 0) {
					// Store culprit
					SegmentDescriptor faultyComponent = processing.removeLast();

					// Rollback
					for (SegmentDescriptor d : processing) {
						try {
							manager.getComponentManager().unregister(d, bundleContext);
						} catch (Exception e1) {
							addError(e1, d.getId(), flow, manager);
							LOGGER.warn("Could not rollback deployment of component {}", d.getId());
						}
					}

					if (flow.has("typeDefinitions")) {
						unregisterTypes(flow.get("typeDefinitions").asObject(), flow.getString("version"),
								manager.getTypeAdmin());
					}

					if (flow != null) {
						flow.put("status", SliceState.undeployed.toString());
						manager.getFlowStore().save(flowKey.toString(), flow);
					}
					LOGGER.warn("DeployJob-" + flowKey + ": Could not process component " + faultyComponent.getId()
							+ ", rolled back deployment!");
				} else {
					LOGGER.warn("DeployJob-" + flowKey + ": Error while preparing deployment!", e);
				}
			}
		};
	}

	private static void assignLocalVersions(JsonObject flow, SegmentDescriptor descriptor) {
		descriptor.setVersion(flow.getString("version"));

		// Assign local version dependencies
		String version = flow.getString("version");
		descriptor.getDependencies().values().stream().filter(dep -> dep.getFunctionTargets() != null)
				.forEach(dep -> dep.getFunctionTargets().entrySet().stream()
						.filter(e -> Link.USE_PARENT_VERSION.equals(e.getValue()))
						.forEach(e -> dep.getFunctionTargets().put(e.getKey(), version)));

		// Assign local documentation versions
		descriptor.getDocReferenceTypes().forEach(docSchema -> {
			if (docSchema.has("@typeRef") && docSchema.getString("@typeRef").contains(Link.USE_PARENT_VERSION)) {
				docSchema.asObject().put("@typeRef",
						docSchema.getString("@typeRef").replace(Link.USE_PARENT_VERSION, version));
			}
		});
	}

	private static void unregisterTypes(JsonObject typeDefinitions, String sliceVersion, TypeAdmin typeAdmin) {
		typeDefinitions.keySet().forEach(id -> typeAdmin.unregister(Identifier.parse(id, sliceVersion)));
	}

	/**
	 * Creates a job that attempts to undeploy the specified flow from the
	 * LimeDS runtime.
	 */
	public static Runnable createUndeployJob(Identifier flowKey, DefaultSliceAdmin manager, boolean changeState) {
		return () -> {
			LinkedList<SegmentDescriptor> processing = new LinkedList<>();
			JsonObject flow = null;
			String phase = "init";
			try {
				flow = manager.getActiveFlows().get(flowKey.toString());
				if (flow == null) {
					throw new Exception("Could not find an active flow with the specified key: " + flowKey);
				}

				// 1. Get the components for this flow
				JsonArray components = flow.get("segments").asArray();

				// 2. Unregister types
				if (flow.has("typeDefinitions")) {
					unregisterTypes(flow.get("typeDefinitions").asObject(), flow.getString("version"),
							manager.getTypeAdmin());
				}

				// 3. For each flow component in the definition
				for (JsonValue component : components) {
					phase = component.getString("id");

					// Parse the descriptor
					SegmentDescriptor descriptor = Json.to(SegmentDescriptor.class, component.asObject());
					processing.add(descriptor);

					// Only undeploy if the component was defined in this flow.
					if (isDefined(descriptor, flow)) {
						// ... just unregister the component
						manager.getComponentManager().unregister(descriptor, bundleContext);
					}
					// If this is a factory instance...
					else if (descriptor.getTemplateInfo() != null && TemplateDescriptor.Type.FACTORY_INSTANCE
							.equals(descriptor.getTemplateInfo().getType())) {
						// ... delete the config admin entry for this instance.
						Configuration[] results = manager.getConfigAdmin().listConfigurations(
								"(" + TemplateDescriptor.FUNCTION_ID_PROPERTY + "=" + descriptor.getId() + ")");
						if (results.length > 0) {
							results[0].delete();
						}
					}
				}

				/*
				 * After undeploying with the original flow-descriptor, get the
				 * most recent definition from storage.
				 */
				flow = manager.getFlowStore().findByKey(flowKey.toString()).get();
				if (changeState) {
					// 4. State change + persist
					flow.put("status", SliceState.undeployed.toString());
					manager.getFlowStore().save(flowKey.toString(), flow);
				}

				manager.getActiveFlows().remove(flowKey.toString());

				resetError(flow, manager);
				LOGGER.info("UndeployJob-" + flowKey + ": Sucessfully undeployed " + processing.size() + " segments!");

			} catch (Exception e) {
				addError(e, phase, flow, manager);

				if (processing.size() > 0) {
					// Store culprit
					SegmentDescriptor faultyComponent = processing.removeLast();

					if (flow != null) {
						flow.put("status", SliceState.deployed.toString());
						manager.getFlowStore().save(flowKey.toString(), flow);
					}
					LOGGER.warn("UndeployJob-" + flowKey + ": Could not process component " + faultyComponent.getId()
							+ ", rolled back undeployment!", e);
				} else {
					LOGGER.warn("UndeployJob-" + flowKey + ": Error while preparing undeployment!", e);
				}
			}

		};
	}

	/**
	 * Creates a job that generates a .jar OSGi bundle that represents the
	 * specified flow as a deployment unit.
	 */
	public static Runnable createPublishJob(Identifier flowKey, DefaultSliceAdmin manager) {
		return () -> {
			JsonObject flow = manager.getFlowStore().findByKey(flowKey.toString()).get().asObject();
			flow.put("modelVersion", ModelInfo.VERSION);

			// Assign local versions
			JsonArray segments = flow.get("segments").asArray().stream().map(segment -> {
				try {
					SegmentDescriptor segmentDesc = Json.to(SegmentDescriptor.class, segment.asObject());
					assignLocalVersions(flow, segmentDesc);
					return Json.from(segmentDesc);
				} catch (Exception e) {
					LOGGER.warn("Error while assigning local version for " + segment.getString("id"));
					return segment;
				}
			}).collect(Json.toArray());
			flow.asObject().put("segments", segments);

			// Calculate type dependencies
			calculateTypeDependencies(flow);

			// Create manifest file
			Manifest manifest = new SliceManifest(flow).generate();

			File targetDir = new File(DefaultSliceAdmin.FLOW_BASE_DIR + "/generated");
			targetDir.mkdirs();
			try (JarWriter writer = new JarWriter(manifest, new File(targetDir, flowKey + ".jar"))) {
				// Generate the JSON schema data types

				String flowId = flow.getString("id");

				// Generate the script source files
				JsonObject handlers = new JsonObject();
				flow.get("codeAttachments").asObject().forEach(Errors.wrapBiConsumer((id, code) -> {
					String fileName = id.substring(flowId.length() + 1)
							+ getExtension(getLanguage(id, flow.get("segments").asArray()));
					writer.addFile(fileName, code.asString());
					handlers.put(id, fileName);
				}));
				flow.remove("codeAttachments");
				flow.put("handlers", handlers);

				// Generate the type definition files
				if (flow.has("typeDefinitions")) {
					JsonObject types = new JsonObject();
					flow.get("typeDefinitions").asObject().forEach(Errors.wrapBiConsumer((id, schema) -> {
						String fileName = "types/" + TypeDependencyUtil.getId(id) + ".json";
						writer.addFile(fileName, schema.toPrettyString());
						types.put(TypeDependencyUtil.getId(id), fileName);
					}));
					flow.remove("typeDefinitions");
					flow.put("types", types);
				}

				flow.remove("status");

				// Generate the flow descriptor file
				writer.addFile(JarWriter.SLICE_DESCRIPTOR_FILE, flow.toPrettyString());
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		};
	}

	private static void calculateTypeDependencies(JsonObject flow) {
		Set<String> typeDependencies = new HashSet<>();
		// Add type dependencies for documentation types
		flow.get("segments").asArray().stream()
				.map(Errors.wrapFunction(seg -> Json.to(SegmentDescriptor.class, seg.asObject())))
				.flatMap(desc -> desc.getDocReferenceTypes().stream())
				.forEach(docRef -> typeDependencies.addAll(TypeDependencyUtil.eval(flow, docRef)));

		if (flow.has("typeDefinitions")) {
			flow.get("typeDefinitions").asObject().forEach(Errors.wrapBiConsumer((id, schema) -> {
				typeDependencies.addAll(TypeDependencyUtil.eval(flow, schema));
			}));
		}

		flow.put("typeDependencies",
				typeDependencies.stream().map(dep -> new JsonPrimitive(dep)).collect(Json.toArray()));
	}

	private static String getExtension(String language) {
		switch (language.toLowerCase()) {
		case "javascript":
			return ".js";
		case "python":
			return ".py";
		default:
			return "." + language;
		}
	}

	private static String getLanguage(String id, JsonArray components) {
		return components.stream().filter(json -> json.getString("id").equals(id)).findAny().get()
				.getString("language");
	}

}
