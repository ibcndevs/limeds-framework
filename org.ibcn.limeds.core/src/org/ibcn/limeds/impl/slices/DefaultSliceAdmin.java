/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.impl.slices;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.ibcn.limeds.SegmentAdmin;
import org.ibcn.limeds.SliceAdmin;
import org.ibcn.limeds.TypeAdmin;
import org.ibcn.limeds.bindings.LanguageAdapterFactory;
import org.ibcn.limeds.json.JsonArray;
import org.ibcn.limeds.json.JsonObject;
import org.ibcn.limeds.util.Identifier;
import org.osgi.service.cm.ConfigurationAdmin;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.component.annotations.ReferencePolicy;

/**
 * DefaultSliceAdmin implements the LimeDS SliceAdmin interface and is
 * responsible for the (un)registration of LimeDS Data Flows and supports making
 * changes to their deployment state.
 * 
 * @author wkerckho
 *
 */
@Component(immediate = true)
public class DefaultSliceAdmin implements SliceAdmin {

	static final String FLOW_BASE_DIR = "./slices";

	private ExecutorService threadPool;

	/*
	 * The flowStore managers the flows stored on disk.
	 */
	private SliceStore flowStore;

	@Reference(cardinality = ReferenceCardinality.MANDATORY)
	private volatile SegmentAdmin componentManager;

	private Map<String, LanguageAdapterFactory> languageBindings = new HashMap<>();
	private Map<String, JsonObject> activeFlows = new HashMap<>();

	@Reference(cardinality = ReferenceCardinality.MANDATORY)
	private volatile ConfigurationAdmin configAdmin;

	@Reference(cardinality = ReferenceCardinality.MANDATORY)
	private volatile TypeAdmin typeAdmin;

	@Activate
	private synchronized void start() {
		flowStore = new SliceStore(FLOW_BASE_DIR);
		threadPool = Executors.newSingleThreadExecutor();

		checkDeployed();
	}

	@Deactivate
	private synchronized void stop() throws Exception {
		undeployAll();
		threadPool.shutdown();
		threadPool.awaitTermination(30 * 1000, TimeUnit.MILLISECONDS);
		threadPool = null;
		flowStore = null;
	}

	@Reference(policy = ReferencePolicy.DYNAMIC, cardinality = ReferenceCardinality.AT_LEAST_ONE)
	private synchronized void bindLanguageAdapterFactory(LanguageAdapterFactory factory) {
		languageBindings.put(factory.getLanguageId().toUpperCase(), factory);
	}

	// Required for DS
	@SuppressWarnings("unused")
	private synchronized void unbindLanguageAdapterFactory(LanguageAdapterFactory factory) {
		languageBindings.remove(factory.getLanguageId().toUpperCase());
	}

	public SliceStore getFlowStore() {
		return flowStore;
	}

	public SegmentAdmin getComponentManager() {
		return componentManager;
	}

	public Map<String, LanguageAdapterFactory> getLanguageBindings() {
		return languageBindings;
	}

	public Map<String, JsonObject> getActiveFlows() {
		return activeFlows;
	}

	public ConfigurationAdmin getConfigAdmin() {
		return configAdmin;
	}

	public TypeAdmin getTypeAdmin() {
		return typeAdmin;
	}

	/**
	 * Deploys the stored flows that were in a DEPLOYED state.
	 */
	private void checkDeployed() {
		// 1. Use the list() operation to find all flows that should be deployed
		// 2. Schedule deploy jobs for these flows
		list().stream().filter(flow -> SliceState.deployed.toString().equals(flow.getString("status")))
				.forEach(flow -> threadPool.execute(SliceOperations.createDeployJob(new Identifier(flow), this)));
	}

	/**
	 * Undeploy all flows
	 */
	private void undeployAll() {
		// 1. Use the list() operation to find all flows that should be
		// undeployed
		// 2. Schedule undeploy jobs for these flows
		list().stream().filter(flow -> SliceState.deployed.toString().equals(flow.get("status").asString()))
				.forEach(flow -> threadPool.execute(SliceOperations.createDeployJob(new Identifier(flow), this)));
	}

	@Override
	public JsonArray list() {
		// 1. Find available flows on disk
		// 2. Add runtime info retrieved from registry
		return flowStore.findAll();
	}

	@Override
	public JsonObject get(Identifier sliceId) {
		JsonObject value = flowStore.findByKey(sliceId.toString()).get();
		return value;
	}

	@Override
	public void sync(JsonObject flow) {
		if (!flow.has("status")) {
			flow.put("status", SliceState.undeployed.toString());
		}
		flowStore.save(new Identifier(flow).toString(), flow);
	}

	@Override
	public void publish(Identifier sliceId) {
		threadPool.execute(SliceOperations.createPublishJob(sliceId, this));
	}

	@Override
	public void changeState(Identifier sliceId, SliceState targetState, boolean async) throws Exception {
		JsonObject flow = flowStore.findByKey(sliceId.toString()).get();
		SliceState oldState = SliceState.valueOf(flow.getString("status"));
		if (targetState.equals(oldState) && !SliceState.deployed.equals(targetState)) {
			throw new Exception("Flow already in state " + targetState);
		} else if (SliceState.processing.equals(targetState)) {
			throw new Exception("'processing' is not a valid target state!");
		} else if (SliceState.processing.equals(oldState)) {
			throw new Exception("Cannot change the state of a flow that is being processed!");
		} else {
			// Set state to processing
			flow.asObject().put("status", SliceState.processing.toString());
			flowStore.save(sliceId.toString(), flow.asObject());

			if (SliceState.deployed.equals(targetState)) {
				if (SliceState.deployed.equals(oldState)) {
					// Already was deployed, so we undeploy first
					Runnable job = SliceOperations.createUndeployJob(sliceId, this, true);
					if (async) {
						// Schedule the job
						threadPool.execute(job);
					} else {
						// Run now
						job.run();
					}
				}
				// Create deployjob
				Runnable job = SliceOperations.createDeployJob(sliceId, this);
				if (async) {
					threadPool.execute(job);
				} else {
					job.run();
				}
			} else if (SliceState.undeployed.equals(targetState)
					|| (SliceState.terminated.equals(targetState) && !SliceState.undeployed.equals(oldState))) {
				// Create undeployjob
				Runnable job = SliceOperations.createUndeployJob(sliceId, this, true);
				if (async) {
					// Schedule the job
					threadPool.execute(job);
				} else {
					job.run();
				}
			}

			if (SliceState.terminated.equals(targetState)) {
				flowStore.delete(sliceId.toString());
			}
		}
	}

}
