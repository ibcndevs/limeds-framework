/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.impl.validation;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.ibcn.limeds.SegmentContext;
import org.ibcn.limeds.ServiceSegment;
import org.ibcn.limeds.TypeAdmin;
import org.ibcn.limeds.annotations.Validated;
import org.ibcn.limeds.aspects.Aspect;
import org.ibcn.limeds.descriptors.DocumentationDescriptors;
import org.ibcn.limeds.exceptions.ValidationException;
import org.ibcn.limeds.json.JsonValue;

/**
 * The ValidationAspect implements a validation system for LimeDS components
 * that have validation configured for either their input or output arguments.
 * 
 * @author wkerckho
 */
public class ValidationAspect implements Aspect {

	private SegmentContext context;
	private DocumentationDescriptors.Operation applyDoc;
	private Set<DocumentationDescriptors.Operation> operationDocs;
	private boolean validateInput = false;
	private boolean validateOutput = false;
	private TypeAdmin typeAdmin;

	public ValidationAspect(SegmentContext context, TypeAdmin typeAdmin) {
		this.context = context;
		this.typeAdmin = typeAdmin;
		validateInput = context.getDescriptor().getConfigureAspects().get(Validated.ASPECT_ID).getBool("validateInput");
		validateOutput = context.getDescriptor().getConfigureAspects().get(Validated.ASPECT_ID)
				.getBool("validateOutput");

		applyDoc = context.getDescriptor().getDocumentation().stream().filter(doc -> doc.getId().equals("apply"))
				.findFirst().get();
		/*
		 * If the Segment is a ServiceSegment (i.e. it contains aliases defining
		 * higher level operations on top of the apply function), we store this
		 * info in the operationDocs attribute.
		 */
		if (context.getInstance() instanceof ServiceSegment) {
			operationDocs = context.getDescriptor().getDocumentation().stream().filter(e -> !e.getId().equals("apply"))
					.collect(Collectors.toSet());
		}
	}

	private boolean matchesOp(DocumentationDescriptors.Operation op, JsonValue[] input) {
		String operation = input[0].asString();
		return operation.equals(op.getId()) && (input.length - 1) == op.getIn().size();
	}

	@Override
	public void init() {
		// Nothing here
	}

	@Override
	public void doBefore(JsonValue... input) throws Exception {
		if (validateInput) {
			if (!(context.getInstance() instanceof ServiceSegment)) {
				validateInput(applyDoc.getIn(), input);
			} else {
				// Validate operation
				typeAdmin.validate(applyDoc.getIn().get(0).getSchema(), input[0]);

				// Validate input args
				Optional<DocumentationDescriptors.Operation> op = operationDocs.stream()
						.filter(e -> matchesOp(e, input)).findFirst();
				if (op.isPresent()) {
					validateInput(op.get().getIn(), Arrays.stream(input).skip(1).toArray(size -> new JsonValue[size]));
				}
			}

		}
	}

	private void validateInput(List<DocumentationDescriptors.Input> doc, JsonValue[] args) throws Exception {
		for (int i = 0; i < doc.size(); i++) {
			try {
				typeAdmin.validate(doc.get(i).getSchema(), args[i]);
			} catch (Exception e) {
				throw new ValidationException("argument #" + (i + 1) + " is invalid", e);
			}
		}
	}

	@Override
	public void doAfter(JsonValue[] input, JsonValue output, Optional<Throwable> error) throws Exception {
		// No point in checking if the call failed
		if (validateOutput && !error.isPresent()) {
			try {
				if (!(context.getInstance() instanceof ServiceSegment)) {
					typeAdmin.validate(applyDoc.getOut().getSchema(), output);
				} else {
					typeAdmin.validate(operationDocs.stream().filter(e -> matchesOp(e, input)).findFirst().get()
							.getOut().getSchema(), output);
				}
			} catch (Exception e) {
				throw new ValidationException("Invalid output", e);
			}
		}
	}

	@Override
	public void destroy() {
		// Nothing here
	}

}
