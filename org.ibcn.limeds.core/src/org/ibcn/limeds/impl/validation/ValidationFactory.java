/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.impl.validation;

import java.util.Optional;

import org.ibcn.limeds.SegmentContext;
import org.ibcn.limeds.TypeAdmin;
import org.ibcn.limeds.annotations.Validated;
import org.ibcn.limeds.aspects.Aspect;
import org.ibcn.limeds.aspects.AspectFactory;
import org.ibcn.limeds.json.JsonValue;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

/**
 * This class provides an AspectFactory implementation for the creation of
 * ValidationAspect instances.
 * 
 * @author wkerckho
 *
 */
@Component(immediate = true)
public class ValidationFactory implements AspectFactory {

	@Reference
	private TypeAdmin typeAdmin;

	@Override
	public int getPriority() {
		return Integer.MIN_VALUE;
	}

	@Override
	public Optional<Aspect> createInstance(SegmentContext context) {
		JsonValue cfg = context.getDescriptor().getConfigureAspects().get(Validated.ASPECT_ID);
		if (cfg != null && (cfg.getBool("validateInput") || cfg.getBool("validateOutput"))
				&& context.getDescriptor().getDocumentation() != null
				&& !context.getDescriptor().getDocumentation().isEmpty()) {
			return Optional.of(new ValidationAspect(context, typeAdmin));
		}
		return Optional.empty();
	}

}
