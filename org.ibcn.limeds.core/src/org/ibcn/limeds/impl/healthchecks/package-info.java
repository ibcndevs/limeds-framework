/**
 * (Internal) This package contains the health check system implementation.
 */
package org.ibcn.limeds.impl.healthchecks;