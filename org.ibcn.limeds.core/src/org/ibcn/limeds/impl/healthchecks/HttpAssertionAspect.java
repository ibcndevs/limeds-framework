/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.impl.healthchecks;

import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.ibcn.limeds.aspects.Aspect;
import org.ibcn.limeds.descriptors.HttpAssertionDescriptor;
import org.ibcn.limeds.json.JsonValue;

/**
 * The HttpAssertionAspect implements a health check system for LimeDS
 * components that have valid HttpAssertion configurations, as defined by their
 * set of HttpAssertionDescriptors.
 */
public class HttpAssertionAspect implements Aspect {

	private Set<HttpAssertionDescriptor> httpAssertions;
	private Set<String> monitorIds;

	public HttpAssertionAspect(Set<HttpAssertionDescriptor> httpAssertions) {
		this.httpAssertions = httpAssertions;
	}

	@Override
	public void init() {
		monitorIds = httpAssertions.stream().map(HealthMonitor.activeInstance::register).collect(Collectors.toSet());
	}

	@Override
	public void doBefore(JsonValue... input) throws Exception {
		for (String monitorId : monitorIds) {
			HealthMonitor.activeInstance.check(monitorId);
		}
	}

	@Override
	public void doAfter(JsonValue[] input, JsonValue output, Optional<Throwable> error) throws Exception {
		// Do nothing
	}

	@Override
	public void destroy() {
		monitorIds.forEach(HealthMonitor.activeInstance::unregister);
	}

}
