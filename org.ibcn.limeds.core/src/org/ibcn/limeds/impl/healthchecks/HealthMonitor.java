/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.impl.healthchecks;

import java.util.Hashtable;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.ibcn.commons.restclient.api.Client;
import org.ibcn.commons.restclient.api.ClientContext;
import org.ibcn.commons.restclient.api.ClientReceiver;
import org.ibcn.commons.restclient.api.ClientResponse;
import org.ibcn.limeds.annotations.HttpMethod;
import org.ibcn.limeds.descriptors.HttpAssertionDescriptor;
import org.ibcn.limeds.exceptions.HttpAssertionFailedException;
import org.ibcn.limeds.json.Json;
import org.ibcn.limeds.json.JsonValue;
import org.osgi.framework.ServiceRegistration;
import org.osgi.service.component.ComponentContext;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

/**
 * HealthMonitor implements the default Health Check strategy for the system. It
 * supports checking the status of an external resource before each request that
 * is sent to it from LimeDS without taking to much of a toll on the external
 * resource (by employing a caching mechanism). When the external resource
 * cannot be resolved, a periodic job is scheduled that will poll the resource
 * until it comes back available again.
 * 
 * @author wkerckho
 *
 */
@Component(immediate = true)
public class HealthMonitor {

	static HealthMonitor activeInstance;
	private static final long REQUEST_WINDOW_MS = 5000;
	private static final long FIXED_PERIOD_MS = 1000;

	private static final Logger LOGGER = LoggerFactory.getLogger(HealthMonitor.class);

	private ComponentContext context;
	private Map<String, MonitorInstance> registry = new ConcurrentHashMap<>();
	private LoadingCache<MonitorInstance, Boolean> monitorCache;
	private ScheduledExecutorService taskQueue = Executors.newScheduledThreadPool(4);
	@Reference
	private Client restClient;

	@Activate
	public void start(ComponentContext context) {
		activeInstance = this;
		this.context = context;
		CacheLoader<MonitorInstance, Boolean> loader = new CacheLoader<MonitorInstance, Boolean>() {

			@Override
			public Boolean load(MonitorInstance instance) throws Exception {
				return !instance.isBroken() ? instance.check() : false;
			}
		};
		monitorCache = CacheBuilder.newBuilder().expireAfterWrite(REQUEST_WINDOW_MS, TimeUnit.MILLISECONDS)
				.build(loader);
	}

	@Deactivate
	public void stop() {
		activeInstance = null;
	}

	/**
	 * Register a HttpAssertion with the HealthMonitor system. The system will
	 * then monitor the external HTTP dependency and resolve the service
	 * dependency for the FunctionalSegment when the external dependency is
	 * available.
	 * 
	 * @param assertion
	 * @return A String-identifier which can be used to manually check or
	 *         unregister the assertion.
	 */
	public String register(HttpAssertionDescriptor assertion) {
		String id = UUID.randomUUID().toString();
		registry.put(id, registry.values().stream().filter(mi -> mi.getAssertion().equals(assertion)).findFirst()
				.orElseGet(() -> {
					MonitorInstance instance = new MonitorInstance(assertion);
					instance.check();
					return instance;
				}));
		return id;
	}

	/**
	 * Removes the HttpAssertion from the HealthMonitor system. The external
	 * dependency will no longer be checked and the service dependency of the
	 * FunctionalSegment will not be available.
	 * 
	 * @param assertionReference
	 */
	public void unregister(String assertionReference) {
		registry.remove(assertionReference);
	}

	public void check(String assertionReference) throws HttpAssertionFailedException {
		try {
			if (!monitorCache.get(registry.get(assertionReference))) {
				throw new HttpAssertionFailedException();
			}
		} catch (ExecutionException e) {
			throw new HttpAssertionFailedException(e);
		}
	}

	private static class MonitorInstance {

		private final HttpAssertionDescriptor assertion;
		private ServiceRegistration<HttpAssertionService> registration;

		public MonitorInstance(HttpAssertionDescriptor assertion) {
			this.assertion = assertion;
		}

		public HttpAssertionDescriptor getAssertion() {
			return assertion;
		}

		public boolean check() {
			// Do the health check
			if (activeInstance != null && activeInstance.assertTarget(assertion)) {
				// External endpoint up & running
				if (registration == null) {
					/*
					 * Register an OSGi service that will resolve Flow Function
					 * dependencies on the external system
					 */
					LOGGER.warn("(re)Connected with " + assertion.getTarget() + ", updating services");
					Hashtable<String, Object> properties = new Hashtable<>();
					properties.put("target.url", assertion.getTarget());
					properties.put("target.method", assertion.getMethod());

					registration = activeInstance.context.getBundleContext().registerService(HttpAssertionService.class,
							new HttpAssertionService() {
							}, properties);
				}
				return true;
			} else {
				// Failure detected
				if (registration != null) {
					/*
					 * Unregister the OSGi service that resolved the Flow
					 * Functions depending on the external system
					 */
					LOGGER.warn("Lost connection with " + assertion.getTarget() + ", updating services");
					registration.unregister();
					registration = null;
				}

				/*
				 * Schedule periodical iteration of check until the service is
				 * back up
				 */
				activeInstance.taskQueue.schedule(() -> check(), FIXED_PERIOD_MS, TimeUnit.MILLISECONDS);
				return false;
			}
		}

		public boolean isBroken() {
			return registration == null;
		}

	}

	private boolean assertTarget(HttpAssertionDescriptor descriptor) {
		ClientContext context = restClient.target(descriptor.getTarget());
		ClientReceiver receiver = null;
		try {
			if (HttpMethod.HEAD.equals(descriptor.getMethod())) {
				receiver = context.head();
			} else if (HttpMethod.GET.equals(descriptor.getMethod())) {
				receiver = context.get();
			} else if (HttpMethod.PUT.equals(descriptor.getMethod())) {
				receiver = context.putJson(descriptor.getRequest().toString());
			} else if (HttpMethod.POST.equals(descriptor.getMethod())) {
				receiver = context.postJson(descriptor.getRequest().toString());
			}

			if (receiver != null) {
				if (descriptor.getResponseValidator() != null) {
					ClientResponse<JsonValue> response = receiver.returnObject(Json.getDeserializer());
					return response.status() == 200;
				} else {
					ClientResponse<?> response = receiver.returnNoBody();
					return response.status() == 200;
				}
			}

		} catch (Exception e) {
			// Do nothing, check failed
			// TODO log!
		}
		return false;
	}

}
