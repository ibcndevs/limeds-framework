/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.impl.healthchecks;

import java.util.Optional;

import org.ibcn.limeds.aspects.AspectFactory;
import org.ibcn.limeds.SegmentContext;
import org.ibcn.limeds.aspects.Aspect;
import org.osgi.service.component.annotations.Component;

/**
 * This class provides an AspectFactory implementation for the creation of
 * HttpAssertionAspect instances.
 * 
 * @author wkerckho
 *
 */
@Component
public class HttpAssertionFactory implements AspectFactory {

	@Override
	public int getPriority() {
		return 0;
	}

	@Override
	public Optional<Aspect> createInstance(SegmentContext context) {
		if (context.getDescriptor().getHttpAssertions() != null
				&& context.getDescriptor().getHttpAssertions().size() > 0) {
			return Optional.of(new HttpAssertionAspect(context.getDescriptor().getHttpAssertions()));
		}
		return Optional.empty();
	}

}
