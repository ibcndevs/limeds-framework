/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.impl.issues;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.RandomAccess;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

import org.apache.commons.lang.CharSet;
import org.ibcn.limeds.IssueAdmin;
import org.ibcn.limeds.exceptions.SegmentExecutionException;
import org.ibcn.limeds.json.Json;
import org.ibcn.limeds.json.JsonArray;
import org.ibcn.limeds.json.JsonObject;
import org.ibcn.limeds.util.ErrorUtil;
import org.ibcn.limeds.util.Errors;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.metatype.annotations.Designate;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;
import org.slf4j.LoggerFactory;

import com.google.common.io.Files;

@ObjectClassDefinition(name = "LimeDS Issue Admin")
@interface IssueAdminConfig {
	boolean enable() default true;

	String outputFile() default "errors.log";
}

@Component(immediate = true)
@Designate(ocd = IssueAdminConfig.class)
public class DefaultIssueAdmin implements IssueAdmin {

	private static final String SEPARATOR = "|";

	private IssueAdminConfig config;
	private ExecutorService storeRunner;
	private File outputFile;

	@Activate
	public void start(IssueAdminConfig config) {
		this.config = config;
		storeRunner = Executors.newSingleThreadExecutor();
		outputFile = new File(config.outputFile());
	}

	@Deactivate
	public void stop() {
		storeRunner.shutdown();
		storeRunner = null;
	}

	@Override
	public void report(Throwable error, Class<?> context) {
		if (config.enable()) {
			Throwable relevantError = ErrorUtil.findRelevantError(error);
			storeRunner.execute(Errors.wrapRunnable(() -> {
				String entry = createEntry(context, relevantError);
				if (outputFile.exists()) {
					Files.append("\n" + entry, outputFile, Charset.forName("UTF-8"));
				} else {
					Files.write(entry, outputFile, Charset.forName("UTF-8"));
				}
			}));
		}
	}

	@Override
	public Collection<JsonObject> getReportedIssues() {
		try {
			return Files.readLines(outputFile, Charset.forName("UTF-8")).stream()
					.map(Errors.wrapFunction(this::readAsJson)).collect(Collectors.toList());
		} catch (Exception e) {
			LoggerFactory.getLogger(getClass()).warn("Error while processing issues log file...");
			return new LinkedList<>();
		}
	}

	@Override
	public Collection<JsonObject> getLastReportedIssues(int amount) {

		try {
			return readLastLines(outputFile, Charset.forName("UTF-8"), amount).stream()
					.map(Errors.wrapFunction(this::readAsJson)).collect(Collectors.toList());
		} catch (Exception e) {
			LoggerFactory.getLogger(getClass()).warn("Error while processing issues log file...");
			return new LinkedList<>();
		}
	}

	public String createEntry(Class<?> context, Throwable error) {
		LinkedList<SegmentExecutionException> trace = new LinkedList<>();
		buildTrace(trace, error);
		SegmentExecutionException origin = trace.pollLast();
		// timestamp;source;type;message;location;trace
		String result = System.currentTimeMillis() + SEPARATOR + origin.getContext().getDescriptor().toString()
				+ SEPARATOR + ErrorUtil.findRelevantError(origin.getCause()).getClass().getName() + SEPARATOR
				+ ErrorUtil.findRelevantError(origin.getCause()).getMessage() + SEPARATOR + origin.getLocation();
		Collections.reverse(trace);
		String traceJson = trace.stream().map(se -> Json.objectBuilder()
				.add("segmentId", se.getContext().getDescriptor().toString()).add("location", se.getLocation()).build())
				.collect(Json.toArray()).toString();
		return result + SEPARATOR + traceJson;
	}

	public JsonObject readAsJson(String csvEntry) throws Exception {
		String[] parts = csvEntry.split("\\" + SEPARATOR);
		JsonObject result = new JsonObject();
		result.put("timestamp", parts[0]);
		result.put("source", parts[1]);
		result.put("type", parts[2]);
		result.put("message", parts[3]);
		result.put("location", parts[4]);
		result.put("trace", (JsonArray) Json.from(parts[5]));
		return result;
	}

	private void buildTrace(List<SegmentExecutionException> trace, Throwable error) {
		if (error instanceof SegmentExecutionException) {
			trace.add((SegmentExecutionException) error);
		}

		if (error.getCause() != null) {
			buildTrace(trace, error.getCause());
		}
	}

	@Override
	public void clearReportedIssues() {
		try {
			java.nio.file.Files.deleteIfExists(outputFile.toPath());
		} catch (IOException e) {
			LoggerFactory.getLogger(getClass()).warn("Error while clearing issues log file...");
		}
	}
	
	private List<String> readLastLines(File file, Charset charset, int lines) {
		java.io.RandomAccessFile fileHandler = null;
		try {
			fileHandler = new java.io.RandomAccessFile(file, "r");
			long fileLength = fileHandler.length() - 1;
			StringBuilder sb = new StringBuilder();
			int line = 0;

			for (long filePointer = fileLength; filePointer != -1; filePointer--) {
				fileHandler.seek(filePointer);
				int readByte = fileHandler.readByte();

				if (readByte == 0xA) { // \n
					if (filePointer < fileLength) {
						line = line + 1;
					}
				} else if (readByte == 0xD) { // \r
					if (filePointer < fileLength - 1) {
						line = line + 1;
					}
				}
				if (line >= lines) {
					break;
				}
				sb.append((char) readByte);
			}

			String lastLine = sb.reverse().toString();
			return Arrays.asList(lastLine.split("\n"));
		} catch (java.io.FileNotFoundException e) {
			return new LinkedList<String>();
		} catch (java.io.IOException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (fileHandler != null) {
				try {
					fileHandler.close();
				} catch (IOException e) {
				}
			}
		}
	}

}
