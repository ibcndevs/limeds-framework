/**
 * This package contains the event bus implementation.
 */
@org.osgi.annotation.versioning.Version("0.1.0")
package org.ibcn.limeds.impl.eventbus;