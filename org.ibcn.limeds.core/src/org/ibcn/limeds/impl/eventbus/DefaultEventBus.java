/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.impl.eventbus;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.ibcn.limeds.EventBus;
import org.ibcn.limeds.FunctionalSegment;
import org.ibcn.limeds.IssueAdmin;
import org.ibcn.limeds.ServicePropertyNames;
import org.ibcn.limeds.json.JsonValue;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.component.annotations.ReferencePolicy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.SetMultimap;

@Component
public class DefaultEventBus implements EventBus {

	private static final Logger LOGGER = LoggerFactory.getLogger(DefaultEventBus.class);

	private ExecutorService threadPool = Executors.newSingleThreadExecutor();
	private SetMultimap<String, FunctionalSegment> registry = HashMultimap.create();
	private IssueAdmin issueAdmin;

	@Reference(cardinality = ReferenceCardinality.MANDATORY, policy = ReferencePolicy.DYNAMIC)
	public void bindIssueAdmin(IssueAdmin issueAdmin) {
		this.issueAdmin = issueAdmin;
	}

	public void unbindIssueAdmin(IssueAdmin issueAdmin) {
		if (this.issueAdmin == issueAdmin) {
			this.issueAdmin = null;
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Reference(cardinality = ReferenceCardinality.MULTIPLE, policy = ReferencePolicy.DYNAMIC, target = "("
			+ ServicePropertyNames.SEGMENT_SUBSCRIPTIONS + "=*)")
	public void bindHandler(FunctionalSegment function, Map properties) {
		Set<String> subscriptions = (Set<String>) properties.get(ServicePropertyNames.SEGMENT_SUBSCRIPTIONS);
		subscriptions.forEach(channelID -> registry.put(channelID, function));
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void unbindHandler(FunctionalSegment function, Map properties) {
		Set<String> subscriptions = (Set<String>) properties.get(ServicePropertyNames.SEGMENT_SUBSCRIPTIONS);
		subscriptions.forEach(channelID -> registry.get(channelID).remove(function));
	}

	@Override
	public void broadcast(String channelID, JsonValue message) {
		threadPool.execute(() -> registry.get(channelID).forEach(handler -> {
			try {
				handler.apply(message);
			} catch (Exception e) {
				issueAdmin.report(e, DefaultEventBus.class);
				LOGGER.warn("Handler generated exception while processing event.", e);
			}
		}));
	}

}
