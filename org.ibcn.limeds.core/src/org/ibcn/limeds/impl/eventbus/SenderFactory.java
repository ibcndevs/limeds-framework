/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.impl.eventbus;

import org.ibcn.limeds.EventBus;
import org.ibcn.limeds.FunctionalSegment;
import org.ibcn.limeds.annotations.Configurable;
import org.ibcn.limeds.annotations.Segment;
import org.ibcn.limeds.annotations.Service;
import org.ibcn.limeds.json.JsonValue;

@Segment(id = "limeds.events.SenderFactory", factory = true)
public class SenderFactory implements FunctionalSegment {

	@Configurable(description = "The channel where the event should be published on.")
	private String channelID;

	@Service
	private EventBus eventBus;

	@Override
	public JsonValue apply(JsonValue... input) throws Exception {
		if (input.length == 1) {
			eventBus.broadcast(channelID, input[0]);
			return null;
		} else {
			throw new IllegalArgumentException(
					"The event sender expects a single parameter, i.e. the event message Json value. Found "
							+ input.length + " arguments!");
		}
	}

}
