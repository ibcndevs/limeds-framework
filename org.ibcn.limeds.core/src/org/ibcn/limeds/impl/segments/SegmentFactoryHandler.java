/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.impl.segments;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Collections;
import java.util.Dictionary;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.ibcn.limeds.SegmentAdmin;
import org.ibcn.limeds.descriptors.DependencyDescriptor;
import org.ibcn.limeds.descriptors.SegmentDescriptor;
import org.ibcn.limeds.descriptors.SegmentLanguage;
import org.ibcn.limeds.descriptors.TemplateDescriptor;
import org.ibcn.limeds.descriptors.TemplateDescriptor.ConfigProperty;
import org.ibcn.limeds.impl.slices.SegmentAdapter;
import org.ibcn.limeds.json.Json;
import org.ibcn.limeds.util.ConfigUtil;
import org.ibcn.limeds.util.Errors;
import org.osgi.framework.BundleContext;
import org.osgi.service.cm.ConfigurationException;
import org.osgi.service.cm.ManagedServiceFactory;
import org.osgi.service.metatype.MetaTypeProvider;
import org.osgi.service.metatype.ObjectClassDefinition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import aQute.bnd.annotation.headers.RequireCapability;

/**
 * The SegmentFactoryHandler takes of all the OSGi related handling & processing
 * required for the LimeDS component factory mechanism to work.
 * 
 * @author wkerckho
 *
 */
@RequireCapability(ns = "osgi.service", filter = "(objectClass=org.ibcn.limeds.config.LimeDSPersistenceManager)", effective = "resolve")
public class SegmentFactoryHandler extends DependencyActivatorBase implements MetaTypeProvider, ManagedServiceFactory {

	private static final Logger LOGGER = LoggerFactory.getLogger(SegmentFactoryHandler.class);

	private SegmentDescriptor descriptor;
	private Object instance;
	private SegmentAdmin componentManager;

	private Map<String, SegmentDescriptor> registeredInstances = new HashMap<>();
	private BundleContext context;

	public SegmentFactoryHandler(SegmentDescriptor descriptor, Object instance, SegmentAdmin componentManager) {
		this.descriptor = descriptor;
		this.instance = instance;
		this.componentManager = componentManager;
	}

	@Override
	public void init(BundleContext context, DependencyManager manager) throws Exception {
		this.context = context;

		Dictionary<String, Object> properties = new Hashtable<>();
		properties.put("service.pid", descriptor.getId());
		properties.put("metatype.factory.pid", descriptor.getId());
		manager.add(manager.createComponent()
				.setInterface(new String[] { MetaTypeProvider.class.getName(), ManagedServiceFactory.class.getName() },
						properties)
				.setImplementation(this));
	}

	@Override
	public ObjectClassDefinition getObjectClassDefinition(String id, String locale) {
		if (descriptor.getId().equals(id)) {
			return MetaTypeProviderImpl.configToMetaType(descriptor);
		} else {
			return null;
		}
	}

	@Override
	public String[] getLocales() {
		return null;
	}

	@Override
	public String getName() {
		return descriptor.getId();
	}

	@Override
	public void updated(String pid, Dictionary<String, ?> properties) throws ConfigurationException {
		try {
			// If a config already exists, delete it first
			deleted(pid);

			SegmentDescriptor newDescriptor = deriveDescriptorFromTemplate(properties);
			Object newInstance = null;
			if (SegmentLanguage.Java.isEqual(descriptor.getLanguage())) {
				newInstance = instance.getClass().newInstance();

				// Prepare handlerClass for dependency injection
				newDescriptor.getDependencies().forEach(Errors.wrapBiConsumer((depId, depDesc) -> {
					Field injectionField = instance.getClass().getDeclaredField(depId);
					injectionField.setAccessible(true);
					depDesc.setInstanceMirror(injectionField);
				}));
			} else {
				newInstance = new SegmentAdapter(newDescriptor,
						((SegmentAdapter) instance).getScriptWrapper().newInstance());
			}

			// Process configurable links
			Map<String, DependencyDescriptor> dependencies = newDescriptor.getDependencies();
			Collections.list(properties.keys()).stream()
					.filter(key -> key.startsWith(TemplateDescriptor.DYNAMIC_DEPENDENCY_PREFIX))
					.forEach(key -> dependencies
							.get(key.substring(TemplateDescriptor.DYNAMIC_DEPENDENCY_PREFIX.length()))
							.setPropertyFilter((String) properties.get(key)));

			componentManager.register(newDescriptor, newInstance, context);

			synchronized (registeredInstances) {
				registeredInstances.put(pid, newDescriptor);
			}
		} catch (Exception e) {
			LOGGER.warn("Could not register FunctionalSegment factory instance with id " + pid, e);
		}
	}

	private SegmentDescriptor deriveDescriptorFromTemplate(Dictionary<String, ?> properties) throws Exception {
		SegmentDescriptor newDescriptor = Json.to(SegmentDescriptor.class, Json.from(descriptor));
		newDescriptor.setId((String) properties.get(TemplateDescriptor.FUNCTION_ID_PROPERTY));
		boolean export = properties.get(TemplateDescriptor.EXPORT_PROPERTY) != null
				? Boolean.parseBoolean(properties.get(TemplateDescriptor.EXPORT_PROPERTY).toString()) : true;
		newDescriptor.setExport(export);
		Map<String, ConfigProperty> propToDesc = newDescriptor.getTemplateInfo().getConfiguration().stream()
				.collect(Collectors.toMap(e -> e.name,
						e -> new ConfigProperty(e.name, e.type, e.value, e.description, e.possibleValues)));

		newDescriptor.getTemplateInfo().getConfiguration().clear();

		Enumeration<String> keys = properties.keys();
		while (keys.hasMoreElements()) {
			String key = keys.nextElement();
			if (!ConfigUtil.IGNORE_LIST.contains(key)) {
				Object value = properties.get(key);
				if (propToDesc.containsKey(key)) {
					ConfigProperty cp = propToDesc.get(key);
					if (value != null) {
						cp.value = value.getClass().isArray() ? stripBrackets(Arrays.toString((Object[]) value))
								: "" + value;
					}
					newDescriptor.getTemplateInfo().getConfiguration().add(cp);
				}
			}
		}

		newDescriptor.getTemplateInfo().setFactoryId(descriptor.getId());
		newDescriptor.getTemplateInfo().setInstanceId((String) properties.get("service.pid"));
		newDescriptor.getTemplateInfo().setType(TemplateDescriptor.Type.FACTORY_INSTANCE);
		return newDescriptor;
	}

	@Override
	public void deleted(String pid) {
		synchronized (registeredInstances) {
			if (registeredInstances.containsKey(pid)) {
				try {
					componentManager.unregister(registeredInstances.get(pid), context);
				} catch (Exception e) {
					LOGGER.warn("Could not unregister FunctionalSegment factory instace with id " + pid, e);
				}
			}
		}
	}

	private String stripBrackets(String arrayRepr) {
		return arrayRepr.substring(1, arrayRepr.length() - 1);
	}

}
