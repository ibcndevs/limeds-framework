/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.impl.segments;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;

import org.ibcn.limeds.descriptors.SegmentDescriptor;
import org.ibcn.limeds.descriptors.TemplateDescriptor;
import org.osgi.service.metatype.AttributeDefinition;
import org.osgi.service.metatype.MetaTypeProvider;
import org.osgi.service.metatype.ObjectClassDefinition;

public class MetaTypeProviderImpl implements MetaTypeProvider {

	private SegmentDescriptor descriptor;

	public MetaTypeProviderImpl(SegmentDescriptor descriptor) {
		this.descriptor = descriptor;
	}

	/**
	 * Provides the metatype information (configuration properties) for this
	 * FunctionalSegment. This information is used to allow existing OSGi
	 * services (such as the Felix Webconsole) to configure the
	 * FunctionalSegment. This method can only be called if the
	 * FunctionalSegment is a configurable instance.
	 */
	@Override
	public ObjectClassDefinition getObjectClassDefinition(String id, String locale) {
		if (descriptor.getId().equals(id) && descriptor.getTemplateInfo() != null
				&& TemplateDescriptor.Type.CONFIGURABLE_INSTANCE.equals(descriptor.getTemplateInfo().getType())) {
			return configToMetaType(descriptor);
		}
		return null;
	}

	@Override
	public String[] getLocales() {
		return null;
	}

	public static ObjectClassDefinition configToMetaType(SegmentDescriptor descriptor) {
		return new ObjectClassDefinition() {

			@Override
			public String getName() {
				return descriptor.getId();
			}

			@Override
			public InputStream getIcon(int size) throws IOException {
				return null;
			}

			@Override
			public String getID() {
				return null;
			}

			@Override
			public String getDescription() {
				return null;
			}

			@Override
			public AttributeDefinition[] getAttributeDefinitions(int filter) {
				if (filter == OPTIONAL) {
					return null;
				} else {
					return descriptor.getTemplateInfo().getConfiguration().stream().map(p -> new AttributeDefinition() {

						@Override
						public String getName() {
							return p.name;
						}

						@Override
						public String getID() {
							return p.name;
						}

						@Override
						public String getDescription() {
							return p.description;
						}

						@Override
						public int getCardinality() {
							return p.type.endsWith("[]") ? Integer.MAX_VALUE : 0;
						}

						@Override
						public int getType() {
							if ("int".equals(p.type) || Integer.class.getName().equals(p.type)
									|| Integer[].class.getTypeName().equals(p.type)) {
								return AttributeDefinition.INTEGER;
							} else if ("long".equals(p.type) || Long.class.getName().equals(p.type)
									|| Long[].class.getName().equals(p.type)) {
								return AttributeDefinition.LONG;
							} else if ("double".equals(p.type) || Double.class.getName().equals(p.type)
									|| Double[].class.getTypeName().equals(p.type)) {
								return AttributeDefinition.DOUBLE;
							} else if ("float".equals(p.type) || Float.class.getName().equals(p.type)
									|| Float[].class.getTypeName().equals(p.type)) {
								return AttributeDefinition.FLOAT;
							} else if ("boolean".equals(p.type) || Boolean.class.getName().equals(p.type)
									|| Boolean[].class.getTypeName().equals(p.type)) {
								return AttributeDefinition.BOOLEAN;
							} else if (String.class.getName().equals(p.type)
									|| String[].class.getTypeName().equals(p.type)) {
								return AttributeDefinition.STRING;
							}
							return 1;
						}

						@Override
						public String[] getOptionValues() {
							return p.possibleValues;
						}

						@Override
						public String[] getOptionLabels() {
							return p.possibleValues;
						}

						@Override
						public String validate(String value) {
							return null;
						}

						@Override
						public String[] getDefaultValue() {
							if (getCardinality() > 0) {
								return p.value != null && !p.value.isEmpty() ? Arrays.stream(p.value.split(","))
										.map(s -> s.trim()).toArray(size -> new String[size]) : null;
							} else {
								return p.value != null && !p.value.isEmpty() ? new String[] { p.value } : null;
							}
						}
					}).toArray(size -> new AttributeDefinition[size]);
				}
			}
		};
	}

}
