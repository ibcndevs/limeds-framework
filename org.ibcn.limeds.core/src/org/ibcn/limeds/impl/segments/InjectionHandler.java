/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.impl.segments;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import org.ibcn.limeds.FunctionalSegment;
import org.ibcn.limeds.ServicePropertyNames;
import org.ibcn.limeds.descriptors.DependencyDescriptor;
import org.ibcn.limeds.util.Errors;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Constants;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceReference;

/**
 * InjectionHandler is a class dedicated to the injection of OSGi services or
 * Flow Function instances as dependencies in the configured variables for a
 * specified component implementation.
 * 
 * @author wkerckho
 *
 */
public class InjectionHandler {

	private static final BundleContext context = FrameworkUtil.getBundle(InjectionHandler.class).getBundleContext();

	public static final String ADDED = "bind";
	public static final String REMOVED = "unbind";

	private FunctionalSegment serviceInstance;
	private DependencyDescriptor descriptor;
	private Field instanceField;

	private String dependencyId;

	public InjectionHandler(String dependencyId, DependencyDescriptor descriptor, FunctionalSegment serviceInstance) {
		this.dependencyId = dependencyId;
		this.serviceInstance = serviceInstance;
		this.descriptor = descriptor;
		try {
			instanceField = DependencyDescriptor.class.getDeclaredField("instance");
		} catch (Exception e) {
			e.printStackTrace();
		}
		instanceField.setAccessible(true);
	}

	public synchronized void bind(ServiceReference<?> ref, Object o) throws Exception {
		try {
			String instanceId = (String) ref.getProperty(ServicePropertyNames.SEGMENT_ID);
			Long serviceId = (Long) ref.getProperty(Constants.SERVICE_ID);
			Tuple[] dependencyContainers = descriptor.getInstanceMirror() != null
					? new Tuple[] { new Tuple(descriptor, instanceField),
							new Tuple(serviceInstance, descriptor.getInstanceMirror()) }
					: new Tuple[] { new Tuple(descriptor, instanceField) };
			Optional<Long> boundId = bindToField(o, dependencyContainers, instanceId, serviceId);
			boundId.ifPresent(id -> {
				descriptor.getBoundServiceIds().add(id);
				serviceInstance.bind(dependencyId, descriptor, o);
			});
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@SuppressWarnings("unchecked")
	/*
	 * Binds the specified service instance to the specified fields. Returns the
	 * id of the service if it was effectively bound.
	 */
	private Optional<Long> bindToField(Object o, Tuple[] dependencyContainers, String instanceId, Long serviceId)
			throws Exception {
		if (descriptor.isCollection()) {
			Arrays.stream(dependencyContainers).forEach(Errors.wrapConsumer(dependencyContainer -> {
				if (dependencyContainer.field.get(dependencyContainer.instance) == null) {
					dependencyContainer.field.set(dependencyContainer.instance, new HashSet<Object>());
				}
				Set<Object> list = (Set<Object>) dependencyContainer.field.get(dependencyContainer.instance);
				list.add(o);
			}));
			return Optional.of(serviceId);
		} else if (descriptor.hasPreferredBinding()) {
			if (descriptor.getBoundServiceIds().isEmpty()) {
				/*
				 * No service is bound, so the new instance is always bounded!
				 */
				Arrays.stream(dependencyContainers).forEach(Errors.wrapConsumer(dependencyContainer -> {
					dependencyContainer.field.set(dependencyContainer.instance, o);
				}));
				return Optional.of(serviceId);
			} else if (!descriptor.getPreferredId().equals(instanceId)) {
				/*
				 * Since the new instance is not preferred and instances are
				 * already bound, we add this one to the alternative list!
				 */
				descriptor.getAlternativeServiceIds().add(serviceId);
				return Optional.empty();
			} else {
				/*
				 * The new instance is the preferred service, so we bind it and
				 * move the previously bound service(s) to the list of
				 * alternatives!
				 */
				descriptor.getAlternativeServiceIds().addAll(descriptor.getBoundServiceIds());
				descriptor.getBoundServiceIds().clear();
				Arrays.stream(dependencyContainers).forEach(Errors.wrapConsumer(dependencyContainer -> {
					dependencyContainer.field.set(dependencyContainer.instance, o);
				}));
				return Optional.of(serviceId);
			}
		} else {
			Arrays.stream(dependencyContainers).forEach(Errors.wrapConsumer(dependencyContainer -> {
				try {
					dependencyContainer.field.set(dependencyContainer.instance, o);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}));
			return Optional.of(serviceId);
		}
	}

	public synchronized void unbind(ServiceReference<?> ref, Object o) throws Exception {
		String instanceId = (String) ref.getProperty(ServicePropertyNames.SEGMENT_ID);
		Long serviceId = (Long) ref.getProperty(Constants.SERVICE_ID);
		Tuple[] dependencyContainers = descriptor.getInstanceMirror() != null ? new Tuple[] {
				new Tuple(serviceInstance, descriptor.getInstanceMirror()), new Tuple(descriptor, instanceField) }
				: new Tuple[] { new Tuple(descriptor, instanceField) };
		Optional<Long> unboundId = unbindFromField(o, dependencyContainers, instanceId, serviceId);
		unboundId.ifPresent(id -> {
			descriptor.getBoundServiceIds().remove((Long) ref.getProperty(Constants.SERVICE_ID));
			serviceInstance.unbind(dependencyId, descriptor, o);
		});
	}

	@SuppressWarnings("unchecked")
	/*
	 * Unbinds the specified service instance to the specified field. Returns
	 * the id of the service if it was effectively unbound.
	 */
	private Optional<Long> unbindFromField(Object o, Tuple[] dependencyContainers, String instanceId, Long serviceId)
			throws Exception {
		if (descriptor.isCollection()) {
			Arrays.stream(dependencyContainers).forEach(Errors.wrapConsumer(dependencyContainer -> {
				Set<Object> list = (Set<Object>) dependencyContainer.field.get(dependencyContainer.instance);
				list.remove(o);
			}));
			return Optional.of(serviceId);
		} else if (descriptor.hasPreferredBinding()) {
			if (descriptor.getBoundServiceIds().contains(serviceId)) {
				/*
				 * This service is currently bound, remove it and find an
				 * alternative!
				 */
				Optional<Long> altServiceId = descriptor.getAlternativeServiceIds().stream().findAny();
				Optional<Object> altService = altServiceId.isPresent()
						? getServiceInstance(descriptor.getType(), altServiceId.get()) : Optional.empty();
				Arrays.stream(dependencyContainers).forEach(Errors.wrapConsumer(dependencyContainer -> {
					dependencyContainer.field.set(dependencyContainer.instance,
							altService.isPresent() ? altService.get() : null);
				}));
				if (altService.isPresent()) {
					descriptor.getBoundServiceIds().add(altServiceId.get());
				}
				return Optional.of(serviceId);
			} else if (descriptor.getAlternativeServiceIds().contains(serviceId)) {
				/*
				 * This service is not currently bound, but is in the
				 * alternatives list. Remove from the alternatives list!
				 */
				descriptor.getAlternativeServiceIds().remove(serviceId);
			}
			return Optional.empty();
		} else {
			Arrays.stream(dependencyContainers).forEach(Errors.wrapConsumer(dependencyContainer -> {
				dependencyContainer.field.set(dependencyContainer.instance, null);
			}));
			return Optional.of(serviceId);
		}
	}

	public static Optional<Object> getServiceInstance(String className, Long serviceId) {
		try {
			ServiceReference<?>[] references = context.getServiceReferences(className,
					"(service.id=" + serviceId + ")");
			if (references.length > 0) {
				return Optional.of(context.getService(references[0]));
			}
		} catch (InvalidSyntaxException e) {
			// Ignore
		}
		return Optional.empty();
	}

	private static class Tuple {
		public final Object instance;
		public final Field field;

		Tuple(Object instance, Field field) {
			this.instance = instance;
			this.field = field;
		}

	}

}
