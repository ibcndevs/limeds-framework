/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.impl.segments;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.ibcn.limeds.FunctionalSegment;
import org.ibcn.limeds.SegmentAdmin;
import org.ibcn.limeds.ServicePropertyNames;
import org.ibcn.limeds.aspects.AspectFactory;
import org.ibcn.limeds.descriptors.SegmentDescriptor;
import org.ibcn.limeds.descriptors.TemplateDescriptor;
import org.ibcn.limeds.impl.http.HttpManager;
import org.ibcn.limeds.json.Json;
import org.ibcn.limeds.json.JsonArray;
import org.ibcn.limeds.json.JsonObject;
import org.ibcn.limeds.util.Errors;
import org.ibcn.limeds.versioning.VersionUtil;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Filter;
import org.osgi.framework.FrameworkUtil;
import org.osgi.service.cm.ConfigurationAdmin;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.component.annotations.ReferencePolicy;
import org.slf4j.LoggerFactory;

/**
 * DefaultSegmentAdmin implements the LimeDS SegmentAdmin interface. Its main
 * task is to take ComponentDescriptors in combination with their implementing
 * instances and setup the necessary handler objects in order to accomplish the
 * dynamic LimeDS runtime environment.
 * 
 * @author wkerckho
 *
 */
@Component
public class DefaultSegmentAdmin implements SegmentAdmin {

	public static DefaultSegmentAdmin INSTANCE;

	private ExecutorService tasker = Executors.newSingleThreadExecutor();

	private Map<SegmentDescriptor, SegmentHandler> managedComponents = new HashMap<>();
	private Map<SegmentDescriptor, SegmentFactoryHandler> managedComponentFactories = new HashMap<>();
	@Reference(cardinality = ReferenceCardinality.MANDATORY)
	private volatile HttpManager httpManager;
	@Reference(cardinality = ReferenceCardinality.MANDATORY)
	private volatile ConfigurationAdmin configAdmin;
	private Comparator<AspectFactory> comparator = Comparator.comparing(af -> af.getPriority() + af.getClass().getName());
	private Set<AspectFactory> registeredComponentAspects = new TreeSet<>(comparator);

	@Activate
	public void start() {
		INSTANCE = this;
	}

	@Deactivate
	public void stop() {
		INSTANCE = null;
	}

	public HttpManager getHttpManager() {
		return httpManager;
	}

	public ConfigurationAdmin getConfigAdmin() {
		return configAdmin;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.ibcn.limeds.flowmanager.ComponentManager#addComponentAspect(org.ibcn
	 * .limeds.api.dataflow.aspects.ComponentAspect)
	 */
	@Override
	@Reference(cardinality = ReferenceCardinality.MULTIPLE, policy = ReferencePolicy.DYNAMIC)
	public void addAspectFactory(AspectFactory aspectFactory) throws Exception {
		tasker.execute(() -> {
			registeredComponentAspects.add(aspectFactory);
			managedComponents.values().forEach(Errors.wrapConsumer((SegmentHandler h) -> h.notifyAspectAdded(registeredComponentAspects)));
		});
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.ibcn.limeds.flowmanager.ComponentManager#removeComponentAspect(org
	 * .ibcn.limeds.api.dataflow.aspects.ComponentAspect)
	 */
	@Override
	public void removeAspectFactory(AspectFactory aspectFactory) {
		tasker.execute(() -> {
			registeredComponentAspects.remove(aspectFactory);
			managedComponents.values().forEach(Errors.wrapConsumer((SegmentHandler h) -> h.notifyAspectRemoved(registeredComponentAspects)));
		});
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.ibcn.limeds.flowmanager.ComponentManager#getRegisteredAspects()
	 */
	@Override
	public Set<AspectFactory> getRegisteredAspects() {
		return registeredComponentAspects;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.ibcn.limeds.flowmanager.ComponentManager#register(org.ibcn.limeds
	 * .api.dataflow.descriptors.ComponentDescriptor, java.lang.Object,
	 * org.osgi.framework.BundleContext)
	 */
	@Override
	public void register(SegmentDescriptor descriptor, Object instance, BundleContext context) throws Exception {
		Future<?> future = tasker.submit(Errors.wrapRunnable(() -> {
			if (descriptor.getTemplateInfo() != null
					&& TemplateDescriptor.Type.FACTORY.equals(descriptor.getTemplateInfo().getType())) {
				// Descriptor specifies a component factory
				SegmentFactoryHandler factoryHandler = new SegmentFactoryHandler(descriptor, instance, this);
				SegmentFactoryHandler previous = managedComponentFactories.put(descriptor, factoryHandler);
				if (previous != null) {
					previous.stop(context);
				}
				factoryHandler.start(context);
			} else {
				// Descriptor specifies a regular component
				SegmentHandler handler = new SegmentHandler(descriptor, (FunctionalSegment) instance, this);
				SegmentHandler previous = managedComponents.put(descriptor, handler);
				if (previous != null) {
					previous.stop(context);
				}
				handler.start(context);
			}
		}));
		future.get();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.ibcn.limeds.flowmanager.ComponentManager#unregister(org.ibcn.limeds
	 * .api.dataflow.descriptors.ComponentDescriptor,
	 * org.osgi.framework.BundleContext)
	 */
	@Override
	public void unregister(SegmentDescriptor descriptor, BundleContext context) throws Exception {
		Future<?> future = tasker.submit(Errors.wrapRunnable(() -> {
			if (descriptor.getTemplateInfo() != null
					&& TemplateDescriptor.Type.FACTORY.equals(descriptor.getTemplateInfo().getType())) {
				// Descriptor specifies a component factory
				SegmentFactoryHandler handler = managedComponentFactories.remove(descriptor);
				if (handler != null) {
					handler.stop(context);
				}
			} else {
				// Descriptor specifies a regular component
				SegmentHandler handler = managedComponents.remove(descriptor);
				if (handler != null) {
					handler.stop(context);
				}
			}
		}));
		future.get();
	}

	@Override
	public Optional<JsonObject> getSegmentInfo(String componentId, String version) {
		return managedComponents.keySet().stream()
				.filter(d -> componentId.equals(d.getId()) && version.equals(d.getVersion()))
				.map(d -> managedComponents.get(d).getMetaData()).findFirst();
	}

	@Override
	public JsonArray getRegisteredHttpOperations() {
		return httpManager.getMetaData();
	}

	@Override
	public JsonArray getSegmentsInfo() {
		return managedComponents.values().stream().map(v -> v.getMetaData()).collect(Json.toArray());
	}

	@Override
	public Set<SegmentDescriptor> getRegisteredSegments() {
		return managedComponents.keySet();
	}

	@Override
	public JsonArray getSegmentFactoriesInfo() {
		return managedComponentFactories.keySet().stream().map(c -> Json.from(c)).collect(Json.toArray());
	}

	@Override
	public JsonObject getSegmentFactoryInfo(String componentId, String version) {
		return managedComponentFactories.keySet().stream()
				.filter(d -> componentId.equals(d.getId()) && version.equals(d.getVersion())).map(c -> Json.from(c))
				.findFirst().orElse(null);
	}

	@Override
	public Optional<SegmentDescriptor> findRegisteredSegment(String componentId, String versionExpr) {
		if ("latest".equals(versionExpr)) {
			return getRegisteredSegments().stream().filter(segment -> componentId.equals(segment.getId()))
					.sorted(segmentSortVersionDescending).findFirst();
		} else if (versionExpr.contains("*") || versionExpr.contains("^")) {
			try {
				Filter versionFilter = FrameworkUtil
						.createFilter(VersionUtil.createFilter(ServicePropertyNames.SEGMENT_VERSION, versionExpr));
				return getRegisteredSegments().stream().filter(segment -> componentId.equals(segment.getId()))
						.filter(segment -> {
							Map<String, String> versionProperty = new HashMap<>();
							versionProperty.put(ServicePropertyNames.SEGMENT_VERSION,
									VersionUtil.createVersionPropertyValue(segment.getVersion()));
							return versionFilter.matches(versionProperty);
						}).sorted(segmentSortVersionDescending).findFirst();
			} catch (Exception e) {
				LoggerFactory.getLogger(getClass()).warn("Invalid versionExpr for findRegisteredSegment request!", e);
				return Optional.empty();
			}
		} else {
			return getRegisteredSegments().stream()
					.filter(segment -> componentId.equals(segment.getId()) && versionExpr.equals(segment.getVersion()))
					.findFirst();
		}
	}

	private static Comparator<SegmentDescriptor> segmentSortVersionDescending = (s1, s2) -> VersionUtil
			.createVersionPropertyValue(s2.getVersion())
			.compareTo(VersionUtil.createVersionPropertyValue(s1.getVersion()));

	@Override
	public SegmentDescriptor findDescriptor(FunctionalSegment segment) {
		return managedComponents.entrySet().stream().filter(e -> e.getValue().getContext().getInstance() == segment)
				.map(e -> e.getKey()).findFirst().get();
	}

}
