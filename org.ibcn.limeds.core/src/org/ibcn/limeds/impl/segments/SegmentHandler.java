/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.impl.segments;

import java.lang.reflect.Proxy;
import java.util.Dictionary;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Set;

import org.apache.felix.dm.Component;
import org.apache.felix.dm.ComponentDeclaration;
import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.apache.felix.dm.ServiceDependency;
import org.ibcn.limeds.FunctionalSegment;
import org.ibcn.limeds.SegmentContext;
import org.ibcn.limeds.ServicePropertyNames;
import org.ibcn.limeds.api.API_Info;
import org.ibcn.limeds.aspects.AspectFactory;
import org.ibcn.limeds.descriptors.DependencyDescriptor;
import org.ibcn.limeds.descriptors.HttpAssertionDescriptor;
import org.ibcn.limeds.descriptors.SegmentDescriptor;
import org.ibcn.limeds.descriptors.TemplateDescriptor;
import org.ibcn.limeds.impl.healthchecks.HttpAssertionService;
import org.ibcn.limeds.json.Json;
import org.ibcn.limeds.json.JsonObject;
import org.ibcn.limeds.util.ConfigUtil;
import org.ibcn.limeds.util.Errors;
import org.ibcn.limeds.util.FilterUtil;
import org.ibcn.limeds.util.Pair;
import org.ibcn.limeds.versioning.VersionUtil;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Constants;
import org.osgi.service.metatype.MetaTypeProvider;

/**
 * The SegmentHandler takes of all the OSGi related handling & processing
 * required for the LimeDS component dynamic behaviour to work (life cycle
 * management, dependency management, etc..).
 * 
 * @author wkerckho
 *
 */
public class SegmentHandler extends DependencyActivatorBase {

	private SegmentDescriptor descriptor;
	private FunctionalSegment instance;
	private Object proxiedInstance;
	private Component functionComponent;
	private Component metaTypeComponent;
	private BundleContext context;

	private Set<InjectionHandler> injectionHandlers = new HashSet<>();

	private DefaultSegmentAdmin parent;

	private AspectsHandler aspectsHandler;

	public SegmentHandler(SegmentDescriptor descriptor, FunctionalSegment instance, DefaultSegmentAdmin parent) {
		this.descriptor = descriptor;
		this.instance = instance;
		this.parent = parent;
	}

	@Override
	public synchronized void init(BundleContext context, DependencyManager manager) throws Exception {
		this.context = context;

		// Create basic service definition for the Segment
		functionComponent = createFunctionComponent();

		// Create metatype service definition for the Segment
		registerMetaType(manager);

		// Take care of dependencies
		descriptor.getDependencies().forEach(this::createServiceDependency);

		// Setup Health Checks (dependencies on other systems)
		if (descriptor.getHttpAssertions() != null) {
			descriptor.getHttpAssertions().forEach(this::createExternalDependency);
		}

		/*
		 * Setup callback methods for this component for init,start,stop &
		 * destroy events (respectively). Currently used to register the
		 * function http endpoint (if any) when the component is activated and
		 * unregister it when the component is deactivated (e.g. when a
		 * dependency is missing).
		 */
		functionComponent.setCallbacks(this, null, "segmentStarted", "segmentStopped", null);

		// Add component to manager
		manager.add(functionComponent);
	}

	protected synchronized void segmentStarted() throws Exception {
		registerHttpEndpoint();
		instance.started();
	}

	protected synchronized void segmentStopped() throws Exception {
		unregisterHttpEndpoint();
		instance.stopped();
	}

	public synchronized void destroy(BundleContext context, DependencyManager manager) throws Exception {
		if (functionComponent != null && manager != null) {
			manager.remove(functionComponent);
		}

		if (metaTypeComponent != null && manager != null) {
			manager.remove(metaTypeComponent);
		}

		unregisterHttpEndpoint();

		// Clean up AspectHandler
		if (aspectsHandler != null) {
			aspectsHandler.destroy();
		}
	}

	protected void registerHttpEndpoint() {
		if (descriptor.getHttpOperation() != null && descriptor.getHttpOperation().getMethod() != null
				&& descriptor.getHttpOperation().getPath() != null
				&& !descriptor.getHttpOperation().getPath().isEmpty()) {
			parent.getHttpManager().registerOperation(descriptor, (FunctionalSegment) proxiedInstance);
		}
	}

	protected void unregisterHttpEndpoint() {
		if (descriptor.getHttpOperation() != null && descriptor.getHttpOperation().getMethod() != null
				&& descriptor.getHttpOperation().getPath() != null
				&& !descriptor.getHttpOperation().getPath().isEmpty()) {
			parent.getHttpManager().unregisterOperation(descriptor, (FunctionalSegment) proxiedInstance);
		}
	}

	private void registerMetaType(DependencyManager manager) {
		if (descriptor.getTemplateInfo() != null
				&& TemplateDescriptor.Type.CONFIGURABLE_INSTANCE.equals(descriptor.getTemplateInfo().getType())) {
			Dictionary<String, Object> props = new Hashtable<>();
			props.put(MetaTypeProvider.METATYPE_PID, descriptor.getId());

			manager.add(createComponent().setInterface(MetaTypeProvider.class.getName(), props)
					.setImplementation(new MetaTypeProviderImpl(descriptor)));
		}
	}

	private Component createFunctionComponent() throws Exception {
		Component builder = createComponent();

		Dictionary<String, Object> props = new Hashtable<>();
		props.put(ServicePropertyNames.SEGMENT_ID, descriptor.getId());
		props.put(ServicePropertyNames.SEGMENT_VERSION,
				VersionUtil.createVersionPropertyValue(descriptor.getVersion()));
		props.put(ServicePropertyNames.SEGMENT_TAGS, descriptor.getTags().toArray(new String[] {}));
		props.put(ServicePropertyNames.SEGMENT_SUBSCRIPTIONS, descriptor.getSubscriptions());
		if (descriptor.getSchedule() != null && !descriptor.getSchedule().isEmpty()) {
			props.put(ServicePropertyNames.SEGMENT_SCHEDULE, descriptor.getSchedule());
		}
		if (descriptor.getTemplateInfo() != null
				&& (TemplateDescriptor.Type.FACTORY_INSTANCE.equals(descriptor.getTemplateInfo().getType()))) {
			props.put("limeds.segment.factory", descriptor.getTemplateInfo().getFactoryId());
		}

		descriptor.getServiceProperties().stream().map(this::splitServiceProperty)
				.forEach(p -> props.put(p.left(), p.right()));

		Set<String> interfaces = new HashSet<>();
		interfaces.addAll(descriptor.getInterfaces());

		Class<?>[] interfaceClasses = interfaces.stream()
				.map(Errors.wrapFunction(interfaceName -> loadClassDefinition(interfaceName)))
				.toArray(size -> new Class<?>[size]);

		builder = builder.setInterface(interfaces.toArray(new String[] {}), props);

		// Register proxy with support for modular ComponentAspects
		aspectsHandler = new AspectsHandler(new SegmentContext(descriptor, (FunctionalSegment) instance), this);
		aspectsHandler.setupChain(parent.getRegisteredAspects());
		try {
			proxiedInstance = Proxy.newProxyInstance(getClass().getClassLoader(), interfaceClasses, aspectsHandler);
			builder = builder.setImplementation(proxiedInstance);
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (descriptor.getTemplateInfo() != null
				&& (TemplateDescriptor.Type.CONFIGURABLE_INSTANCE.equals(descriptor.getTemplateInfo().getType())
						|| (TemplateDescriptor.Type.FACTORY_INSTANCE.equals(descriptor.getTemplateInfo().getType())))) {
			String pid = descriptor.getTemplateInfo().getInstanceId() != null
					? descriptor.getTemplateInfo().getInstanceId() : descriptor.getId();
			builder = builder.add(createConfigurationDependency().setPid(pid));

			/*
			 * If configurable props are complete, create default transient
			 * config entry
			 */
			if (TemplateDescriptor.Type.CONFIGURABLE_INSTANCE.equals(descriptor.getTemplateInfo().getType())
					&& descriptor.getTemplateInfo().getConfiguration().stream().allMatch(cp -> cp.value != null)) {
				ConfigUtil.createDefaultConfig(parent.getConfigAdmin(), descriptor);
			}
		}

		return builder;

	}

	private Pair<String, String> splitServiceProperty(String property) {
		String[] parts = property.split("=");
		return new Pair<>(parts[0].trim(), parts[1].trim());
	}

	private Class<?> loadClassDefinition(String interfaceName) throws ClassNotFoundException {
		return getClass().getClassLoader().loadClass(interfaceName);
	}

	private ServiceDependency createServiceDependency(String dependencyId, DependencyDescriptor dependencyDescriptor) {
		ServiceDependency result = createServiceDependency();
		result.setRequired(dependencyDescriptor.isRequired());
		result.setService(dependencyDescriptor.getServiceFilter());

		InjectionHandler injectionHandler = new InjectionHandler(dependencyId, dependencyDescriptor, instance);
		injectionHandlers.add(injectionHandler);

		result.setCallbacks(injectionHandler, InjectionHandler.ADDED, InjectionHandler.REMOVED);
		functionComponent.add(result);
		return result;
	}

	private ServiceDependency createExternalDependency(HttpAssertionDescriptor assertion) {
		ServiceDependency result = createServiceDependency();
		result.setRequired(true);
		String filter = FilterUtil.and(FilterUtil.isType(HttpAssertionService.class),
				FilterUtil.and(FilterUtil.equals("target.url", assertion.getTarget().toString()),
						FilterUtil.equals("target.method", assertion.getMethod().toString())))
				.toString();
		result.setService(filter);
		functionComponent.add(result);
		return result;
	}

	public JsonObject getMetaData() {
		JsonObject metaData = Json.from(descriptor);
		metaData.put("active", isActive());

		try {
			if (isActive() && functionComponent.getServiceRegistration().getReference() != null) {
				metaData.put("serviceId", (Long) functionComponent.getServiceRegistration().getReference()
						.getProperty(Constants.SERVICE_ID));
			}
		} catch (IllegalStateException e) {
			// Service is active but registration process is ongoing
			metaData.put("serviceId", "n/a");
		}

		metaData.put("sourceBundleId", context.getBundle().getBundleId());
		metaData.put("systemSegment", API_Info.class.getPackage().equals(instance.getClass().getPackage()));

		metaData.get("dependencies").asObject().forEach((variable, dep) -> dep.asObject().put("serviceFilter",
				descriptor.getDependencies().get(variable).getServiceFilter()));

		return metaData;
	}

	public boolean isActive() {
		return functionComponent.getComponentDeclaration().getState() == ComponentDeclaration.STATE_REGISTERED;
	}

	public SegmentContext getContext() {
		return new SegmentContext(descriptor, instance);
	}

	public synchronized void notifyAspectAdded(Set<AspectFactory> upToDateSet) throws Exception {
		aspectsHandler.setupChain(upToDateSet);
	}

	public synchronized void notifyAspectRemoved(Set<AspectFactory> upToDateSet) throws Exception {
		aspectsHandler.setupChain(upToDateSet);
	}

}
