/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.impl.segments;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Collections;
import java.util.Dictionary;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.ibcn.limeds.SegmentContext;
import org.ibcn.limeds.ServiceSegment;
import org.ibcn.limeds.aspects.Aspect;
import org.ibcn.limeds.aspects.AspectFactory;
import org.ibcn.limeds.descriptors.SegmentLanguage;
import org.ibcn.limeds.exceptions.HandledByAspect;
import org.ibcn.limeds.exceptions.SegmentExecutionException;
import org.ibcn.limeds.impl.slices.SegmentAdapter;
import org.ibcn.limeds.json.JsonPrimitive;
import org.ibcn.limeds.json.JsonValue;
import org.ibcn.limeds.util.Errors;
import org.ibcn.limeds.util.Errors.ErrorWrapper;
import org.osgi.service.cm.ConfigurationException;
import org.slf4j.LoggerFactory;

/**
 * The AspectsHandler class wraps Flow Function instances as a proxy before
 * registration as an OSGi service. This allows the LimeDS system to enable
 * additional features to the Flow Function such as receiving and processing
 * configuration events, applying doBefore and doAfter aspects, etc...
 * 
 * @author wkerckho
 */
public class AspectsHandler implements InvocationHandler {

	private static class OpMD {
		public boolean segmentOp = false;
		public JsonValue[] jsonArgs;
	}

	private SegmentContext context;
	private List<Aspect> aspectChain = new LinkedList<>();
	private SegmentHandler parent;

	public AspectsHandler(SegmentContext context, SegmentHandler parent) throws Exception {
		this.context = context;
		this.parent = parent;
	}

	public SegmentContext getContext() {
		return context;
	}

	public synchronized void setupChain(Set<AspectFactory> factories) {
		aspectChain.forEach(a -> a.destroy());
		aspectChain.clear();

		factories.stream().map(f -> f.createInstance(context)).filter(Optional::isPresent).map(Optional::get)
				.forEach(aspectChain::add);

		aspectChain.forEach(a -> a.init());
	}

	@SuppressWarnings("unchecked")
	@Override
	public Object invoke(Object proxy, Method method, Object[] input) throws Throwable {
		OpMD md = new OpMD();
		String calledMethod = method.getName();
		if ("equals".equals(calledMethod)) {
			return proxy == input[0];
		} else if ("hashCode".equals(calledMethod)) {
			return System.identityHashCode(proxy);
		} else if ("toString".equals(calledMethod)) {
			return context.getDescriptor().getId() + "(Proxied Handler)";
		} else if ("apply".equals(calledMethod) || isSegmentOp(method, input, md)) {
			JsonValue[] jsonInput = !md.segmentOp
					? ((input != null && input.length > 0) ? (JsonValue[]) input[0] : null) : md.jsonArgs;

			JsonValue output = null;
			Throwable error = null;
			try {
				runBeforeAspects(jsonInput);

				if (!md.segmentOp) {
					// Do functional apply call and capture result
					output = context.getInstance().apply(jsonInput);
				} else {
					// Do Java method call and capture result and (un)box JSON
					Object[] arg = Arrays.stream(jsonInput).skip(1).map(ServiceSegment::unbox)
							.toArray(size -> new Object[size]);
					output = ServiceSegment.box((method.invoke(context.getInstance(), arg)));
				}
			} catch (ErrorWrapper ew) {
				if (ew.getWrappedError() instanceof HandledByAspect) {
					output = ((HandledByAspect) ew.getWrappedError()).getContent();
				} else {
					error = ew.getWrappedError();
				}
			} catch (HandledByAspect ehba) {
				output = ehba.getContent();
			} catch (Exception e) {
				error = e;
			}

			// Execute all registered doAfter methods
			final Optional<Throwable> success = Optional.ofNullable(error);
			final JsonValue afterOutput = output;
			Optional<Exception> afterError = Optional.empty();
			try {
				runAfterAspects(success, afterOutput, jsonInput);
			} catch (Exception e) {
				afterError = Optional.of(e);
			}

			if (error == null && afterError.isPresent()) {
				error = afterError.get();
			}

			// Output error when applicable
			if (error != null) {
				throw new SegmentExecutionException(context, error);
			}

			// Return result
			return !md.segmentOp ? output : ServiceSegment.unbox(output);
		} else if ("getDescriptor".equals(calledMethod)) {
			return context.getDescriptor();
		} else if ("updated".equals(calledMethod)) {
			this.updated((Dictionary<String, ?>) input[0]);
			return null;
		} else {
			for (Method m : context.getInstance().getClass().getDeclaredMethods()) {
				if (m.getName().equals(calledMethod)) {
					return m.invoke(context.getInstance(), input);
				}
			}
			return null;
		}
	}

	private boolean isSegmentOp(Method method, Object[] input, OpMD md) {
		md.segmentOp = context.getInstance() instanceof ServiceSegment
				&& ((ServiceSegment) context.getInstance()).getSupportedOperations().contains(method);
		if (md.segmentOp) {
			LinkedList<JsonValue> argList = new LinkedList<>();
			argList.add(new JsonPrimitive(method.getName()));
			argList.addAll(Arrays.stream(input).map(ServiceSegment::box).collect(Collectors.toList()));
			md.jsonArgs = argList.toArray(new JsonValue[] {});
		}
		return md.segmentOp;
	}

	private void runAfterAspects(final Optional<Throwable> success, final JsonValue afterOutput,
			final JsonValue[] originalInput) {
		aspectChain.forEach(Errors.wrapConsumer(aspect -> aspect.doAfter(originalInput, afterOutput, success)));
	}

	private void runBeforeAspects(JsonValue[] jsonInput) {
		// Execute all registered doBefore methods
		aspectChain.forEach(Errors.wrapConsumer(aspect -> aspect.doBefore(jsonInput)));
	}

	public void destroy() throws Exception {
		aspectChain.forEach(aspect -> aspect.destroy());
	}

	public synchronized void updated(Dictionary<String, ?> properties) throws ConfigurationException {
		// Inject configuration
		if (properties != null) {
			if (SegmentLanguage.Java.isEqual(context.getDescriptor().getLanguage())) {
				Arrays.stream(context.getInstance().getClass().getDeclaredFields())
						.filter(f -> properties.get(f.getName()) != null).forEach(Errors.wrapConsumer(f -> {
							f.setAccessible(true);
							f.set(context.getInstance(), properties.get(f.getName()));
						}));
			} else {
				Collections.list(properties.keys()).forEach(
						key -> ((SegmentAdapter) context.getInstance()).setConfigProperty(key, properties.get(key)));
			}

			if (parent.isActive()) {
				/*
				 * Only call the configUpdated handler if the Segment was
				 * already active!
				 */
				try {
					context.getInstance().configUpdated();
				} catch (Exception e) {
					LoggerFactory.getLogger(getClass()).warn("Calling configUpdated for " + context.getDescriptor()
							+ " after it was started, caused an exception!", e);
				}
			}
		}
	}

}
