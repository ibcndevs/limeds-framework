/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.impl.segments;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.ibcn.limeds.SegmentAdmin;
import org.ibcn.limeds.TypeAdmin;
import org.ibcn.limeds.bindings.LanguageAdapter;
import org.ibcn.limeds.bindings.LanguageAdapterFactory;
import org.ibcn.limeds.descriptors.SegmentDescriptor;
import org.ibcn.limeds.descriptors.SegmentLanguage;
import org.ibcn.limeds.impl.slices.SegmentAdapter;
import org.ibcn.limeds.json.Json;
import org.ibcn.limeds.json.JsonObject;
import org.ibcn.limeds.json.model.ClassToType;
import org.ibcn.limeds.util.Errors;
import org.ibcn.limeds.util.Identifier;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.BundleEvent;
import org.osgi.framework.hooks.bundle.EventHook;
import org.osgi.service.component.ComponentContext;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.component.annotations.ReferencePolicy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Multimap;
import com.google.common.collect.MultimapBuilder;

/**
 * BundleListener implements an OSGi event hook to be updated whenever an OSGi
 * bundle is activated or deactivated. We use this to trigger the activation and
 * deactivation of Java classes annotated with LimeDS annotations.
 * 
 * @author wkerckho
 *
 */
@Component(immediate = true)
public class BundleListener implements EventHook {

	private volatile boolean active = false;
	private final Logger LOGGER = LoggerFactory.getLogger(BundleListener.class);
	private ExecutorService taskRunner = Executors.newSingleThreadExecutor();

	private Multimap<Long, SegmentDescriptor> registry = MultimapBuilder.hashKeys().hashSetValues().build();
	private Multimap<Long, Identifier> types = MultimapBuilder.hashKeys().hashSetValues().build();

	@Reference(cardinality = ReferenceCardinality.MANDATORY)
	private volatile SegmentAdmin manager;
	@Reference(cardinality = ReferenceCardinality.MANDATORY)
	private volatile TypeAdmin typeAdmin;

	private Map<String, LanguageAdapterFactory> languageBindings = new HashMap<>();

	@Activate
	public void activate(ComponentContext context) {
		taskRunner.execute(() -> {
			Arrays.stream(context.getBundleContext().getBundles()).forEach(b -> processBundleStart(b));
			active = true;
		});
	}

	@Deactivate
	public void deactivate(ComponentContext context) {
		taskRunner.execute(() -> {
			Long[] keys = registry.keySet().toArray(new Long[] {});

			for (Long key : keys) {
				try {
					processBundleStop(context.getBundleContext().getBundle(key));
				} catch (IllegalStateException e) {
					// Ignore
				}
			}
			active = false;
		});
	}

	@Reference(policy = ReferencePolicy.DYNAMIC, cardinality = ReferenceCardinality.AT_LEAST_ONE)
	private synchronized void bindLanguageAdapterFactory(LanguageAdapterFactory factory) {
		languageBindings.put(factory.getLanguageId().toUpperCase(), factory);
	}

	// Required for DS
	@SuppressWarnings("unused")
	private synchronized void unbindLanguageAdapterFactory(LanguageAdapterFactory factory) {
		languageBindings.remove(factory.getLanguageId().toUpperCase());
	}

	@Override
	public void event(BundleEvent event, Collection<BundleContext> contexts) {
		taskRunner.execute(() -> {
			if (active) {
				Bundle source = event.getBundle();
				if (event.getType() == BundleEvent.STARTED) {
					processBundleStart(source);
				} else if (event.getType() == BundleEvent.STOPPING) {
					processBundleStop(source);
				}
			}
		});
	}

	private void processBundleStart(Bundle source) {
		if (source.getHeaders().get("LimeDS-Slices") != null) {
			Arrays.stream(source.getHeaders().get("LimeDS-Slices").split(","))
					.map(sliceLoc -> source.getEntry(sliceLoc.trim())).filter(url -> url != null).forEach(url -> {
						try {
							JsonObject sliceDescriptor = Json.from(url.openStream());
							if (sliceDescriptor.has("segments")) {
								sliceDescriptor.get("segments").asArray().forEach(segmentJson -> {
									String handlerName = sliceDescriptor.get("handlers")
											.getString(segmentJson.getString("id"));
									SegmentDescriptor segmentDescriptor = null;
									try {
										Object handlerInstance = null;
										segmentDescriptor = Json.to(SegmentDescriptor.class, segmentJson.asObject());
										segmentDescriptor.setVersion(sliceDescriptor.getString("version"));
										if (SegmentLanguage.Java.isEqual(segmentDescriptor.getLanguage())) {
											segmentDescriptor.setHandlerName(handlerName);
											Class<?> handlerClass = source.loadClass(handlerName);
											handlerInstance = handlerClass.newInstance();

											/*
											 * Prepare handlerClass for
											 * dependency injection
											 */
											segmentDescriptor.getDependencies()
													.forEach(Errors.wrapBiConsumer((depId, depDesc) -> {
														Field injectionField = handlerClass.getDeclaredField(depId);
														injectionField.setAccessible(true);
														depDesc.setInstanceMirror(injectionField);
													}));
											deploySegment(segmentDescriptor, handlerInstance, source);
										} else {
											if (languageBindings
													.containsKey(segmentDescriptor.getLanguage().toUpperCase())) {
												LanguageAdapter scriptWrapper = languageBindings
														.get(segmentDescriptor.getLanguage().toUpperCase())
														.createAdapter(source.getEntry(handlerName).openStream());
												handlerInstance = new SegmentAdapter(segmentDescriptor, scriptWrapper);
												deploySegment(segmentDescriptor, handlerInstance, source);
											} else {
												// Add to queue
												throw new Exception(
														"This runtime has no active support for the requested language "
																+ segmentDescriptor.getLanguage());
											}
										}
									} catch (ClassNotFoundException | NoClassDefFoundError e) {
										LOGGER.warn("Could not load class {}, skipping dataflow checking...",
												handlerName);
									} catch (InstantiationException | IllegalAccessException e) {
										LOGGER.error("Could not instantiate dataflow component {}",
												segmentJson.getString("id"));
									} catch (Exception e) {
										LOGGER.error("Error while registering dataflow component "
												+ segmentJson.getString("id"), e);
										try {
											if (segmentDescriptor != null) {
												manager.unregister(segmentDescriptor, source.getBundleContext());
											}
										} catch (Exception e1) {
											LOGGER.warn("Problems cleaning up deployment of {} after error...",
													segmentJson.getString("id"));
										}
									}
								});
							}

							if (sliceDescriptor.has("types")) {
								sliceDescriptor.get("types").asObject().forEach((typeRef, path) -> {
									try {
										JsonObject schema = null;
										if (path.asString().endsWith(".json")) {
											// Regular JSON schema definition
											schema = Json.from(source.getEntry(path.asString()).openStream());
										} else if (path.asString().endsWith(".java")) {
											// Definition is Java POJO class
											schema = new ClassToType(source.loadClass(typeRef)).parseType();
										}
										Identifier typeId = new Identifier(typeRef,
												sliceDescriptor.getString("version"));
										typeAdmin.replaceVariables(schema, sliceDescriptor);
										typeAdmin.register(typeId, schema);
										types.put(source.getBundleId(), typeId);
									} catch (Exception e) {
										LOGGER.error("Error while registering type " + typeRef, e);
									}
								});
							}
						} catch (Exception e) {
							LOGGER.warn("Could not read the slice descriptor {} for bundle {}.", url,
									source.getSymbolicName());
						}
					});
		}
	}

	private void deploySegment(SegmentDescriptor descriptor, Object instance, Bundle source) throws Exception {
		manager.register(descriptor, instance, source.getBundleContext());
		registry.put(source.getBundleId(), descriptor);
	}

	private void processBundleStop(Bundle source) {
		if (source != null && types.containsKey(source.getBundleId())) {
			types.removeAll(source.getBundleId()).forEach(typeId -> typeAdmin.unregister(typeId));
		}

		if (source != null && registry.containsKey(source.getBundleId())) {
			Collection<SegmentDescriptor> descriptors = registry.get(source.getBundleId());
			for (SegmentDescriptor descriptor : descriptors) {
				try {
					LOGGER.debug("Cleaning up {}", descriptor.getId());
					manager.unregister(descriptor, source.getBundleContext());
				} catch (Exception e) {
					LOGGER.warn("Error while unregistering dataflow component " + descriptor.getId() + "!", e);
				}
			}
			registry.removeAll(source.getBundleId());

		}
	}

}
