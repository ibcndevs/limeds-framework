/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.impl.cache;

import java.util.Optional;

import org.ibcn.limeds.FunctionalSegment;
import org.ibcn.limeds.SegmentContext;
import org.ibcn.limeds.annotations.Cached;
import org.ibcn.limeds.aspects.Aspect;
import org.ibcn.limeds.descriptors.CacheDescriptor;
import org.ibcn.limeds.exceptions.HandledByAspect;
import org.ibcn.limeds.json.Json;
import org.ibcn.limeds.json.JsonArray;
import org.ibcn.limeds.json.JsonValue;
import org.slf4j.LoggerFactory;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

/**
 * The CacheAspect implements a configurable cache for LimeDS components that
 * have valid cache configuration, as defined by their CacheDescriptor.
 * 
 * @author wkerckho
 *
 */
public class CacheAspect implements Aspect {

	private SegmentContext context;
	private LoadingCache<JsonArray, JsonValue> cache;

	public CacheAspect(SegmentContext context) {
		this.context = context;
	}

	@Override
	public void init() {
		try {
			CacheDescriptor cd = Json.to(CacheDescriptor.class,
					context.getDescriptor().getConfigureAspects().get(Cached.ASPECT_ID).asObject());
			CacheLoader<JsonArray, JsonValue> loader = new CacheLoader<JsonArray, JsonValue>() {

				@Override
				public JsonValue load(JsonArray input) throws Exception {
					return ((FunctionalSegment) context.getInstance()).apply(input.toArray(new JsonValue[] {}));
				}

			};
			if (cd.getRetentionDuration() != -1l) {
				cache = CacheBuilder.newBuilder()
						.expireAfterWrite(cd.getRetentionDuration(), cd.getRetentionDurationUnit())
						.maximumSize(cd.getSize()).recordStats().build(loader);
			} else {
				cache = CacheBuilder.newBuilder().maximumSize(cd.getSize()).recordStats().build(loader);
			}
		} catch (Exception e) {
			LoggerFactory.getLogger(getClass()).warn("Could not initialize CacheAspect for " + context.getDescriptor());
		}
	}

	@Override
	public void doBefore(JsonValue... input) throws Exception {
		throw new HandledByAspect(cache.get(Json.arrayOf(input)));
	}

	@Override
	public void doAfter(JsonValue[] input, JsonValue output, Optional<Throwable> error) throws Exception {
		// Nothing to do here
	}

	@Override
	public void destroy() {
		cache = null;
	}

}
