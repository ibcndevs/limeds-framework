/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.impl.cache;

import java.util.Optional;

import org.ibcn.limeds.aspects.AspectFactory;
import org.ibcn.limeds.SegmentContext;
import org.ibcn.limeds.annotations.Cached;
import org.ibcn.limeds.aspects.Aspect;
import org.ibcn.limeds.descriptors.CacheDescriptor;
import org.ibcn.limeds.json.Json;
import org.osgi.service.component.annotations.Component;
import org.slf4j.LoggerFactory;

/**
 * This class provides an AspectFactory implementation for the creation of
 * CacheAspect instances.
 */
@Component
public class CacheAspectFactory implements AspectFactory {

	@Override
	public Optional<Aspect> createInstance(SegmentContext context) {
		try {
			if (context.getDescriptor().getConfigureAspects().containsKey(Cached.ASPECT_ID)) {
				CacheDescriptor cd = Json.to(CacheDescriptor.class,
						context.getDescriptor().getConfigureAspects().get(Cached.ASPECT_ID).asObject());
				if (cd != null) {
					return Optional.of(new CacheAspect(context));
				}
			}
		} catch (Exception e) {
			LoggerFactory.getLogger(this.getClass())
					.warn("Could not create CacheAspect instance for " + context.getDescriptor());
		}
		return Optional.empty();
	}

	@Override
	public int getPriority() {
		return 5;
	}

}
