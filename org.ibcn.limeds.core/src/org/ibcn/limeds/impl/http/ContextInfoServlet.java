/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.impl.http;

import java.io.IOException;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ibcn.limeds.json.JsonObject;
import org.ibcn.limeds.util.FilterUtil;
import org.osgi.service.cm.Configuration;
import org.osgi.service.cm.ConfigurationAdmin;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

@SuppressWarnings("serial")
@Component(immediate = true, service = Servlet.class, property = "alias=" + ContextInfoServlet.CONTEXT_PATH)
public class ContextInfoServlet extends HttpServlet {

	static final String CONTEXT_PATH = "/__context";

	@Reference
	private volatile ConfigurationAdmin configAdmin;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		try {
			JsonObject info = new JsonObject();
			Configuration[] configs = configAdmin
					.listConfigurations(FilterUtil.equals("service.pid", HttpManager.class.getName()));
			if (configs != null && configs.length > 0) {
				info.put("LIMEDS_API_ROOT", "" + configs[0].getProperties().get("alias"));
			} else {
				info.put("LIMEDS_API_ROOT", HttpManager.DEFAULT_HTTP_ROOT);
			}
			resp.setHeader("Access-Control-Allow-Origin", "*");
			resp.setContentType("application/json");
			resp.getOutputStream().write(info.toString().getBytes("UTF-8"));
		} catch (Exception e) {
			resp.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		}

	}

	@Override
	protected void doOptions(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.setHeader("Access-Control-Allow-Origin", "*");
		resp.setHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, HEAD");
		resp.setHeader("Access-Control-Allow-Headers", req.getHeader("Access-Control-Request-Headers"));
		resp.setStatus(200);
	}

}
