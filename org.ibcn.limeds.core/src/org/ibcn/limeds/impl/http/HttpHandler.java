/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.impl.http;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.util.Collections;
import java.util.EnumSet;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ibcn.limeds.FunctionalSegment;
import org.ibcn.limeds.IssueAdmin;
import org.ibcn.limeds.SegmentContext;
import org.ibcn.limeds.ServicePropertyNames;
import org.ibcn.limeds.annotations.AuthMode;
import org.ibcn.limeds.annotations.HttpMethod;
import org.ibcn.limeds.annotations.LoadBalancingStrategy;
import org.ibcn.limeds.descriptors.SegmentDescriptor;
import org.ibcn.limeds.exceptions.ExceptionWithStatus;
import org.ibcn.limeds.exceptions.NoContentException;
import org.ibcn.limeds.exceptions.SegmentExecutionException;
import org.ibcn.limeds.cluster.LoadBalancingController;
import org.ibcn.limeds.json.Json;
import org.ibcn.limeds.json.JsonValue;
import org.ibcn.limeds.security.HttpAuthProvider;
import org.ibcn.limeds.versioning.VersionUtil;
import org.osgi.framework.Filter;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.InvalidSyntaxException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.ibcn.limeds.json.JsonObject;
import org.ibcn.limeds.json.JsonObjectBuilder;
import org.ibcn.limeds.json.JsonPrimitive;

/**
 * A HttpHandler instance handles the configured HTTP endpoint for a Flow
 * Function. Its process method is called whenever its matchesRequest method
 * returns true for requests sent to the HttpManager.
 * 
 * @author wkerckho
 *
 */
public class HttpHandler {

	private static final Logger LOGGER = LoggerFactory.getLogger(HttpHandler.class);

	private static final String API_VERSION_HEADER = "x-limeds-api-version";
	private static final String PARAM_PATTERN = "\\{(.*?)\\}";

	private SegmentDescriptor descriptor;
	private String versionIdentifier;
	private FunctionalSegment function;
	private String pathPattern;
	private String[] pathParts;
	private boolean loadBalanced;
	private SegmentContext context;

	public HttpHandler(SegmentDescriptor descriptor, FunctionalSegment function) {
		this.descriptor = descriptor;
		this.function = function;
		pathPattern = descriptor.getHttpOperation().getPath().replaceAll(PARAM_PATTERN, "[^/]+");
		pathParts = descriptor.getHttpOperation().getPath().split("/");
		loadBalanced = !LoadBalancingStrategy.NONE.equals(descriptor.getHttpOperation().getLoadBalancing());
		context = new SegmentContext(descriptor, function);
		versionIdentifier = VersionUtil.createVersionPropertyValue(descriptor.getVersion());
	}

	public SegmentDescriptor getDescriptor() {
		return descriptor;
	}

	public boolean matchesRequest(HttpServletRequest request) {
		if (HttpMethod.valueOf(request.getMethod()).equals(descriptor.getHttpOperation().getMethod())) {
			if (Pattern.matches(pathPattern, getRequestedPath(request))) {
				if (request.getHeader(API_VERSION_HEADER) != null) {
					// Check version match
					try {
						Filter versionFilter = FrameworkUtil.createFilter(VersionUtil.createFilter(
								ServicePropertyNames.SEGMENT_VERSION, request.getHeader(API_VERSION_HEADER)));
						Map<String, String> versionProperty = new HashMap<>();
						versionProperty.put(ServicePropertyNames.SEGMENT_VERSION, versionIdentifier);
						return versionFilter.matches(versionProperty);
					} catch (InvalidSyntaxException e) {
						LOGGER.warn("Request to {} has invalid version expression!", getRequestedPath(request));
					}
				} else {
					return true;
				}
			}
		}
		return false;
	}

	public void process(HttpServletRequest request, HttpServletResponse response, HttpAuthProvider authProvider,
			LoadBalancingController lbController, IssueAdmin issueAdmin) throws IOException {
		setXOrigHeader(response);
		try {
			if (lbController == null || !loadBalanced || lbController.isTarget(request)
					|| !lbController.handle(request, response, context)) {
				/*
				 * If no load balancing is configured or the request is the
				 * result of a load balancing action on another machine or the
				 * load balancer couldn't handle the request, we execute the
				 * request here.
				 */
				JsonObject httpContext = getHttpContext(request);
				if (allow(request, response, authProvider, httpContext)) {
					JsonValue input = parseInput(request);
					JsonValue output = function.apply(input, httpContext);
					processResponse(request, response, output, httpContext);
				}
			}
		} catch (Exception e) {
			Throwable t = e;
			if (e instanceof SegmentExecutionException) {
				t = ((SegmentExecutionException) e).getCause();
				issueAdmin.report(e, HttpHandler.class);
			}

			if (t instanceof ExceptionWithStatus) {
				ExceptionWithStatus se = (ExceptionWithStatus) t;
				response.setStatus(se.getStatus());
				response.getWriter().print(se.getMessage());
			} else {
				try {
					response.setStatus(500);
					e.printStackTrace(response.getWriter());
				} catch (Exception e1) {
					e.printStackTrace();
				}
			}
		}
	}

	private boolean allow(HttpServletRequest request, HttpServletResponse response, HttpAuthProvider authProvider,
			JsonObject httpContext) {
		if (authProvider != null && !AuthMode.NONE.equals(descriptor.getHttpOperation().getAuthMode())) {
			/*
			 * Execute authorization to check if we can allow this call to the
			 * operation
			 */
			Optional<JsonValue> auth = authProvider.authorizeRequest(request, response, descriptor.getHttpOperation());
			if (auth.isPresent()) {
				// authorized (add auth object)
				httpContext.put("auth", auth.get());
				return true;
			} else {
				// not authorized
				return false;
			}

		} else {
			return true;
		}
	}

	private JsonValue parseInput(HttpServletRequest request) throws Exception {
		if (EnumSet.of(HttpMethod.POST, HttpMethod.PUT).contains(descriptor.getHttpOperation().getMethod())) {
			try {
				return Json.from(new BufferedInputStream(request.getInputStream()));
			} catch (NoContentException e) {
				return new JsonObject();
			}
		} else {
			return new JsonObject();
		}
	}

	private JsonObject getHttpContext(HttpServletRequest request) {
		JsonObjectBuilder requestContextBuilder = Json.objectBuilder();
		// Parse headers
		JsonObjectBuilder headerBuilder = Json.objectBuilder();
		Collections.list((Enumeration<String>) request.getHeaderNames())
				.forEach(key -> headerBuilder.add(key, request.getHeader(key)));
		requestContextBuilder.add("headers", headerBuilder);

		// Parse query parameters
		JsonObjectBuilder queryBuilder = Json.objectBuilder();
		Collections.list((Enumeration<String>) request.getParameterNames())
				.forEach(key -> queryBuilder.add(key, request.getParameter(key)));
		requestContextBuilder.add("query", queryBuilder);

		// Parse path parameters
		requestContextBuilder.add("path", getPathParams(getRequestedPath(request)));

		requestContextBuilder.add("fullPath",
				request.getRequestURI() + (request.getQueryString() != null ? '?' + request.getQueryString() : ""));

		// Prepare response context (can be set by clients)
		return Json.objectBuilder().add("request", requestContextBuilder)
				.add("response", Json.objectBuilder().add("headers", new JsonObject())).build();
	}

	private JsonObject getPathParams(String requestPath) {
		String[] requestParts = requestPath.split("/");
		if (requestParts.length == pathParts.length) {
			JsonObjectBuilder result = Json.objectBuilder();
			for (int i = 0; i < pathParts.length; i++) {
				if (!pathParts[i].equals(requestParts[i]) && pathParts[i].startsWith("{")) {
					result.add(pathParts[i].substring(1, pathParts[i].length() - 1), requestParts[i]);
				}
			}
			return result.build();
		} else {
			throw new RuntimeException("Could not parse request path!");
		}
	}

	private void setXOrigHeader(HttpServletResponse response) {
		response.setHeader("Access-Control-Allow-Origin", HttpManager.XHR_ORIGIN);
		response.setHeader("Access-Control-Allow-Credentials", "true");
	}

	private void processResponse(HttpServletRequest request, HttpServletResponse response, JsonValue output,
			JsonObject httpContext) throws Exception {

		// Process output headers
		if (httpContext.get("response").has("headers")
				&& httpContext.get("response").get("headers") instanceof JsonObject) {
			httpContext.get("response").get("headers").asObject()
					.forEach((key, value) -> response.setHeader(key, value.asString()));
		}

		// Set status
		response.setStatus(
				httpContext.get("response").has("status") ? httpContext.get("response").getInt("status") : 200);

		if (output != null && !(output instanceof JsonPrimitive && output.asPrimitive().getValue() == null)) {
			response.setCharacterEncoding("UTF-8");
			response.setContentType("application/json");
			response.getOutputStream().write(output.toString().getBytes("UTF-8"));
		}
	}

	private String getRequestedPath(HttpServletRequest request) {
		if (request.getPathInfo() == null) {
			return request.getServletPath();
		} else {
			return request.getPathInfo();
		}
	}

}
