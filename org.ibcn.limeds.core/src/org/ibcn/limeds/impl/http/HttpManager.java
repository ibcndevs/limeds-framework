/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.impl.http;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ibcn.limeds.FunctionalSegment;
import org.ibcn.limeds.HttpMonitor;
import org.ibcn.limeds.HttpMonitor.Tracker;
import org.ibcn.limeds.IssueAdmin;
import org.ibcn.limeds.cluster.LoadBalancingController;
import org.ibcn.limeds.descriptors.HttpOperationDescriptor;
import org.ibcn.limeds.descriptors.SegmentDescriptor;
import org.ibcn.limeds.json.Json;
import org.ibcn.limeds.json.JsonArray;
import org.ibcn.limeds.json.JsonObject;
import org.ibcn.limeds.security.HttpAuthProvider;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.component.annotations.ReferencePolicy;
import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.Designate;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

/**
 * The HttpManager is the entry point to LimeDS for all incoming HTTP requests.
 * From here, requests are redirected to the appropriate Flow Functions.
 * 
 * @author wkerckho
 *
 */

@ObjectClassDefinition(name = "LimeDS HTTP Manager Configuration")
@interface HttpManagerConfig {
	@AttributeDefinition(description = "The root for LimeDS Http Endpoints")
	String alias() default HttpManager.DEFAULT_HTTP_ROOT;

	@AttributeDefinition(description = "Value for the XHR header Access-Control-Allow-Origin")
	String xhrOrigin() default "*";

	@AttributeDefinition(description = "Comma separated list of headers that should also be exposed (XHR)")
	String exposedHeaders() default "";

	@AttributeDefinition(description = "Indicates how long the results of a preflight request can be cached (Access-Control-Max-Age header)")
	int preflightCacheTime() default 600;
}

@SuppressWarnings("serial")
@Component(service = { HttpManager.class, Servlet.class }, property = "alias=" + HttpManager.DEFAULT_HTTP_ROOT)
@Designate(ocd = HttpManagerConfig.class)
public class HttpManager extends HttpServlet {

	public static final String DEFAULT_HTTP_ROOT = "/";

	static String XHR_ORIGIN = "*";
	private final Set<HttpHandler> registry = new CopyOnWriteArraySet<>();
	private HttpAuthProvider authProvider = null;
	private LoadBalancingController lbController = null;
	private IssueAdmin issueAdmin = null;
	private HttpManagerConfig config = null;
	private HttpMonitor monitor = null;

	// Component id -> HttpOperation
	private Map<String, HttpOperationDescriptor> metaData = new HashMap<>();

	@Activate
	public void start(HttpManagerConfig config) {
		XHR_ORIGIN = config.xhrOrigin();
		this.config = config;
	}

	@Reference(cardinality = ReferenceCardinality.MANDATORY, policy = ReferencePolicy.DYNAMIC)
	public void bindIssueAdmin(IssueAdmin issueAdmin) {
		this.issueAdmin = issueAdmin;
	}

	public void unbindIssueAdmin(IssueAdmin issueAdmin) {
		if (this.issueAdmin == issueAdmin) {
			this.issueAdmin = null;
		}
	}

	@Reference(policy = ReferencePolicy.DYNAMIC, cardinality = ReferenceCardinality.OPTIONAL)
	public synchronized void bindAuthProvider(HttpAuthProvider authProvider) {
		this.authProvider = authProvider;
	}

	public synchronized void unbindAuthProvider(HttpAuthProvider authProvider) {
		if (authProvider.equals(this.authProvider)) {
			this.authProvider = null;
		}
	}

	@Reference(policy = ReferencePolicy.DYNAMIC, cardinality = ReferenceCardinality.OPTIONAL)
	public void bindLoadBalancingController(LoadBalancingController lbController) {
		this.lbController = lbController;
	}

	public void unbindLoadBalancingController(LoadBalancingController lbController) {
		if (lbController.equals(this.lbController)) {
			this.lbController = null;
		}
	}

	@Reference(policy = ReferencePolicy.DYNAMIC, cardinality = ReferenceCardinality.OPTIONAL)
	public void bindHttpMonitor(HttpMonitor httpMonitor) {
		monitor = httpMonitor;
	}

	public void unbindHttpMonitor(HttpMonitor httpMonitor) {
		if (monitor.equals(httpMonitor)) {
			monitor = null;
		}
	}

	public void registerOperation(SegmentDescriptor descriptor, FunctionalSegment function) {
		synchronized (metaData) {
			String key = createKey(descriptor);
			// Register if this component was not registered before.
			if (!metaData.containsKey(key)) {
				registry.add(new HttpHandler(descriptor, function));
				metaData.put(key, descriptor.getHttpOperation());
			}
		}
	}

	public void unregisterOperation(SegmentDescriptor descriptor, FunctionalSegment function) {
		synchronized (metaData) {
			String key = createKey(descriptor);
			if (metaData.containsKey(key)) {
				registry.removeIf(h -> h.getDescriptor().getId().equals(descriptor.getId()));
				metaData.remove(key);
			}
		}
	}

	private String createKey(SegmentDescriptor descriptor) {
		return descriptor.getId() + " @" + descriptor.getVersion();
	}

	public JsonArray getMetaData() {
		synchronized (metaData) {
			return metaData.keySet().stream().map(key -> op2json(key, metaData.get(key))).collect(Json.toArray());
		}
	}

	private JsonObject op2json(String key, HttpOperationDescriptor op) {
		return Json.objectBuilder().add("flowId", key).add("method", op.getMethod().toString())
				.add("path", op.getPath()).build();
	}

	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		if (monitor == null) {
			process(request, response);
		} else {
			try (Tracker tracker = monitor.startTracking(request, response)) {
				process(request, response);
			}
		}
	}

	private void process(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// Handle options
		if ("OPTIONS".equals(request.getMethod())) {
			response.setHeader("Access-Control-Allow-Origin", XHR_ORIGIN);
			response.setHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, HEAD");
			response.setHeader("Access-Control-Allow-Headers", request.getHeader("Access-Control-Request-Headers"));
			response.setHeader("Access-Control-Allow-Credentials", "true");
			response.setHeader("Access-Control-Max-Age", "" + config.preflightCacheTime());
			response.setStatus(200);
		} else {
			// Check if a handler exists that matches this request
			Optional<HttpHandler> match = registry.stream().filter(h -> h.matchesRequest(request))
					.sorted((h1, h2) -> h2.getDescriptor().getVersion().compareTo(h1.getDescriptor().getVersion()))
					.findFirst();
			if (match.isPresent()) {
				response.setHeader("Access-Control-Expose-Headers", config.exposedHeaders());
				// Let the handler process the request
				match.get().process(request, response, authProvider, lbController, issueAdmin);
			} else {
				// Else we return a 404 Not Found exception
				response.sendError(HttpServletResponse.SC_NOT_FOUND);
			}
		}
	}

}
