/**
 * (Internal) This package contains the http manager implementation that allows
 * Flow Functions to be exposed on the Web.
 */
package org.ibcn.limeds.impl.http;