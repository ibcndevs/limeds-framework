/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds;

import java.util.Optional;

import org.ibcn.limeds.json.JsonObject;
import org.ibcn.limeds.json.JsonValue;
import org.ibcn.limeds.util.Identifier;

/**
 * This interface defines the TypeAdmin service, which is responsible for
 * managing the JSON schema types for the LimeDS instance.
 * 
 * @author wkerckho
 *
 */
public interface TypeAdmin {

	/**
	 * Register the specified type with the system.
	 * <p>
	 * Note that an attribute @typeVersion in the schema will override the
	 * specified version String. This is useful for fast evolving Slices that
	 * still need to export some stable schema definitions.
	 */
	void register(Identifier sliceId, JsonObject schema);

	/**
	 * Unregisters a type using the specified id.
	 */
	void unregister(Identifier sliceId);

	/**
	 * Return all registered types.
	 */
	JsonObject getRegisterdTypes();

	/**
	 * Optionally return the type with the specified name and version.
	 */
	Optional<JsonObject> getRegisteredType(Identifier sliceId);

	/**
	 * Resolve the specified schema using this TypeAdmin. It does this by
	 * replacing all @typeRef attributes with their respective schemas
	 * recursively.
	 * 
	 * @throws Exception
	 *             Throws an exception if the schema can not be resolved.
	 */
	JsonValue resolve(JsonValue schema) throws Exception;

	/**
	 * Validates the provided document based on the specified schema (which will
	 * be resolved first using this TypeAdmin).
	 * 
	 * @throws Exception
	 *             Throws an exception if the provided document is not valid.
	 */
	void validate(JsonValue schema, JsonValue document) throws Exception;

	/**
	 * Checks if the provided schema is specified correctly.
	 * 
	 * @param schema
	 * @throws Exception
	 *             Throws an exception is the schema contains errors (e.g.
	 *             invalid syntax).
	 */
	void validateSchema(JsonValue schema) throws Exception;

	/**
	 * Replace LimeDS variables that can be used in schema definitions such as
	 * e.g. ${sliceVersion} which is used as a placeholder for the version of
	 * the slice that is registering the schema.
	 * 
	 * @param schema
	 *            The schema that contains variables.
	 * @param context
	 *            The descriptor for the slice that is registering the schema.
	 */
	void replaceVariables(JsonValue schema, JsonObject context);

	/**
	 * Returns true if the schema proves to be an optional definition.
	 * <p>
	 * If the schema is an array, the schema is optional if the definition that
	 * is contained in the array is optional. If the schema is an object, the
	 * schema is optional if the definition that is contained in the object is
	 * optional. If the schema is a primitive, the schema is optional if the
	 * type declaration for the primitive has the optional modifier *.
	 * 
	 * @throws Exception
	 */
	boolean isOptional(JsonValue schema) throws Exception;

}
