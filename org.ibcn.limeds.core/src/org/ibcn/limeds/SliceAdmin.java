/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds;

import org.ibcn.limeds.json.JsonArray;
import org.ibcn.limeds.json.JsonObject;
import org.ibcn.limeds.util.Identifier;

/**
 * This interface defines the SliceAdmin service, which implements the backend
 * for the VIsual Slice Editor.
 * 
 * @author wkerckho
 *
 */
public interface SliceAdmin {

	/**
	 * This enum defines the various states a Slice can be in.
	 * 
	 * @author wkerckho
	 *
	 */
	public enum SliceState {
		deployed, undeployed, processing, terminated;
	}

	/**
	 * Return the editable Slices registered with the system.
	 * 
	 * @return A JsonArray instance containing JsonObjects defining the
	 *         registered Data Flows
	 */
	JsonArray list();

	/**
	 * Return the Slice registered with the specified id and version.
	 * 
	 * @param sliceId
	 *            The id of the Slice to be retrieved
	 * @return A JsonObject defining the requested Slice
	 */
	JsonObject get(Identifier sliceId);

	/**
	 * Synchronize the specified Slice with the system. The updated Slice is
	 * stored but changes are not automatically reflected at runtime. Use the
	 * changeState method to e.g. re-deploy the targeted Slice.
	 * 
	 * @param slice
	 *            A JsonObject containing the updated Data Flow definition
	 */
	void sync(JsonObject slice);

	/**
	 * Publish the specified Slice as a self-describing deployable module that
	 * can be installed on any LimeDS instance.
	 * 
	 * @param sliceId
	 *            The id of the Slice that is to be published
	 */
	void publish(Identifier sliceId);

	/**
	 * Perform a state-change for the specified Slice.
	 * 
	 * @param sliceId
	 *            The id of the Slice to perform a state-change on
	 * @param targetState
	 *            The intended state after this operation
	 * @param async
	 *            True if the operation should be performed independently of the
	 *            request thread, false otherwise
	 * @throws Exception
	 */
	void changeState(Identifier sliceId, SliceState targetState, boolean async) throws Exception;

}
