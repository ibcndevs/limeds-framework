/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.cluster;

import java.util.Set;

import org.ibcn.limeds.SegmentContext;

/**
 * Developers can add new load balancing strategies to the system by
 * implementing this simple interface.
 * 
 * @author wkerckho
 *
 */
public interface LoadBalancingStrategyProvider {

	/**
	 * The id of this strategy. A Segment can be setup to load balancing by
	 * specifying the appropriate id in the HttpOperation configuration.
	 * 
	 * @return
	 */
	String getStrategyId();

	/**
	 * This method implements the actual load balancing. Based on the current
	 * component context (supplied as an argument), the method must return the
	 * target machine(s) for load balancing.
	 * <p>
	 * If a single target is returned, the system will perform a redirect
	 * (faster, more scalable option). If multiple targets are returned, the
	 * system will duplicate the original HTTP call to all target machines
	 * (slower, less scalable option, can be useful for reliability).
	 * 
	 * @param context
	 *            A SegmentContext
	 * @return A set of target nodes for load balancing
	 */
	Set<ClusterNode> getTargetHosts(SegmentContext context);

}
