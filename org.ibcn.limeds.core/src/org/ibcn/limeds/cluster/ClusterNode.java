/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.cluster;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Collections;

/**
 * When LimeDS is setup in a cluster, the class ClusterNode can be used to
 * describe the details of each node in the cluster.
 * 
 * @author wkerckho
 *
 */
public class ClusterNode {

	private final String host;
	private final int port;
	private final boolean localNode;

	/**
	 * Create a new ClusterNode based on the provided String representation.
	 * <p>
	 * If only a hostname or ip is provided, the default system port is used for
	 * communication. If a hostname or ip followerd by a port-number seperated
	 * by a colon (e.g. localhost:9999) is provided, the specified port-number
	 * is used.
	 * 
	 * @param address
	 *            The String representation of the cluster node.
	 */
	public ClusterNode(String address) {
		String[] parts = address.split(":");
		if (parts.length == 2) {
			this.host = parts[0];
			this.port = Integer.parseInt(parts[1]);
		} else {
			this.host = address;
			port = 8080;
		}
		localNode = checkLocalNode();
	}

	/**
	 * Get the hostname or ip of the node.
	 */
	public String getHost() {
		return host;
	}

	/**
	 * Get the port to use for LimeDS communication with the node.
	 */
	public int getPort() {
		return port;
	}

	/**
	 * Return true if the node runs on the local machine, false otherwise.
	 */
	public boolean isLocalNode() {
		return localNode;
	}

	private boolean checkLocalNode() {
		try {
			InetAddress inet = InetAddress.getByName(host);
			return Collections.list(NetworkInterface.getNetworkInterfaces()).stream()
					.flatMap(iface -> Collections.list(iface.getInetAddresses()).stream())
					.anyMatch(addr -> addr.getHostAddress().equals(inet.getHostAddress()));
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * Return a string representation of the node, e.g. localhost:999
	 */
	@Override
	public String toString() {
		return host + ":" + port;
	}
}
