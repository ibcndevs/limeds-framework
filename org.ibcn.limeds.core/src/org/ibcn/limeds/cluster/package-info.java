/**
 * This package contains the interfaces and model classes used for LimeDS
 * network clustering.
 */
@org.osgi.annotation.versioning.Version("1.0.0")
package org.ibcn.limeds.cluster;
