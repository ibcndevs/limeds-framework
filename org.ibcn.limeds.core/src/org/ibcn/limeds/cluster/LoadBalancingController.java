/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.cluster;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ibcn.limeds.SegmentContext;

/**
 * This interface defines a load balancing controller.
 * 
 * @author wkerckho
 *
 */
public interface LoadBalancingController {

	/**
	 * Check whether the specified request should be load balanced.
	 * 
	 * @param request
	 *            The incoming http request
	 * @return True if the request is a candidate for load balancing, false
	 *         otherwise.
	 */
	boolean isTarget(HttpServletRequest request);

	/**
	 * Handle load balancing for the specified request.
	 * 
	 * @param request
	 *            The incoming http request
	 * @param response
	 *            The outgoing http response
	 * @param context
	 *            The current context of the HTTP-enabled Segment that is being
	 *            load balanced
	 * @return True if the controller was able to handle the request, false
	 *         otherwise
	 * @throws Exception
	 */
	boolean handle(HttpServletRequest request, HttpServletResponse response, SegmentContext context) throws Exception;

}
