/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds;

import org.ibcn.limeds.descriptors.DependencyDescriptor;
import org.ibcn.limeds.descriptors.SegmentDescriptor;
import org.ibcn.limeds.impl.segments.DefaultSegmentAdmin;
import org.ibcn.limeds.json.JsonValue;

/**
 * Interface defining a FunctionalSegment: a LimeDS component that provides a
 * well-defined and self-contained part of a Slice as a function that takes zero
 * or more arguments and produces some output (or returns null).
 * <p>
 * A FunctionalSegment can be used to implement:
 * <ul>
 * <li>Input adapters that provide access to data available in external systems
 * (web, database, ...).
 * <li>Consumers that can take input data to trigger some process.
 * <li>Filter components that can generate new data by combining the results of
 * various other components.
 * <li>etc...
 * </ul>
 * 
 * @author wkerckho
 *
 */
public interface FunctionalSegment {

	/**
	 * Apply this function (on the optional input).
	 * 
	 * @param input
	 *            Zero or more JsonValue instances that represent the arguments
	 *            for this function. If this function is exposed as a Web
	 *            operation, the first argument will always contain the JSON
	 *            body of the request and the second argument an object
	 *            representing the HTTP context (headers, path parameters,
	 *            etc...
	 * @return A JsonValue containing the function output or null
	 * @throws Exception
	 */
	JsonValue apply(JsonValue... input) throws Exception;

	/**
	 * Returns the meta-data descriptor for the Segment. This method can only be
	 * called for registered Segments, otherwise it will return null.
	 * 
	 * @return A SegmentDescriptor meta-data object.
	 */
	default SegmentDescriptor getDescriptor() {
		return DefaultSegmentAdmin.INSTANCE.findDescriptor(this);
	}

	/**
	 * Override this method to handle lifecycle start events for the Segment.
	 */
	default void started() throws Exception {

	}

	/**
	 * Override this method to handle lifecycle stop events for the Segment.
	 */
	default void stopped() throws Exception {

	}

	/**
	 * Override this method to handle configuration updates while the Segment is
	 * already running. The default behaviour is just to restart (call stopped
	 * first, then started again) the Segment.
	 */
	default void configUpdated() throws Exception {
		stopped();
		started();
	}

	default void bind(String dependencyId, DependencyDescriptor desc, Object instance) {

	}

	default void unbind(String dependencyId, DependencyDescriptor desc, Object instance) {

	}

}
