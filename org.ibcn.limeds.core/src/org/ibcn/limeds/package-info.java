/**
 * This package contains the main interfaces for interacting with LimeDS.
 */
@org.osgi.annotation.versioning.Version("1.6.0")
package org.ibcn.limeds;
