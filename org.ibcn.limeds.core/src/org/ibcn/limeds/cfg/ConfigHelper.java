/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.cfg;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.ibcn.limeds.FunctionalSegment;
import org.ibcn.limeds.descriptors.TemplateDescriptor;
import org.ibcn.limeds.json.Json;
import org.ibcn.limeds.json.JsonObject;
import org.ibcn.limeds.util.ConfigUtil;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.component.annotations.ReferencePolicy;
import org.osgi.service.metatype.AttributeDefinition;
import org.osgi.service.metatype.MetaTypeProvider;
import org.osgi.service.metatype.ObjectClassDefinition;

/**
 * This class helps other services in getting information on instanceable
 * configurations. These are either services with MetaType information for which
 * no configuration has been defined at the moment, or factory services that can
 * be instantiated with a configuration.
 * 
 * @author wkerckho
 *
 */
@Component(service = ConfigHelper.class, immediate = true)
public class ConfigHelper {

	private static final String SERVICE_PID = "metatype.pid";
	private static final String FACTORY_PID = "metatype.factory.pid";

	private Map<String, JsonObject> instanceables = new HashMap<>();

	@Reference(policy = ReferencePolicy.DYNAMIC, cardinality = ReferenceCardinality.MULTIPLE, target = "(|("
			+ SERVICE_PID + "=*)(" + FACTORY_PID + "=*))")
	public synchronized void bindProvider(MetaTypeProvider provider, Map<String, Object> properties) {
		String id = properties.containsKey(FACTORY_PID) ? "" + properties.get(FACTORY_PID)
				: "" + properties.get(SERVICE_PID);
		JsonObject desc = new JsonObject();
		desc.put("type", properties.containsKey(FACTORY_PID) ? TemplateDescriptor.Type.FACTORY.toString()
				: TemplateDescriptor.Type.CONFIGURABLE_INSTANCE.toString());
		ObjectClassDefinition ocd = provider.getObjectClassDefinition(id, null);
		desc.put("name", ocd.getName());
		desc.put("configuration", Arrays.stream(ocd.getAttributeDefinitions(ObjectClassDefinition.ALL))
				.map(this::ADToProp).collect(Json.toArray()));
		desc.put("sliceConfig", Arrays.asList((String[]) properties.get(Constants.OBJECTCLASS))
				.contains(FunctionalSegment.class.getName()));
		instanceables.put(id, desc);
	}

	public synchronized void unbindProvider(MetaTypeProvider provider, Map<String, Object> properties) {
		String id = properties.containsKey(FACTORY_PID) ? "" + properties.get(FACTORY_PID)
				: "" + properties.get(SERVICE_PID);
		instanceables.remove(id);
	}

	public Map<String, JsonObject> getInstanceables() {
		return instanceables;
	}

	private JsonObject ADToProp(AttributeDefinition ad) {
		JsonObject prop = new JsonObject();
		prop.put("name", ad.getID());
		prop.put("type", getType(ad.getType(), ad.getCardinality()));
		if (ad.getDefaultValue() != null) {
			String defaultRepr = Arrays.toString(ad.getDefaultValue());
			prop.put("value", defaultRepr.substring(1, defaultRepr.length() - 1));
		}

		if (ad.getOptionValues() != null) {
			prop.put("possibleValues", Json.arrayOf(ad.getOptionValues()));
		}
		return prop;
	}

	private String getType(int typeId, int cardinality) {
		switch (typeId) {
		case AttributeDefinition.BOOLEAN:
			return cardinality > 0 ? boolean[].class.getTypeName() : ConfigUtil.P_BOOL_NAME;
		case AttributeDefinition.DOUBLE:
			return cardinality > 0 ? double[].class.getTypeName() : ConfigUtil.P_DOUBLE_NAME;
		case AttributeDefinition.FLOAT:
			return cardinality > 0 ? float[].class.getTypeName() : ConfigUtil.P_FLOAT_NAME;
		case AttributeDefinition.INTEGER:
			return cardinality > 0 ? int[].class.getTypeName() : ConfigUtil.P_INT_NAME;
		case AttributeDefinition.LONG:
			return cardinality > 0 ? long[].class.getTypeName() : ConfigUtil.P_LONG_NAME;
		case AttributeDefinition.SHORT:
			return cardinality > 0 ? short[].class.getTypeName() : ConfigUtil.P_SHORT_NAME;
		default:
			return cardinality > 0 ? String[].class.getTypeName() : String.class.getName();
		}
	}

}
