/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.jar.JarEntry;
import java.util.jar.JarInputStream;
import java.util.stream.Collectors;

import org.ibcn.limeds.json.Json;
import org.ibcn.limeds.json.JsonObject;
import org.ibcn.limeds.json.JsonValue;

public class JarProcessor {

	private Set<String> slicePaths;
	private Map<String, byte[]> files = new HashMap<>();

	public JarProcessor(InputStream in) throws Exception {
		try (JarInputStream jarIn = new JarInputStream(in)) {
			if (jarIn.getManifest().getMainAttributes().getValue("LimeDS-Slices") != null) {
				String[] slices = jarIn.getManifest().getMainAttributes().getValue("LimeDS-Slices").split(",");
				slicePaths = Arrays.stream(slices).map(e -> e.trim()).collect(Collectors.toSet());

				JarEntry entry = jarIn.getNextJarEntry();
				while (entry != null) {
					if (!entry.isDirectory()) {
						if (slicePaths.contains(entry.getName())) {
							byte[] b = new byte[2048];
							OutputStream out = new ByteArrayOutputStream();
							int len = 0;
							while (len != -1) {
								len = jarIn.read(b);
								if (len > 0) {
									out.write(b, 0, len);
								}
							}
							out.flush();
							out.close();

							files.put(entry.getName(), ((ByteArrayOutputStream) out).toByteArray());
						}
					}
					entry = jarIn.getNextJarEntry();
				}
			} else {
				throw new Exception(
						"The supplied jar " + jarIn.getManifest().getMainAttributes().getValue("Bundle-SymbolicName")
								+ "_" + jarIn.getManifest().getMainAttributes().getValue("Bundle-Version")
								+ " does not contain LimeDS Meta-data!");
			}
		}
	}

	public Set<String> getSlicePaths() {
		return slicePaths;
	}

	public JsonValue readDescriptor(String path) throws Exception {
		JsonObject result = Json.from(new ByteArrayInputStream(files.get(path))).asObject();
		// Remove handler info
		result.remove("handlers");

		// Remove info on non-exported segments
		if (result.has("segments")) {
			result.get("segments").asArray().removeIf(segment -> !segment.getBool("export"));
		}

		return result;
	}

}
