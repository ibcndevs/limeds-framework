/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.util;

/**
 * Small utility class that can rethrow exceptions wrapped in RuntimeExceptions
 * for code that is called using lambda's.
 * <p>
 * Loosely based on the Errors mechanism of the Durian library:
 * https://github.com/diffplug/durian.
 * 
 * @author wkerckho
 *
 */
public class Errors {

	@SuppressWarnings("serial")
	public static class ErrorWrapper extends RuntimeException {

		private final Throwable wrappedError;

		public ErrorWrapper(Throwable error) {
			initCause(error);
			this.wrappedError = error;
		}

		public Throwable getWrappedError() {
			return wrappedError;
		}

		@Override
		public String getMessage() {
			return (wrappedError.getMessage() != null ? wrappedError.getMessage() : "") + "(wrapped "
					+ wrappedError.getClass().getName() + ")";
		}

	}

	@FunctionalInterface
	public interface Runnable<E extends Throwable> {
		void run() throws E;
	}

	@FunctionalInterface
	public interface Supplier<T, E extends Throwable> {
		T get() throws E;
	}

	@FunctionalInterface
	public interface Consumer<T, E extends Throwable> {
		void accept(T t) throws E;
	}

	@FunctionalInterface
	public interface Function<T, R, E extends Throwable> {
		R apply(T t) throws E;
	}

	@FunctionalInterface
	public interface Predicate<T, E extends Throwable> {
		boolean test(T t) throws E;
	}

	@FunctionalInterface
	public interface BiConsumer<T, U, E extends Throwable> {
		void accept(T t, U u) throws E;
	}

	@FunctionalInterface
	public interface BiFunction<T, U, R, E extends Throwable> {
		R apply(T t, U u) throws E;
	}

	@FunctionalInterface
	public interface BiPredicate<T, U, E extends Throwable> {
		boolean accept(T t, U u) throws E;
	}

	public static java.lang.Runnable wrapRunnable(Runnable<Throwable> runnable) {
		return () -> {
			try {
				runnable.run();
			} catch (Throwable e) {
				throw new ErrorWrapper(e);
			}
		};
	}

	public static <T> java.util.function.Consumer<T> wrapConsumer(Consumer<T, Throwable> consumer) {
		return (arg) -> {
			try {
				consumer.accept(arg);
			} catch (Throwable e) {
				throw new ErrorWrapper(e);
			}
		};
	}

	public static <T, U> java.util.function.BiConsumer<T, U> wrapBiConsumer(BiConsumer<T, U, Throwable> consumer) {
		return (arg1, arg2) -> {
			try {
				consumer.accept(arg1, arg2);
			} catch (Throwable e) {
				throw new ErrorWrapper(e);
			}
		};
	}

	public static <T, R> java.util.function.Function<T, R> wrapFunction(Function<T, R, Throwable> function) {
		return (arg) -> {
			try {
				return function.apply(arg);
			} catch (Throwable e) {
				throw new ErrorWrapper(e);
			}
		};
	}

}
