/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.util;

import java.util.Arrays;
import java.util.stream.Collectors;

import org.osgi.framework.Constants;

/**
 * Small utility class for creating OSGi service filters.
 * 
 * @author wkerckho
 *
 */
public class FilterUtil {

	public static String not(String filter) {
		return "(!" + filter + ")";
	}

	public static String and(String... filters) {
		return "(&" + Arrays.stream(filters).collect(Collectors.joining()) + ")";
	}

	public static String or(String... filters) {
		return "(|" + Arrays.stream(filters).collect(Collectors.joining()) + ")";
	}

	public static String isType(Class<?> serviceType) {
		return "(" + Constants.OBJECTCLASS + "=" + serviceType.getName() + ")";
	}

	public static String isType(String serviceTypeName) {
		return "(" + Constants.OBJECTCLASS + "=" + serviceTypeName + ")";
	}

	public static String contains(String attribute, String value) {
		return "(" + attribute + "=*" + value + "*)";
	}

	public static String equals(String attribute, String value) {
		return "(" + attribute + "=" + value + ")";
	}

	public static String greaterThanOrEqual(String attribute, String value) {
		return "(" + attribute + ">=" + value + ")";
	}

	public static String lesserThanOrEqual(String attribute, String value) {
		return "(" + attribute + "<=" + value + ")";
	}
}
