/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.util;

import org.ibcn.limeds.json.JsonValue;

/**
 * Utility class for representing ids of the format [name]_[version] which are
 * used a lot in LimeDS (slices, segments, types).
 * 
 * @author wkerckho
 *
 */
public class Identifier implements Comparable<Identifier> {

	private final String name;
	private final String version;

	public Identifier(String name, String version) {
		this.name = name;
		this.version = version;
	}

	public Identifier(JsonValue sliceDesc) {
		this(sliceDesc.getString("id"), sliceDesc.getString("version"));
	}

	public String getName() {
		return name;
	}

	public String getVersion() {
		return version;
	}

	@Override
	public String toString() {
		return name + "_" + version;
	}

	public static Identifier parse(String identifierStr) {
		int _pos = identifierStr.lastIndexOf('_');
		if (_pos > 0 && _pos < (identifierStr.length() - 1)) {
			return new Identifier(identifierStr.substring(0, _pos),
					identifierStr.substring(_pos + 1, identifierStr.length()));
		} else {
			return new Identifier(identifierStr, "*");
		}
	}

	public static Identifier parse(String identifierStr, String overrideVersion) {
		Identifier identifier = Identifier.parse(identifierStr);
		return new Identifier(identifier.getName(), overrideVersion);
	}

	@Override
	public int compareTo(Identifier o) {
		return toString().compareTo(o.toString());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((version == null) ? 0 : version.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Identifier other = (Identifier) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (version == null) {
			if (other.version != null)
				return false;
		} else if (!version.equals(other.version))
			return false;
		return true;
	}

}
