/**
 * This package contains some framework utility classes.
 */
@org.osgi.annotation.versioning.Version("1.1.0")
package org.ibcn.limeds.util;
