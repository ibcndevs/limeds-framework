/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.util;

import java.util.Arrays;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.ibcn.limeds.descriptors.SegmentDescriptor;
import org.ibcn.limeds.descriptors.TemplateDescriptor.ConfigProperty;
import org.osgi.service.cm.Configuration;
import org.osgi.service.cm.ConfigurationAdmin;

public class ConfigUtil {

	public static List<String> IGNORE_LIST = Arrays.asList("$.id", "_transient", "service.pid", "service.factoryPid",
			"limeds.configId");

	public static final String P_INT_NAME = "int";
	public static final String P_BOOL_NAME = "boolean";
	public static final String P_DOUBLE_NAME = "double";
	public static final String P_FLOAT_NAME = "float";
	public static final String P_LONG_NAME = "long";
	public static final String P_SHORT_NAME = "short";

	@SuppressWarnings("unchecked")
	private static <T> List<T> toArray(String arrayRepr) {
		return (List<T>) Arrays.stream(arrayRepr.split(",")).map(p -> p.trim()).collect(Collectors.toList());
	}

	/**
	 * Converts a property value represented as a String to the appropiate type.
	 * 
	 * @param propertyValue
	 * @return
	 */
	public static Object convertValue(String propertyValue, String type) {
		if (String.class.getName().equals(type)) {
			return propertyValue;
		} else if (Boolean.class.getName().equals(type) || P_BOOL_NAME.equals(type)) {
			return Boolean.parseBoolean(propertyValue);
		} else if (Double.class.getName().equals(type) || P_DOUBLE_NAME.equals(type)) {
			return Double.parseDouble(propertyValue);
		} else if (Float.class.getName().equals(type) || P_FLOAT_NAME.equals(type)) {
			return Float.parseFloat(propertyValue);
		} else if (Integer.class.getName().equals(type) || P_INT_NAME.equals(type)) {
			return Integer.parseInt(propertyValue);
		} else if (Long.class.getName().equals(type) || P_LONG_NAME.equals(type)) {
			return Long.parseLong(propertyValue);
		} else if (Short.class.getName().equals(type) || P_SHORT_NAME.equals(type)) {
			return Short.parseShort(propertyValue);
		} else if (String[].class.getTypeName().equals(type)) {
			return toArray(propertyValue).toArray(new String[] {});
		} else if (Boolean[].class.getTypeName().equals(type)) {
			return toArray(propertyValue).toArray(new Boolean[] {});
		} else if (Double[].class.getTypeName().equals(type)) {
			return toArray(propertyValue).toArray(new Double[] {});
		} else if (Float[].class.getTypeName().equals(type)) {
			return toArray(propertyValue).toArray(new Float[] {});
		} else if (Integer[].class.getTypeName().equals(type)) {
			return toArray(propertyValue).toArray(new Integer[] {});
		} else if (Long[].class.getTypeName().equals(type)) {
			return toArray(propertyValue).toArray(new Long[] {});
		} else if (Short[].class.getTypeName().equals(type)) {
			return toArray(propertyValue).toArray(new Short[] {});
		} else {
			return null;
		}
	}

	public static Object getDefault(String type) {
		if (String.class.getName().equals(type)) {
			return null;
		} else if (Boolean.class.getName().equals(type) || P_BOOL_NAME.equals(type)) {
			return false;
		} else if (Double.class.getName().equals(type) || P_DOUBLE_NAME.equals(type)) {
			return 0.0d;
		} else if (Float.class.getName().equals(type) || P_FLOAT_NAME.equals(type)) {
			return 0.0f;
		} else if (Integer.class.getName().equals(type) || P_INT_NAME.equals(type)) {
			return 0;
		} else if (Long.class.getName().equals(type) || P_LONG_NAME.equals(type)) {
			return 0L;
		} else if (Short.class.getName().equals(type) || P_SHORT_NAME.equals(type)) {
			return 0;
		} else if (String[].class.getTypeName().equals(type)) {
			return new String[] {};
		} else if (Boolean[].class.getTypeName().equals(type)) {
			return new Boolean[] {};
		} else if (Double[].class.getTypeName().equals(type)) {
			return new Double[] {};
		} else if (Float[].class.getTypeName().equals(type)) {
			return new Float[] {};
		} else if (Integer[].class.getTypeName().equals(type)) {
			return new Integer[] {};
		} else if (Long[].class.getTypeName().equals(type)) {
			return new Long[] {};
		} else if (Short[].class.getTypeName().equals(type)) {
			return new Short[] {};
		} else {
			return null;
		}
	}

	public static void injectDefaults(Object targetInstance, List<ConfigProperty> props) {
		Arrays.stream(targetInstance.getClass().getDeclaredFields())
				.filter(f -> props.stream().anyMatch(prop -> f.getName().equals(prop.name)))
				.forEach(Errors.wrapConsumer(f -> {
					Optional<ConfigProperty> match = props.stream()
							.filter(p -> p.name.equals(f.getName()) && p.value != null && !p.value.isEmpty()).findAny();
					f.setAccessible(true);
					f.set(targetInstance,
							match.isPresent() ? ConfigUtil.convertValue(match.get().value, match.get().type)
									: ConfigUtil.getDefault(f.getType().getName()));
				}));
	}

	public static void createDefaultConfig(ConfigurationAdmin configAdmin, SegmentDescriptor descriptor)
			throws Exception {
		String pid = descriptor.getId();

		if (configAdmin.listConfigurations(FilterUtil.equals("service.pid", pid)) == null) {
			Configuration config = configAdmin.getConfiguration(pid, null);
			Dictionary<String, Object> dict = new Hashtable<>();

			// Add template instantiated properties to config
			descriptor.getTemplateInfo().getConfiguration().forEach(cp -> {
				dict.put(cp.name, ConfigUtil.convertValue(cp.value, cp.type));
			});

			/*
			 * Indicate that the LimeDS config manager should not persist this
			 * config
			 */
			dict.put("_transient", true);

			config.update(dict);
		}
	}

}
