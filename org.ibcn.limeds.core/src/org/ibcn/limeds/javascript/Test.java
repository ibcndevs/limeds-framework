/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.javascript;

import org.ibcn.limeds.bindings.LanguageAdapter;
import org.ibcn.limeds.json.Json;
import org.ibcn.limeds.json.JsonArray;
import org.ibcn.limeds.json.JsonObject;

public class Test {

	public static void main(String[] args) throws Exception {
		JavascriptAdapterFactory factory = new JavascriptAdapterFactory();
		LanguageAdapter adapter = factory.createAdapter(Test.class.getResourceAsStream("test.js"));

		JsonObject complexObject = Json.objectBuilder().add("singleAttribute", "check")
				.add("nestedObject", Json.objectBuilder().add("prop1", true).add("prop2", 9999))
				.add("nestedArray", Json.arrayOf(1, 2, 3, 4, 5, 6, 7, 8))
				.add("nestedComplexArray",
						Json.arrayOf(Json.objectBuilder().add("id", 0).build(),
								Json.objectBuilder().add("id", 1).build(), Json.objectBuilder().add("id", 2).build()))
				.build();
		adapter.call("runTests", adapter.wrap(new JsonObject(), complexObject, new JsonArray(),
				Json.arrayOf("Dit", "is", "een", "array")));
	}

}
