/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.javascript;

import java.io.InputStream;

import org.ibcn.limeds.bindings.LanguageAdapter;
import org.ibcn.limeds.bindings.LanguageAdapterFactory;
import org.ibcn.limeds.descriptors.SegmentLanguage;
import org.osgi.service.component.annotations.Component;

/**
 * The JavascriptAdapterFactory exposes a language binding for Javascript
 * conforming with the specification in org.ibcn.limeds.bindings.
 * 
 * @author wkerckho
 *
 */
@Component
public class JavascriptAdapterFactory implements LanguageAdapterFactory {

	@Override
	public LanguageAdapter createAdapter(InputStream codeStream) throws Exception {
		return new JavascriptBinding(codeStream);
	}

	@Override
	public LanguageAdapter createAdapter(String code) throws Exception {
		return new JavascriptBinding(code);
	}

	@Override
	public String getLanguageId() {
		return SegmentLanguage.Javascript.toString();
	}

}
