/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.javascript;

import java.util.AbstractList;

import org.ibcn.limeds.json.JsonArray;
import org.ibcn.limeds.json.JsonValue;

/**
 * Utility class that allows the LimeDS JSON Array model to be used as native
 * JSON in Nashorn.
 * 
 * @author wkerckho
 *
 */
public class JavascriptArrayDecorator extends AbstractList<Object> {

	private final JavascriptBinding context;
	private final JsonArray array;

	public JavascriptArrayDecorator(JavascriptBinding context, JsonArray array) {
		this.context = context;
		this.array = array;
	}

	@Override
	public Object get(int index) {
		if (index >= 0 && index < array.size()) {
			return context.wrap(array.get(index))[0];
		} else {
			throw new IndexOutOfBoundsException();
		}
	}

	@Override
	public Object set(int index, Object element) {
		JsonValue previous = array.get(index);
		array.set(index, context.unwrap(element));
		return context.wrap(previous)[0];
	}

	@Override
	public void add(int index, Object element) {
		array.add(index, context.unwrap(element));
	}

	@Override
	public Object remove(int index) {
		return context.wrap(array.remove(index))[0];
	}

	@Override
	public int size() {
		return array.size();
	}

	public JsonArray getArray() {
		return array;
	}

	@Override
	public int hashCode() {
		return array.hashCode();
	}

	@Override
	public boolean equals(Object other) {
		return array.equals(other);
	}

}
