/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.javascript;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Set;

import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;

import org.ibcn.limeds.bindings.LanguageAdapter;
import org.ibcn.limeds.json.Json;
import org.ibcn.limeds.json.JsonArray;
import org.ibcn.limeds.json.JsonObject;
import org.ibcn.limeds.json.JsonPrimitive;
import org.ibcn.limeds.json.JsonValue;
import org.ibcn.limeds.util.Errors;
import org.ibcn.limeds.util.StreamUtil;

/**
 * JavascriptBinding for LimeDS, implementing the LanguageAdapter interface.
 * 
 * @author wkerckho
 *
 */
public class JavascriptBinding implements LanguageAdapter {

	private String code;
	private ScriptEngine engine = new ScriptEngineManager().getEngineByName("nashorn");

	public JavascriptBinding(InputStream codeStream) throws Exception {
		this.code = StreamUtil.getStringFromInputStream(codeStream);
		initEngine();
	}

	public JavascriptBinding(String code) throws Exception {
		this.code = code;
		initEngine();
	}

	private void initEngine() throws Exception {
		// Load LimeDS JS lib
		engine.eval(new InputStreamReader(JavascriptBinding.class.getResourceAsStream("limeds-lib.js")));
		engine.eval(code);
	}

	@Override
	public Object[] wrap(JsonValue... args) {
		return Arrays.stream(args).map(json -> {
			if (json instanceof JsonArray) {
				return new JavascriptArrayDecorator(this, json.asArray());
			} else if (json instanceof JsonObject) {
				return new JavascriptObjectDecorator(this, json.asObject());
			} else {
				return json.asPrimitive().getValue();
			}
		}).toArray(size -> new Object[size]);

	}

	@Override
	public JsonValue unwrap(Object obj) {
		if (obj instanceof JavascriptArrayDecorator) {
			return ((JavascriptArrayDecorator) obj).getArray();
		} else if (obj instanceof JavascriptObjectDecorator) {
			return ((JavascriptObjectDecorator) obj).getObject();
		} else if (ScriptObjectMirror.isInstance(obj)) {
			try {
				ScriptObjectMirror mirror = new ScriptObjectMirror(obj);
				if (mirror.isArray()) {
					// Parse array
					return mirror.values().stream().map(this::unwrap).collect(Json.toArray());
				} else {
					// Parse object
					JsonObject object = new JsonObject();
					mirror.keySet().forEach(Errors.wrapConsumer(k -> {
						object.put(k, unwrap(mirror.get(k)));
					}));
					return object;
				}
			} catch (Exception e) {
				return null;
			}
		} else {
			return new JsonPrimitive(obj);
		}
	}

	@Override
	public Object call(String functionName, Object... params) throws Exception {
		return ((Invocable) engine).invokeFunction(functionName, params);
	}

	@Override
	public void setGlobal(String name, Object value) {
		engine.put(name, value);
	}

	@Override
	public LanguageAdapter newInstance() throws Exception {
		return new JavascriptBinding(code);
	}

	@Override
	public Object getAliasRouter(Object baseFunction, Set<String> aliases) throws Exception {
		return call("_createAliasRouter", baseFunction, aliases.toArray(new String[] {}));
	}

}
