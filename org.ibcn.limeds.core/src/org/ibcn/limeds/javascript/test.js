var assert = function (label, val, expectedVal) {
	if (val !== expectedVal) {
		throw "Assertion failed: " + label;
	}
};

var runTests = function (javaEmptyObject, javaComplexObject, javaEmptyArray, javaArray) {
	var jsEmptyObject = {};
	var jsComplexObject = {
		singleAttribute: "check",
		nestedObject: {
			prop1: true,
			prop2: 9999
		},
		nestedArray: [1, 2, 3, 4, 5, 6, 7, 8],
		nestedComplexArray: [{
			id: 0
		},
			{
				id: 1
			},
			{
				id: 2
			}]
	};

	var jsEmptyArray = [];
	var jsArray = ["Dit", "is", "een", "array"];

	// Tests
	// cloneDeep
	assert('cloneDeep(complex JS object)', (function () {
		var copy = _limeds.cloneDeep(jsComplexObject);
		var result = _limeds.isEqual(jsComplexObject, copy);
		copy.singleAttribute = "ok";
		var result = result && !_limeds.isEqual(jsComplexObject, copy);
		return result;
	})(), true);

	// every
	assert("every(js array)", _limeds.every(jsArray,function(val) { return val.length >= 2}),true);

	// map
	assert("map(js array of strings to int)", (function () {
		var arr = _limeds.map(jsArray, function (val) { return val.length});
		var check = [3,2,3,5];
		return _limeds.isEqual(arr, check);
	})(), true);

	assert("map(java array of strings to int)", (function () {
		var arr = _limeds.map(javaArray, function (val) { return val.length});
		var check = [3,2,3,5];
		return _limeds.isEqual(arr, check);
	})(), true);

	assert("map(js object)", (function () {
		var arr = _limeds.map(jsComplexObject, function (key,val) { return key});
		var check = ["singleAttribute","nestedObject", "nestedArray","nestedComplexArray"];
		return _limeds.isEqual(arr, check);
	})(), true);

	assert("map(java object)", (function () {
		var arr = _limeds.map(javaComplexObject, function (key,val) { return key});
		var check = ["singleAttribute","nestedObject", "nestedArray","nestedComplexArray"];
		return _limeds.isEqual(arr, check);
	})(), true);

	//Test isEmpty
	assert("isEmpty(empty Java object)", _limeds.isEmpty(javaEmptyObject), true);
	assert("isEmpty(empty JS object)", _limeds.isEmpty(jsEmptyObject), true);
	assert("isEmpty(complex Java object)", _limeds.isEmpty(javaComplexObject), false);
	assert("isEmpty(complex JS object)", _limeds.isEmpty(jsComplexObject), false);
	assert("isEmpty(emtpy Java array)", _limeds.isEmpty(javaEmptyArray), true);
	assert("isEmpty(empty JS array)", _limeds.isEmpty(jsEmptyArray), true);
	assert("isEmpty(Java array)", _limeds.isEmpty(javaArray), false);
	assert("isEmpty(JS array", _limeds.isEmpty(jsArray), false);

	//Test has
	assert("has(empty JS object)", _limeds.has(jsEmptyObject, "nestedObject"), false);
	assert("has(empty Java object)", _limeds.has(javaEmptyObject, "nestedObject"), false);
	assert("has(JS object)", _limeds.has(jsComplexObject, "nestedObject"), true);
	assert("has(Java object)", _limeds.has(javaComplexObject, "nestedObject"), true);

	//Test isEqual
	assert("isEqual(equal Java objects)", _limeds.isEqual(javaComplexObject, javaComplexObject), true);
	assert("isEqual(equal JS objects)", _limeds.isEqual(jsComplexObject, jsComplexObject), true);
	assert("isEqual(equal JS & Java objects)", _limeds.isEqual(javaComplexObject, jsComplexObject), true);
	assert("isEqual(not equal JS & empty objects)", _limeds.isEqual(jsComplexObject, jsEmptyObject), false);

	// 
};