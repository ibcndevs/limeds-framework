/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.javascript;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Collection;
import java.util.Set;

import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;

/**
 * According to the Eclipse compiler, the package "jdk.nashorn.api.scripting" is
 * no public API, so there is an access restriction in place for all the types
 * in this package. This can be circumvented by setting a custom compiler in the
 * project settings, but this can lead to other issues depending on the
 * build-situation.
 * <p>
 * This is why we added our own ScriptObjectMirror object that uses reflection
 * to call the methods that we need in LimeDS.
 * 
 * @author wkerckho
 *
 */
public class ScriptObjectMirror {

	private static Method isArray;
	private static Method values;
	private static Method keySet;
	private static Method get;
	private static Method convertToScriptObjectMirror;

	{
		ScriptEngine engine = new ScriptEngineManager().getEngineByName("nashorn");
		engine.eval("function getObject() { return {}; };");
		Object scriptObjectMirror = ((Invocable) engine).invokeFunction("getObject");
		isArray = scriptObjectMirror.getClass().getDeclaredMethod("isArray");
		values = scriptObjectMirror.getClass().getDeclaredMethod("values");
		keySet = scriptObjectMirror.getClass().getDeclaredMethod("keySet");
		get = scriptObjectMirror.getClass().getDeclaredMethod("get", Object.class);

		Class<?> scriptUtils = Class.forName("jdk.nashorn.api.scripting.ScriptUtils", false,
				scriptObjectMirror.getClass().getClassLoader());
		convertToScriptObjectMirror = Arrays.stream(scriptUtils.getDeclaredMethods())
				.filter(m -> m.getName().equals("wrap")).findFirst().get();
	}

	private final Object target;

	public ScriptObjectMirror(Object target) throws Exception {
		if (target.getClass().getSuperclass() != null
				&& target.getClass().getSuperclass().getName().equals("jdk.nashorn.internal.runtime.ScriptObject")) {
			this.target = convertToScriptObjectMirror.invoke(null, target);
		} else {
			this.target = target;
		}
	}

	public boolean isArray() throws Exception {
		return (boolean) isArray.invoke(target);
	}

	@SuppressWarnings("unchecked")
	public Collection<Object> values() throws Exception {
		return (Collection<Object>) values.invoke(target);
	}

	@SuppressWarnings("unchecked")
	public Set<String> keySet() throws Exception {
		return (Set<String>) keySet.invoke(target);
	}

	public Object get(Object key) throws Exception {
		return get.invoke(target, key);
	}

	public static boolean isInstance(Object object) {
		return object != null && (object.getClass().getName().equals("jdk.nashorn.api.scripting.ScriptObjectMirror")
				|| object.getClass().getSuperclass().getName().equals("jdk.nashorn.internal.runtime.ScriptObject"));
	}

}
