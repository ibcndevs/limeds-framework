var Arrays = Java.type("java.util.Arrays");
var getTypeName = Object.prototype.toString;

var _limeds = {
	/*
	 * Returns a new array that contains the elements of both parameter arrays.
	 */
	concat: function (targetArray, arrayToAdd) {
		var arr = [];
		_limeds.forEach(targetArray, function (item) {
			arr.push(item);
		});
		_limeds.forEach(arrayToAdd, function (item) {
			arr.push(item);
		});
		return arr;
	},
	/*
	 * Returns a deep-copy of the supplied container type (object or array).
	 */
	cloneDeep: function (object) {
		if (_limeds.isType(object,
			"org.ibcn.limeds.javascript.JavascriptObjectDecorator")
			|| _limeds.isType(object, "Object")) {
			var tmp = _limeds.map(object, function (key, val) {
				return [key, _limeds.cloneDeep(val)];
			});
			return _limeds.reduce(tmp, function (result, item) {
				result[item[0]] = item[1];
				return result;
			}, {});
		} else if (_limeds.isType(object,
			"org.ibcn.limeds.javascript.JavascriptArrayDecorator")
			|| _limeds.isType(object, "Array")) {
			return _limeds.map(object, function (el) {
				return _limeds.cloneDeep(el);
			});
		} else {
			return object;
		}
	},
	/*
	 * Returns true if the provided predicate resolves to true for each entry of
	 * the supplied container type (object or array).
	 */
	every: function (object, predicateFunction) {
		return _limeds.isEmpty(_limeds.filter(object, function (p1, p2) {
			return !predicateFunction(p1, p2);
		}));
	},
	/*
	 * Returns a filtered copy of the supplied container type (object or array)
	 * based on the provided filter function.
	 * 
	 * When the container is an object, a filter function with two arguments
	 * should be provided (key, value).
	 */
	filter: function (object, filterFunction) {
		if (_limeds.isType(object,
			"org.ibcn.limeds.javascript.JavascriptObjectDecorator")
			|| _limeds.isType(object, "Object")) {
			var result = {};
			_limeds.forEach(object, function (key, val) {
				if (filterFunction(key, val)) {
					result[key] = val;
				}
			});
			return result;
		} else if (_limeds.isType(object,
			"org.ibcn.limeds.javascript.JavascriptArrayDecorator")
			|| _limeds.isType(object, "Array")
			|| _limeds.getType(object).startsWith("[")) {
			var result = [];
			_limeds.forEach(object, function (el) {
				if (filterFunction(el)) {
					result.push(el);
				}
			});
			return result;
		}
	},
	/*
	 * Executes the provided iteration function for each entry of the supplied
	 * container type (object or array).
	 * 
	 * When the container is an object, an iteration function with two arguments
	 * should be provided (key, value).
	 */
	forEach: function (object, iterFunction) {
		if (_limeds.isType(object,
			"org.ibcn.limeds.javascript.JavascriptObjectDecorator")
			|| _limeds.isType(object,
				"org.ibcn.limeds.javascript.JavascriptArrayDecorator")) {
			object.forEach(iterFunction);
		} else if (_limeds.isType(object, "Object")) {
			var keys = _limeds.keySet(object);
			for (var i = 0; i < keys.length; i++) {
				iterFunction(keys[i], object[keys[i]]);
			}
		} else if (_limeds.isType(object, "Array")) {
			for (var i = 0; i < object.length; i++) {
				iterFunction(object[i]);
			}
		} else if (_limeds.getType(object).startsWith("[")) {
			Arrays.stream(object).forEach(iterFunction);
		}
	},
	/*
	 * Checks if an object contains a specified key.
	 */
	has: function (object, key) {
		if (_limeds.isType(object,
			"org.ibcn.limeds.javascript.JavascriptObjectDecorator")) {
			return object.containsKey(key) && object.get(key) != null;
		} else if (_limeds.isType(object, "Object")) {
			return object.hasOwnProperty(key) && object[key] !== null;
		} else {
			return false;
		}
	},
	/*
	 * Returns the size of a container type (object or array).
	 */
	size: function (object) {
		if (_limeds.isType(object,
			"org.ibcn.limeds.javascript.JavascriptObjectDecorator")
			|| _limeds.isType(object,
				"org.ibcn.limeds.javascript.JavascriptArrayDecorator")) {
			return object.size();
		} else if (_limeds.isType(object, "Object")) {
			return _limeds.keySet(object).length;
		} else if (_limeds.isType(object, "Array")
			|| _limeds.getType(object).startsWith("[")) {
			return object.length;
		} else {
			return 0;
		}
	},
	/*
	 * Checks if the supplied container type (object or array) is empty.
	 */
	isEmpty: function (object) {
		return _limeds.size(object) === 0;
	},
	/*
	 * Checks if the two arguments are equal.
	 */
	isEqual: function (object, other) {
		if ((object == null && other != null)
			|| (object != null && other == null)) {
			return false;
		}

		if ((_limeds.isType(object,
			"org.ibcn.limeds.javascript.JavascriptObjectDecorator") || _limeds
				.isType(object,
				"org.ibcn.limeds.javascript.JavascriptArrayDecorator"))
			&& (_limeds.isType(other,
				"org.ibcn.limeds.javascript.JavascriptObjectDecorator") || _limeds
					.isType(other,
					"org.ibcn.limeds.javascript.JavascriptArrayDecorator"))) {
			return object.equals(other);
		} else if (_limeds.isType(object,
			"org.ibcn.limeds.javascript.JavascriptObjectDecorator")
			|| _limeds.isType(object, "Object")) {
			return _limeds.size(object) == _limeds.size(other)
				&& _limeds.every(object, function (k, v) {
					return _limeds.isEqual(v, other[k])
				});
		} else if (_limeds.isType(object,
			"org.ibcn.limeds.javascript.JavascriptArrayDecorator")
			|| _limeds.isType(object, "Array")) {
			return _limeds.size(object) == _limeds.size(other)
				&& _limeds.every(_limeds
					.getIndexArray(_limeds.size(object)), function (
						index) {
						return _limeds.isEqual(object[index], other[index]);
					});
		} else {
			return object === other;
		}
		return false;
	},
	/*
	 * Get an index array of a certain size, e.g. getIndexArray(3) produces
	 * [0,1,2];
	 */
	getIndexArray: function (size) {
		var result = [];
		while (size > 0) {
			result.unshift(--size);
		}
		return result;
	},
	/*
	 * Returns the type of the arguments as a String.
	 */
	getType: function (object) {
		var typeName = getTypeName.call(object);
		return typeName.substring("[object ".length, typeName.length - 1);
	},
	/*
	 * Checks if the first argument is of the type described by the second
	 * argument as a String.
	 */
	isType: function (object, typeStr) {
		return _limeds.getType(object) === typeStr;
	},
	/*
	 * Returns the key-set of an object type as an array.
	 */
	keySet: function (object) {
		if (_limeds.isType(object,
			"org.ibcn.limeds.javascript.JavascriptObjectDecorator")) {
			var keySet = [];
			object.keySet().forEach(function (el) {
				keySet.push(el);
			});
			return keySet;
		} else if (_limeds.isType(object, "Object")) {
			return Object.keys(object);
		} else {
			return [];
		}
	},
	/*
	 * Returns an array obtained by executing the supplied mapping function on
	 * the provided container type (object or array). based on the provided
	 * filter function.
	 * 
	 * When the container is an object, a mapping function with two arguments
	 * should be provided (key, value).
	 */
	map: function (object, mapperFunction) {
		if (_limeds.isType(object,
			"org.ibcn.limeds.javascript.JavascriptObjectDecorator")
			|| _limeds.isType(object, "Object")) {
			var result = [];
			_limeds.forEach(object, function (key, val) {
				result.push(mapperFunction(key, val));
			});
			return result;
		} else if (_limeds.isType(object,
			"org.ibcn.limeds.javascript.JavascriptArrayDecorator")
			|| _limeds.isType(object, "Array")
			|| _limeds.getType(object).startsWith("[")) {
			var result = [];
			_limeds.forEach(object, function (el) {
				result.push(mapperFunction(el));
			});
			return result;
		}
	},
	/*
	 * Reduces an array / list of values to a single value by applying the
	 * supplied reduceFunction to each entry. The last argument of this function
	 * is the initial value for the reduce operation.
	 * 
	 * The supplied reduceFunction must have two arguments: the temporary value
	 * of the single value that is being built up and the current element that
	 * needs to be reduced. The function must always return the new temprary
	 * value.
	 */
	reduce: function (list, reduceFunction, initialState) {
		if (_limeds.isType(list,
			"org.ibcn.limeds.javascript.JavascriptArrayDecorator")) {
			return list.stream().reduce(initialState, reduceFunction);
		} else if (_limeds.isType(list, "Array")
			|| _limeds.getType(object).startsWith("[")) {
			var result = initialState;
			_limeds.forEach(list, function (el) {
				result = reduceFunction(result, el);
			});
			return result;
		}
	},
	/*
	 * Checks if there is a least one entry in the supplied container type
	 * (object or array) for which the supplied predicate function resolves to
	 * true. This operation returns immediately if this is the case.
	 */
	some: function (object, predicateFunction) {
		if (_limeds.isType(object,
			"org.ibcn.limeds.javascript.JavascriptArrayDecorator")) {
			return object.stream().anyMatch(predicateFunction);
		} else if (_limeds.isType(object,
			"org.ibcn.limeds.javascript.JavascriptObjectDecorator")
			|| _limeds.isType(object, "Object")) {
			var keys = _limeds.keySet(object);
			for (var i = 0; i < keys.length; i++) {
				if (predicateFunction(keys[i], object[keys[i]])) {
					return true;
				}
			}
		} else if (_limeds.isType(object, "Array")
			|| _limeds.getType(object).startsWith("[")) {
			for (var i = 0; i < object.length; i++) {
				if (predicateFunction(object[i])) {
					return true;
				}
			}
		}
		return false;
	},
	/*
	 * Returns the current system time in milliseconds.
	 */
	timestamp: function () {
		return java.lang.System.currentTimeMillis();
	}
};

var _createAliasRouter = function(targetObject, aliases) {
	var wrapper = {
		apply: function() {
			var params = [];
			for(var i = 0; i < arguments.length; i++) {
				params.push(arguments[i]);
			}
			return targetObject.expandArgsThenApply(params);
		}
	};
	
	for(var j = 0; j < aliases.length; j++) {
		wrapper[aliases[j]] = (function() {
			var alias = aliases[j];
			return function() {
				var params = [];
				for(var i = 0; i < arguments.length; i++) {
					params.push(arguments[i]);
				}
				return targetObject.apply(alias, params);
			}
		})();
	}
	
	return wrapper;
};