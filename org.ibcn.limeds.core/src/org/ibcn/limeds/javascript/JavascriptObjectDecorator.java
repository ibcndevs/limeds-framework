/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.javascript;

import java.util.AbstractMap;
import java.util.AbstractSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.ibcn.limeds.json.JsonObject;
import org.ibcn.limeds.json.JsonValue;

/**
 * Utility class that allows the LimeDS JSON Object model to be used as native
 * JSON in Nashorn.
 * 
 * @author wkerckho
 *
 */
public class JavascriptObjectDecorator extends AbstractMap<String, Object> {

	private final JavascriptBinding context;
	private final JsonObject object;

	public JavascriptObjectDecorator(JavascriptBinding context, JsonObject object) {
		this.context = context;
		this.object = object;
	}

	public JsonObject getObject() {
		return object;
	}

	@Override
	public Set<Map.Entry<String, Object>> entrySet() {
		return new AbstractSet<Map.Entry<String, Object>>() {

			@Override
			public Iterator<Map.Entry<String, Object>> iterator() {
				return new Iterator<java.util.Map.Entry<String, Object>>() {

					private Iterator<String> keyIter = object.keySet().iterator();
					private Map.Entry<String, Object> removable = null;

					@Override
					public boolean hasNext() {
						return keyIter.hasNext();
					}

					@Override
					public void remove() {
						if (removable != null) {
							object.remove(removable.getKey());
							removable = null;
						}
					}

					@Override
					public Map.Entry<String, Object> next() {
						final String key = keyIter.next();
						removable = new Map.Entry<String, Object>() {

							@Override
							public String getKey() {
								return key;
							}

							@Override
							public Object getValue() {
								return context.wrap(object.get(key))[0];
							}

							@Override
							public JsonValue setValue(Object value) {
								throw new UnsupportedOperationException("Cannot set JSON property using iterator!");
							}

						};
						return removable;
					}

				};
			}

			@Override
			public int size() {
				return object.size();
			}

		};
	}

	@Override
	public Object put(String key, Object value) {
		JsonValue previous = object.put(key, (JsonValue) context.unwrap(value));
		if (previous != null) {
			return context.wrap(previous)[0];
		} else {
			return null;
		}
	}

	@Override
	public int hashCode() {
		return object.hashCode();
	}

	@Override
	public boolean equals(Object other) {
		if (this == other) {
			return true;
		} else if (other instanceof JavascriptObjectDecorator) {
			return object.equals(((JavascriptObjectDecorator) other).object);
		} else {
			return false;
		}
	}

}
