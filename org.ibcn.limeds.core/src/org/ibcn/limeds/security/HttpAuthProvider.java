/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.security;

import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ibcn.limeds.descriptors.HttpOperationDescriptor;
import org.ibcn.limeds.json.JsonValue;

/**
 * Implement this interface and register it as an OSGi service to add
 * authentication/authorization to the HTTP operations exposed by the Segments.
 * 
 * @author wkerckho
 *
 */
public interface HttpAuthProvider {

	/**
	 * Check if the request is authorized.
	 * 
	 * @param request
	 *            The http request to be checked
	 * @param response
	 *            The http response object, allows altering the response (in
	 *            case the request is not valid, e.g. as error reporting)
	 * @param httpOperation
	 *            A descriptor of the HTTP operation to which the request is
	 *            sent
	 * 
	 * @return An optional object containing authentication information provided
	 *         by the system, or <em>empty</em> if the request has not been
	 *         authorized.
	 *         <p>
	 *         The JsonValue containing this authentication information is
	 *         automatically added by the framework as the last argument of the
	 *         apply-function call.
	 */
	Optional<JsonValue> authorizeRequest(HttpServletRequest request, HttpServletResponse response,
			HttpOperationDescriptor httpOperation);

}
