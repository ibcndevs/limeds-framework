/**
 * This package contains interfaces used for security.
 */
@org.osgi.annotation.versioning.Version("1.0.0")
package org.ibcn.limeds.security;
