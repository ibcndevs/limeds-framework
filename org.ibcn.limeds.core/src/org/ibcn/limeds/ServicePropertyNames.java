/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds;

/**
 * This interface bundles a number of service level constants.
 * 
 * @author wkerckho
 *
 */
public interface ServicePropertyNames {

	/**
	 * Defines the name of the service property that represents the id of the
	 * Segment.
	 */
	public static final String SEGMENT_ID = "limeds.segment.id";

	/**
	 * Defines the name of the service property that represents the tags of the
	 * Segment.
	 */
	public static final String SEGMENT_TAGS = "limeds.segment.tags";

	/**
	 * Defines the name of the service property that represents the schedule of
	 * the Segment.
	 */
	public static final String SEGMENT_SCHEDULE = "limeds.segment.schedule";

	/**
	 * Defines the name of the service property that represents the version of
	 * the Segment.
	 */
	public static final String SEGMENT_VERSION = "limeds.segment.version";

	/**
	 * Defines the name of the service property that represents the
	 * subscriptions of the Segment.
	 */
	public static final String SEGMENT_SUBSCRIPTIONS = "limeds.segment.subscriptions";

	public static final String TYPE_ID = "limeds.type.id";

	public static final String TYPE_VERSION = "limeds.type.version";

}
