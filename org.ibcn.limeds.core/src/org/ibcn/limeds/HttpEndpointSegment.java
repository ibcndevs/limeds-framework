/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds;

import java.util.Optional;

import org.ibcn.limeds.json.JsonObject;
import org.ibcn.limeds.json.JsonValue;

/**
 * Utility class that streamlines writing HTTP capable Segments in Java. Extend
 * this class and annotate the apply-method with the @HttpOperation annotation
 * to get started.
 * 
 * @author wkerckho
 *
 */
public abstract class HttpEndpointSegment implements FunctionalSegment {

	@Override
	public JsonValue apply(JsonValue... input) throws Exception {
		HttpContext context = new HttpContext(input.length > 1 ? input[1] : new JsonObject());
		return apply(input[0], context);
	}

	public abstract JsonValue apply(JsonValue input, HttpContext context) throws Exception;

	/**
	 * Definition of the HttpContext that is provided when handling the HTTP
	 * call.
	 */
	public static class HttpContext {

		private JsonObject jsonContext;

		HttpContext(JsonValue httpContext) {
			jsonContext = httpContext.asObject();
			if (!jsonContext.has("response")) {
				jsonContext.put("response", new JsonObject());
			}

			if (!jsonContext.get("response").has("headers")) {
				jsonContext.get("response").asObject().put("headers", new JsonObject());
			}
		}

		/**
		 * Returns the request headers as a JSON object (header name to value
		 * mapping).
		 */
		public JsonObject requestHeaders() {
			return jsonContext.get("request").get("headers").asObject();
		}

		/**
		 * Returns the query paremeters as a JSON object (query parameter name
		 * to value mapping).
		 */
		public JsonObject queryParameters() {
			return jsonContext.get("request").get("query").asObject();
		}

		/**
		 * Utility method for toggleable modes implemented using query
		 * parameters. This method will return true if the specified parameter
		 * is either present (with no value) or has the boolean value true. If
		 * the parameter has the boolean value false or another non-boolean,
		 * non-empty value, the method returns false.
		 * <p>
		 * E.g. when calling the /_limeds/slices/{id}/{version}/deploy
		 * operation, the query parameter ?sync=true will cause the deployment
		 * to be executed synchronously. Using ?sync has the same effect and is
		 * often used as a shorthand. When writing ?sync=false, ?sync=someWord
		 * or when omitting the parameters, the operation is executed
		 * asynchronously.
		 */
		public boolean isActiveQueryFlag(String paramName) {
			return queryParameters().has(paramName)
					&& (queryParameters().getBool(paramName) || queryParameters().getString(paramName).isEmpty());
		}

		/**
		 * Returns the path parameters as a JSON object (path paramter name to
		 * value mapping).
		 */
		public JsonObject pathParameters() {
			return jsonContext.get("request").get("path").asObject();
		}

		/**
		 * Returns the authentication information (if provided) as a JSON value.
		 * Null is returned when authentication or authorization is not enabled
		 * for the call.
		 */
		public JsonValue authInfo() {
			return jsonContext.get("request").get("auth");
		}

		/**
		 * Sets the HTTP response code for this call.
		 */
		public void respondWith(int status) {
			jsonContext.get("response").asObject().put("status", status);
		}

		/**
		 * Sets an HTTP response header.
		 */
		public void respondWith(String headerName, String headerValue) {
			jsonContext.get("response").get("headers").asObject().put(headerName, headerValue);
		}

		public Optional<String> getRequestOrigin() {
			return jsonContext.get("request").get("headers").asObject().entrySet().stream()
					.filter(e -> e.getKey().toLowerCase().equals("origin")).map(e -> e.getValue().asString())
					.findFirst();
		}

		public String getRequestPath() {
			return jsonContext.get("request").getString("fullPath");
		}

		public JsonObject getSource() {
			return jsonContext;
		}

	}

}
