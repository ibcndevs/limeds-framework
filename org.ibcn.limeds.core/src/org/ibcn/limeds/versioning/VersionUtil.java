/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.versioning;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.ibcn.limeds.util.FilterUtil;
import org.ibcn.limeds.util.Pair;

public class VersionUtil {

	/**
	 * Utility method to create a service filter for the supplied version
	 * expression (allows versioning of LimeDS Flow Functions using the OSGi
	 * service layer).
	 * <p>
	 * Currently supports wildcards and caret ranges only
	 */
	public static String createFilter(String propertyName, String versionExpression) {
		/*
		 * Support the use of caret ranges: E.g. "^1.2.3" will generate a filter
		 * that accepts any version >=1.2.3 and <2.0.0.
		 */
		if (versionExpression.startsWith("^")) {
			Pair<String, String> interval = getInterval(versionExpression);
			return FilterUtil.and(FilterUtil.greaterThanOrEqual(propertyName, interval.left()),
					FilterUtil.not(FilterUtil.greaterThanOrEqual(propertyName, interval.right())));
		}

		/*
		 * Support the use of wildcards: E.g. "1.*" will generate a filter that
		 * accepts any version with major digit 1.
		 */
		if (versionExpression.contains("*")) {
			return FilterUtil.equals(propertyName, "*");
			// TODO check code below, not working according to spec
			// String baseVersion =
			// createVersionPropertyValue(versionExpression.replace("*", "0"));
			// String[] baseParts = new String[] { baseVersion.substring(0, 3),
			// baseVersion.substring(3, 6),
			// baseVersion.substring(6, 9) };
			// String[] parts = versionExpression.split("\\.");
			// for (int i = 0; i < parts.length; i++) {
			// if ("*".equals(parts[i])) {
			// baseParts[i] = "*";
			// }
			// }
			// return FilterUtil.equals(propertyName,
			// Arrays.stream(baseParts).collect(Collectors.joining()));
		}

		return FilterUtil.equals(propertyName, createVersionPropertyValue(versionExpression));
	}

	public static Pair<String, String> getInterval(String intervalExpr) {
		String startVersion = intervalExpr.substring(1);
		String[] parts = startVersion.split("\\.");
		String endVersion = (Integer.parseInt(parts[0]) + 1) + ".0.0";
		return new Pair<>(createVersionPropertyValue(startVersion), createVersionPropertyValue(endVersion));
	}

	/**
	 * Utility method that takes a string representation of a LimeDS version and
	 * returns a representative String value which can be used in the OSGi
	 * service registry and correctly supports comparison operations. LimeDS
	 * only supports MAJOR, MINOR &amp; PATCH version numbers &lt; 1000!
	 */
	public static String createVersionPropertyValue(String version) {
		if (version != null && !version.isEmpty()) {
			String[] parts = version.split("\\.");
			if (parts.length <= 3) {
				long result = 0;
				long multiplier = 1000000;
				for (int i = 0; i < parts.length; i++) {
					result += Integer.parseInt(parts[i]) * multiplier;
					multiplier /= 1000;
				}
				String strResult = "" + result;
				int length = strResult.length();
				for (int i = 0; i < 9 - length; i++) {
					strResult = "0" + strResult;
				}
				return strResult;
			}
		}
		return "000000000";
	}

	/**
	 * Utility method to look up the version of a package in a package listing
	 * formatted according to the OSGi Manifest spec (used for listing
	 * private/exported &amp; imported packages.
	 */
	public static String parseVersion(String packageName, String packageListing) {
		Pattern packageVersionPattern = Pattern.compile(packageName + ";version=\"(.*?)\"");
		Matcher matcher = packageVersionPattern.matcher(packageListing);
		if (matcher.find()) {
			return matcher.group(1);
		} else {
			return null;
		}
	}

	/**
	 * Restore a version number string property value to a readable dotted
	 * version notation.
	 * 
	 * @param propertyValue
	 */
	public static String restore(String propertyValue) {
		return removeLeadingZeros(propertyValue.substring(0, 3)) + "."
				+ removeLeadingZeros(propertyValue.substring(3, 6)) + "."
				+ removeLeadingZeros(propertyValue.substring(6, 9));
	}

	private static int removeLeadingZeros(String number) {
		int i = 0;
		while ('0' != number.charAt(i)) {
			i++;
		}
		return Integer.parseInt(number.substring(i));
	}

}
