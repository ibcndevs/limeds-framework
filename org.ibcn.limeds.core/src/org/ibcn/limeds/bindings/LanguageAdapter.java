/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.bindings;

import java.util.Set;

import org.ibcn.limeds.json.JsonValue;

/**
 * This interface defines the operations that need to be supported for the
 * implementation of a LimeDS language binding. The result is an adapter that
 * can interact with the target language in a standardized way.
 * 
 * @author wkerckho
 *
 */
public interface LanguageAdapter {

	/**
	 * Wrap the LimeDS JsonValue representation of the specified arguments into
	 * a representation that is compatible with the target language.
	 * <p>
	 * E.g. native JSON for Javascript or List/Dictionary combination for
	 * Python.
	 * 
	 * @param args
	 *            The JsonValue instances to be converted
	 * @return An object array that represents the converted instances
	 */
	Object[] wrap(JsonValue... args);

	/**
	 * Unwrap objects coming from the target language into a LimeDS JsonValue
	 * representation.
	 * 
	 * @param obj
	 *            The native target language object to be converted
	 * @return A LimeDS JsonValue instance
	 */
	JsonValue unwrap(Object obj);

	/**
	 * Calls a function implementation in the provided language.
	 * 
	 * @param functionName
	 * @param args
	 * @return
	 * @throws Exception
	 */
	Object call(String functionName, Object... args) throws Exception;

	/**
	 * Creates a LimeDS alias router
	 */
	Object getAliasRouter(Object baseFunction, Set<String> aliases) throws Exception;

	/**
	 * Set a global variable for the execution context of the target language.
	 * 
	 * @param name
	 *            The name of the variable
	 * @param value
	 *            The value of the variable
	 */
	void setGlobal(String name, Object value);

	/**
	 * Create a new instance of the LanguageAdapter using the same underlying
	 * code, but with a clean state-context
	 * 
	 * @return
	 * @throws Exception
	 */
	LanguageAdapter newInstance() throws Exception;

}
