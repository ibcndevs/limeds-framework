/**
 * This package contains the interfaces necessary for the integration of
 * non-Java languages in LimeDS.
 */
@org.osgi.annotation.versioning.Version("1.1.0")
package org.ibcn.limeds.bindings;
