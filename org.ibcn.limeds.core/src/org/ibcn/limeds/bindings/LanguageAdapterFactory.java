/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.bindings;

import java.io.InputStream;

/**
 * The LanguageAdapter interface can be implemented and exposed as an OSGi
 * service in order to add LimeDS support for additional programming languages.
 * <p>
 * By default, only a Javascript binding is provided.
 * 
 * @author wkerckho
 *
 */
public interface LanguageAdapterFactory {

	/**
	 * Get the ID for the language that is supported by this factory.
	 * 
	 * @return A String id, the same id used in the SegmentDescriptor to
	 *         specify the language of the component.
	 */
	String getLanguageId();

	/**
	 * Creates a binding instance for the language supported by this factory.
	 * 
	 * @param codeStream
	 *            The code to load for this instance as an input stream.
	 * @return A LanguageAdapter instance
	 * @throws Exception
	 */
	LanguageAdapter createAdapter(InputStream codeStream) throws Exception;

	/**
	 * Creates a binding instance for the language supported by this factory.
	 * 
	 * @param code
	 *            The code to load for this instance as a String.
	 * @return A LanguageAdapter instance
	 * @throws Exception
	 */
	LanguageAdapter createAdapter(String code) throws Exception;

}
