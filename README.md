**Welcome to the LimeDS Public Repository**

LimeDS is an OSGi-based Java framework targeted at developers that need an agile solution for building REST/JSON-based server applications in rapidly changing environments. More information can be found on the [website](http://limeds.intec.ugent.be) or on the [Wiki](https://bitbucket.org/ibcndevs/limeds-framework/wiki/Home).