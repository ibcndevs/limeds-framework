/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ibcn.limeds.devsetup;

import java.util.Arrays;
import java.util.stream.Collectors;

/**
 *
 * @author wkerckho
 */
public class Util {

    public static boolean OUTPUT_LOGS = false;

    public static void print(String... args) {
        System.out.println(Arrays.stream(args).collect(Collectors.joining(" ")));
    }

    public static void log(String... args) {
        if (OUTPUT_LOGS) {
            System.out.print("[log] ");
            print(args);
        }
    }

}
