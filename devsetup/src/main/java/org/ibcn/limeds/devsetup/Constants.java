package org.ibcn.limeds.devsetup;

/**
 *
 * @author wkerckho
 */
public class Constants {

    public static final String PROMPT = "";
    public static final String APP_NAME = "LimeDS Workspace Manager";

    public static final String BASE_URL = "https://bitbucket.org/ibcndevs/limeds-framework/raw/master/devsetup/scripts";
    public static final String[] SCRIPTS = new String[]{"init-workspace", "add-project", "update-workspace", "update-project"};
    public static final String STORAGE = "./.dev-setup";

}
