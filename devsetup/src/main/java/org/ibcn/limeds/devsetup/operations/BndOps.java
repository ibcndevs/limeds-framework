/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ibcn.limeds.devsetup.operations;

import aQute.lib.utf8properties.UTF8Properties;
import com.budhash.cliche.Command;
import java.io.File;
import java.io.FileInputStream;
import java.util.Arrays;
import java.util.Optional;
import org.apache.commons.io.FileUtils;
import static org.ibcn.limeds.devsetup.Util.print;

/**
 *
 * @author wkerckho
 */
public class BndOps {

    public static final BndOps INSTANCE = new BndOps();

    private BndOps() {
    }

    @Command
    public String getDefinedVersion(String bndLoc) throws Exception {
        UTF8Properties props = new UTF8Properties();
        props.load(new FileInputStream(bndLoc));
        return props.getProperty("Bundle-Version").replace(".${tstamp}", "");
    }

    @Command
    public void updateDependencyVersion(String bndLoc, String moduleName, String newVersion) throws Exception {
        UTF8Properties props = new UTF8Properties();
        props.load(new FileInputStream(bndLoc));
        String buildpath = props.getProperty("-buildpath");
        String[] parts = buildpath.split(",");
        Optional<String> dependency = Arrays.stream(parts).map(part -> part.trim()).filter(part -> part.startsWith(moduleName)).findFirst();
        if (dependency.isPresent()) {
            IOOps.INSTANCE.replaceInFile(bndLoc, dependency.get(), moduleName + ";version=" + newVersion.substring(0, newVersion.lastIndexOf('.')));
        }
    }

    @Command
    public void addRunRequirements(String runLoc, String projectName) throws Exception {
        File rundir = new File(runLoc);
        Arrays.stream(rundir.listFiles()).filter(f -> f.getName().endsWith(".bndrun")).forEach(f -> {
            try {
                UTF8Properties props = new UTF8Properties();
                props.load(new FileInputStream(f));
                String[] reqs = props.getProperty("-runrequires").split(",");
                String lastReq = reqs[reqs.length - 1].trim();
                IOOps.INSTANCE.replaceInFile(f.getAbsolutePath(), lastReq, lastReq + ",\\\n\tosgi.identity;filter:='(osgi.identity=" + projectName + ")'");
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        });
    }

    @Command
    public void addProjectPackage(String bndLoc, String packageName, boolean export) throws Exception {
        UTF8Properties props = new UTF8Properties();
        props.load(new FileInputStream(bndLoc));
        String propName = export ? "Export-Package" : "Private-Package";

        if (props.containsKey(propName)) {
            //Add after last subentry
            String[] reqs = props.getProperty(propName).split(",");
            String lastReq = reqs[reqs.length - 1].trim();

            IOOps.INSTANCE.replaceInFile(bndLoc, lastReq,
                    lastReq + ",\\\n\t" + packageName);
        } else {
            //Append new prop-val to file
            FileUtils.writeStringToFile(new File(bndLoc), "\n" + propName + ":\\\n\t" + packageName, "UTF-8", true);
        }
    }

    @Command
    public void updateRunRequirements(String runLoc, String moduleName, String newVersion) throws Exception {
        File rundir = new File(runLoc);
        Arrays.stream(rundir.listFiles()).filter(f -> f.getName().endsWith(".bndrun")).forEach(f -> {
            try {
                UTF8Properties props = new UTF8Properties();
                props.load(new FileInputStream(f));
                String[] reqs = props.getProperty("-runrequires").split(",");
                Optional<String> req = Arrays.stream(reqs).map(part -> part.trim()).filter(part -> part.contains(moduleName + ")")).findFirst();
                if (req.isPresent()) {
                    IOOps.INSTANCE.replaceInFile(f.getAbsolutePath(), req.get(), "osgi.identity;filter:='(&(osgi.identity=" + moduleName + ")(version>=" + newVersion + "))'");
                }
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        });
    }

}
