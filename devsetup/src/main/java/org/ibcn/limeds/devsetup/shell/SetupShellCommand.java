/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ibcn.limeds.devsetup.shell;

import com.budhash.cliche.CLIException;
import com.budhash.cliche.ShellCommand;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

/**
 *
 * @author wkerckho
 */
public class SetupShellCommand {

    protected ShellCommand command;
    protected Object handler;

    public SetupShellCommand(ShellCommand command) {
        this.command = command;
        try {
            Field handlerField = ShellCommand.class.getDeclaredField("handler");
            handlerField.setAccessible(true);
            handler = handlerField.get(command);
        } catch (Exception e) {
            //Ignore
        }
    }

    public Method getMethod() {
        return command.getMethod();
    }

    public Object invoke(Object[] parameters) throws CLIException {
        assert getMethod() != null;
        try {
            Object result = getMethod().invoke(handler, parameters);
            return result;
        } catch (Exception ex) {
            throw new CLIException(ex);
        }
    }

}
