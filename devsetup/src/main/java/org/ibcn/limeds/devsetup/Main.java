/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ibcn.limeds.devsetup;

import com.budhash.cliche.CLIException;
import com.budhash.cliche.Command;
import com.budhash.cliche.Param;
import com.budhash.cliche.Shell;
import com.budhash.cliche.ShellDependent;
import java.io.File;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import static org.ibcn.limeds.devsetup.Util.log;
import org.ibcn.limeds.devsetup.operations.BasicOps;
import org.ibcn.limeds.devsetup.operations.IOOps;
import static org.ibcn.limeds.devsetup.Util.print;
import org.ibcn.limeds.devsetup.operations.BndOps;
import org.ibcn.limeds.devsetup.operations.RepoOps;
import org.ibcn.limeds.devsetup.shell.SetupShell;

/**
 *
 * @author wkerckho
 */
public class Main implements ShellDependent {

    private SetupShell shell;

    @Command(description = "Sets up a LimeDS workspace in the current directory. Also checks if the prerequisites are installed.")
    public void initWorkspace() throws Exception {
        if (!new File("./cnf").exists()) {
            //Create script dir
            new File(Constants.STORAGE + "/scripts").mkdirs();

            //Download latest scripts
            log("Fetching latest scripts...");
            for (String script : Constants.SCRIPTS) {
                IOOps.INSTANCE.downloadFile(Constants.BASE_URL + "/" + script, Constants.STORAGE + "/scripts/" + script);
            }

            //Run init-workspace script
            log("Executing initialization process...");
            shell.executeScript(Constants.STORAGE + "/scripts/init-workspace");
        } else {
            print("Could not initialize workspace as it already exists.");
        }
    }

    @Command
    public void listTemplates() {
        File templateDir = new File(Constants.STORAGE + "/templates");
        List<String> templates = new LinkedList<>();
        if (templateDir.exists()) {
            for (File entry : templateDir.listFiles()) {
                if (entry.isDirectory()) {
                    templates.add(entry.getName());
                }
            }
        }

        if (templates.size() > 0) {
            templates.forEach(t -> print(t));
        } else {
            print("(No templates found!)");
        }
    }

    @Command(description = "Initializes an empty LimeDS project using the general template.")
    public void addProject(@Param(name = "projectName", description = "The name of the project.") String name) throws CLIException {
        addProject("general", name);
    }

    @Command(description = "Initializes an empty LimeDS project and adds it to the workspace.")
    public void addProject(@Param(name = "template", description = "The template to use.") String template, @Param(name = "projectName", description = "The name of the project.") String name) throws CLIException {
        print("Generating project", name, "based on template", template, "...");
        shell.executeScript(Constants.STORAGE + "/scripts/add-project", name, template);
    }

    @Command
    public void addPackage(String name, String project) throws Exception {
        addPackage(name, project, false);
    }

    @Command
    public void addPackage(String name, String project, boolean export) throws Exception {
        if (name.startsWith(".")) {
            name = project + name;
        }

        File packageDir = new File("./" + project + "/src/" + name.replace(".", "/"));
        if (!packageDir.exists()) {
            Collection<String> content = new LinkedList<>();
            content.add("@org.osgi.annotation.versioning.Version(\"0.1.0\")");
            content.add("package " + name + ";");
            FileUtils.writeLines(new File(packageDir, "package-info.java"), "UTF-8", content);
            BndOps.INSTANCE.addProjectPackage("./" + project + "/bnd.bnd", name, export);
            print("Added package", name, "to", project, "!");
        } else {
            print("Abort: a package named " + name + " already exists!");
        }
    }

    @Command(description = "Updates the workpace in the current directory to be compatible with the latest LimeDS versions.")
    public void updateWorkspace() throws Exception {
        if (new File("./cnf").exists()) {
            //Download latest scripts
            log("Fetching latest scripts...");
            for (String script : Constants.SCRIPTS) {
                IOOps.INSTANCE.downloadFile(Constants.BASE_URL + "/" + script, Constants.STORAGE + "/scripts/" + script);
            }

            shell.executeScript(Constants.STORAGE + "/scripts/update-workspace");
        } else {
            print("Create a workspace first!");
        }
    }

    @Command
    public void forEachProject(String scriptLoc) throws Exception {
        Collection<String> reservedFolders = Arrays.asList("cnf", "run", ".dev-setup");
        for (File file : new File("./").listFiles()) {
            if (file.isDirectory() && !reservedFolders.contains(file.getName()) && new File(file, ".project").exists()) {
                shell.executeScript(scriptLoc, file.getName());
            }
        }
    }

    @Command
    public String help() throws Exception {
        return IOUtils.toString(Main.class.getResourceAsStream("/help.txt"), "UTF-8");
    }

    @Override
    public void cliSetShell(Shell shell) {
        this.shell = (SetupShell) shell;
    }

    public static void main(String[] args) throws Exception {
        SetupShell shell = (SetupShell) SetupShell.createShell(new Main(), BasicOps.INSTANCE, IOOps.INSTANCE, BndOps.INSTANCE, RepoOps.INSTANCE);
        if (args.length > 0) {
            shell.processLine(Arrays.stream(args).collect(Collectors.joining(" ")));
        } else {
            shell.commandLoop();
        }
    }

}
