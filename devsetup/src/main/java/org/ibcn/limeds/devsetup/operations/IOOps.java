/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ibcn.limeds.devsetup.operations;

import com.budhash.cliche.Command;
import java.io.File;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Optional;
import net.lingala.zip4j.core.ZipFile;
import org.apache.commons.io.FileUtils;

/**
 *
 * @author wkerckho
 */
public class IOOps {

    public static final IOOps INSTANCE = new IOOps();

    private IOOps() {
    }

    @Command(description = "Downloads a package (in ZIP format) from the specified URL and installs (unpacks) it in the specified directory (if it does not exist.")
    public void downloadAndExtract(String packageUrl, String targetDir) throws Exception {
        URL url = new URL(packageUrl + "?_t=" + System.currentTimeMillis());

        //Download package
        File tmp = File.createTempFile("limeds-dev-setup", ".zip");
        FileUtils.copyURLToFile(url, tmp);
        ZipFile zipFile = new ZipFile(tmp);
        zipFile.extractAll(targetDir);
    }

    @Command(description = "Downloads the content for the specified URL to the specified target file (if it does not exist).")
    public void downloadFile(String fileUrl, String targetFile) throws Exception {
        URL url = new URL(fileUrl + "?_t=" + System.currentTimeMillis());

        //Download file
        File target = new File(targetFile);
        FileUtils.copyURLToFile(url, target);
    }

    @Command
    public String findFile(String rootDir, String textToMatch) {
        Optional<String> result = Arrays.stream(new File(rootDir).listFiles()).filter(file -> file.getName().contains(textToMatch)).map(file -> file.getAbsolutePath()).findFirst();
        if (result.isPresent()) {
            return result.get();
        } else {
            return null;
        }
    }

    @Command
    public void moveFile(String srcFile, String destFolder) throws Exception {
        File source = new File(srcFile);
        File destination = new File(destFolder);
        if (source.isDirectory()) {
            for (File f : source.listFiles()) {
                FileUtils.moveToDirectory(f, destination, true);
            }
            //Remove old dir
            FileUtils.deleteDirectory(source);
        } else {
            FileUtils.moveToDirectory(source, destination, true);
        }
    }

    @Command
    public void copyFile(String srcFile, String destFolder) throws Exception {
        File source = new File(srcFile);
        File destination = new File(destFolder);
        if (source.isDirectory()) {
            FileUtils.copyDirectory(source, new File(destination, source.getName()));
        } else {
            FileUtils.copyFileToDirectory(source, destination);
        }
    }

    @Command
    public void makeDir(String target) throws Exception {
        new File(target).mkdirs();
    }

    @Command
    public void removeDir(String target) throws Exception {
        FileUtils.deleteDirectory(new File(target));
    }

    @Command
    public void removeFile(String target) throws Exception {
        FileUtils.deleteQuietly(new File(target));
    }

    @Command
    public void renameFile(String target, String newName) {
        new File(target).renameTo(new File(newName));
    }

    @Command
    public void replaceInFile(String targetFile, String occurence, String replacement) throws Exception {
        Path path = new File(targetFile).toPath();
        String content = new String(Files.readAllBytes(path));
        content = content.replace(occurence, replacement);
        Files.write(path, content.getBytes());
    }

}
