/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ibcn.limeds.devsetup.operations;

import com.budhash.cliche.Command;
import com.budhash.cliche.Shell;
import com.budhash.cliche.ShellDependent;
import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.JsonPath;
import java.io.File;
import java.net.URL;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.ibcn.limeds.devsetup.Constants;
import org.ibcn.limeds.devsetup.shell.SetupShell;

/**
 *
 * @author wkerckho
 */
public class RepoOps implements ShellDependent {

    public static final RepoOps INSTANCE = new RepoOps();

    private SetupShell shell;

    private RepoOps() {
    }

    @Command
    public String getLatestTag(String apiCall, String jsonPath) throws Exception {
        URL target = new URL(apiCall);
        String json = IOUtils.toString(target, "UTF-8");
        Object document = Configuration.defaultConfiguration().jsonProvider().parse(json);
        String release = JsonPath.read(document, jsonPath);
        FileUtils.writeStringToFile(new File(Constants.STORAGE + "/releaseInfo"), release, "UTF-8");
        return release;
    }

    @Override
    public void cliSetShell(Shell shell) {
        this.shell = (SetupShell) shell;
    }

}
