package org.ibcn.limeds.devsetup.operations;

import com.budhash.cliche.Command;
import com.budhash.cliche.Shell;
import com.budhash.cliche.ShellDependent;
import java.io.IOException;
import org.ibcn.limeds.devsetup.Util;
import org.ibcn.limeds.devsetup.shell.SetupShell;

/**
 *
 * @author wkerckho
 */
public class BasicOps implements ShellDependent {

    public static final BasicOps INSTANCE = new BasicOps();

    private SetupShell shell;

    private BasicOps() {
    }

    @Command(description = "Prints the supplied arguments to the console.")
    public void print(String... args) {
        Util.print(args);
    }

    @Command
    public void log(String... args) {
        Util.log(args);
    }

    @Command
    public void toggleLog() {
        Util.OUTPUT_LOGS = !Util.OUTPUT_LOGS;
        print("Logging is now turned", Util.OUTPUT_LOGS ? "ON" : "OFF");
    }

    @Command
    public void setvar(String name, String value) {
        shell.setVariable(name, value);
        log("Variable", name, "set to", value);
    }

    @Override
    public void cliSetShell(Shell shell) {
        this.shell = (SetupShell) shell;
    }

    @Command
    public void executeScript(String script) throws Exception {
        shell.executeScript(script);
    }

    @Command
    public void waitForUser(String message) throws IOException {
        print(message, "(Press enter to continue!)");
        System.in.read();
    }

}
