/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ibcn.limeds.devsetup.shell;

import com.budhash.cliche.CLIException;
import com.budhash.cliche.CommandTable;
import com.budhash.cliche.ConsoleIO;
import com.budhash.cliche.DashJoinedNamer;
import com.budhash.cliche.HelpCommandHandler;
import com.budhash.cliche.InputConversionEngine;
import com.budhash.cliche.OutputConversionEngine;
import com.budhash.cliche.Shell;
import com.budhash.cliche.Token;
import com.budhash.cliche.util.ArrayHashMultiMap;
import com.budhash.cliche.util.MultiMap;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.ibcn.limeds.devsetup.Constants;
import static org.ibcn.limeds.devsetup.Util.print;

/**
 *
 * @author wkerckho
 */
public class SetupShell extends Shell {

    private static final ConsoleIO io = new ConsoleIO();
    private static final OutputConversionEngine outputConverter = new OutputConversionEngine();
    private static final InputConversionEngine inputConverter = new InputConversionEngine();
    private static final String HINT_MESSAGE = "This is the CLI interface of the LimeDS workspace manager. Type ?list to list the available commands!";
    private final Map<String, String> variables = new HashMap<>();
    private final CommandTable commandTable;

    public static Shell createShell(Object... handlers) {
        List<String> path = new ArrayList<>(1);
        path.add(Constants.PROMPT);

        MultiMap<String, Object> modifAuxHandlers = new ArrayHashMultiMap<>();
        modifAuxHandlers.put("!", io);

        Shell theShell = new SetupShell(new Shell.Settings(io, io, modifAuxHandlers, false),
                new CommandTable(new DashJoinedNamer(true)), path);
        theShell.setAppName(Constants.APP_NAME);

        theShell.addMainHandler(theShell, "!");
        theShell.addMainHandler(new HelpCommandHandler(), "?");
        for (Object h : handlers) {
            theShell.addMainHandler(h, "");
        }

        return theShell;
    }

    private SetupShell(Settings s, CommandTable commandTable, List<String> path) {
        super(s, commandTable, path);
        this.commandTable = commandTable;
    }

    @Override
    public void processLine(String line) throws CLIException {
        if (line.trim().equals("?")) {
            io.output(HINT_MESSAGE, outputConverter);
        } else {
            List<Token> tokens = Token.tokenize(line);
            if (tokens.size() > 0) {
                String discriminator = tokens.get(0).getString();
                processCommand(discriminator, tokens);
            }
        }
    }

    private void processCommand(String discriminator, List<Token> tokens)
            throws CLIException {
        assert discriminator != null;
        assert !discriminator.equals("");

        SetupShellCommand commandToInvoke = new SetupShellCommand(commandTable.lookupCommand(
                discriminator, tokens));

        Class<?>[] paramClasses = commandToInvoke.getMethod()
                .getParameterTypes();
        Object[] parameters = inputConverter.convertToParameters(replaceVariables(tokens),
                paramClasses, commandToInvoke.getMethod().isVarArgs());

        Object invocationResult = commandToInvoke.invoke(parameters);

        if (invocationResult != null) {
            io.output(invocationResult, outputConverter);
            variables.put("output", invocationResult.toString());
        }
    }

    public void executeScript(String scriptFile, String... args) throws CLIException {
        for (int i = 1; i <= args.length; i++) {
            variables.put("" + i, args[i - 1]);
        }

        try {
            for (String cmd : Files.readAllLines(new File(scriptFile).toPath())) {
                try {
                    processLine(cmd);
                } catch (Exception ex) {
                    print("Error, aborting script!");
                    ex.printStackTrace();
                    break;
                }
            }
        } catch (IOException e) {
            throw new CLIException("Error while reading script " + scriptFile);
        } finally {
            for (int i = 1; i <= args.length; i++) {
                variables.remove("" + i);
            }
        }
    }

    private List<Token> replaceVariables(List<Token> tokens) {
        return tokens.stream().map(token -> {
            String content = token.getString();
            for (String var : variables.keySet()) {
                if (content.contains("${" + var + "}")) {
                    content = content.replace("${" + var + "}", variables.get(var));
                }
            }
            return new Token(token.getIndex(), content);
        }).collect(Collectors.toList());
    }

    public void setVariable(String name, String value) {
        variables.put(name, value);
    }

}
