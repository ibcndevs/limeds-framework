/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.commons.webresources;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.FileTime;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Map;

/**
 * This class enables listing directory indexes in webresource folder by adding
 * a .listdir file in de directory. This does not work recursively by design!
 * 
 * @author tdupont
 *
 */
public class DirectoryLister {
	private final Path directory;
	private final String urlPath;
	private final DateFormat df = new SimpleDateFormat("dd-MMM-yyyy HH:mm '<small>'ss'</small>'");
	private final String css = getCss();

	public DirectoryLister(String urlPath, Path directory) {
		this.urlPath = urlPath.replace('\\', '/');
		this.directory = directory;
	}

	/**
	 * InputStream with the html contents of a page that lists the directories.
	 * DONT FORGET TO CLOSE IT!
	 * 
	 * @param directory
	 * @return
	 * @throws IOException
	 */
	public InputStream getInputStream() throws IOException {
		StringBuilder builder = new StringBuilder();
		builder.append("<html>");
		builder.append("<head>");
		builder.append("<title>Index of " + urlPath + "</title>");
		builder.append("</head>");
		builder.append("<style>");
		builder.append(css);
		builder.append("</style>");
		builder.append("<body>");
		builder.append("<h1>Index of " + urlPath + "</h1>");
		builder.append("<table>");
		builder.append("<tr>");
		builder.append("<th></td>");
		builder.append("<th>Name</td>");
		builder.append("<th>Last modified</td>");
		builder.append("<th>Size</td>");
		builder.append("</tr>");
		builder.append("<tr><td colspan=\"4\"><hr></td></tr>");
		builder.append(addParentDir());
		Files.list(directory).sorted(this::sortByDirThenName).forEachOrdered(p -> builder.append(getEntryLine(p)));
		builder.append("<tr><td colspan=\"4\"><hr></td></tr>");
		builder.append("</table>");
		builder.append("<address>limeDS webresources at " + urlPath + "</address>");
		builder.append("</body>");
		builder.append("</html>");
		return new ByteArrayInputStream(builder.toString().getBytes());
	}

	private String addParentDir() {
		return "<tr>" + "<td><div class=\"icon-parent\"></div></td>" + "<td><a href=\"..\">Parent directory</a></td>"
				+ "<td align=\"right\"></td>" + "<td align=\"right\">-</td>" + "</tr>";
	}

	/**
	 * Comparator for sorting alphabetically but put directories first
	 * 
	 * @param a
	 * @param b
	 * @return
	 */
	private int sortByDirThenName(Path a, Path b) {
		boolean aDir = Files.isDirectory(a);
		boolean bDir = Files.isDirectory(b);
		String aName = a.getFileName().toString();
		String bName = b.getFileName().toString();

		if ((aDir && bDir) || (!aDir && !bDir)) {
			return aName.compareToIgnoreCase(bName);
		} else {
			return aDir ? -1 : 1;
		}
	}

	private String getEntryLine(Path entry) {
		StringBuilder builder = new StringBuilder();
		try {
			File file = entry.toFile();
			String name = file.getName();

			Map<String, Object> attrs = Files.readAttributes(entry, "lastModifiedTime,size,isDirectory");
			Boolean isDir = (Boolean) attrs.get("isDirectory");
			String modified = df.format(((FileTime) attrs.get("lastModifiedTime")).toMillis());
			Long size = (Long) attrs.get("size");
			String icon = isDir ? "icon-dir" : "icon-file";
			builder.append("<tr>");
			builder.append("<td><div class=\"" + icon + "\"></div></td>");
			builder.append("<td><a href=\"" + name + "\">" + name + "</a></td>");
			builder.append("<td align=\"right\">" + modified + "</td>");
			builder.append("<td align=\"right\">" + (isDir ? "-" : humanReadableByteCount(size, true)) + "</td>");
			builder.append("</tr>");
		} catch (IOException e) {
			e.printStackTrace();
		}
		return builder.toString();
	}

	private String humanReadableByteCount(long bytes, boolean si) {
		int unit = si ? 1000 : 1024;
		if (bytes < unit)
			return bytes + " B";
		int exp = (int) (Math.log(bytes) / Math.log(unit));
		String pre = (si ? "kMGTPE" : "KMGTPE").charAt(exp - 1) + (si ? "" : "i");
		if (exp - 1 == 0) {
			return String.format("%.0f %sB", bytes / Math.pow(unit, exp), pre);
		} else {
			return String.format("%.1f %sB", bytes / Math.pow(unit, exp), pre);
		}
	}

	private String getCss() {
		InputStream in = DirectoryLister.class.getResourceAsStream("style.css");
		return inputStreamToString(in);
	}

	private String inputStreamToString(InputStream in) {
		StringBuilder builder = new StringBuilder();
		try (BufferedReader reader = new BufferedReader(new InputStreamReader(in))) {
			String line;
			while ((line = reader.readLine()) != null) {
				builder.append(line);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return builder.toString();
	}
}
