/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.commons.webresources;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.common.io.ByteStreams;

public class HostServlet extends HttpServlet {

	private static final long serialVersionUID = -5522436220493753832L;

	private String alias;
	private String baseDir;
	private String defaultPage;
	private Map<String, String> httpHeaders;
	private String custom404Path;
	private final String directoryListingFile = ".listdir";

	public HostServlet(String alias, String baseDir, String defaultPage, Map<String, String> httpHeaders,
			String custom404Path) {
		this.alias = alias;
		this.baseDir = substitueHomeDir(baseDir);
		this.defaultPage = defaultPage;
		this.httpHeaders = httpHeaders;
		this.custom404Path = custom404Path;
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String pathInfo = req.getServletPath() == null ? "" : req.getServletPath();
		pathInfo += req.getPathInfo() == null ? "" : req.getPathInfo();

		if (!pathInfo.startsWith("/")) {
			pathInfo = "/" + pathInfo;
		}

		// Get File to serve, dont replace alias if alias == "/"
		File target = new File(baseDir + (alias.equals("/") ? pathInfo : pathInfo.replaceFirst(alias, "")));
		if (target.exists() && target.isDirectory()) {
			if (!pathInfo.endsWith("/")) {
				resp.sendRedirect(pathInfo+"/");
				return;
			}
			target = new File(target, defaultPage);
		}

		// Add headers
		httpHeaders.forEach((k, v) -> resp.setHeader(k, v));

		if (target.exists() && target.isFile()) {
			// Set Mime-type
			resp.setContentType(MimeTypeUtil.guessContentType(target.getName()));

			// Write file to response
			try (FileInputStream input = new FileInputStream(target)) {
				long length = ByteStreams.copy(input, resp.getOutputStream());
				resp.setContentLengthLong(length);
			}
		} else if (listDirectoryFilePresent(target.getParentFile())) {
			String urlPath = req.getServletPath() + req.getPathInfo().substring(0, req.getPathInfo().lastIndexOf('/'));
			//Path pp = new File(baseDir).toPath().relativize(target.getParentFile().toPath().normalize());
			Path dir = target.getParentFile().toPath();
			resp.setContentType("text/html");
			try (InputStream input = new DirectoryLister(urlPath, dir).getInputStream()) {
				long length = ByteStreams.copy(input, resp.getOutputStream());
				resp.setContentLengthLong(length);
			}
		} else {
			if (custom404Path != null) {
				try (InputStream input = new FileInputStream(new File(baseDir, custom404Path))) {
					resp.setContentType("text/html");
					long length = ByteStreams.copy(input, resp.getOutputStream());
					resp.setContentLengthLong(length);
				}
			} else {
				resp.sendError(HttpServletResponse.SC_NOT_FOUND);
			}
		}
	}

	/**
	 * Returns whether the .listdir file is present or not. On any exceptions it
	 * is assumed the file is not there. This search is non-recursive!
	 * 
	 * @param directory
	 *            The directory to search in.
	 * @return
	 */
	private boolean listDirectoryFilePresent(File directory) {
		try {
			File listingfile = new File(directory, directoryListingFile);
			return Files.list(directory.toPath()).anyMatch(listingfile.toPath()::equals);
		} catch (IOException e) {
			// If anything goes wrong, return false and assume the directory
			// listing file is not present
			return false;
		}
	}

	/**
	 * Replaces urls of the form ~/blargh or ~\blargh to $user.home/blargh. Will
	 * NOT replace urls of the form ~otheruser/blargh or /blargh/file~ or
	 * /blargh/~/foo
	 * 
	 * @param path
	 *            Path with possible ~ a the start
	 * @return
	 */
	private String substitueHomeDir(String path) {
		// Allow for all styles of path writing, even if not correct for current
		// platform
		if (path.startsWith("~/") || path.startsWith("~\\")) {
			path = System.getProperty("user.home") + path.substring(1);
		}
		return path;
	}

}
