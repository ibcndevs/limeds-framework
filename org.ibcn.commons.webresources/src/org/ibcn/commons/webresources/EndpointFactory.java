/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.commons.webresources;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Collections;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import org.osgi.service.cm.ConfigurationException;
import org.osgi.service.cm.ManagedServiceFactory;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.http.HttpService;
import org.osgi.service.metatype.AttributeDefinition;
import org.osgi.service.metatype.MetaTypeProvider;
import org.osgi.service.metatype.ObjectClassDefinition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Component(property = { "service.pid=web.resources", "metatype.factory.pid=web.resources" })
public class EndpointFactory implements ManagedServiceFactory, MetaTypeProvider {

	private Logger LOGGER = LoggerFactory.getLogger(EndpointFactory.class);

	@Reference
	private volatile HttpService httpService;

	// PID to alias mapping
	private Map<String, String> registry = new HashMap<>();

	@Override
	public String getName() {
		return "Web Resources Configurator";
	}

	@Override
	public void updated(String pid, Dictionary<String, ?> properties) throws ConfigurationException {
		LOGGER.info("Detected WebResource config: " + properties);
		String targetDirectory = null;
		String rootPath = null;
		String defaultPage = null;
		String custom404Path = null;
		try {
			targetDirectory = (String) properties.get("targetDirectory");
			rootPath = (String) properties.get("wwwRoot");
			defaultPage = (String) properties.get("defaultPage");
			custom404Path = (String) properties.get("errorPage");
		} catch (Exception ex) {
			LOGGER.warn("Could not add web resource configuration due to missing property: "
					+ (targetDirectory == null ? "targetDirectory" : rootPath == null ? "rootPath" : "defaultPage"),
					ex);
		}

		// Parse HTTP headers
		Map<String, String> httpHeaders = Collections.list(properties.keys()).stream()
				.filter(s -> !Arrays.asList("targetDirectory", "wwwRoot", "defaultPage", "felix.fileinstall.filename",
						"service.factoryPid", "service.pid", "errorPage").contains(s))
				.collect(Collectors.toMap(s -> s, s -> (String) properties.get(s)));

		try {
			checkedDelete(pid);
			httpService.registerServlet(rootPath,
					new HostServlet(rootPath, targetDirectory, defaultPage, httpHeaders, custom404Path), null, null);
			registry.put(pid, rootPath);
		} catch (Exception e) {
			LOGGER.warn("Could not register the web resource for alias: " + rootPath);
		}
	}

	@Override
	public void deleted(String pid) {
		checkedDelete(pid);
		registry.remove(pid);
	}

	private void checkedDelete(String pid) {
		if (registry.containsKey(pid)) {
			httpService.unregister(registry.remove(pid));
		}
	}

	@Override
	public ObjectClassDefinition getObjectClassDefinition(String id, String locale) {
		if ("web.resources".equals(id)) {
			return new ObjectClassDefinition() {

				@Override
				public String getName() {
					return "IBCN Web Resource Factory";
				}

				@Override
				public String getID() {
					return "web.resources";
				}

				@Override
				public String getDescription() {
					// TODO Auto-generated method stub
					return null;
				}

				@Override
				public AttributeDefinition[] getAttributeDefinitions(int filter) {
					return new AttributeDefinition[] { getDefinition("targetDirectory", null),
							getDefinition("wwwRoot", null), getDefinition("defaultPage", "index.html"),
							getDefinition("errorPage", null) };
				}

				@Override
				public InputStream getIcon(int size) throws IOException {
					// TODO Auto-generated method stub
					return null;
				}
			};
		} else {
			return null;
		}
	}

	@Override
	public String[] getLocales() {
		// TODO Auto-generated method stub
		return null;
	}

	private AttributeDefinition getDefinition(String name, String defaultValue) {
		return new AttributeDefinition() {

			@Override
			public String getName() {
				return name;
			}

			@Override
			public String getID() {
				return name;
			}

			@Override
			public String getDescription() {
				return null;
			}

			@Override
			public int getCardinality() {
				return 0;
			}

			@Override
			public int getType() {
				return AttributeDefinition.STRING;
			}

			@Override
			public String[] getOptionValues() {
				return null;
			}

			@Override
			public String[] getOptionLabels() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public String validate(String value) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public String[] getDefaultValue() {
				return defaultValue != null ? new String[] { defaultValue } : null;
			}

		};
	}

}
