/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.webmodules.host.impl;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ibcn.limeds.webmodules.host.api.WebHelper;

import com.google.common.io.ByteStreams;

@SuppressWarnings("serial")
public class HttpHandler extends HttpServlet {

	private WebHelper helper;

	public HttpHandler(WebHelper helper) throws Exception {
		this.helper = helper;
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String pathInfo = req.getServletPath() == null ? "" : req.getServletPath();
		pathInfo += req.getPathInfo() == null ? "" : req.getPathInfo();

		if (!pathInfo.startsWith("/")) {
			pathInfo = "/" + pathInfo;
		}
		
		// Redirect of pathInfo exists, points to a valid directory AND does not end with a "/"
		if (helper.isExistingDir(pathInfo)) {
			if (!pathInfo.endsWith("/")) {
				resp.sendRedirect(pathInfo + "/");
				return;
			}
		}

		URL targetUrl = helper.get(pathInfo);
		if (targetUrl != null) {
			try (InputStream input = targetUrl.openStream()) {
				resp.setContentType(MimeTypeUtil.guessContentType(targetUrl.getFile()));
				long length = ByteStreams.copy(input, resp.getOutputStream());
				resp.setContentLengthLong(length);
			} catch (Exception e) {
				e.printStackTrace();
				resp.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			}
		} else {
			resp.sendError(HttpServletResponse.SC_NOT_FOUND);
		}

	}

}
