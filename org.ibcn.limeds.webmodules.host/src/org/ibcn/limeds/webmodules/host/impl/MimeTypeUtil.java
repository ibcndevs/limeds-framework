/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.webmodules.host.impl;

public class MimeTypeUtil {

	public static String guessContentType(String fileName) {
		if (matches(fileName, ".js")) {
			return "application/javascript";
		}
		if (matches(fileName, ".json")) {
			return "application/json";
		}
		if (matches(fileName, ".ttf")) {
			return "application/x-font-ttf";
		}
		if (matches(fileName, ".svg")) {
			return "images/svg+xml";
		}
		if (matches(fileName, ".css")) {
			return "text/css";
		}
		if (matches(fileName, ".html")) {
			return "text/html";
		}
		if (matches(fileName, ".png")) {
			return "image/png";
		}
		if (matches(fileName, ".gif")) {
			return "image/gif";
		}
		if (matches(fileName, ".ico")) {
			return "image/x-icon";
		}
		return "application/octect-stream";
	}

	private static boolean matches(String fileName, String suffix) {
		return fileName.toLowerCase().endsWith(suffix);
	}

}
