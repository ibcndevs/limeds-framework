/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.webmodules.host.api;

import java.io.File;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Enumeration;
import java.util.Hashtable;

import javax.servlet.Servlet;

import org.ibcn.limeds.FunctionalSegment;
import org.ibcn.limeds.json.Json;
import org.ibcn.limeds.json.JsonValue;
import org.ibcn.limeds.webmodules.host.impl.HttpHandler;
import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.ServiceRegistration;

public abstract class WebHelper implements FunctionalSegment {

	public static final String ENABLE_WEB_HELPER = "ibcn.webhelper.enabled=true";
	private BundleContext context;
	private ServiceRegistration<Servlet> reg;

	public WebHelper() {
		context = FrameworkUtil.getBundle(this.getClass()).getBundleContext();
	}

	public abstract String getWwwRoot();

	public abstract String jarLocation();

	public abstract String getIndexPage();

	public abstract String getErrorPage();

	@Override
	public void started() throws Exception {
		Hashtable<String, Object> props = new Hashtable<>();
		props.put("alias", getWwwRoot());
		reg = context.registerService(Servlet.class, new HttpHandler(this), props);
	}

	@Override
	public void stopped() throws Exception {
		try {
			reg.unregister();
		} catch (IllegalStateException e) {
			// Ignore
			// The service has already been unregistered!
		}
	}

	@Override
	public JsonValue apply(JsonValue... input) throws Exception {
		return Json.objectBuilder().add("wwwRoot", getWwwRoot()).add("jarLocation", jarLocation())
				.add("indexPage", getIndexPage()).add("errorPage", getErrorPage()).build();
	}

	public URL get(String path) {
		String location = (getWwwRoot().equals("/") ? path : path.replaceFirst(getWwwRoot(), ""));
		location = location.endsWith("/") ? location.substring(0, location.length() -1) : location;

		// Try to get the file for the specified path
		String target = "/" + jarLocation() + location;
		URL targetUrl = this.getClass().getResource(target);

		if (targetUrl == null || !isFile(target)) {
			// Try to get the index page
			target = "/" + jarLocation() + location + "/" + getIndexPage();
			targetUrl = this.getClass().getResource(target);
			if (targetUrl == null || !isFile(target)) {
				// Try showing the custom error page
				targetUrl = this.getClass().getResource("/" + jarLocation() + "/" + getErrorPage());
			}
		}

		return targetUrl;
	}

	private boolean isFile(String resource) {
		Enumeration<URL> result = context.getBundle().findEntries(resource, "*", false);
		return result == null;
	}

	/**
	 * Allows you to check before parsing any further if the requested resource is an existingDir
	 * @param path Pathinfo of the request
	 * @return True is existing AND directorty, false otherwise.
	 */
	public boolean isExistingDir(String path) {
		if (path.equals(getWwwRoot())) {
			return true;
		}
		String location = (getWwwRoot().equals("/") ? path : path.replaceFirst(getWwwRoot(), ""));
		String resource = "/" + jarLocation() + location;
		if (resource.endsWith("/")) {
			resource = resource.substring(0, resource.length()-1);
		}
		int idx = resource.lastIndexOf("/");
		String parent = resource.substring(0, idx);
		String child = resource.substring(idx+1);
		Enumeration<URL> result = context.getBundle().findEntries(parent, child, false);
		if (result != null) {
			while (result.hasMoreElements()) {
				URL u = result.nextElement();
				String url = u.toString();
				if (url.endsWith("/")) {
					return true;
				}
			}
		}
		return false;
	}

}
