/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.json_min;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.LinkedHashMap;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Subclass of JsonValue for representing JSON objects. It is based on a Java
 * Map implementation for maximum ease-of-use.
 * 
 * @author wkerckho
 *
 */
@SuppressWarnings("serial")
public class JsonObject extends LinkedHashMap<String, JsonValue> implements JsonValue {

	/**
	 * By overriding this method we can convert a JsonValue that is actually a
	 * JSON object to JsonObject without using a cast.
	 */
	@Override
	public JsonObject asObject() {
		return this;
	}

	/**
	 * Adds a boolean entry to the object.
	 */
	public void put(String key, boolean value) {
		put(key, new JsonPrimitive(value));
	}

	/**
	 * Adds an integer entry to the object.
	 */
	public void put(String key, int value) {
		put(key, new JsonPrimitive(value));
	}

	/**
	 * Adds a long entry to the object.
	 */
	public void put(String key, long value) {
		put(key, new JsonPrimitive(value));
	}

	/**
	 * Adds a double entry to the object.
	 */
	public void put(String key, double value) {
		put(key, new JsonPrimitive(value));
	}

	/**
	 * Adds a String entry to the object.
	 */
	public void put(String key, String value) {
		put(key, new JsonPrimitive(value));
	}

	@Override
	public JsonValue get(String key) {
		return super.get(key);
	}

	@Override
	public boolean has(String key) {
		return containsKey(key) && get(key) != null;
	}

	@Override
	public JsonValue get(int index) {
		return get(keySet().toArray()[index]);
	}

	/**
	 * Prints the JsonObject.
	 */
	@Override
	public String toString() {
		return "{" + entrySet().stream().map(e -> "\"" + e.getKey() + "\":" + e.getValue())
				.collect(Collectors.joining(",")) + "}";
	}

	private String generateSpacing(int indent) {
		return Stream.generate(String::new).limit(indent).collect(Collectors.joining(" "));
	}

	/**
	 * Prints the JsonObject as a formatted string with the specified
	 * indentation.
	 */
	@Override
	public String toPrettyString(int baseIndent, int indentIncrement) {
		return "{\n" + entrySet().stream()
				.map(e -> generateSpacing(baseIndent + indentIncrement) + "\"" + e.getKey() + "\" : "
						+ e.getValue().toPrettyString(baseIndent + indentIncrement, indentIncrement))
				.collect(Collectors.joining(",\n", "", "\n")) + generateSpacing(baseIndent) + "}";
	}

	/**
	 * Writes the JsonObject to the specified output stream using the specified
	 * character set.
	 */
	@Override
	public void write(OutputStream out, String charSet) throws IOException {
		out.write("{".getBytes(charSet));
		if (size() > 0) {
			String[] keys = keySet().toArray(new String[] {});
			byte[] comma = ",".getBytes(charSet);
			for (int i = 0; i < keys.length - 1; i++) {
				writeEntry(keys[i], get(keys[i]), out, charSet);
				out.write(comma);
			}
			writeEntry(keys[keys.length - 1], get(keys[keys.length - 1]), out, charSet);
		}
		out.write("}".getBytes(charSet));
	}

	private void writeEntry(String key, JsonValue val, OutputStream out, String charSet) throws IOException {
		out.write(("\"" + key + "\":").getBytes(charSet));
		val.write(out, charSet);
	}

	@Override
	public void write(PrintWriter out) {
		out.print("{");
		if (size() > 0) {
			String[] keys = keySet().toArray(new String[] {});
			out.print("\"" + keys[0] + "\":");
			get(keys[0]).write(out);
			JsonValue tmp = null;
			for (int i = 1; i < keys.length; i++) {
				out.print(",\"" + keys[i] + "\":");
				tmp = get(keys[i]);
				if (tmp instanceof JsonPrimitive) {
					out.print(tmp.toString());
				} else {
					tmp.write(out);
				}
			}
		}
		out.print("}");
	}

}
