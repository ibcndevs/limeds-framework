/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.json_min;

import java.lang.reflect.Method;
import java.util.Collection;
import java.util.Set;

import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

/**
 * Alternative JSON parser that creates LimeDS JsonValues from Strings by
 * calling the internal Nashorn parser.
 * 
 * @author wkerckho
 *
 */
public class JsonParser {

	public static final JsonParser INSTANCE = new JsonParser();

	private ScriptEngine engine = new ScriptEngineManager().getEngineByName("nashorn");
	private Object json;

	private JsonParser() {
		try {
			json = engine.eval("JSON");
		} catch (ScriptException e) {
			// Should not happen
			e.printStackTrace();
		}
	}

	public JsonValue from(String jsonString) throws Exception {
		return nashornToJson(((Invocable) engine).invokeMethod(json, "parse", jsonString));
	}

	public JsonValue nashornToJson(Object output) throws Exception {
		if (output.getClass().getName().equals("jdk.nashorn.api.scripting.ScriptObjectMirror")) {
			boolean isArray = callGetter(output, "isArray");
			if (isArray) {
				// Parse array
				Collection<Object> values = callGetter(output, "values");
				JsonArray array = new JsonArray();
				for (Object val : values) {
					array.add(nashornToJson(val));
				}
				return array;
			} else {
				// Parse object
				JsonObject object = new JsonObject();
				Set<String> keys = callGetter(output, "keySet");
				for (String key : keys) {
					object.put(key, nashornToJson(getKey(output, "get", key)));
				}
				return object;
			}
		} else {
			return new JsonPrimitive(output);
		}
	}

	@SuppressWarnings("unchecked")
	private <T> T callGetter(Object instance, String methodName) throws Exception {
		Method method = instance.getClass().getDeclaredMethod(methodName);
		method.setAccessible(true);
		return (T) method.invoke(instance);
	}

	@SuppressWarnings("unchecked")
	private <T> T getKey(Object instance, String methodName, Object key) throws Exception {
		Method method = instance.getClass().getDeclaredMethod(methodName, Object.class);
		method.setAccessible(true);
		return (T) method.invoke(instance, key);
	}

}
