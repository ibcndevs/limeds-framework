/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.json_min;

import java.util.Arrays;
import java.util.stream.Collector;

public class Json {

	public static JsonValue from(String json) throws Exception {
		return JsonParser.INSTANCE.from(json);
	}

	/**
	 * Create a JsonObjectBuilder instance for building a JsonObject.
	 * 
	 * @return
	 */
	public static JsonObjectBuilder objectBuilder() {
		return new JsonObjectBuilder();
	}

	/**
	 * Creates a JSON-array of JsonValue instances represented by an JsonArray.
	 * 
	 * @param elements
	 *            Zero or more instances of JsonNode
	 * @return an JsonArray instance
	 */
	public static JsonArray arrayOf(JsonValue... elements) {
		return Arrays.stream(elements).collect(Json.toArray());
	}

	/**
	 * Creates a JSON-array of integers represented by an JsonArray.
	 * 
	 * @param elements
	 *            Zero or more integers
	 * @return an JsonArray instance
	 */
	public static JsonArray arrayOf(Integer... elements) {
		return Arrays.stream(elements).map(JsonPrimitive::new).collect(Json.toArray());
	}

	/**
	 * Creates a JSON-array of longs represented by an JsonArray.
	 * 
	 * @param elements
	 *            Zero or more longs
	 * @return an JsonArray instance
	 */
	public static JsonArray arrayOf(Long... elements) {
		return Arrays.stream(elements).map(JsonPrimitive::new).collect(Json.toArray());
	}

	/**
	 * Creates a JSON-array of doubles represented by an JsonArray.
	 * 
	 * @param elements
	 *            Zero or more doubles
	 * @return an JsonArray instance
	 */
	public static JsonArray arrayOf(Double... elements) {
		return Arrays.stream(elements).map(JsonPrimitive::new).collect(Json.toArray());
	}

	/**
	 * Creates a JSON-array of booleans represented by an JsonArray.
	 * 
	 * @param elements
	 *            Zero or more booleans
	 * @return an JsonArray instance
	 */
	public static JsonArray arrayOf(Boolean... elements) {
		return Arrays.stream(elements).map(JsonPrimitive::new).collect(Json.toArray());
	}

	/**
	 * Creates a JSON-array of Strings represented by an JsonArray.
	 * 
	 * @param elements
	 *            Zero or more Strings
	 * @return an JsonArray instance
	 */
	public static JsonArray arrayOf(String... elements) {
		return Arrays.stream(elements).map(JsonPrimitive::new).collect(Json.toArray());
	}

	/**
	 * A collector utility method to be used in conjunction with Java 8 stream
	 * expression.
	 * 
	 * E.g. collect a List of String as an ArrayNode:
	 * 
	 * <pre>
	 * {
	 * 	&#064;code
	 * 	List&lt;String&gt; response = retrieveSomeList();
	 * 	ArrayNode array = response.stream().map(JsonNodeFactory.instance::textNode).collect(Json.toArray());
	 * }
	 * </pre>
	 * 
	 * @return An ArrayNode instance
	 */
	public static Collector<JsonValue, ?, JsonArray> toArray() {
		return Collector.of(JsonArray::new, JsonArray::add, (left, right) -> {
			left.addAll(right);
			return left;
		});
	}

}
