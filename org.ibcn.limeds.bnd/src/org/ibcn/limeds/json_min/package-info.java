/**
 * This package specifies and implements a reduced version of the LimeDS JSON
 * model.
 */
@org.osgi.annotation.versioning.Version("0.3.0")
package org.ibcn.limeds.json_min;
