/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.json_min;

/**
 * An utility class that facilitates building JSON objects in Java using a
 * builder pattern.
 * 
 * @author wkerckho
 *
 */
public class JsonObjectBuilder {

	private JsonObject model = new JsonObject();

	/**
	 * Adds a field with the specified to the object and set its value to the
	 * specified JsonValue (creating a nested JSON object).
	 * 
	 * @param key
	 *            The name of the field
	 * @param node
	 *            The value of the field
	 * @return The current ObjectBuilder instance
	 */
	public JsonObjectBuilder add(String key, JsonValue node) {
		model.put(key, node);
		return this;
	}

	/**
	 * Adds a field with the specified to the object and set its value to the
	 * specified String.
	 * 
	 * @param key
	 *            The name of the field
	 * @param value
	 *            The value of the field
	 * @return The current ObjectBuilder instance
	 */
	public JsonObjectBuilder add(String key, String value) {
		model.put(key, value);
		return this;
	}

	/**
	 * Adds a field with the specified to the object and set its value to the
	 * specified int.
	 * 
	 * @param key
	 *            The name of the field
	 * @param value
	 *            The value of the field
	 * @return The current ObjectBuilder instance
	 */
	public JsonObjectBuilder add(String key, int value) {
		model.put(key, value);
		return this;
	}

	/**
	 * Adds a field with the specified to the object and set its value to the
	 * specified long.
	 * 
	 * @param key
	 *            The name of the field
	 * @param value
	 *            The value of the field
	 * @return The current ObjectBuilder instance
	 */
	public JsonObjectBuilder add(String key, long value) {
		model.put(key, value);
		return this;
	}

	/**
	 * Adds a field with the specified to the object and set its value to the
	 * specified double.
	 * 
	 * @param key
	 *            The name of the field
	 * @param value
	 *            The value of the field
	 * @return The current ObjectBuilder instance
	 */
	public JsonObjectBuilder add(String key, double value) {
		model.put(key, value);
		return this;
	}

	/**
	 * Adds a field with the specified to the object and set its value to the
	 * specified boolean.
	 * 
	 * @param key
	 *            The name of the field
	 * @param value
	 *            The value of the field
	 * @return The current ObjectBuilder instance
	 */
	public JsonObjectBuilder add(String key, boolean value) {
		model.put(key, value);
		return this;
	}

	/**
	 * Adds a field with the specified to the object and set its value to the
	 * JSON object as represented by another ObjectBuilder instance.
	 * 
	 * @param key
	 *            The name of the field
	 * @param builder
	 *            The value of the field
	 * @return The current ObjectBuilder instance
	 */
	public JsonObjectBuilder add(String key, JsonObjectBuilder builder) {
		return this.add(key, builder.build());
	}

	/**
	 * Builds the JsonObject based on the current state of this builder.
	 * 
	 * @return A JsonObject instance
	 */
	public JsonObject build() {
		return model;
	}

}
