/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.json_min;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;

/**
 * This interface represents a JSON node and specifies some utility methods to
 * facilitate navigating and manipulating JSON types from Java.
 * 
 * @author wkerckho
 *
 */
public interface JsonValue {

	/**
	 * Return the JsonValue as a JsonArray (will throw an exception if this JSON
	 * node is not an array).
	 */
	default public JsonArray asArray() {
		throw new UnsupportedOperationException(
				"Cannot return JsonValue of type " + this.getClass().getSimpleName() + " as JsonArray.");
	}

	/**
	 * Return the JsonValue as a JsonObject (will throw an exception if this
	 * JSON node is not an object).
	 */
	default public JsonObject asObject() {
		throw new UnsupportedOperationException(
				"Cannot return JsonValue of type " + this.getClass().getSimpleName() + " as JsonObject.");
	}

	/**
	 * Return the JsonValue as a JsonPrimitive (will throw an exception if this
	 * JSON node is not a primitive).
	 */
	default public JsonPrimitive asPrimitive() {
		throw new UnsupportedOperationException(
				"Cannot return JsonValue of type " + this.getClass().getSimpleName() + " as JsonPrimitive.");
	}

	/**
	 * Return the JsonValue as a boolean (will throw an exception if this JSON
	 * node is not a JSON primitive that contains a boolean or a value that can
	 * be converted to a boolean).
	 */
	default public boolean asBoolean() {
		throw new UnsupportedOperationException(
				"Cannot return JsonValue of type " + this.getClass().getSimpleName() + " as boolean.");
	}

	/**
	 * Return the JsonValue as an integer (will throw an exception if this JSON
	 * node is not a JSON primitive that contains an integer or a value that can
	 * be converted to an integer).
	 */
	default public int asInt() {
		throw new UnsupportedOperationException(
				"Cannot return JsonValue of type " + this.getClass().getSimpleName() + " as int.");
	}

	/**
	 * Return the JsonValue as a double (will throw an exception if this JSON
	 * node is not a JSON primitive that contains a double or a value that can
	 * be converted to a double).
	 */
	default public double asDouble() {
		throw new UnsupportedOperationException(
				"Cannot return JsonValue of type " + this.getClass().getSimpleName() + " as double.");
	}

	/**
	 * Return the JsonValue as a long (will throw an exception if this JSON node
	 * is not a JSON primitive that contains a long or a value that can be
	 * converted to a long).
	 */
	default public long asLong() {
		throw new UnsupportedOperationException(
				"Cannot return JsonValue of type " + this.getClass().getSimpleName() + " as long.");
	}

	/**
	 * Return the JsonValue as a string (will throw an exception if this JSON
	 * node is not a JSON primitive that contains a string or a value that can
	 * be converted to a string).
	 */
	default public String asString() {
		throw new UnsupportedOperationException(
				"Cannot return JsonValue of type " + this.getClass().getSimpleName() + " as String.");
	}

	/**
	 * Get the child JsonValue with the specified key.
	 */
	JsonValue get(String key);

	/**
	 * Checks if the JsonValue contains the specified key and has a non-null
	 * value for this key. (Assuming the JsonValue is a JsonObject)
	 * 
	 * @param key
	 * @return
	 */
	boolean has(String key);

	/**
	 * Get the child JsonValue with the specified key as a boolean (can throw a
	 * runtime exception if this request cannot be satisfied, e.g. when the
	 * child is not the expected type).
	 */
	default public Boolean getBool(String key) {
		JsonValue val = get(key);
		return val != null ? val.asBoolean() : null;
	}

	/**
	 * Get the child JsonValue with the specified key as an integer (can throw a
	 * runtime exception if this request cannot be satisfied, e.g. when the
	 * child is not the expected type).
	 */
	default public Integer getInt(String key) {
		JsonValue val = get(key);
		return val != null ? val.asInt() : null;
	}

	/**
	 * Get the child JsonValue with the specified key as a double (can throw a
	 * runtime exception if this request cannot be satisfied, e.g. when the
	 * child is not the expected type).
	 */
	default public Double getDouble(String key) {
		JsonValue val = get(key);
		return val != null ? val.asDouble() : null;
	}

	/**
	 * Get the child JsonValue with the specified key as a long (can throw a
	 * runtime exception if this request cannot be satisfied, e.g. when the
	 * child is not the expected type).
	 */
	default public Long getLong(String key) {
		JsonValue val = get(key);
		return val != null ? val.asLong() : null;
	}

	/**
	 * Get the child JsonValue with the specified key as a String (can throw a
	 * runtime exception if this request cannot be satisfied, e.g. when the
	 * child is not the expected type).
	 */
	default public String getString(String key) {
		JsonValue val = get(key);
		return val != null ? val.asString() : null;
	}

	/**
	 * Get the child JsonValue at the specified index.
	 */
	JsonValue get(int index);

	/**
	 * Get the child JsonValue at the specified index as a boolean (can throw a
	 * runtime exception if this request cannot be satisfied, e.g. when the
	 * child is not the expected type).
	 */
	default public boolean getBool(int index) {
		return get(index).asBoolean();
	}

	/**
	 * Get the child JsonValue at the specified index as an integer (can throw a
	 * runtime exception if this request cannot be satisfied, e.g. when the
	 * child is not the expected type).
	 */
	default public int getInt(int index) {
		return get(index).asInt();
	}

	/**
	 * Get the child JsonValue at the specified index as a long (can throw a
	 * runtime exception if this request cannot be satisfied, e.g. when the
	 * child is not the expected type).
	 */
	default public long getLong(int index) {
		return get(index).asLong();
	}

	/**
	 * Get the child JsonValue at the specified index as a double (can throw a
	 * runtime exception if this request cannot be satisfied, e.g. when the
	 * child is not the expected type).
	 */
	default public double getDouble(int index) {
		return get(index).asDouble();
	}

	/**
	 * Get the child JsonValue at the specified index as a string (can throw a
	 * runtime exception if this request cannot be satisfied, e.g. when the
	 * child is not the expected type).
	 */
	default public String getString(int index) {
		return get(index).asString();
	}

	/**
	 * Prints the JsonValue as a formatted string with proper indentation.
	 */
	default public String toPrettyString() {
		return toPrettyString(0, 2);
	}

	/**
	 * Prints the JsonValue as a formatted string with the specified
	 * indentation.
	 */
	String toPrettyString(int baseIndent, int indentIncrement);

	/**
	 * Writes the JsonValue to the specified output stream using the specified
	 * character set.
	 */
	void write(OutputStream out, String charSet) throws IOException;
	
	/**
	 * Writes the JsonValue to the specified writer.
	 */
	void write(PrintWriter out);

}
