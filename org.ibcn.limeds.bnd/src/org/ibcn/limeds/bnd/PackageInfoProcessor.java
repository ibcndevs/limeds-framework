/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.bnd;

import java.util.Arrays;
import java.util.Set;

import org.ibcn.limeds.annotations.IncludeTypes;
import org.ibcn.limeds.bnd.util.Debugger;
import org.ibcn.limeds.bnd.util.TypeDependencyUtil;
import org.ibcn.limeds.json_min.Json;
import org.ibcn.limeds.json_min.JsonObject;
import org.ibcn.limeds.json_min.JsonValue;

import aQute.bnd.osgi.Analyzer;
import aQute.bnd.osgi.Annotation;
import aQute.bnd.osgi.Clazz;

public class PackageInfoProcessor extends Processor {

	private JsonObject types = new JsonObject();
	private Set<String> typeDependencies;

	public PackageInfoProcessor(Clazz clazz, Analyzer analyzer) throws Exception {
		super(clazz, analyzer);
	}

	public JsonObject getTypes() throws Exception {
		// Analyze class
		clazz.parseClassFileWithCollector(this);
		return types;
	}

	public Set<String> getTypeDependencies() {
		return typeDependencies;
	}

	@Override
	public void visitAnnotation(String annotationName, Annotation annotation) throws Exception {
		if (IncludeTypes.class.getName().equals(annotationName)) {
			processIncludeTypes(annotation);
		}
	}

	private void processIncludeTypes(Annotation annotation) {
		Arrays.stream(getSafely(annotation, "value", new Object[] {})).map(e -> "" + e).forEach(type -> {
			String typePath = clazz.getClassName().getPackageRef().getPath() + "/" + type;
			try {
				// Open the referenced file (also checks if it's valid json
				JsonValue schema = Json
						.from(getStringFromInputStream(analyzer.findResource(typePath).openInputStream()));
				// Calculate schema dependencies
				typeDependencies = TypeDependencyUtil.getExternalDependencies(analyzer, schema);

				String id = clazz.getClassName().getPackageRef().getFQN() + "."
						+ type.substring(0, type.lastIndexOf('.'));
				types.put(id, typePath);
			} catch (Exception e) {
				analyzer.error("The included type " + typePath + " is not a valid JSON file.");
			}
		});
		Debugger.log(clazz, member, annotation, types.toString());
	}

}
