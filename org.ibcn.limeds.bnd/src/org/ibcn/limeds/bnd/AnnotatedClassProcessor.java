/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.bnd;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.ibcn.limeds.annotations.ApplySchedule;
import org.ibcn.limeds.annotations.AuthMode;
import org.ibcn.limeds.annotations.Cached;
import org.ibcn.limeds.annotations.Configurable;
import org.ibcn.limeds.annotations.ConfigureAspect;
import org.ibcn.limeds.annotations.ConfigureAspects;
import org.ibcn.limeds.annotations.HttpAssertion;
import org.ibcn.limeds.annotations.HttpAssertions;
import org.ibcn.limeds.annotations.HttpDoc;
import org.ibcn.limeds.annotations.HttpMethod;
import org.ibcn.limeds.annotations.HttpOperation;
import org.ibcn.limeds.annotations.Link;
import org.ibcn.limeds.annotations.Links;
import org.ibcn.limeds.annotations.LoadBalancingStrategy;
import org.ibcn.limeds.annotations.Prefer;
import org.ibcn.limeds.annotations.Segment;
import org.ibcn.limeds.annotations.Service;
import org.ibcn.limeds.annotations.Validated;
import org.ibcn.limeds.bnd.util.Debugger;
import org.ibcn.limeds.bnd.util.ManifestUtil;
import org.ibcn.limeds.bnd.util.TypeDependencyUtil;
import org.ibcn.limeds.json_min.Json;
import org.ibcn.limeds.json_min.JsonArray;
import org.ibcn.limeds.json_min.JsonObject;
import org.ibcn.limeds.json_min.JsonPrimitive;
import org.ibcn.limeds.json_min.JsonValue;

import aQute.bnd.osgi.Analyzer;
import aQute.bnd.osgi.Annotation;
import aQute.bnd.osgi.Clazz;
import aQute.bnd.osgi.Clazz.FieldDef;
import aQute.bnd.osgi.Descriptors.TypeRef;

public class AnnotatedClassProcessor extends Processor {

	private static final String FUNCTION_TYPE = "org.ibcn.limeds.FunctionalSegment";
	private static final String FUNCTION_SET_TYPE = "Ljava/util/Set<Lorg/ibcn/limeds/FunctionalSegment;>;";
	private static final String FUNCTION_COLLECTION_TYPE = "Ljava/util/Collection<Lorg/ibcn/limeds/FunctionalSegment;>;";
	private static final String FUNCTION_ID_PROPERTY = "$.id";
	private static final String DYNAMIC_DEPENDENCY_PREFIX = "$.dependencies.";

	private static final String[] FIELD_ORDER = new String[] { "id", "version", "description", "export", "language",
			"tags", "serviceProperties", "interfaces", "subscriptions", "schedule", "templateInfo", "documentation",
			"httpDocumentation", "httpOperation", "dependencies", "httpAssertions", "configureAspects" };

	private JsonObject descriptor = new JsonObject();
	private Map<String, String> preferredLinks = new HashMap<>();
	private Set<TypeRef> typeClasses = new HashSet<>();
	private Set<String> typeDependencies = new HashSet<>();

	public AnnotatedClassProcessor(Clazz clazz, Analyzer analyzer) throws Exception {
		super(clazz, analyzer);
	}

	public JsonObject generateDescriptor() throws Exception {
		// Analyze class
		clazz.parseClassFileWithCollector(this);

		// Special case: field has configurable and link annotation:
		fieldToAnnotations.entrySet().stream().filter(fieldHas(Configurable.class, Link.class))
				.forEach(e -> processConfigurableLink(e.getKey(),
						e.getValue().stream()
								.filter(a -> Configurable.class.getName().equals(a.getName().getFQN())).findFirst()
								.get(),
						e.getValue().stream().filter(a -> Link.class.getName().equals(a.getName().getFQN())).findFirst()
								.get()));

		/*
		 * Process documentation
		 */
		descriptor.put("documentation", new DocumentationParser(fieldToAnnotations, this).getDocumentation());

		// Process preferred links
		if (descriptor.has("dependencies")) {
			preferredLinks.forEach((depId, prefTarget) -> {
				if (descriptor.get("dependencies").has(depId)) {
					descriptor.get("dependencies").get(depId).asObject().put("preferredId", prefTarget);
				}
			});
		}

		// Enforce JSON attribute order
		List<String> fieldOrder = Arrays.asList(FIELD_ORDER);
		List<Map.Entry<String, JsonValue>> sortedResult = descriptor.entrySet().stream()
				.sorted((f1, f2) -> fieldOrder.indexOf(f1.getKey()) - fieldOrder.indexOf(f2.getKey()))
				.collect(Collectors.toList());
		JsonObject finalResult = new JsonObject();
		sortedResult.forEach(e -> finalResult.put(e.getKey(), e.getValue()));
		return finalResult;
	}

	public Set<TypeRef> getTypeClasses() {
		return typeClasses;
	}

	public Set<String> getTypeDependencies() {
		return typeDependencies;
	}

	@Override
	protected void visitAnnotation(String annotationName, Annotation annotation) throws Exception {
		if (ApplySchedule.class.getName().equals(annotationName)) {
			processSchedule(annotation);
		} else if (ConfigureAspects.class.getName().equals(annotationName)) {
			processConfigureAspects(annotation);
		} else if (ConfigureAspect.class.getName().equals(annotationName)) {
			processConfigureAspect(annotation);
		} else if (Validated.class.getName().equals(annotationName)) {
			processValidated(annotation);
		} else if (Cached.class.getName().equals(annotationName)) {
			processCache(annotation);
		} else if (Configurable.class.getName().equals(annotationName)) {
			processConfig(annotation);
		} else if (HttpDoc.class.getName().equals(annotationName)) {
			processHttpDoc(annotation);
		} else if (HttpAssertion.class.getName().equals(annotationName)) {
			processAssertion(annotation);
		} else if (HttpAssertions.class.getName().equals(annotationName)) {
			processAssertions(annotation);
		} else if (HttpOperation.class.getName().equals(annotationName)) {
			processHttp(annotation);
		} else if (Segment.class.getName().equals(annotationName)) {
			processComponent(annotation);
		} else if (Link.class.getName().equals(annotationName)) {
			processLink(annotation);
		} else if (Links.class.getName().equals(annotationName)) {
			processMultiLink(annotation);
		} else if (Prefer.class.getName().equals(annotationName)) {
			processPrefer(annotation);
		} else if (Service.class.getName().equals(annotationName)) {
			processService(annotation);
		}
	}

	private void processConfigureAspect(Annotation annotation) {
		String aspectId = getSafely(annotation, "id", "");
		String configJsonPath = clazz.getClassName().getPackageRef().getPath() + "/"
				+ getSafely(annotation, "config", "");
		try {
			// Open the referenced file (also checks if it's valid json
			JsonValue cfg = Json
					.from(getStringFromInputStream(analyzer.findResource(configJsonPath).openInputStream()));

			if (cfg instanceof JsonObject) {
				if (!descriptor.has("configureAspects")) {
					descriptor.put("configureAspects", new JsonObject());
				}
				descriptor.get("configureAspects").asObject().put(aspectId, cfg);
			} else {
				analyzer.error("The aspect configuration must be a JSON object!");
			}
		} catch (Exception e) {
			analyzer.error("Could not process aspect configuration at " + configJsonPath, e);
		}
	}

	private void processConfigureAspects(Annotation annotation) {
		Arrays.stream(getSafely(annotation, "value", new Object[] {})).map(e -> (Annotation) e).forEach(ca -> {
			processConfigureAspect(ca);
		});
	}

	private void processValidated(Annotation annotation) {
		boolean validateInput = getSafely(annotation, "input", true);
		boolean validateOutput = getSafely(annotation, "output", true);

		if (!descriptor.has("configureAspects")) {
			descriptor.put("configureAspects", new JsonObject());
		}
		descriptor.get("configureAspects").asObject().put(Validated.ASPECT_ID,
				Json.objectBuilder().add("validateInput", validateInput).add("validateOutput", validateOutput).build());
	}

	private void processSchedule(Annotation annotation) throws Exception {
		String scheduleExpr = getSafely(annotation, "value", "");
		Debugger.log(clazz, member, annotation, scheduleExpr);
		if (!scheduleExpr.isEmpty()) {
			descriptor.put("schedule", scheduleExpr);
		}
	}

	private void processCache(Annotation annotation) throws Exception {
		JsonObject cacheDesc = new JsonObject();
		cacheDesc.put("size", getSafely(annotation, "size", 512));
		cacheDesc.put("retentionDuration", getSafely(annotation, "retentionDuration()", -1l));
		cacheDesc.put("retentionDurationUnit",
				getSafely(annotation, "retentionDurationUnit", TimeUnit.MILLISECONDS.toString()));

		Debugger.log(clazz, member, annotation, cacheDesc.toString());
		if (!descriptor.has("configureAspects")) {
			descriptor.put("configureAspects", new JsonObject());
		}
		descriptor.get("configureAspects").asObject().put(Cached.ASPECT_ID, cacheDesc);
	}

	private void processConfig(Annotation annotation) throws Exception {
		initTemplateDescriptor();

		if (member instanceof FieldDef) {
			FieldDef field = (FieldDef) member;
			// The field is a configurable property
			JsonObject configEntry = new JsonObject();
			configEntry.put("name", field.getName());
			configEntry.put("type", field.getType().getFQN());

			String defaultValue = getSafely(annotation, "defaultValue", "");
			if (!defaultValue.isEmpty()) {
				configEntry.put("value", defaultValue);
			}

			JsonArray possibleValues = Arrays.stream(getSafely(annotation, "possibleValues", new Object[] {}))
					.map(e -> new JsonPrimitive("" + e)).collect(Json.toArray());
			if (possibleValues.size() > 0) {
				configEntry.put("possibleValues", possibleValues);
			}

			String description = getSafely(annotation, "description", "");
			if (!description.isEmpty()) {
				configEntry.put("description", description);
			}

			descriptor.get("templateInfo").get("configuration").asArray().add(configEntry);
		}
		Debugger.log(clazz, member, annotation, descriptor.get("templateInfo").toString());
	}

	private void processHttpDoc(Annotation annotation) {
		JsonObject httpDoc = new JsonObject();

		JsonValue schema = parseSchema(annotation, "querySchema", "queryRef", "queryClass", false);
		if (schema != null) {
			httpDoc.put("querySchema", schema);
		}

		// Process pathInfo
		httpDoc.put("pathInfo", Arrays.stream(getSafely(annotation, "pathInfo", new Object[] {}))
				.map(e -> new JsonPrimitive("" + e)).collect(Json.toArray()));

		descriptor.put("httpDocumentation", httpDoc);
	}

	public JsonValue parseSchema(Annotation annotation, String schemaAttr, String schemaRefAttr, String schemaClassAttr,
			boolean collection) {
		TypeRef defaultClass = analyzer.getTypeRefFromFQN(Object.class.getName());
		TypeRef schemaClass = getSafely(annotation, schemaClassAttr, defaultClass);

		if (!schemaClass.equals(defaultClass)
				&& ManifestUtil.getPackageVersion(analyzer, schemaClass.getPackageRef().getFQN()) == null) {
			analyzer.error("Imported Java class " + schemaClass.getFQN()
					+ " cannot be used as JSON schema classes, use " + schemaRefAttr + " instead!");
			return null;
		}

		// Check for schemaClass
		if (!schemaClass.equals(defaultClass)) {
			JsonValue schema = generateTypeRef(schemaClass.getFQN());
			typeClasses.add(schemaClass);
			typeDependencies.addAll(TypeDependencyUtil.getExternalDependencies(analyzer, schema));
			return collection ? Json.arrayOf(schema) : schema;
		} else {
			// No schemaClass defined, check for schema
			String schemaStr = getSafely(annotation, schemaAttr, "");
			if (!schemaStr.isEmpty()) {
				try {
					JsonValue schema = Json.from(schemaStr);
					typeDependencies.addAll(TypeDependencyUtil.getExternalDependencies(analyzer, schema));
					return collection ? Json.arrayOf(schema) : schema;
				} catch (Exception e) {
					analyzer.error("Could not parse JSON String for " + schemaAttr + " in " + clazz.getFQN(), e);
				}
			} else {
				// No schema defined, check for schemaRef
				String ref = getSafely(annotation, schemaRefAttr, "");
				if (!ref.isEmpty()) {
					JsonValue schema = generateTypeRef(ref);
					typeDependencies.addAll(TypeDependencyUtil.getExternalDependencies(analyzer, schema));
					return collection ? Json.arrayOf(schema) : schema;
				}
			}
		}
		return null;
	}

	private JsonValue generateTypeRef(String input) {
		int delimIndex = input.lastIndexOf('_');
		int packageEndIndex = input.lastIndexOf('.');
		if (delimIndex > 0) {
			String targetVersion = input.substring(delimIndex + 1);
			if (Link.USE_PARENT_VERSION.equals(targetVersion)) {
				String packageVersion = ManifestUtil.getPackageVersion(analyzer, input.substring(0, packageEndIndex));
				targetVersion = packageVersion != null ? packageVersion : "*";
			}
			return Json.objectBuilder().add("@typeRef", input.substring(0, delimIndex) + "_" + targetVersion).build();
		} else {
			String packageVersion = ManifestUtil.getPackageVersion(analyzer, input.substring(0, packageEndIndex));
			return Json.objectBuilder().add("@typeRef", input + "_" + (packageVersion != null ? packageVersion : "*"))
					.build();
		}
	}

	private void processAssertions(Annotation annotation) throws Exception {
		Debugger.log(clazz, member, annotation, "(expand)");

		Arrays.stream(getSafely(annotation, "assertions", new Object[] {})).map(e -> (Annotation) e)
				.forEach(assertion -> {
					processAssertion(assertion);
				});
	}

	private void processAssertion(Annotation assertion) {
		JsonObject assertDesc = new JsonObject();
		assertDesc.put("method", getSafely(assertion, "method", HttpMethod.HEAD.toString()));
		assertDesc.put("target", getSafely(assertion, "target", ""));

		String request = getSafely(assertion, "request", "");
		if (!request.isEmpty()) {
			assertDesc.put("request", request);
			assertDesc.put("requestMimeType", getSafely(assertion, "requestMimeType", "text/plain"));
		}

		String jsValidator = getSafely(assertion, "jsValidator", "");
		if (!jsValidator.isEmpty()) {
			assertDesc.put("responseValidatorScript", jsValidator);
		}

		if (!descriptor.has("httpAssertions")) {
			descriptor.put("httpAssertions", new JsonArray());
		}
		Debugger.log(clazz, member, assertion, assertDesc.toString());
		descriptor.get("httpAssertions").asArray().add(assertDesc);
	}

	private void processHttp(Annotation annotation) throws Exception {
		JsonObject httpDesc = new JsonObject();
		String groupId = getSafely(annotation, "groupId", "");
		if (!groupId.isEmpty()) {
			httpDesc.put("groupId", groupId);
		}
		httpDesc.put("method", getSafely(annotation, "method", HttpMethod.GET.toString()));
		httpDesc.put("path", getSafely(annotation, "path", ""));

		JsonArray authorityRequired = Arrays.stream(getSafely(annotation, "authorityRequired", new Object[] {}))
				.map(e -> new JsonPrimitive("" + e)).collect(Json.toArray());
		if (authorityRequired.size() > 0) {
			httpDesc.put("authorityRequired", authorityRequired);
		}
		httpDesc.put("authMode", getSafely(annotation, "authMode", AuthMode.NONE.toString()));
		httpDesc.put("loadBalancing", getSafely(annotation, "loadBalancing", LoadBalancingStrategy.NONE));
		descriptor.put("httpOperation", httpDesc);
		Debugger.log(clazz, member, annotation, httpDesc.toString());
	}

	private void processComponent(Annotation annotation) throws Exception {
		Debugger.log(clazz, member, annotation, "(component-root)");
		String componentId = getSafely(annotation, "id", "");
		descriptor.put("id", !componentId.isEmpty() ? componentId : clazz.getFQN());
		descriptor.put("version",
				ManifestUtil.getPackageVersion(analyzer, clazz.getClassName().getPackageRef().getFQN()));

		String description = getSafely(annotation, "description", "");
		if (!description.isEmpty()) {
			descriptor.put("description", description);
		}

		descriptor.put("export", getSafely(annotation, "export", true));
		descriptor.put("language", "Java");

		JsonArray tags = Arrays.stream(getSafely(annotation, "tags", new Object[] {}))
				.map(e -> new JsonPrimitive("" + e)).collect(Json.toArray());
		if (tags.size() > 0) {
			descriptor.put("tags", tags);
		}

		JsonArray serviceProperties = Arrays.stream(getSafely(annotation, "serviceProperties", new Object[] {}))
				.map(e -> new JsonPrimitive("" + e)).collect(Json.toArray());
		if (serviceProperties.size() > 0) {
			descriptor.put("serviceProperties", serviceProperties);
		}

		Set<String> interfaces = new HashSet<>();
		Set<String> compInterfaces = Arrays.stream(getSafely(annotation, "additionalInterfaces", new Object[] {}))
				.map(e -> ((TypeRef) e).getFQN()).collect(Collectors.toSet());
		if (clazz.getInterfaces() != null && clazz.getInterfaces().length > 0) {
			interfaces.addAll(
					Arrays.stream(clazz.getInterfaces()).map(typeRef -> typeRef.getFQN()).collect(Collectors.toSet()));
		}
		if (compInterfaces.size() > 0) {
			interfaces.addAll(compInterfaces);
		}
		interfaces.add(FUNCTION_TYPE);
		descriptor.put("interfaces", interfaces.stream().map(name -> new JsonPrimitive(name)).collect(Json.toArray()));

		JsonArray subscriptions = Arrays.stream(getSafely(annotation, "subscriptions", new Object[] {}))
				.map(e -> new JsonPrimitive("" + e)).collect(Json.toArray());
		if (subscriptions.size() > 0) {
			descriptor.put("subscriptions", subscriptions);
		}

		boolean factory = getSafely(annotation, "factory", false);
		if (factory) {
			initTemplateDescriptor();
			descriptor.get("templateInfo").asObject().put("type", "FACTORY");
			descriptor.get("templateInfo").get("configuration").asArray().add(
					Json.objectBuilder().add("name", FUNCTION_ID_PROPERTY).add("type", String.class.getName()).build());
		}
	}

	private void processMultiLink(Annotation annotation) throws Exception {
		Annotation[] links = Arrays.stream(getSafely(annotation, "value", new Object[] {})).map(e -> (Annotation) e)
				.toArray(size -> new Annotation[size]);
		if (member instanceof FieldDef && links.length > 0) {
			JsonObject linkDesc = new JsonObject();
			linkDesc.put("required", true);

			FieldDef field = (FieldDef) member;
			String fieldType = field.getSignature() != null ? field.getSignature() : field.getType().getFQN();

			if (fieldType.equals(FUNCTION_COLLECTION_TYPE) || fieldType.equals(FUNCTION_SET_TYPE)) {
				linkDesc.put("collection", true);
			} else if (fieldType.equals(FUNCTION_TYPE)) {
				linkDesc.put("collection", false);
			} else {
				analyzer.error("Cannot process annotated field " + field.getName()
						+ ": only fields of type FlowFunction or a Collection or Set of FlowFunctions can be annotated with @Link");
			}

			JsonObject functionTargets = new JsonObject();
			Arrays.stream(links).forEach(link -> {
				String id = getSafely(link, "target", Link.VOID_LINK_TARGET);
				String version = getSafely(link, "version", Link.ACCEPT_ALL_VERSIONS);
				functionTargets.put(id, Link.USE_PARENT_VERSION.equals(version)
						? ManifestUtil.getPackageVersion(analyzer, clazz.getClassName().getPackageRef().getFQN())
						: version);
			});

			linkDesc.put("type", FUNCTION_TYPE);
			linkDesc.put("functionTargets", functionTargets);

			if (!descriptor.has("dependencies")) {
				descriptor.put("dependencies", new JsonObject());
			}
			Debugger.log(clazz, member, annotation, linkDesc.toString());
			descriptor.get("dependencies").asObject().put(field.getName(), linkDesc);
		}

	}

	private void processLink(Annotation annotation) throws Exception {
		if (member instanceof FieldDef) {
			JsonObject linkDesc = new JsonObject();
			linkDesc.put("required", true);

			FieldDef field = (FieldDef) member;
			String fieldType = field.getSignature() != null ? field.getSignature() : field.getType().getFQN();

			if (fieldType.equals(FUNCTION_COLLECTION_TYPE) || fieldType.equals(FUNCTION_SET_TYPE)) {
				linkDesc.put("collection", true);
			} else if (fieldType.equals(FUNCTION_TYPE)) {
				linkDesc.put("collection", false);
			} else {
				analyzer.error("Cannot process annotated field " + field.getName()
						+ ": only fields of type FunctionalSegment or a Collection or Set of FunctionalSegment can be annotated with @Link");
			}

			String version = getSafely(annotation, "version", Link.ACCEPT_ALL_VERSIONS);
			linkDesc.put("functionTargets", Json.objectBuilder()
					.add(getSafely(annotation, "target", Link.VOID_LINK_TARGET), Link.USE_PARENT_VERSION.equals(version)
							? ManifestUtil.getPackageVersion(analyzer, clazz.getClassName().getPackageRef().getFQN())
							: version)
					.build());
			linkDesc.put("type", FUNCTION_TYPE);

			if (!descriptor.has("dependencies")) {
				descriptor.put("dependencies", new JsonObject());
			}

			Debugger.log(clazz, member, annotation, linkDesc.toString());
			descriptor.get("dependencies").asObject().put(field.getName(), linkDesc);
		}
	}

	private void processPrefer(Annotation annotation) throws Exception {
		String preferredId = getSafely(annotation, "value", "");
		Debugger.log(clazz, member, annotation, preferredId);
		if (member instanceof FieldDef && !preferredId.isEmpty()) {
			preferredLinks.put(((FieldDef) member).getName(), preferredId);
		}
	}

	private void processService(Annotation annotation) throws Exception {
		if (member instanceof FieldDef) {
			JsonObject serviceDesc = new JsonObject();

			FieldDef field = (FieldDef) member;
			String fieldType = field.getType().getFQN();
			serviceDesc.put("required", getSafely(annotation, "required", true));

			String[] sigs = (field.getSignature() != null ? field.getSignature() : member.getDescriptor().toString())
					.split("[<;>]");
			if ("Ljava/util/Collection".equals(sigs[0]) || "Ljava/util/Set".equals(sigs[0])) {
				serviceDesc.put("type", sigs[1].substring(1).replace('/', '.'));
			} else {
				serviceDesc.put("type", fieldType);
			}

			boolean collection = fieldType.equals(Collection.class.getName()) || fieldType.equals(Set.class.getName());
			serviceDesc.put("collection", collection);

			if (collection) {

			} else {
				serviceDesc.put("type", fieldType);
			}

			serviceDesc.put("propertyFilter", getSafely(annotation, "filter", ""));

			if (!descriptor.has("dependencies")) {
				descriptor.put("dependencies", new JsonObject());
			}
			descriptor.get("dependencies").asObject().put(field.getName(), serviceDesc);
			Debugger.log(clazz, member, annotation, serviceDesc.toString());
		}
	}

	private void processConfigurableLink(FieldDef field, Annotation configurable, Annotation link) {
		// Remove regular config entry
		descriptor.get("templateInfo").get("configuration").asArray()
				.removeIf(e -> e.getString("name").equals(field.getName()));

		// Replace with configurable link
		JsonObject configEntry = new JsonObject();
		configEntry.put("name", DYNAMIC_DEPENDENCY_PREFIX + field.getName());
		configEntry.put("type", field.getType().getFQN());
		descriptor.get("templateInfo").get("configuration").asArray().add(configEntry);

		// Flag dependency as configurable
		descriptor.get("dependencies").get(field.getName()).asObject().put("configurable", true);
	}

	private void initTemplateDescriptor() {
		if (!descriptor.has("templateInfo")) {
			JsonObject templateDesc = new JsonObject();
			templateDesc.put("type", "CONFIGURABLE_INSTANCE");
			templateDesc.put("configuration", new JsonArray());
			descriptor.put("templateInfo", templateDesc);
		}
	}

}
