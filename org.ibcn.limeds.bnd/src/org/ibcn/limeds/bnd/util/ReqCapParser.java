/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.bnd.util;

import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.ibcn.limeds.json_min.JsonObject;
import org.ibcn.limeds.json_min.JsonValue;

import aQute.bnd.osgi.Analyzer;

public class ReqCapParser {

	private static final String TYPE_ID = "limeds.type.id";
	private static final String TYPE_VERSION = "limeds.type.version";

	private JsonObject slice;
	private Analyzer analyzer;

	public ReqCapParser(JsonObject slice, Analyzer analyzer) {
		this.slice = slice;
		this.analyzer = analyzer;
	}

	private void writeCapability(String namespace, String... properties) {
		ManifestUtil.appendEntry(analyzer, "Provide-Capability",
				namespace + ";" + Arrays.stream(properties).collect(Collectors.joining(";")));
	}

	private void writeRequirement(String namespace, String filter, boolean mustResolve) {
		ManifestUtil.appendEntry(analyzer, "Require-Capability",
				namespace + ";filter:=\"" + filter + "\";effective:=" + (mustResolve ? "resolve" : "active"));
	}

	private String stringProp(String name, String value) {
		return name + ":String=\"" + value + "\"";
	}

	public void writeManifest() {
		Set<String> typeDependencies = new HashSet<>();

		// Process segment req/caps
		if (slice.has("segments")) {
			slice.get("segments").asArray().forEach(component -> {
				if (component.getBool("export")) {
					// Add capability
					writeCapability("limeds.segment", stringProp(FilterUtil.SEGMENT_ID, component.getString("id")),
							stringProp(FilterUtil.SEGMENT_VERSION,
									FilterUtil.createVersionPropertyValue(component.getString("version"))));
				}

				// Add requirements
				if (component.has("dependencies")) {
					component.get("dependencies").asObject().forEach((id, dependency) -> {
						if (!dependency.has("configurable")) {
							if (dependency.has("functionTargets")) {
								// The dependency is a LimeDS link
								writeRequirement("limeds.segment", getFunctionFilter(dependency), true);
							} else {
								// The dependency is a OSGi service dependency
								writeRequirement("osgi.service", getServiceFilter(dependency), false);
							}
						}
					});
				}
			});
		}

		// Process type caps
		if (slice.has("types")) {
			slice.get("types").asObject().keySet().forEach(typeId -> writeCapability("limeds.type",
					stringProp(TYPE_ID, typeId),
					stringProp(TYPE_VERSION, FilterUtil.createVersionPropertyValue(slice.getString("version")))));
		}

		// Process type reqs
		if (slice.has("typeDependencies")) {
			typeDependencies.addAll(slice.get("typeDependencies").asArray().stream().map(e -> e.asString())
					.collect(Collectors.toSet()));
		}

		// Add requirements for type dependencies
		typeDependencies.forEach(type -> {
			int delimIndex = type.lastIndexOf('_');
			String id = delimIndex > 0 ? type.substring(0, delimIndex) : type;
			String version = delimIndex > 0 ? type.substring(delimIndex + 1, type.length()) : "*";
			writeRequirement("limeds.type",
					FilterUtil.and(FilterUtil.eq(TYPE_ID, id), FilterUtil.createVersionFilter(TYPE_VERSION, version)),
					true);
		});
	}

	private String getFunctionFilter(JsonValue dependency) {
		JsonObject functionTargets = dependency.get("functionTargets").asObject();
		if (functionTargets.size() == 1) {
			String key = functionTargets.keySet().iterator().next();
			return FilterUtil.and(FilterUtil.eq(FilterUtil.SEGMENT_ID, key),
					FilterUtil.createVersionFilter(FilterUtil.SEGMENT_VERSION, functionTargets.getString(key)));
		} else {
			List<String> exprs = new LinkedList<>();
			for (String id : functionTargets.keySet()) {
				exprs.add(FilterUtil.and(FilterUtil.eq(FilterUtil.SEGMENT_ID, id),
						FilterUtil.createVersionFilter(FilterUtil.SEGMENT_VERSION, functionTargets.getString(id))));
			}

			return FilterUtil.or(exprs.toArray(new String[] {}));
		}
	}

	private String getServiceFilter(JsonValue dependency) {
		String classFilter = FilterUtil.eq("objectClass", dependency.getString("type"));
		return classFilter;
	}

}
