/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.bnd.util;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;
import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

import aQute.bnd.osgi.Annotation;
import aQute.bnd.osgi.Clazz;
import aQute.bnd.osgi.Clazz.FieldDef;

public class Debugger {

	private static final boolean OUTPUT_DEBUG_INFO = false;

	@SuppressWarnings("unchecked")
	public static void log(Clazz clazz, FieldDef member, String annotationName, Object annotation, String output) {
		try {
			InvocationHandler handlerClass = Proxy.getInvocationHandler(annotation);
			Field propField = handlerClass.getClass().getDeclaredField("properties");
			propField.setAccessible(true);
			Map<String, ?> properties = (Map<String, ?>) propField.get(handlerClass);
			log(clazz, member, annotationName, properties, output);
		} catch (Exception e) {
			log(clazz, member, annotationName, null, output);
		}
	}

	public static void log(Clazz clazz, FieldDef member, Annotation annotation, String output) {
		log(clazz, member, annotation.getName().getShortName(),
				annotation.keySet().stream().map(k -> (Map.Entry<String, Object>) new Map.Entry<String, Object>() {

					@Override
					public String getKey() {
						return k;
					}

					@Override
					public Object getValue() {
						if (annotation.get(k) instanceof Object[]) {
							return Arrays.toString((Object[]) annotation.get(k));
						} else {
							return "" + annotation.get(k);
						}
					}

					@Override
					public Object setValue(Object value) {
						return null;
					}

				}).collect(Collectors.toMap(e -> e.getKey(), e -> e.getValue())), output);
	}
	
	public static void alwaysLog(Clazz clazz, FieldDef member, Annotation annotation, String output) {
		alwaysLog(clazz, member, annotation.getName().getShortName(),
				annotation.keySet().stream().map(k -> (Map.Entry<String, Object>) new Map.Entry<String, Object>() {

					@Override
					public String getKey() {
						return k;
					}

					@Override
					public Object getValue() {
						if (annotation.get(k) instanceof Object[]) {
							return Arrays.toString((Object[]) annotation.get(k));
						} else {
							return "" + annotation.get(k);
						}
					}

					@Override
					public Object setValue(Object value) {
						return null;
					}

				}).collect(Collectors.toMap(e -> e.getKey(), e -> e.getValue())), output);
	}

	public static void log(Clazz clazz, FieldDef member, String annotationName, Map<String, ?> annotationProps,
			String output) {
		String props = annotationProps != null ? "" + annotationProps : "{#could not read props#}";
		String memberName = member != null ? member.getName() : "class";
		log("[" + clazz.getFQN() + ":" + memberName + "] @" + annotationName + " " + props + " -> " + output);
	}
	
	public static void alwaysLog(Clazz clazz, FieldDef member, String annotationName, Map<String, ?> annotationProps,
			String output) {
		String props = annotationProps != null ? "" + annotationProps : "{#could not read props#}";
		String memberName = member != null ? member.getName() : "class";
		alwaysLog("[" + clazz.getFQN() + ":" + memberName + "] @" + annotationName + " " + props + " -> " + output);
	}

	public static void log(String message) {
		if (OUTPUT_DEBUG_INFO) {
			System.out.println(message);
		}
	}
	
	public static void alwaysLog(String message) {
		System.out.println(message);
	}

}
