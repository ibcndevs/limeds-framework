/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.bnd.util;

import aQute.bnd.osgi.Analyzer;

public class ManifestUtil {

	public static String getPackageVersion(Analyzer analyzer, String packageName) {
		if (analyzer.getContained().getByFQN(packageName) != null) {
			String version = analyzer.getContained().getByFQN(packageName).getVersion();
			if (version != null) {
				return analyzer.getContained().getByFQN(packageName).getVersion();
			} else {
				analyzer.warning("The package " + packageName + " does not specify a version, defaulting to 0.0.0");
				return "0.0.0";
			}
		} else {
			return null;
		}
	}

	public static void appendEntry(Analyzer analyzer, String entryName, String entryValue) {
		if (analyzer.getProperty(entryName) != null) {
			if (!analyzer.getProperty(entryName).contains(entryValue)) {
				analyzer.setProperty(entryName, analyzer.getProperty(entryName) + "," + entryValue);
			}
		} else {
			analyzer.setProperty(entryName, entryValue);
		}
	}

}
