/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.bnd.util;

public class FilterUtil {
	public static final String SEGMENT_ID = "limeds.segment.id";
	public static final String SEGMENT_VERSION = "limeds.segment.version";

	public static String and(String... filters) {
		String result = "(&";
		for (String filter : filters) {
			result += filter;
		}
		return result + ")";
	}

	public static String or(String... filters) {
		String result = "(|";
		for (String filter : filters) {
			result += filter;
		}
		return result + ")";
	}

	public static String not(String filter) {
		return "(!" + filter + ")";
	}

	public static String gte(String propName, String value) {
		return "(" + propName + ">=" + value + ")";
	}

	public static String eq(String propName, String value) {
		return "(" + propName + "=" + value + ")";
	}

	/**
	 * Utility method to create a service filter for the supplied version
	 * expression (allows versioning of LimeDS Flow Functions using the OSGi
	 * service layer).
	 * <p>
	 * Currently supports wildcards and caret ranges only
	 */
	public static String createVersionFilter(String propertyName, String versionExpression) {
		/*
		 * Support the use of caret ranges: E.g. "^1.2.3" will generate a filter
		 * that accepts any version >=1.2.3 and <2.0.0.
		 */
		if (versionExpression.startsWith("^")) {
			String startVersion = versionExpression.substring(1);
			String[] parts = startVersion.split("\\.");
			String endVersion = (Integer.parseInt(parts[0]) + 1) + ".0.0";
			return and(gte(propertyName, createVersionPropertyValue(startVersion)),
					not(gte(propertyName, createVersionPropertyValue(endVersion))));
		}

		/*
		 * Support the use of wildcards: E.g. "1.*" will generate a filter that
		 * accepts any version with major digit 1.
		 */
		if (versionExpression.contains("*")) {
			String baseVersion = createVersionPropertyValue(versionExpression.replace("*", "0"));
			String[] baseParts = new String[] { baseVersion.substring(0, 3), baseVersion.substring(3, 6),
					baseVersion.substring(6, 9) };
			String[] parts = versionExpression.split("\\.");
			String versionString = "";
			for (int i = 0; i < parts.length; i++) {
				if ("*".equals(parts[i])) {
					baseParts[i] = "*";
				}
				versionString += baseParts[i];
			}
			return eq(propertyName, versionString);
		}

		return eq(propertyName, createVersionPropertyValue(versionExpression));
	}

	/**
	 * Utility method that takes a string representation of a LimeDS version and
	 * returns a representative String value which can be used in the OSGi
	 * service registry and correctly supports comparison operations. LimeDS
	 * only supports MAJOR, MINOR & PATCH version numbers < 1000!
	 */
	public static String createVersionPropertyValue(String version) {
		if (version != null && !version.isEmpty()) {
			String[] parts = version.split("\\.");
			if (parts.length <= 3) {
				long result = 0;
				long multiplier = 1000000;
				for (int i = 0; i < parts.length; i++) {
					result += Integer.parseInt(parts[i]) * multiplier;
					multiplier /= 1000;
				}
				String strResult = "" + result;
				int length = strResult.length();
				for (int i = 0; i < 9 - length; i++) {
					strResult = "0" + strResult;
				}
				return strResult;
			}
		}
		return "000000000";
	}
}
