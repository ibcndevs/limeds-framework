/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.bnd.util;

import java.util.HashSet;
import java.util.Set;

import org.ibcn.limeds.json_min.Json;
import org.ibcn.limeds.json_min.JsonArray;
import org.ibcn.limeds.json_min.JsonObject;
import org.ibcn.limeds.json_min.JsonPrimitive;
import org.ibcn.limeds.json_min.JsonValue;

import aQute.bnd.osgi.Analyzer;

public class TypeDependencyUtil {

	public static Set<String> getExternalDependencies(Analyzer analyzer, JsonValue schema) {
		Debugger.log("Checking dependencies for: " + schema);
		Set<String> result = new HashSet<>();
		if (schema instanceof JsonArray) {
			schema.asArray().forEach(attrSchema -> result.addAll(getExternalDependencies(analyzer, attrSchema)));
		} else if (schema instanceof JsonObject) {
			if (schema.has("@typeRef")) {
				String typeRef = schema.getString("@typeRef");
				if (!isLocalType(analyzer, typeRef)) {
					result.add(typeRef);
				} else {
					// Load local type (if JSON file)
					if (analyzer.getTarget().getResources().containsKey(toPath(typeRef, ".json"))) {
						try {
							JsonValue loadedSchema = loadSchema(analyzer, typeRef);
							Debugger.log("Loaded schema: " + loadedSchema);
							result.addAll(getExternalDependencies(analyzer, loadedSchema));
						} catch (Exception e) {
							analyzer.error("Local typeRef could not be loaded: " + typeRef + " (due to " + e + ")");
						}
					}
				}
			} else {
				schema.asObject().forEach((attrName, attrSchema) -> {
					if (!(attrSchema instanceof JsonPrimitive)) {
						result.addAll(getExternalDependencies(analyzer, attrSchema));
					}
				});
			}
		}
		return result;
	}

	private static boolean isLocalType(Analyzer analyzer, String typeRef) {
		return analyzer.getTarget().getResources().containsKey(toPath(typeRef, ".json"))
				|| analyzer.getTarget().getResources().containsKey(toPath(typeRef, ".class"));
	}

	private static JsonValue loadSchema(Analyzer analyzer, String typeRef) throws Exception {
		return Json.from(StreamUtil.getStringFromInputStream(
				analyzer.getTarget().getResources().get(toPath(typeRef, ".json")).openInputStream()));
	}

	private static String toPath(String typeRef, String extension) {
		int versionDelimIndex = typeRef.lastIndexOf('_');
		return typeRef.substring(0, versionDelimIndex).replace(".", "/") + extension;
	}

}
