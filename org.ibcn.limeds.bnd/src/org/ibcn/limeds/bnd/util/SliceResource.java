/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.bnd.util;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;

import org.ibcn.limeds.json_min.JsonObject;

import aQute.bnd.osgi.WriteResource;

public class SliceResource extends WriteResource {

	private JsonObject jsonFlow;

	public SliceResource(JsonObject jsonFlow) {
		this.jsonFlow = jsonFlow;
	}

	@Override
	public void write(OutputStream out) throws IOException, Exception {
		OutputStreamWriter ow = new OutputStreamWriter(out, "UTF-8");
		PrintWriter pw = new PrintWriter(ow);
		try {
			pw.print(jsonFlow.toPrettyString());
		} finally {
			pw.flush();
		}
	}

	@Override
	public long lastModified() {
		return 0;
	}

}
