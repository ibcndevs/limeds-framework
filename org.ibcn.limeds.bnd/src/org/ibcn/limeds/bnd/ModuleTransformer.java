/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.bnd;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.ibcn.limeds.annotations.IncludeTypes;
import org.ibcn.limeds.annotations.JsonType;
import org.ibcn.limeds.annotations.ModelInfo;
import org.ibcn.limeds.annotations.Segment;
import org.ibcn.limeds.bnd.util.ManifestUtil;
import org.ibcn.limeds.bnd.util.ReqCapParser;
import org.ibcn.limeds.bnd.util.SliceResource;
import org.ibcn.limeds.json_min.Json;
import org.ibcn.limeds.json_min.JsonArray;
import org.ibcn.limeds.json_min.JsonObject;
import org.ibcn.limeds.json_min.JsonPrimitive;

import aQute.bnd.osgi.Analyzer;
import aQute.bnd.osgi.Clazz;

public class ModuleTransformer {

	private Analyzer analyzer;
	private Collection<Clazz> classes;
	private Collection<Clazz> packageInfoClasses;
	private Collection<Clazz> typeClasses;
	private Map<String, JsonArray> flows = new HashMap<>();
	private Map<String, JsonObject> flowTypes = new HashMap<>();
	private Map<String, JsonArray> flowTypeDependencies = new HashMap<>();
	private Map<String, JsonObject> idToClassName = new HashMap<>();
	private Set<String> providedTypes;

	public ModuleTransformer(Analyzer analyzer) throws Exception {
		this.analyzer = analyzer;
		classes = analyzer.getClasses("", "annotation", Segment.class.getName());
		packageInfoClasses = analyzer.getClasses("", "annotation", IncludeTypes.class.getName());
		typeClasses = analyzer.getClasses("", "annotation", JsonType.class.getName());
	}

	public void execute() throws Exception {
		generateSegmentDescriptors();
		processSchemaTypes();
		addSlicesToModule();
		addTypeOnlySlicesToModule();
	}

	private void generateSegmentDescriptors() throws Exception {
		// Generate a JSON descriptor for each Segment
		for (Clazz clazz : classes) {
			// A flow is created per package
			String packageName = clazz.getClassName().getPackageRef().getFQN();
			if (!flows.containsKey(packageName)) {
				flows.put(packageName, new JsonArray());
			}

			// Process the annotated class
			AnnotatedClassProcessor processor = new AnnotatedClassProcessor(clazz, analyzer);
			JsonObject component = processor.generateDescriptor();

			// Add potential discovered Java types
			if (!processor.getTypeClasses().isEmpty()) {
				processor.getTypeClasses().forEach(e -> {
					if (!flowTypes.containsKey(e.getPackageRef().getFQN())) {
						flowTypes.put(e.getPackageRef().getFQN(), new JsonObject());
					}
					flowTypes.get(e.getPackageRef().getFQN()).put(e.getFQN(), e.getFQN() + ".java");
				});
			}

			// Register jar type dependencies
			if (!flowTypeDependencies.containsKey(packageName)) {
				flowTypeDependencies.put(packageName, new JsonArray());
			}
			flowTypeDependencies.get(packageName).addAll(processor.getTypeDependencies().stream()
					.map(v -> new JsonPrimitive(v)).collect(Collectors.toSet()));

			// Add a mapping so we know which class represents the component
			if (!idToClassName.containsKey(packageName)) {
				idToClassName.put(packageName, new JsonObject());
			}
			idToClassName.get(packageName).put(component.getString("id"), clazz.getFQN());

			// Register the component descriptor with the right flow
			flows.get(packageName).add(component);

			// Add the necessary manifest entries for the component
			new ReqCapParser(component, analyzer).writeManifest();
		}
	}

	private void processSchemaTypes() throws Exception {
		for (Clazz clazz : typeClasses) {
			String packageName = clazz.getClassName().getPackageRef().getFQN();
			if (!flowTypes.containsKey(packageName)) {
				flowTypes.put(packageName, new JsonObject());
			}
			flowTypes.get(packageName).asObject().put(clazz.getFQN(), clazz.getFQN() + ".java");
		}

		for (Clazz clazz : packageInfoClasses) {
			String packageName = clazz.getClassName().getPackageRef().getFQN();
			PackageInfoProcessor processor = new PackageInfoProcessor(clazz, analyzer);
			JsonObject types = processor.getTypes();
			Set<String> dependencies = processor.getTypeDependencies();
			if (!types.isEmpty()) {
				if (!flowTypes.containsKey(packageName)) {
					flowTypes.put(packageName, new JsonObject());
				}
				flowTypes.get(packageName).putAll(types);
			}

			if (!dependencies.isEmpty()) {
				if (!flowTypeDependencies.containsKey(packageName)) {
					flowTypeDependencies.put(packageName, new JsonArray());
				}
				flowTypeDependencies.get(packageName)
						.addAll(dependencies.stream().map(v -> new JsonPrimitive(v)).collect(Collectors.toSet()));
			}
		}

		// Calculate list of types this module provides
		providedTypes = flowTypes.values().stream().flatMap(typeMapping -> typeMapping.keySet().stream())
				.collect(Collectors.toSet());
	}

	private void addSlicesToModule() {
		// Process the flows
		for (String flowId : flows.keySet()) {
			JsonObject flow = new JsonObject();

			// Generate basic meta-data
			flow.put("id", flowId);
			flow.put("version", ManifestUtil.getPackageVersion(analyzer, flowId));
			flow.put("modelVersion", ModelInfo.VERSION);

			// Add all components
			flow.put("segments", flows.get(flowId));
			flow.put("handlers", idToClassName.get(flowId));
			if (flowTypes.containsKey(flowId)) {
				JsonObject types = flowTypes.remove(flowId);
				flow.put("types", types);
				if (flowTypeDependencies.containsKey(flowId)) {
					flow.put("typeDependencies", flowTypeDependencies.get(flowId).stream()
							.filter(typeRef -> !providedTypes
									.contains(typeRef.asString().substring(0, typeRef.asString().lastIndexOf('_'))))
							.collect(Json.toArray()));
				}
			}

			// Output as file
			writeToJar(analyzer, flowId, flow);
		}
	}

	private void addTypeOnlySlicesToModule() {
		if (!flowTypes.isEmpty()) {
			for (String flowId : flowTypes.keySet()) {
				JsonObject flow = new JsonObject();

				// Generate basic meta-data
				flow.put("id", flowId);
				flow.put("version", ManifestUtil.getPackageVersion(analyzer, flowId));
				flow.put("modelVersion", ModelInfo.VERSION);
				flow.put("types", flowTypes.get(flowId));
				if (flowTypeDependencies.containsKey(flowId)) {
					flow.put("typeDependencies", flowTypeDependencies.get(flowId).stream()
							.filter(typeRef -> !providedTypes
									.contains(typeRef.asString().substring(0, typeRef.asString().lastIndexOf('_'))))
							.collect(Json.toArray()));
				}

				writeToJar(analyzer, flowId, flow);
			}
		}
	}

	private void writeToJar(Analyzer analyzer, String flowId, JsonObject flow) {
		String flowPath = "LimeDS/" + analyzer.validResourcePath(flowId, "Invalid flow name") + ".json";
		analyzer.getJar().putResource(flowPath, new SliceResource(flow));
		// Add manifest entry
		ManifestUtil.appendEntry(analyzer, "LimeDS-Slices", flowPath);

		// Generate calculated requirements and capabilities
		ReqCapParser reqCapAdder = new ReqCapParser(flow, analyzer);
		reqCapAdder.writeManifest();
	}

}
