/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.bnd;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.ibcn.limeds.bnd.util.Debugger;

import aQute.bnd.osgi.Analyzer;
import aQute.bnd.osgi.Annotation;
import aQute.bnd.osgi.ClassDataCollector;
import aQute.bnd.osgi.Clazz;
import aQute.bnd.osgi.Clazz.FieldDef;
import aQute.bnd.osgi.Clazz.MethodDef;

public abstract class Processor extends ClassDataCollector {

	protected Clazz clazz;
	protected Analyzer analyzer;
	protected FieldDef member;

	protected Map<FieldDef, Collection<Annotation>> fieldToAnnotations = new HashMap<>();

	public Processor(Clazz clazz, Analyzer analyzer) {
		this.clazz = clazz;
		this.analyzer = analyzer;
	}

	public Analyzer getAnalyzer() {
		return analyzer;
	}

	public Clazz getClazz() {
		return clazz;
	}

	@Override
	public final void field(FieldDef field) {
		member = field;
		fieldToAnnotations.put(member, new HashSet<>());
	}

	@Override
	public final void method(MethodDef method) {
		field(method);
	}

	@Override
	public final void memberEnd() {
		member = null;
	}

	public final void annotation(Annotation annotation) throws Exception {
		visitAnnotation(annotation.getName().getFQN(), annotation);

		if (member != null) {
			fieldToAnnotations.get(member).add(annotation);
		}
	}

	protected Predicate<Map.Entry<FieldDef, Collection<Annotation>>> fieldHas(Class<?>... annotations) {
		return e -> {
			Set<String> annotationNames = e.getValue().stream().map(ann -> ann.getName().getFQN())
					.collect(Collectors.toSet());
			return Arrays.stream(annotations).map(annClass -> annClass.getName())
					.allMatch(annName -> annotationNames.contains(annName));
		};
	}

	protected abstract void visitAnnotation(String name, Annotation annotation) throws Exception;

	public <T> T getSafely(Annotation annotation, String attribute, T defaultValue) {
		if (annotation.keySet().contains(attribute)) {
			return annotation.get(attribute);
		} else {
			Debugger.log("Could not find key '" + attribute + "' in " + annotation.getName());
			return defaultValue;
		}
	}

	protected String getStringFromInputStream(InputStream is) throws IOException {
		try (BufferedReader br = new BufferedReader(new InputStreamReader(is))) {
			StringBuilder sb = new StringBuilder();
			String line;
			while ((line = br.readLine()) != null) {
				sb.append(line);
			}
			return sb.toString();
		}
	}

}
