/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.bnd;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

import org.ibcn.limeds.annotations.InputDoc;
import org.ibcn.limeds.annotations.InputDocs;
import org.ibcn.limeds.annotations.OutputDoc;
import org.ibcn.limeds.annotations.ServiceOperation;
import org.ibcn.limeds.json_min.Json;
import org.ibcn.limeds.json_min.JsonArray;
import org.ibcn.limeds.json_min.JsonObject;
import org.ibcn.limeds.json_min.JsonValue;

import aQute.bnd.osgi.Annotation;
import aQute.bnd.osgi.Clazz.FieldDef;

public class DocumentationParser {

	private static final String APPLY_METHOD = "apply";
	private static final Comparator<FieldDef> fieldDefSorter = (def1, def2) -> def2.getPrototype().length
			- def1.getPrototype().length;
	private JsonArray documentation = new JsonArray();
	private AnnotatedClassProcessor processor;

	public DocumentationParser(Map<FieldDef, Collection<Annotation>> annotations, AnnotatedClassProcessor processor) {
		this.processor = processor;
		/*
		 * Filter and Sort service methods per method name in descending order
		 * of number of arguments
		 */

		Map<String, Set<FieldDef>> sortedPrototypes = new LinkedHashMap<>();
		for (FieldDef def : annotations.keySet()) {
			if (APPLY_METHOD.equals(def.getName()) || annotations.get(def).stream()
					.anyMatch(ann -> ServiceOperation.class.getName().equals(ann.getName().getFQN()))) {
				if (!sortedPrototypes.containsKey(def.getName())) {
					sortedPrototypes.put(def.getName(), new TreeSet<>(fieldDefSorter));
				}
				sortedPrototypes.get(def.getName()).add(def);
			}
		}

		/*
		 * Generate compiler error if there are documentation annotations on
		 * service methods with the same name other than the one with the most
		 * arguments.
		 */
		for (String methodName : sortedPrototypes.keySet()) {
			if (sortedPrototypes.get(methodName).stream().skip(1)
					.anyMatch(def -> annotations.get(def).stream().anyMatch(this::isDocAnnotation))) {
				processor.getAnalyzer().error("Could not process method " + processor.getClazz().getFQN() + ":"
						+ methodName
						+ ". When overloading ServiceOperation methods, only the most complete method definition can be annotated with InputDoc or OutputDoc annotations.");
			}
		}

		/*
		 * Generate documentation, one entry per service methods with the same
		 * name. Documentation tags of the method with the most complete
		 * argument list is used for all methods with the same name.
		 */
		for (String methodName : sortedPrototypes.keySet()) {
			FieldDef mostCompleteDef = sortedPrototypes.get(methodName).stream().findFirst().get();
			JsonObject[] in = generateIn(mostCompleteDef, annotations.get(mostCompleteDef));
			JsonObject out = generateOut(mostCompleteDef, annotations.get(mostCompleteDef));
			String description = getDescription(mostCompleteDef, annotations);

			sortedPrototypes.get(methodName)
					.stream().sorted(
							Collections
									.reverseOrder(
											fieldDefSorter))
					.forEach(def -> documentation
							.add(Json.objectBuilder().add("id", methodName).add("description", description)
									.add("in",
											in != null ? Arrays.stream(in).limit(def.getPrototype().length)
													.collect(Json.toArray()) : new JsonArray())
									.add("out", out).build()));
		}

		/*
		 * If ServiceOperations are found, complete apply method documentation
		 * with the available operations as first parameter
		 */
		if (sortedPrototypes.size() > 1) {
			String opSchema = sortedPrototypes.keySet().stream().filter(p -> !p.equals(APPLY_METHOD)).distinct()
					.collect(Collectors.joining(",", "String [", "] (Supported operations for this ServiceSegment)"));
			JsonArray in = new JsonArray();
			in.add(Json.objectBuilder().add("label", "operation").add("schema", opSchema).build());
			documentation.add(Json.objectBuilder().add("id", APPLY_METHOD).add("in", in).build());
		}
	}

	private String getDescription(FieldDef def, Map<FieldDef, Collection<Annotation>> annotations) {
		Optional<Annotation> serviceOperation = annotations.get(def).stream()
				.filter(an -> an.getName().getFQN().equals(ServiceOperation.class.getName())).findFirst();
		return serviceOperation.isPresent() ? processor.getSafely(serviceOperation.get(), "description", "") : "";
	}

	private JsonObject[] generateIn(FieldDef mostCompleteDef, Collection<Annotation> annotations) {
		// Todo also target InputDocs annotation
		List<Annotation> in = annotations.stream().filter(an -> an.getName().getFQN().equals(InputDoc.class.getName()))
				.collect(Collectors.toList());
		if (in.isEmpty()) {
			in = annotations.stream().filter(an -> an.getName().getFQN().equals(InputDocs.class.getName())).flatMap(
					an -> Arrays.stream(processor.getSafely(an, "value", new Object[] {})).map(e -> (Annotation) e))
					.collect(Collectors.toList());
		}

		if (in.size() > 0) {
			return in.stream().map(ann -> {
				JsonObject inputDoc = new JsonObject();

				boolean collection = processor.getSafely(ann, "collection", false);

				String label = processor.getSafely(ann, "label", "");
				if (!label.isEmpty()) {
					inputDoc.put("label", label);
				}

				JsonValue schema = processor.parseSchema(ann, "schema", "schemaRef", "schemaClass", collection);
				if (schema != null) {
					inputDoc.put("schema", schema);
				} else {
					processor.getAnalyzer()
							.error("InputDoc must provide a schema for " + processor.getClazz().getFQN());
				}

				return inputDoc;
			}).toArray(size -> new JsonObject[size]);
		} else {
			return null;
		}
	}

	private JsonObject generateOut(FieldDef mostCompleteDef, Collection<Annotation> annotations) {
		Optional<Annotation> out = annotations.stream()
				.filter(an -> an.getName().getFQN().equals(OutputDoc.class.getName())).findFirst();
		if (out.isPresent()) {
			JsonObject outputDoc = new JsonObject();

			boolean collection = processor.getSafely(out.get(), "collection", false);

			JsonValue schema = processor.parseSchema(out.get(), "schema", "schemaRef", "schemaClass", collection);
			if (schema != null) {
				outputDoc.put("schema", schema);
			} else {
				processor.getAnalyzer().error("OutputDoc must provide a schema for " + processor.getClazz().getFQN());
			}
			return outputDoc;
		} else {
			return Json.objectBuilder().add("schema", new JsonObject()).build();
		}
	}

	private boolean isDocAnnotation(Annotation annotation) {
		String fqn = annotation.getName().getFQN();
		return fqn.equals(InputDoc.class.getName()) || fqn.equals(OutputDoc.class.getName());
	}

	public JsonArray getDocumentation() {
		return documentation;
	}

}
