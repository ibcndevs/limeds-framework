/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotate the apply method of a FunctionalSegment class with this annotation
 * to expose it as a HTTP endpoint, as specified by the properties of the
 * annotation.
 * 
 * @author wkerckho
 *
 */
@Retention(RetentionPolicy.CLASS)
@Target(ElementType.METHOD)
public @interface HttpOperation {

	String groupId() default "";
	
	/**
	 * The HTTP method to be used for the exposed endpoint.
	 */
	HttpMethod method() default HttpMethod.GET;

	/**
	 * The path for this endpoint on the HTTP server (prefixed with the
	 * configured LimeDS api root path, '/' by default).
	 * <p>
	 * It can contains path parameters using the {} syntax (e.g.
	 * /authors/{authorId}/books/{bookId}) that will be inserted in the
	 * FunctionalSegment apply through the httpContext JSON object that is
	 * passed as an additional argument.
	 */
	String path();

	/**
	 * If this attribute contains a non-empty array, clients that call the http
	 * operation must have either all of the authorities or one of the
	 * authorities represented by the String array (this depends on the authMode
	 * attribute).
	 * <p>
	 * e.g. authorityRequired={"admin", "moderator"}
	 */
	String[] authorityRequired() default {};

	/**
	 * The authorization mode for this operation
	 */
	AuthMode authMode() default AuthMode.NONE;

	/**
	 * The load balancing strategy for this operation
	 */
	String loadBalancing() default LoadBalancingStrategy.NONE;
}
