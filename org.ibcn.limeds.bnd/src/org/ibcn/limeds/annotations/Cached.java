/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.concurrent.TimeUnit;

/**
 * Annotate the apply method of a Segment with this annotation to mark the
 * output of this function to be cached by LimeDS.
 * 
 * @author wkerckho
 *
 */
@Retention(RetentionPolicy.CLASS)
@Target(ElementType.METHOD)
public @interface Cached {

	public static final String ASPECT_ID = "limeds.aspects.cache";

	/**
	 * Specifies the maximum size of the cache (how many output objects it can
	 * hold).
	 */
	int size() default 512;

	/**
	 * Specifies the maximum time an object can be stored in the cache (in
	 * milliseconds, unless otherwise specified using retentionDurationUnit).
	 * <p>
	 * By default an unspecified cache retention is used and the cache system
	 * will decide when to evict an entry.
	 */
	long retentionDuration() default -1l;

	/**
	 * Specifies the time unit for the retention duration (default is
	 * milliseconds).
	 */
	TimeUnit retentionDurationUnit() default TimeUnit.MILLISECONDS;

}
