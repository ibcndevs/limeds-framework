/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Identify the annotated class as a LimeDS Segment component.
 * <p>
 * The annotated class is the implementation class of this component.
 * 
 * @author wkerckho
 *
 */
@Retention(RetentionPolicy.CLASS)
@Target(ElementType.TYPE)
public @interface Segment {

	/**
	 * The id of this component.
	 */
	String id() default "";

	/**
	 * The description of this component.
	 * <p>
	 * A brief statement that describes the purpose of the component, can be
	 * omitted.
	 */
	String description() default "";

	/**
	 * The tags of this component.
	 */
	String[] tags() default {};

	/**
	 * Additional low-level (OSGi) service properties of this component.
	 * <p>
	 * Can be required to integrate with existing OSGi services.
	 */
	String[] serviceProperties() default {};

	/**
	 * Additional interfaces that should be added to the OSGi service
	 * registration of the component.
	 * <p>
	 * Can be omitted in most cases.
	 */
	Class<?>[] additionalInterfaces() default {};

	/**
	 * Determines if the component is a factory component.
	 * <p>
	 * Factory components are not added to the LimeDS runtime, but can be
	 * instantiated to create parameterized LimeDS components. E.g. a HttpClient
	 * factory component could allow multiple Segment instances to be created
	 * with a configurable http method and http target url.
	 */
	boolean factory() default false;

	/**
	 * Determines if the component should be publicly visible to other
	 * components.
	 * <p>
	 * Components for which the export attribute is set to false, will not end
	 * up in the LimeDS component index, meaning they will not show up when
	 * building Slices using the Visual Editor. Note that this is not a security
	 * measure, but a way to reduce function cluttering and enable some form of
	 * encapsulation when designing Slices.
	 */
	boolean export() default true;

	/**
	 * Determines the event channels this component should listen to.
	 * <p>
	 * A component can subscribe to multiple channels by specifying an array of
	 * event channel IDs. We advise to enable input validation when using event
	 * channels as a way of guaranteeing compatibility between the sender and
	 * the receiver on the channel.
	 */
	String[] subscriptions() default {};

}
