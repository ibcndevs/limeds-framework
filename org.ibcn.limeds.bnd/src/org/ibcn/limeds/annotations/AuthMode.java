/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.annotations;

/**
 * This enum defines the supported authentication/authorization modes.
 * 
 * @author wkerckho
 *
 */
public enum AuthMode {
	/**
	 * No authorization is required.
	 */
	NONE,
	/**
	 * A client is authorized automatically when it has been authenticated with
	 * the system.
	 */
	AUTHENTICATE_ONLY,
	/**
	 * A client is authorized when it has all the authorities listed in the
	 * authorityRequired array of the HttpOperation (and is authenticated).
	 */
	AUTHORIZE_CONJUNCTIVE,
	/**
	 * A client is authorized when it has one of the authorities listed in the
	 * authorityRequired array of the HttpOperation (and is authenticated).
	 */
	AUTHORIZE_DISJUNCTIVE;
}
