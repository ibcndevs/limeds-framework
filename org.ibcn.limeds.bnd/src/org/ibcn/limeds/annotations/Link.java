/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotate a field of a target class with this annotation to specify a link
 * between this component and another LimeDS component. The link always
 * represents a dependency relation where the component that defines the link
 * will depend on the component that is described by the link.
 * <p>
 * The type of the field MUST be either:
 * <ul>
 * <li>A FunctionalSegment
 * <li>A java.util.Collection or java.util.Set of FunctionalSegments
 * </ul>
 * 
 * @author wkerckho
 *
 */
@Retention(RetentionPolicy.CLASS)
@Target(ElementType.FIELD)
@Repeatable(Links.class)
public @interface Link {

	/*
	 * Use this constant to initialize a link without a specific target. This is
	 * needed, because by default the environment would bind all available
	 * FlowFunctions which can lead to unintended behaviour.
	 */
	public static final String VOID_LINK_TARGET = "__LIMEDS_VOID";
	public static final String ACCEPT_ALL_VERSIONS = "*";
	public static final String USE_PARENT_VERSION = "${sliceVersion}";

	/**
	 * The fully-qualified name of the component that will be linked.
	 */
	String target() default VOID_LINK_TARGET;

	/**
	 * The tags a component must have before it will be linked (use to construct
	 * a one-to-many dependency).
	 */
	String[] tags() default {};

	/**
	 * The version a component must have before it will be linked. This only
	 * applies to id-based links.
	 * <p>
	 * This attribute supports version ranges: E.g. "1.*" will allow any
	 * FunctionalSegment with Major version 1 to be used. Additionally, caret
	 * ranges are also supported: "^1.2.3" will allow any FunctionalSegment
	 * &gt;=1.2.3 and &lt;2.0.0 to be used.
	 * <p>
	 * Eventually we want to support all SemVer range operators (specified here:
	 * https://github.com/npm/node-semver).
	 */
	String version() default ACCEPT_ALL_VERSIONS;

}
