/**
 * This package contains the available annotations for specifying LimeDS Java
 * components.
 */
@org.osgi.annotation.versioning.Version("2.0.0")
package org.ibcn.limeds.annotations;
