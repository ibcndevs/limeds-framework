/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotate a Segment class or its apply method with this annotation to register
 * a HTTP assertion. HTTP assertions are used to model dependencies on external
 * systems that support HTTP. If the external system doesn't respond in the
 * configured way, LimeDS will treat this as if a local dependency becomes
 * unavailable. This allows the user to implement fallback scenarios in
 * combination with the @Prefer annotation to express what should happen when
 * something goes wrong.
 * 
 * @author wkerckho
 *
 */
@Retention(RetentionPolicy.CLASS)
@Target({ ElementType.METHOD, ElementType.TYPE })
@Repeatable(HttpAssertions.class)
public @interface HttpAssertion {

	/**
	 * Set the method the assertion should use.
	 */
	HttpMethod method() default HttpMethod.HEAD;

	/**
	 * Set the target URL for the assertion.
	 */
	String target();

	/**
	 * Set the request body for the assertion (default is empty).
	 */
	String request() default "";

	/**
	 * Set the mime type for the assertion (default is text/plain).
	 */
	String requestMimeType() default "text/plain";

	/**
	 * Set a Javascript validation script to validate the returned response.
	 * (Currently not suppported!)
	 */
	String jsValidator() default "";

}
