/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotate a field of a target class with this annotation to specify a
 * dependency of this component on an OSGi service.
 * <p>
 * The type of the field MUST be either:
 * <ul>
 * <li>A Java interface representing the OSGi service
 * <li>A java.util.Collection of such a Java interface
 * </ul>
 * 
 * @author wkerckho
 *
 */
@Retention(RetentionPolicy.CLASS)
@Target(ElementType.FIELD)
public @interface Service {

	/**
	 * Specifies if this service dependency should be satisfied before
	 * activating this component.
	 * <p>
	 * Defaults to true when omitted. If set to false, the developer should be
	 * aware that the dependency instance could be null or the dependency
	 * collection could be empty when the component is called.
	 */
	boolean required() default true;

	/**
	 * Specifies the service filter that should be used for the OSGi service
	 * lookup process.
	 */
	String filter() default "";

}
