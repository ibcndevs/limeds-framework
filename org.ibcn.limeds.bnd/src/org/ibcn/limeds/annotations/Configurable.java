/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotate a field of a target class with this annotation to mark the field as
 * configurable.
 * <p>
 * Configurable component fields can be edited by users at runtime, which makes
 * this a very useful feature to implement configuration parameters, allowing
 * users to dynamically set e.g. API keys or HTTP endpoint URLs.
 * <p>
 * Configurable fields are also used to define parameters for factory
 * instantiations (See the @Segment annotation)
 * <p>
 * Finally, a function-dependency (annotated with @Link) can also be annotated
 * with @Configurable to indicate that the exact binding of the link should
 * defined when instantiating the factory instance.
 * 
 * @author wkerckho
 *
 */
@Retention(RetentionPolicy.CLASS)
@Target(ElementType.FIELD)
public @interface Configurable {

	/**
	 * Use this attribute to specify a default value for the parameter.
	 */
	String defaultValue() default "";

	/**
	 * By specifying an array of possible values, the property's value can be
	 * restricted to these values. If the user supplies another value, the
	 * framework will throw an exception during instantiation.
	 */
	String[] possibleValues() default {};

	String description() default "";

}
