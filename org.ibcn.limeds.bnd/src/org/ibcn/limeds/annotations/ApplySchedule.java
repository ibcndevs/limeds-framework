/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotate a target class with this annotation to mark the segment as
 * schedulable by the LimeDS scheduler.
 * <p>
 * The type of the class MUST be org.ibcn.limeds.FunctionalSegment
 * 
 * @author wkerckho
 *
 */
@Retention(RetentionPolicy.CLASS)
@Target(ElementType.TYPE)
public @interface ApplySchedule {

	/**
	 * A LimeDS schedule expression (String).
	 * <p>
	 * format: [command: delay/every] [n] [timeUnit: milliseconds, seconds,
	 * minutes, hours, days] [offset (optional)] [n] [timeUnit]
	 * <p>
	 * Example1: schedule every day at 1 am =&gt; "every 1 days offset 1 hours" <br>
	 * Example2: schedule at framework start =&gt; "delay 0 seconds" <br>
	 * Example2: schedule every hour, after 20 minutes =&gt;
	 * "every 1 hours offset 20 minutes"
	 */
	String value();

}
