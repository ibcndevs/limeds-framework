import { NgModule } from '@angular/core';

import { SharedModule }  from '../shared/shared.module';

import { installablesRouting } from './installables.routing';
import { Installables } from './installables.component';

@NgModule({
    imports: [
        SharedModule,
        installablesRouting
    ],
    declarations: [
        Installables
    ]
})
export class InstallablesModule { }