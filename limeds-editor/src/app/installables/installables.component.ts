import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/filter';

import { KeybindingService } from '../shared/keybinding.service';
import { LimedsApi } from '../shared/limeds-api.service';
import { PanelManager, PanelType, PanelAction, PanelResult } from '../panels';

import * as limeds from '../shared/limeds';

interface MyInstallable {
    id: string,
    version: string;
    versions: string[];
    busy: boolean;
}

@Component({
    selector: 'lds-installables',
    templateUrl: './installables.component.html',
    styleUrls: ['./installables.component.css']
})
export class Installables implements OnInit {
    installables: MyInstallable[] = [];
    installed: MyInstallable[] = [];
    registry: { [key: string]: { [key: string]: limeds.Installable } } = {};

    private originalInstallables: limeds.Installable[] = [];

    constructor(
        private keybindings: KeybindingService,
        private api: LimedsApi,
        private panelManager: PanelManager,
        private ref: ViewContainerRef,
        private router: Router
    ) { }


    ngOnInit() {
        this.keybindings.addEventSource(window);
        this.reload();
    }

    /**
     * Returns whether the current installabel is installed locally
     */
    isInstalled(id: string, version: string) {
        return this.registry[id][version].installed;
    }

    openInstallable(installable: MyInstallable) {
        this.panelManager
            .createPanel(PanelType.VIEW_INSTALLABLE, this.ref, true, { id: installable.id, version: installable.version })
            .subscribe(result => {
                switch (result.action) {
                    case PanelAction.CONFIRM:

                        break;
                    case PanelAction.CANCEL:

                        break;
                }
            }
            );
    }

    deployInstallable(event: MouseEvent, installable: MyInstallable) {
        // Stop event from propagating
        event.stopPropagation();
        event.preventDefault();

        // Change icon to busy
        installable.busy = true;

        this.api.installInstallable(installable.id, installable.version).subscribe(
            res => {
                installable.busy = false;
                this.registry[installable.id][installable.version].installed = true;
                // add to installed array
                let v = installable.version;
                if ('latest' === v) {
                    v = installable.versions[1];
                }
                this.installed.push({
                    id: installable.id,
                    version: v,
                    versions: [],
                    busy: false
                });
            },
            error => {
                installable.busy = false
                this.registry[installable.id][installable.version].installed = false;
                console.log(error);
            }
        );
    }

    undeployInstallable(event: MouseEvent, installable: MyInstallable) {
        // Stop event from propagating
        event.stopPropagation();
        event.preventDefault();

        // Change icon to busy
        installable.busy = true;

        this.api.uninstallInstallable(installable.id, installable.version).subscribe(
            res => {
                installable.busy = false;
                this.registry[installable.id][installable.version].installed = false;
                // remove from installed array
                let idx = this.installed.findIndex(item => item.id === installable.id && item.version === installable.version);
                this.installed.splice(idx, 1);
            },
            error => {
                installable.busy = false
                this.registry[installable.id][installable.version].installed = true;
                console.log(error);
            }
        );
    }

    selectVersion(event: MouseEvent, installable: MyInstallable, version: string) {
        installable.version = version;
        event.stopPropagation();
    }

    openWebsite() {
        window.open('http://limeds.intec.ugent.be/');
    }

    openWiki() {
        window.open('https://bitbucket.org/ibcndevs/limeds-framework/wiki/Home');
    }

    openSwagger() {
        window.open('/swagger/');
    }

    openSlices() {
        this.router.navigate(['/slices']);
    }

    hardRefresh() {
        this.installables = [];
        this.installed = [];
        this.reload(true);
    }

    private reload(refresh?: boolean) {
        this.api.listInstallables(!!refresh).subscribe(installables => {
            this.originalInstallables = installables.sort(this.sortInstallables);
            let map: { [key: string]: MyInstallable } = {};
            for (let ins of installables) {
                if (!map[ins.id]) {
                    map[ins.id] = {
                        id: ins.id,
                        version: 'latest',
                        versions: ['latest', ins.version],
                        busy: false
                    }
                }
                else {
                    if (map[ins.id].versions.indexOf(ins.version) < 0) {
                        map[ins.id].versions.push(ins.version);
                    }
                    else {
                        continue;
                    }
                }
                if (!this.registry[ins.id]) {
                    this.registry[ins.id] = {
                        'latest': ins
                    };
                    this.registry[ins.id][ins.version] = ins;
                }
                this.registry[ins.id][ins.version] = ins;

                // If installed add to installed array
                if (ins.installed) {
                    this.installed.push({
                        id: ins.id,
                        version: ins.version,
                        versions: [],
                        busy: false
                    });
                }
            }
            this.installables = [];
            for (let key in map) {
                let val = map[key];
                this.installables.push(val);
            }
            this.installables.sort(this.sortInstallables);
        });
    }

    private sortInstallables = (a, b) => {
        if (a.id.toLowerCase() === b.id.toLowerCase()) {
            return this.sortSemVer(a.version, b.version);
        }
        else {
            let la = a.id.toLowerCase();
            let lb = b.id.toLowerCase();
            return (la < lb ? -1 : (la > lb ? 1 : 0));
        }
    }

    private sortSemVer = (a: string, b: string) => {
        let [aMajor, aMinor, aPatch, aQual] = a.split('.');
        let [bMajor, bMinor, bPatch, bQual] = b.split('.');
        if (aMajor === bMajor) {
            if (aMinor === bMinor) {
                if (aPatch === bPatch) {
                    if (bQual && aQual) {
                        let la = aQual.toLowerCase();
                        let lb = bQual.toLowerCase();
                        return (lb < la ? -1 : (lb > la ? 1 : 0));
                    }
                    else if (aQual) {
                        return 1;
                    } else {
                        return -1;
                    }
                }
                else {
                    return parseInt(bPatch) - parseInt(aPatch);
                }
            }
            else {
                return parseInt(bMinor) - parseInt(aMinor);
            }
        }
        else {
            return parseInt(bMajor) - parseInt(aMajor);
        }
    }
}