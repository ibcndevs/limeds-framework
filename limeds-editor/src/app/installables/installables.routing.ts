import { Routes, RouterModule } from '@angular/router';

import { Installables } from './installables.component';

const installRoutes: Routes = [
    { path: 'install', component: Installables }
];

export const installablesRouting = RouterModule.forChild(installRoutes);