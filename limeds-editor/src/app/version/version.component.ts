import { Component, OnInit } from '@angular/core';
import { Router, Route } from '@angular/router';

import { KeybindingService }  from '../shared/keybinding.service';
import { LimedsApi } from '../shared/limeds-api.service';
import * as limeds from '../shared/limeds';

@Component({
    templateUrl: './version.component.html',
    styleUrls: ['./version.component.css']
})
export class Version implements OnInit {
    info: limeds.HostInfo;

    private activeId: string = '';
    private activeVersion: string = '';


    constructor(
        private api: LimedsApi,
        private keybindings: KeybindingService,
        private router: Router) {
    }

    ngOnInit() {
        this.keybindings.addEventSource(window);
        this.api.getHostInfo().subscribe(info => {
            this.info = info;
        });
    }

    openWebsite() {
        window.open('http://limeds.intec.ugent.be/');
    }

    openWiki() {
        window.open('https://bitbucket.org/ibcndevs/limeds-framework/wiki/Home');
    }

    openSlices() {
        this.router.navigate(['/slices']);
    }

}