import { NgModule } from '@angular/core';

import { SharedModule } from '../shared/shared.module';
import { PanelsModule } from '../panels/panels.module';

import { versionRouting } from './version.routing';
import { Version } from './version.component';
import { LoginComponent } from '../login/login.component';

@NgModule({
    imports: [
        SharedModule,
        versionRouting
    ],
    declarations: [
        Version
    ]
})
export class VersionModule { }