import { Routes, RouterModule } from '@angular/router';

import { Version } from './version.component';
import { LoginComponent } from '../login/login.component';

const versionRoutes: Routes = [
    { path: '', redirectTo: '/version', pathMatch: 'full'}, 
    { path: 'version', component: Version }
];

export const versionRouting = RouterModule.forChild(versionRoutes);