import { Component, ViewContainerRef } from '@angular/core';

// Global css imports
// import '../assets/css/bootstrap.min.css';
// import '../assets/css/font-awesome.min.css';
// import '../assets/css/styles.css';

@Component({
    selector: 'my-app',
    template: '<router-outlet></router-outlet>',
})
export class AppComponent {

}