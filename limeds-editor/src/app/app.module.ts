import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http'

import { ConfigurationEditorModule } from './configuration-editor/configuration-editor.module';
import { FlowEditorModule } from './flow-editor/flow-editor.module';
import { LoginModule } from './login/login.module';
import { InstallablesModule } from './installables/installables.module';
import { ErrorsModule } from './errors/errors.module';
import { PanelsModule } from './panels/panels.module';
import { SharedModule } from './shared/shared.module';
import { SlicesModule } from './slices/slices.module';
import { VersionModule } from './version/version.module';

import { AppComponent } from './app.component';
import { FlowEditor } from './flow-editor/flow-editor.component';
import { routing } from './app.routing';

import { CanDeactivateGuard } from './shared/can-deactivate-guard.service';

@NgModule({
    imports: [
        BrowserModule,
        ReactiveFormsModule,
        HttpModule,
        SharedModule.forRoot(),
        PanelsModule.forRoot(),
        ErrorsModule,
        SlicesModule,
        VersionModule,
        ConfigurationEditorModule,
        InstallablesModule,
        FlowEditorModule,
        LoginModule,
        routing,
    ],
    declarations: [
        AppComponent,
    ],
    bootstrap: [AppComponent],
    providers: [CanDeactivateGuard]
})
export class AppModule { }