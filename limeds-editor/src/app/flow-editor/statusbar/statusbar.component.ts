import { Component, OnInit } from '@angular/core';

import { EditorService } from '../shared/editor.service';
import { Registry } from '../shared/registry.service';
import { SelectionService, Selected } from '../shared/selection.service';
import { Subscription } from 'rxjs/Subscription';
import * as limeds from '../../shared/limeds';

@Component({
    selector: 'statusbar',
    templateUrl: './statusbar.component.html',
    styleUrls: ['./statusbar.component.css']
})
export class Statusbar implements OnInit {
    // public fields
    leftText: string = '-';

    private subscription: Subscription;

    constructor(
        private editor: EditorService,
        private registry: Registry,
        private selection: SelectionService) { }

    ngOnInit() {
        //this.getZoomLevel();
        this.subscription = this.selection.selectionChanged$.subscribe(item => this.onSelectionChange(item));
    }

    public getZoomLevel() {
        return (Math.round(this.editor.getZoomLevel() * 10) / 10).toFixed(1);
    }

    public getMouseLoc() {
        let m = this.editor.getMouseLoc();
        let x = (m.x as number).toFixed(0);
        let y = (m.y as number).toFixed(0);
        return '(' + x + ',' + y + ')';
    }

    public onSelectionChange(change: Selected) {
        switch (change.type) {
            case limeds.LimedsType.Segment:
                this.leftText = change.id;
                break;
            case limeds.LimedsType.Link:
                let link = this.registry.findLink(change.id);
                this.leftText = link.getView().fromVar;
                break;
            case limeds.LimedsType.None:
            default:
                this.leftText = '-';
                break;
        }
    }
}