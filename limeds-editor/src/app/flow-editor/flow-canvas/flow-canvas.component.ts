import {
    Component,
    EventEmitter,
    Inject,
    OnDestroy,
    OnInit,
    Output,
} from '@angular/core';
import { DOCUMENT } from '@angular/platform-browser';
import { CanvasService } from '../shared/canvas.service';
import { Registry } from '../shared/registry.service';
import * as limeds from '../../shared/limeds';

@Component({
    selector: 'flow-canvas',
    templateUrl: './flow-canvas.component.html',
    styleUrls: ['./flow-canvas.component.css']
})
export class FlowCanvas implements OnInit, OnDestroy {
    @Output() panelAction = new EventEmitter<any>();
    @Output() clickAction = new EventEmitter<any>();
    @Output() editorTooltip = new EventEmitter<any>();

    private resizeListener: Function;
    private canvas: HTMLCanvasElement;
    private parent: any;

    constructor(
        private canvasService: CanvasService,
        private registry: Registry,
        @Inject(DOCUMENT) private document
    ) { }

    ngOnInit() {
        this.parent = this.document.querySelector('#canvas-container');
        this.canvas = this.document.querySelector('canvas.flow-canvas') as HTMLCanvasElement;
        this.resizeCanvas(null);

        window.addEventListener('resize', this.resizeCanvas);

        // CreateJS        
        this.canvasService.setStage(this.canvas);

        // Subscribe to panelActions$
        this.canvasService.panelActions$.subscribe(
            action => this.onPanelAction(action),
            error => console.error(error)
        );

        // Subscribe to ctxMenu$
        this.canvasService.ctxMenu$.subscribe(
            action => this.onRightClick(action),
            error => console.error(error)
        );

        // Subscribe to editorTooltips$
        this.canvasService.editorTooltips$.subscribe(
            action => this.onEditorTooltip(action),
            error => console.error(error)
        );
    }

    ngOnDestroy() {
        // Destroy by calling
        window.removeEventListener('resize', this.resizeCanvas);
        this.canvasService.reset();
        this.registry.reset();
    }

    resizeCanvas = (e: UIEvent) => {
        this.canvas.width = this.parent.offsetWidth;
        this.canvas.height = this.parent.offsetHeight;
        if (e) {
            this.canvasService.redrawRegistry();
        }
    };

    private onPanelAction(action: any) {
        this.panelAction.emit(action);
    }

    private onRightClick(action: any) {
        this.clickAction.emit(action);
    }

    private onEditorTooltip(action: any) {
        this.editorTooltip.emit(action);
    }
}