import { AfterViewInit, Component, ElementRef, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Subscription } from 'rxjs/Subscription';

import { ContextMenu } from '../shared/context-menu/context-menu.component';
import { ContextMenuItem } from '../shared/context-menu/menu-item.component';

import { Registry } from './shared/registry.service';
import { SelectionService } from './shared/selection.service';
import * as limeds from '../shared/limeds';


@Component({
    selector: 'lds-segment-ctx-menu',
    templateUrl: './segment-ctx-menu.component.html',
})
export class SegmentContextMenu implements AfterViewInit, OnInit, OnDestroy {
    /**
     * This subject will be notified with the event names of actions
     */
    menuAction$: Subject<string> = new Subject<string>();

    vm = {
        factoryInstance: false,
        readOnly: false
    };

    private handler: Subscription;
    @ViewChild(ContextMenu) private menu: ContextMenu;

    private sub: any;
    private selectionHandler = (result: any) => {
        switch (result.type) {
            case limeds.LimedsType.Segment:
                let segment = this.registry.findSegment(result.id);
                this.vm.factoryInstance = !!segment.getModel().templateInfo && segment.getModel().templateInfo.type == "FACTORY_INSTANCE";
                this.vm.readOnly = segment.getView().type === 'IMPORTED';
                break;

            case limeds.LimedsType.Link:
            case limeds.LimedsType.None:
            default:
                this.vm.factoryInstance = false;
                this.vm.readOnly = false;
        }
    };

    constructor(
        private ref: ElementRef,
        private registry: Registry,
        private selection: SelectionService
    ) { }

    ngOnInit() {
        let selection = this.selection.getCurrentSelection();
        this.selectionHandler(selection);
        this.sub = this.selection.selectionChanged$.subscribe(this.selectionHandler);
    }

    ngOnDestroy() {
        this.sub.unsubscribe();
    }

    ngAfterViewInit() {
        this.handler = this.menu.menuAction$.subscribe(value => this.menuAction$.next(value));
    }

    /**
     * Sets the location of the context menu and shows it there.
     */
    setLocation(x: number, y: number) {
        let el: HTMLDivElement = this.ref.nativeElement;
        el.style.position = 'fixed';
        el.style.display = 'block';
        el.style.left = x + 'px';
        el.style.top = y + 'px';
    }

    /**
     * Hides the context menu.
     */
    hide() {
        let el: HTMLDivElement = this.ref.nativeElement;
        el.style.display = 'none';
    }
}
