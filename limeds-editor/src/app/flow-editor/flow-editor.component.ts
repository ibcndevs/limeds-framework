import {
    Component,
    ComponentRef,
    ComponentFactoryResolver,
    Inject,
    OnDestroy,
    OnInit,
    ViewChild,
    ViewContainerRef
} from '@angular/core';
import { DOCUMENT } from '@angular/platform-browser';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Observable';

import {
    CanvasService,
    DrawService,
    EditorService,
    Registry,
    SelectionService
} from './shared';
import { SegmentContextMenu } from './segment-ctx-menu.component';
import { KeybindingService, Key, KeyCombo, Action } from '../shared/keybinding.service';
import { LimedsApi } from '../shared/limeds-api.service';
import { PanelManager, PanelType, PanelResult, PanelAction, PromptPanelConfig, AreYouSurePanelConfig } from '../panels';
import { CanComponentDeactivate } from '../shared/can-deactivate-guard.service';

import * as limeds from '../shared/limeds';

import { cloneDeep, isEqualWith } from 'lodash';

declare var Notification;

@Component({
    selector: 'flow-editor',
    templateUrl: './flow-editor.component.html',
    styleUrls: ['./flow-editor.component.css'],
})
export class FlowEditor implements OnInit, OnDestroy, CanComponentDeactivate {
    slice: limeds.ISlice;

    private original: limeds.ISlice;
    private ctxMenuRef: ComponentRef<SegmentContextMenu>;
    private routeSub: any;
    private myBadgeTooltip: HTMLDivElement = null;
    private tooltipTimer = null;

    @ViewChild('placeholder', { read: ViewContainerRef }) placeholderRef: ViewContainerRef;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private editor: EditorService,
        private keybindings: KeybindingService,
        private api: LimedsApi,
        private registry: Registry,
        private selection: SelectionService,
        private viewContainerRef: ViewContainerRef,
        private panelManager: PanelManager,
        private resolver: ComponentFactoryResolver
    ) { }

    canDeactivate(): Observable<boolean> {
        return this.isDirty() ? this.showDeactivateDialog() : Observable.of(true);
    }

    ngOnInit() {
        this.routeSub = this.route.params.subscribe(params => {
            let id = limeds.Utils.decodeId(params['id']);
            let version = limeds.Utils.decodeVersion(params['version']);
            if (id) {
                this.api.getSlice(id, version).subscribe(
                    slice => {
                        this.slice = slice;
                        this.original = cloneDeep(slice);
                        this.registry.initWithSlice(slice, this.editor.redraw, this.editor).subscribe(succes => {
                            if (!succes) {
                                let config: AreYouSurePanelConfig = {
                                    title: 'Error loading slice',
                                    description: 'It seems one of the imported segments could not be loaded properly. This most likely because it is no longer available on the server.',
                                    question: 'Pressing \'Ok\' will take you back to the slices overview',
                                    buttons: 'Ok'
                                };
                                this.panelManager.createPanel(PanelType.ARE_YOU_SURE, this.viewContainerRef, true, config).subscribe(result => {
                                    this.original = this.registry.serialize();
                                    this.router.navigateByUrl('');
                                });
                            }
                        });

                        window.onbeforeunload = () => {
                            if (this.isDirty())
                                return "Are you sure you want to leave, you still have unsaved changes?";
                            else
                                return;
                        };

                    },
                    error => {
                        console.error(error);
                        this.router.navigateByUrl('');
                    }
                );
            }
        });

        this.keybindings.addEventSource(window);
        this.keybindings.setKeyBinding(new KeyCombo(Key.KEY_INSERT), new Action(this, this.spawnCreateSegmentPanel));
        this.keybindings.setKeyBinding(new KeyCombo(Key.KEY_DELETE), new Action(this.editor, this.editor.removeSelectedObject));
        this.keybindings.setKeyBinding(new KeyCombo(Key.KEY_ESC), new Action(this.editor, this.editor.clearSelection));
        this.keybindings.setKeyBinding(new KeyCombo(Key.KEY_F1), new Action(this, this.spawnConfigPanel));
        this.keybindings.setKeyBinding(new KeyCombo(Key.KEY_F12, false, true), new Action(this, this.spawnRawCodePanel));
        this.keybindings.setKeyBinding(new KeyCombo(Key.KEY_F11, false, true), new Action(this, this.spawnOriginalCodePanel));
        this.keybindings.setKeyBinding(new KeyCombo(Key.KEY_S, false, true), new Action(this, this.saveSlice));

        window.addEventListener('click', evt => {
            if (evt.button === 2) {
                evt.preventDefault();
                evt.stopPropagation();
            }
            else {
                this.cancelCtxMenus();
            }
        });
        window.addEventListener('contextmenu', evt => {
            evt.preventDefault();
            evt.stopPropagation();
        });


    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }

    onToolbarBtn(event) {
        switch (event) {
            case 'adv-options':
                this.spawnAdvOptionsPanel();
                break;
            case 'create-segment':
                this.spawnCreateSegmentPanel();
                break;
            case 'config-panel':
                this.spawnConfigPanel();
                break;
            case 'color':
                this.spawnColorPanel();
                break;
            case 'config-schemas':
                this.spawnDocumentationPanel();
                break;
            case 'view-doc':
                this.spawnViewDocumentationPanel();
                break;
            case 'edit-configuration':
                this.spawnEditConfigurationPanel();
                break;
            case 'import-segment':
                this.spawnImportSegmentPanel();
                break;
            case 'multi-link':
                this.spawnMultiLinkPanel();
                break;
            case 'script-panel':
                this.spawnScriptPanel();
                break;
            case 'type-editor':
                this.spawnTypesPanel();
                break;
            case 'web-api':
                this.spawnWebApiPanel();
                break;
            case 'rename':
                this.spawnRenameSegmentPanel();
                break;
            case 'toggle-info-layer':
                this.editor.toggleInfoLayer();
                break;
        }
    }

    onPanelAction(event: any) {
        switch (event.subject) {
            case 'link-drawn':
                this.spawnAddLinkPanel(event.fromId, event.toId);
                break;
            case 'link-changed':
                this.onLinkChanged(event.linkId, event.fromId, event.fromVar, event.toId);
                break;
            case 'script-panel':
                this.spawnScriptPanel();
                break;
        }
    }

    onEditorTooltip(event: any) {
        let parent = document.querySelector('flow-editor') as HTMLElement;
        if (event.state === 'over') {
            if (this.myBadgeTooltip === null) {
                this.myBadgeTooltip = document.createElement('div') as HTMLDivElement;
                this.myBadgeTooltip.className = 'my-badge-tooltip';
                this.myBadgeTooltip.style.position = 'fixed';
                parent.appendChild(this.myBadgeTooltip);
            }
            else if (this.tooltipTimer !== null) {
                clearTimeout(this.tooltipTimer);
                this.tooltipTimer = null;
            }
            this.myBadgeTooltip.innerHTML = event.content;
            this.myBadgeTooltip.style.left = event.x + 'px';
            this.myBadgeTooltip.style.top = (event.y - 10) + 'px';

        }
        else if (event.state === 'out') {
            if (this.myBadgeTooltip !== null) {
                let that = this;
                this.tooltipTimer = setTimeout(() => {
                    parent.removeChild(that.myBadgeTooltip);
                    that.myBadgeTooltip = null;
                    this.tooltipTimer = null;
                }, 500);
            }
        }
    }

    onClickAction(action: any) {
        let show = action.show;
        if (action.show) {
            let x = action.x;
            let y = action.y;
            if (this.ctxMenuRef) {
                this.ctxMenuRef.instance.setLocation(x, y);
            }
            else {
                let ref = this.resolver.resolveComponentFactory(SegmentContextMenu);
                this.ctxMenuRef = this.placeholderRef.createComponent(ref, 0);
                // position
                this.ctxMenuRef.instance.setLocation(x, y);
                this.ctxMenuRef.instance.menuAction$.subscribe(event => this.onToolbarBtn(event));
            }
        }
        else {
            this.ctxMenuRef && this.ctxMenuRef.instance.hide();
        }
    }

    openSwagger() {
        window.open('/swagger/');
    }

    openDebugView() {
        window.open('/editor/errors');
    }

    private showDeactivateDialog(): Observable<boolean> {
        let config: AreYouSurePanelConfig = {
            title: 'Unsaved changes detected!',
            description: 'Are you sure you wish to leave now. There are still unsaved changes.',
            question: '\'Ok\': navigate away and loose changes.\'Cancel\': stay here.',
            buttons: 'OkCancel'
        };

        return Observable.create(o => {
            this.panelManager.createPanel(PanelType.ARE_YOU_SURE, this.viewContainerRef, true, config).subscribe(result => {
                o.next(result.action === PanelAction.CONFIRM);
            });
        });
    }

    /**
     * Use this to check for changes
     */
    private isDirty() {
        let clientSlice = this.registry.serialize();
        let differs = !isEqualWith(this.original, clientSlice, (a, b, key) => {
            return 'status' === key ? true : undefined;
        });
        return differs;
    }

    private copySliceToOriginal(slice: limeds.ISlice) {
        this.original = cloneDeep(slice)
    }

    private saveAndDeploySlice() {
        let slice = this.registry.serialize();
        this.api.updateSlice(slice, slice.id, slice.version)
            .do(() => this.copySliceToOriginal(slice))
            .switchMap(() => Observable.of(this.spawnNotification('Saved', 'The slice is saved')))
            .switchMap(() => this.api.deploySlice(slice.id, slice.version, true))
            .do((savedSlice) => this.copySliceToOriginal(savedSlice))
            .subscribe(result2 => {
                this.slice.status = result2.status;
                this.spawnNotification('Slice is active.', 'All types and segments are available now.');
            });
    }

    private saveAndUndeploySlice() {
        let slice = this.registry.serialize();
        this.api.updateSlice(slice, slice.id, slice.version)
            .do(() => this.copySliceToOriginal(slice))
            .switchMap(() => Observable.of(this.spawnNotification('Saved', 'The slice is saved')))
            .switchMap(() => this.api.undeploySlice(slice.id, slice.version, true))
            .do((savedSlice) => this.copySliceToOriginal(savedSlice))
            .subscribe(result2 => {
                this.slice.status = result2.status;
                this.spawnNotification('Slice is deactivated', 'All types and segments are hidden.');
            });
    }

    saveSlice() {
        if (this.slice.status === 'deployed') {
            this.saveAndDeploySlice();
        }
        else {
            let slice = this.registry.serialize();
            this.api.updateSlice(slice, slice.id, slice.version).subscribe(
                result => {
                    this.copySliceToOriginal(slice);
                    this.spawnNotification('Saved', 'The slice is saved');
                }
            );
        }
    }

    saveAsSlice() {
        this.spawnSaveAsPanel(this.slice);
    }

    getActiveState() {
        if (this.slice) {
            switch (this.slice.status) {
                case 'deployed':
                    return 'Active';
                case 'undeployed':
                    return 'Inactive';
                case 'processing':
                    return 'Processing'
                default:
                    return 'Error state';
            }
        }
        else
            return 'Inactive';
    }

    getActiveStateCss(): any {
        if (this.slice) {
            return {
                'fa-toggle-on': this.slice.status === 'deployed',
                'fa-toggle-off': this.slice.status === 'undeployed',
                'fa-question-circle-o': this.slice.status === 'processing'
            };
        } else {
            return {};
        }
    }

    getActiveButtonStateCss(): any {
        if (this.slice) {
            return {
                'btn-lime2': this.slice.status === 'deployed',
                'btn-danger': this.slice.status === 'undeployed',
                'btn-default': this.slice.status === 'processing',
            };
        } else {
            return {};
        }
    }

    toggleActiveState() {
        if (this.slice.status === 'deployed') {
            this.saveAndUndeploySlice();
        }
        else if (this.slice.status === 'undeployed') {
            this.saveAndDeploySlice();
        }
    }

    spawnOriginalCodePanel() {
        this.panelManager.createPanel(PanelType.RAW_CODE, this.viewContainerRef, true, { title: 'Original', code: this.original });
    }

    spawnRawCodePanel() {
        this.panelManager.createPanel(PanelType.RAW_CODE, this.viewContainerRef, true, { title: 'Client serialize' });
    }

    private spawnConfigPanel() {
        if (this.selection.isSelectionActive() && this.selection.getCurrentSelection().type == limeds.LimedsType.Segment) {
            let segment = this.registry.findSegment(this.selection.getCurrentSelection().id);
            // let canExport = !(segment.getModel().templateInfo && segment.getModel().templateInfo.type === 'FACTORY_INSTANCE');
            let config = {
                id: segment.getId(),
                version: segment.getModel().version,
                versionExpressesion: segment.getView().getVersionExpression(),
                export: segment.getModel().export,
                description: segment.getModel().description,
                // canExport: canExport,
                type: segment.getView().type,
                templateInfo: segment.getModel().templateInfo
            };
            let sub = this.panelManager.createPanel(PanelType.CONFIG, this.viewContainerRef, true, config);
            sub.subscribe(result => this.onCloseConfigPanel(segment, result));
        }
    }

    private spawnColorPanel() {
        if (this.selection.isSelectionActive() && this.selection.getCurrentSelection().type == limeds.LimedsType.Segment) {
            let segment = this.registry.findSegment(this.selection.getCurrentSelection().id);
            let sub = this.panelManager.createPanel(PanelType.COLOR, this.viewContainerRef, true, segment.getView().color);
            sub.subscribe(result => this.onCloseColorPanel(segment, result));
        }
    }

    private spawnCreateSegmentPanel() {
        let subject = this.panelManager.createPanel(PanelType.CREATE_SEGMENT, this.viewContainerRef, true);
        subject.subscribe(result => {
            switch (result.action) {
                case PanelAction.CONFIRM:
                    let id = this.registry.getFQN(result.result.name);
                    this.editor.createSegment(id);
                    break;
                case PanelAction.CANCEL:
                default:
                // do nothing
            }
        });
    }

    private spawnScriptPanel() {
        if (this.selection.isSelectionActive() && this.selection.getCurrentSelection().type == limeds.LimedsType.Segment) {
            let segment = this.registry.findSegment(this.selection.getCurrentSelection().id);
            let config = {
                dependencies: segment.getModel().dependencies,
                script: this.registry.getScripts().get(segment.getId()) || '',
                inputDocs: segment.getModel().documentation,
                segmentLabel: segment.getView().label
            };
            let sub = this.panelManager.createPanel(PanelType.SCRIPT, this.viewContainerRef, true, config);

            sub.subscribe(result => this.onCloseScriptPanel(segment, result));
        }
    }

    private spawnWebApiPanel() {
        if (this.selection.isSelectionActive() && this.selection.getCurrentSelection().type == limeds.LimedsType.Segment) {
            let segment = this.registry.findSegment(this.selection.getCurrentSelection().id);
            let config = {
                web: segment.getModel().httpOperation,
                schedule: segment.getModel().schedule,
                subscriptions: segment.getModel().subscriptions,
                httpDocumentation: segment.getModel().httpDocumentation,
                id: segment.getId()
            };
            let sub = this.panelManager.createPanel(PanelType.WEB_API, this.viewContainerRef, true, config);

            sub.subscribe(result => this.onCloseWebApiPanel(segment, result));
        }
    }

    private spawnAddLinkPanel(fromId: string, toId: string) {
        let segmentFrom = this.registry.findSegment(fromId);
        let segmentTo = this.registry.findSegment(toId);
        let createsCycle = this.registry.findLinkBetween(segmentTo.getId(), segmentFrom.getId());
        let config = {
            isFactoryInstance: segmentFrom.getView().type === 'INSTANTIATED',//segmentFrom.getModel().templateInfo && segmentFrom.getModel().templateInfo.type === 'FACTORY_INSTANCE',
            dependencies: segmentFrom.getModel().dependencies,
            toId: segmentTo.getId(),
            toLabel: segmentTo.getView().label,
            cycle: createsCycle
        };
        let sub = this.panelManager.createPanel(PanelType.ADD_LINK, this.viewContainerRef, true, config);

        sub.subscribe(result => this.onCloseAddLinkPanel(fromId, toId, result));
    }

    private spawnImportSegmentPanel() {
        let sub = this.panelManager.createPanel(PanelType.IMPORT_SEGMENT, this.viewContainerRef, true, { sliceId: this.slice.id });
        sub.subscribe(result => this.onCloseImportSegmentPanel(result));
    }

    private spawnFactoryInstancePanel(factory: limeds.ISegmentModel) {
        let config = {
            factory: factory
        };
        let sub = this.panelManager.createPanel(PanelType.FACTORY_INSTANCE, this.viewContainerRef, true, config);
        sub.subscribe(result => this.onCloseFactoryInstancePanel(factory, result));
    }

    private spawnAdvOptionsPanel() {
        if (this.selection.isSelectionActive() && this.selection.getCurrentSelection().type == limeds.LimedsType.Segment) {
            let segment = this.registry.findSegment(this.selection.getCurrentSelection().id);
            let config = segment.getModel().configureAspects || {};
            let sub = this.panelManager.createPanel(PanelType.ADV_OPTIONS, this.viewContainerRef, true, config);

            sub.subscribe(result => this.onCloseAdvOptionsPanel(segment, result));
        }
    }

    private spawnDocumentationPanel() {
        if (this.selection.isSelectionActive() && this.selection.getCurrentSelection().type == limeds.LimedsType.Segment) {
            let segment = this.registry.findSegment(this.selection.getCurrentSelection().id);
            let config = {
                applyDoc: segment.getModel().documentation && segment.getModel().documentation.find(d => 'apply' === d.id),
                slice: this.slice,
                web: segment.getModel().httpOperation && Object.keys(segment.getModel().httpOperation).length > 0
            };
            let sub = this.panelManager.createPanel(PanelType.DOCUMENTATION, this.viewContainerRef, true, config);
            sub.subscribe(result => this.onCloseDocumentationPanel(segment, result));
        }
    }

    private spawnViewDocumentationPanel() {
        if (this.selection.isSelectionActive() && this.selection.getCurrentSelection().type == limeds.LimedsType.Segment) {
            let segment = this.registry.findSegment(this.selection.getCurrentSelection().id);
            let label = segment.view.label;
            let id = segment.getId();
            let config = {
                title: 'View ' + label + ' Documentation',
                var: { collection: false, asVar: false, label: label },
                documentation: segment.getModel().documentation,
                description: segment.getModel().description
            };
            this.panelManager.createPanel(PanelType.VIEW_DOCUMENTATION, this.viewContainerRef, true, config);
        }
    }

    private spawnSaveAsPanel(slice: limeds.ISlice) {
        let config = {
            currentVersion: slice.version,
            id: slice.id
        };
        let sub = this.panelManager.createPanel(PanelType.SAVE_AS, this.viewContainerRef, true, config);
        sub.subscribe(result => this.onCloseSaveAsPanel(result));
    }

    private spawnMultiLinkPanel() {
        if (this.selection.isSelectionActive() && this.selection.getCurrentSelection().type == limeds.LimedsType.Segment) {
            let segment = this.registry.findSegment(this.selection.getCurrentSelection().id);
            let config = {
                segment: segment
            };
            let sub = this.panelManager.createPanel(PanelType.MULTI_LINK, this.viewContainerRef, true, config);
            sub.subscribe(result => this.onCloseMultiPanel(result, segment));
        }
    }

    private spawnTypesPanel() {
        let config = {
            typeDefinitions: this.slice.typeDefinitions || {}
        };
        this.panelManager.createPanel(PanelType.TYPES, this.viewContainerRef, true, config).subscribe(result => this.onCloseTypesPanel(result));
    }

    private spawnEditConfigurationPanel() {
        if (this.selection.isSelectionActive() && this.selection.getCurrentSelection().type == limeds.LimedsType.Segment) {
            let segment = this.registry.findSegment(this.selection.getCurrentSelection().id);
            let config = {
                segment: segment
            };
            let sub = this.panelManager.createPanel(PanelType.EDIT_CONFIGURATION, this.viewContainerRef, true, config);
            sub.subscribe(result => this.onCloseEditConfigurationPanel(result, segment));
        }
    }

    private spawnRenameSegmentPanel() {
        if (this.selection.isSelectionActive() && this.selection.getCurrentSelection().type == limeds.LimedsType.Segment) {
            let segment = this.registry.findSegment(this.selection.getCurrentSelection().id);
            let label = segment.view.label;
            let id = segment.getId();
            let config: PromptPanelConfig = {
                title: 'Rename Segment',
                description: 'Renaming a segment will have some effects. If this Segment is used in other slices, they will no longer refer to this renamed segment, and thus will be borken, until fixed manually. Variables pointing to this segment will point to the renamed one instead, but they will keep their original variable name.',
                okText: 'Rename',
                label: 'Segment name',
                placeholder: 'Rename segment',
                value: label,
                validationFunction: function (newValue: string, oldValue: string) {
                    let check = oldValue !== newValue;
                    let regex = new RegExp(/^[A-Z][^\.\s]+$/g);
                    check = check && regex.test(newValue);
                    check = check && newValue.length !== 0;
                    return check;
                }
            };

            let sub = this.panelManager.createPanel(PanelType.PROMPT, this.viewContainerRef, true, config);
            sub.subscribe(result => this.onCloseRenameSegmentPanel(result, segment));
        }
    }

    private onCloseRenameSegmentPanel(result: PanelResult, segment: limeds.ISegment) {
        switch (result.action) {
            case PanelAction.CONFIRM:
                let newLabel = result.result.input;
                this.registry.renameSegment(segment, newLabel);
                this.editor.redraw();
                break;
            case PanelAction.CANCEL:
            default:
        }
    }

    private onCloseEditConfigurationPanel(result: PanelResult, segment: limeds.ISegment) {
        switch (result.action) {
            case PanelAction.CONFIRM:
                let configuration = result.result.configEntries;
                // No tempalteInfo, make this into a configurable instance.
                if (!segment.getModel().templateInfo) {
                    segment.getModel().templateInfo = {
                        type: 'CONFIGURABLE_INSTANCE',
                        configuration: configuration
                    };
                }
                else {
                    segment.getModel().templateInfo.configuration = configuration;
                }
                // No configuration
                if (segment.getModel().templateInfo.configuration.length === 0) {
                    delete segment.getModel().templateInfo;
                }
                break;
            case PanelAction.CANCEL:
            default:
        }
        this.editor.redraw();
    }

    private onCloseTypesPanel(result: PanelResult) {
        switch (result.action) {
            case PanelAction.CONFIRM:
                this.slice.typeDefinitions = result.result.typeDefinitions;
            case PanelAction.CANCEL:
            default:
        }
        // this.editor.redraw();
    }

    private onCloseMultiPanel(result: PanelResult, segment: limeds.ISegment) {
        switch (result.action) {
            case PanelAction.CONFIRM:
            case PanelAction.CANCEL:
            default:
        }
        this.editor.redraw();
    }

    private onCloseSaveAsPanel(result: PanelResult) {
        switch (result.action) {
            case PanelAction.CONFIRM:
                // saving
                let version = result.result.newVersion;
                let open = result.result.open;
                let undeploy = result.result.undeploy;
                let slice = cloneDeep(this.registry.serialize());

                let possiblyUndeploy: Observable<boolean> = Observable.create(o => {
                    if (undeploy) {
                        this.spawnNotification('Deactivating slice', 'Deactivating the current version...');
                        this.api.undeploySlice(slice.id, slice.version, true).subscribe(
                            res => {
                                this.slice.status = res.status;
                                this.spawnNotification('Slice is deactivated', 'All types and segments are hidden.');
                                o.next(true)
                            },
                            err => o.next(true)
                        );
                    }
                    else {
                        o.next(true);
                    }
                });

                possiblyUndeploy
                    .do(() => {
                        slice.version = version;
                        slice.status = 'undeployed';
                    })
                    .switchMap(() => this.api.createSlice(slice))
                    .subscribe(() => {
                        if (open) {
                            this.spawnNotification('Version bumped', 'New version saved, opening now...');
                            let id = limeds.Utils.encodeId(slice.id);
                            let version = limeds.Utils.encodeVersion(slice.version);
                            window.open('/editor/view/' + id + ';' + 'version=' + version);
                        } else {
                            this.spawnNotification('Version bumped', 'New version saved in slices overview');
                        }
                    });


                break;
            case PanelAction.CANCEL:
            default:
        }
        this.editor.redraw();
    }


    private onCloseDocumentationPanel(segment: limeds.ISegment, result: PanelResult) {
        switch (result.action) {
            case PanelAction.CONFIRM:
                if (!segment.getModel().documentation) {
                    segment.getModel().documentation = [];
                }
                let applyIdx = segment.getModel().documentation.findIndex(d => 'apply' === d.id);
                if (applyIdx > -1) {
                    segment.getModel().documentation.splice(applyIdx, 1);
                }
                segment.getModel().documentation.push(result.result.applyDoc);
                // Update script in registry
                let script = this.registry.getScripts().get(segment.getId());
                let applyDoc = segment.getModel().documentation && result.result.applyDoc;
                this.registry.getScripts().set(segment.getId(), limeds.CodeTemplates.replaceVars(script, result.result.applyDoc));

                break;
            case PanelAction.CANCEL:
            default:
        }
        this.editor.redraw();
    }

    private onCloseAdvOptionsPanel(segment: limeds.ISegment, result: PanelResult) {
        switch (result.action) {
            case PanelAction.CONFIRM:
                segment.getModel().configureAspects = result.result;
                break;
            case PanelAction.CANCEL:
            default:
        }
        this.editor.redraw();
    }

    private onCloseFactoryInstancePanel(factory: limeds.ISegmentModel, result: PanelResult) {
        switch (result.action) {
            case PanelAction.CONFIRM:
                let configuration = result.result.configEntries;
                let deps = result.result.dependencies;
                let name = result.result.name;
                this.editor.instantiateFactory(factory, name, configuration, deps);
                break;
            case PanelAction.CANCEL:
            default:
        }
        this.editor.redraw();
    }

    private onCloseImportSegmentPanel(result: PanelResult) {
        switch (result.action) {
            case PanelAction.CONFIRM:
                let id = result.result.segment.id;
                let isFactory = result.result.segment.factory;
                let versionExpression = result.result.version;
                if (!isFactory) {
                    setTimeout(() => {
                        let segment = this.registry.findSegment(id);
                        if (segment) {
                            setTimeout(() => {
                                let config = {
                                    title: 'Segment is already imported!',
                                    description: 'A Segment with version expression ' + segment.getView().getVersionExpression() + ' is already present. You cannot have multiple version active in one Slice.',
                                    question: 'Would you like to overwrite it with the selected version (' + versionExpression + ')? [Yes: overwrite, No: rename, Cancel: go back]',
                                    buttons: 'YesNoCancel'
                                };
                                let subject = this.panelManager.createPanel(PanelType.ARE_YOU_SURE, this.viewContainerRef, true, config)
                                subject.subscribe(question => {
                                    switch (question.action) {
                                        case PanelAction.CONFIRM:
                                            setTimeout(() => {
                                                let name = segment.getView().label;
                                                let subject = this.panelManager.createPanel(PanelType.CREATE_SEGMENT, this.viewContainerRef, true, { name: name, edit: true });
                                                let newVersion = question.result.choice === 'ok' ? versionExpression : segment.getView().getVersionExpression();
                                                subject.subscribe(result2 => {
                                                    switch (result2.action) {
                                                        case PanelAction.CONFIRM:
                                                            this.editor.importSegment(id, result2.result.name, newVersion);
                                                            break;
                                                        case PanelAction.CANCEL:
                                                        default:
                                                        // do nothing
                                                    }
                                                });
                                            });
                                            break;
                                        case PanelAction.CANCEL:
                                            break;
                                    }
                                });
                            });
                        }
                        else {
                            let name = this.registry.getLabelFromFQN(id);
                            let subject = this.panelManager.createPanel(PanelType.CREATE_SEGMENT, this.viewContainerRef, true, { name: name });
                            subject.subscribe(result2 => {
                                switch (result2.action) {
                                    case PanelAction.CONFIRM:
                                        this.editor.importSegment(id, result2.result.name, versionExpression);
                                        break;
                                    case PanelAction.CANCEL:
                                    default:
                                    // do nothing
                                }
                            });
                        }
                    });
                }
                else {
                    setTimeout(() => {
                        let factory = result.result.factory;
                        this.spawnFactoryInstancePanel(factory);
                    });
                }
                break;
            case PanelAction.CANCEL:
            default:
        }
        this.editor.redraw();
    }

    private onCloseConfigPanel(segment: limeds.ISegment, result: PanelResult) {
        switch (result.action) {
            case PanelAction.CONFIRM:
                // Create link
                segment.getModel().export = result.result.export;
                segment.getModel().description = result.result.description;
                if (result.result.templateInfo) {
                    segment.getModel().templateInfo = result.result.templateInfo;
                }
                else {
                    delete segment.getModel().templateInfo;
                }
                break;
            case PanelAction.CANCEL:
            default:
        }
        this.editor.redraw();
    }

    private onLinkChanged(linkId: string, fromId: string, fromVar: string, toId: string) {
        // Memove link (not dependency)
        this.registry.delLink(linkId);

        // Make new link, but set to overwrite
        let linkView = new limeds.LinkView(fromId, fromVar, toId);
        let link = new limeds.Link(linkView);
        this.registry.addLink(link);
        this.selection.changeSelection(link.getView().getId(), limeds.LimedsType.Link);
        // Redraw is handled by changing the selection to the new link
        //this.editor.redraw();
    }

    private onCloseAddLinkPanel(fromId: string, toId: string, result: PanelResult) {
        switch (result.action) {
            case PanelAction.CONFIRM:
                // Create link
                let varName = result.result.varName;
                let linkView = new limeds.LinkView(fromId, varName, toId);
                let link = new limeds.Link(linkView);
                this.registry.addLink(link);
                break;
            case PanelAction.CANCEL:
            default:
        }
        this.editor.redraw();
    }

    private onCloseColorPanel(segment: limeds.ISegment, result: PanelResult) {
        switch (result.action) {
            case PanelAction.CONFIRM:
                let color = result.result.color;
                segment.getView().color = color;
                this.editor.recacheSegment(segment);
                this.editor.redraw();
                break;
            case PanelAction.CANCEL:
            default:
            //do nothing
        }
        this.editor.redraw();
    }

    private onCloseScriptPanel(segment: limeds.ISegment, result: PanelResult) {
        switch (result.action) {
            case PanelAction.CONFIRM:
                let script = result.result.script;
                this.registry.getScripts().set(segment.getId(), script);
                break;
            case PanelAction.CANCEL:
            default:
            // do nothing
        }
        this.editor.redraw();
    }

    private onCloseWebApiPanel(segment: limeds.ISegment, result: PanelResult) {
        switch (result.action) {
            case PanelAction.CONFIRM:
                let web = result.result.web;
                let schedule = result.result.schedule;
                let subscriptions = result.result.subscriptions;
                let httpDocumentation = result.result.httpDocumentation;

                // subscriptions
                segment.getModel().subscriptions = subscriptions;

                // schedule
                segment.getModel().schedule = schedule;

                // httpOperation
                segment.getModel().httpOperation = web;

                // httpDocumentation
                segment.getModel().httpDocumentation = httpDocumentation;

                // Change template header
                let httpOn = Object.keys(web).length > 0;
                let script = this.registry.getScripts().get(segment.getId());
                script = limeds.CodeTemplates.toggleHttpArgs(script, httpOn);
                // Update script in registry
                let applyDoc = segment.getModel().documentation && segment.getModel().documentation['apply'];
                this.registry.getScripts().set(segment.getId(), limeds.CodeTemplates.replaceVars(script, applyDoc));
                break;
            case PanelAction.CANCEL:
            default:
            // do nothing
        }
        this.editor.redraw();
    }

    private spawnNotification(title: string, body: string) {
        this.requestNotificationPermission().then(
            () => {
                let options = {
                    icon: './assets/img/logo.png',
                    body: body,
                    requireInteraction: false,
                    sticky: false
                };
                let n = new Notification(title, options);
                setTimeout(() => n.close(), 2500);
            },
            error => console.error(error)
        );
    }

    private requestNotificationPermission() {
        if ("Notification" in window) {
            if (Notification.permission !== 'granted' && Notification.permission !== 'denied') {
                return Notification.requestPermission().then(
                    perm => perm === 'granted',
                    () => false
                );
            }
            else {
                return Promise.resolve(Notification.permission === 'granted');
            }
        }
        else {
            return Promise.resolve(false);
        }
    }

    private cancelCtxMenus() {
        this.ctxMenuRef && this.ctxMenuRef.instance.hide();
    }

}
