import { Inject, Injectable } from '@angular/core';

import { APP_CONFIG, } from '../../shared/config';
import * as limeds from '../../shared/limeds';

interface BezierPoints {
    from: createjs.Point;
    cp1: createjs.Point;
    cp2: createjs.Point;
    to: createjs.Point;
}

interface MultiplexBounds {
    x: number,
    y: number,
    radius: number
}

@Injectable()
export class DrawService {
    private infoLayer: boolean = false;

    /**
     * Segment cache indexed on color string
     */
    private segmentCache: Map<string, createjs.DisplayObject> = new Map<string, createjs.DisplayObject>();
    private connectorCache: Map<string, createjs.DisplayObject> = new Map<string, createjs.DisplayObject>();

    constructor( @Inject(APP_CONFIG) private config) {
        this.segmentCache.set(config.segment_color, this.createSegmentGraphics(config.segment_color));
        this.connectorCache.set(config.segment_color, this.createConnectorGraphics(config.segment_color));
    }

    /**
     * Get a cached displayObject if available, else create one. and return it.
     */
    public getCachedSegment(color: string) {
        if (!this.segmentCache.has(color)) {
            this.segmentCache.set(color, this.createSegmentGraphics(color));
        }
        return this.segmentCache.get(color);
    }

    /**
     * Get a chached displayObject if available, else create one. and return it.
     */
    public getCachedConnector(color: string) {
        if (color === null) {
            color = 'read_only';
        }
        if (!this.connectorCache.has(color)) {
            this.connectorCache.set(color, this.createConnectorGraphics(color));
        }
        return this.connectorCache.get(color);
    }

    /**
     * Draw a link complete with arrow from an ISegment to another ISegment.
     * 
     * @param line The shape to use, inject the existing one for a redraw, otherwise a new createjs.Shape
     * @param start The start of the link
     * @param stop The destination of the link
     * @param isSelected Whether the link has to be drawn as currently selected.
     */
    public link(line: createjs.Shape, start: limeds.ISegment, stop: limeds.ISegment, isSelected: boolean): createjs.Graphics {
        let g = line.graphics;
        let C = this.config;
        let bp = this.getBezierPoints(start, stop);
        let color = isSelected ? C.link_selected_color : C.link_color;
        let thickness = isSelected ? C.link_selected_thickness : C.link_thickness;
        g.clear();
        line['stroke'] = g.setStrokeStyle(thickness).command;
        g.beginStroke(color);
        g.moveTo(bp.from.x, bp.from.y);

        if (bp.from.x === bp.to.x || bp.from.y === bp.to.y) {
            g.lineTo(bp.to.x, bp.to.y);
        } else {
            g.bezierCurveTo(bp.cp1.x, bp.cp1.y, bp.cp2.x, bp.cp2.y, bp.to.x, bp.to.y);
        }

        // Draw arrow
        let t = C.link_arrow_location;
        g.beginFill(color);
        let p1 = LineUtils.getPerpendicularPointFromBezierPoint(bp.from, bp.cp1, bp.cp2, bp.to, t, 4);
        let p2 = LineUtils.getDirectionalPointFromBezierPoint(bp.from, bp.cp1, bp.cp2, bp.to, t, 9);
        let p3 = LineUtils.getPerpendicularPointFromBezierPoint(bp.from, bp.cp1, bp.cp2, bp.to, t, -4);
        g.moveTo(p1.x, p1.y);
        g.lineTo(p2.x, p2.y);
        g.lineTo(p3.x, p3.y);
        g.closePath();

        // Draw move handler
        if (isSelected) {
            let point = LineUtils.getBezierPoint(bp.from, bp.cp1, bp.cp2, bp.to, C.link_change_handle_location);
            let radius = C.link_change_handle_radius;
            g.setStrokeStyle(C.link_change_handle_border_thickness);
            g.beginStroke(C.link_change_handle_border_color);
            g.beginFill(C.link_change_handle_color);
            g.drawCircle(point.x, point.y, radius);
            g.endFill();
            g.endStroke();
        }

        return g;
    }

    public multiLink(line: createjs.Shape, start: limeds.ISegment, stop: limeds.ISegment[], t: number, x: number, y: number, isSelected: boolean): createjs.Graphics {
        let g = line.graphics;
        let C = this.config;

        // Calc position
        let posStart = start.getView().getCenterPoint();
        let positions = stop.map(segment => segment.getView().getCenterPoint());
        let posEnd = positions.reduce((prev, curr) => new createjs.Point(prev.x + curr.x, prev.y + curr.y));
        posEnd = new createjs.Point(posEnd.x / positions.length, posEnd.y / positions.length);

        let multiplex = t > 0 ? LineUtils.getLinePoint(posStart, posEnd, t) : new createjs.Point(x, y);

        // Line to multiplex
        let bp = this.getBezierPointsTo(start, multiplex.x, multiplex.y);
        let color = isSelected ? C.link_selected_color : C.link_color;
        let color_multi = isSelected ? C.link_selected_color : C.link_multi_color;
        let thickness = isSelected ? C.link_selected_thickness : C.link_thickness;
        let thickness_multi = C.link_multi_thickness;
        let thickness_multiplex = C.link_multi_multiplex_thickness;
        g.clear();
        line['stroke'] = g.setStrokeStyle(thickness).command;
        g.beginStroke(color);
        g.moveTo(bp.from.x, bp.from.y);

        if (bp.from.x === bp.to.x || bp.from.y === bp.to.y) {
            g.lineTo(bp.to.x, bp.to.y);
        } else {
            g.bezierCurveTo(bp.cp1.x, bp.cp1.y, bp.cp2.x, bp.cp2.y, bp.to.x, bp.to.y);
        }
        g.endStroke();

        // Draw arrow
        let tA = C.link_arrow_location;
        g.beginStroke(color)
        g.beginFill(color);
        let p1 = LineUtils.getPerpendicularPointFromBezierPoint(bp.from, bp.cp1, bp.cp2, bp.to, tA, 4);
        let p2 = LineUtils.getDirectionalPointFromBezierPoint(bp.from, bp.cp1, bp.cp2, bp.to, tA, 9);
        let p3 = LineUtils.getPerpendicularPointFromBezierPoint(bp.from, bp.cp1, bp.cp2, bp.to, tA, -4);
        g.moveTo(p1.x, p1.y);
        g.lineTo(p2.x, p2.y);
        g.lineTo(p3.x, p3.y);
        g.endFill();
        g.endStroke();



        // Draw other end links
        for (let end of stop) {
            let bp2 = this.getBezierPointsFrom(multiplex.x, multiplex.y, end);
            g.setStrokeStyle(thickness_multi)
            g.beginStroke(color_multi);
            g.moveTo(bp2.from.x, bp2.from.y);

            if (bp2.from.x === bp2.to.x || bp2.from.y === bp2.to.y) {
                g.lineTo(bp2.to.x, bp2.to.y);
            } else {
                g.bezierCurveTo(bp2.cp1.x, bp2.cp1.y, bp2.cp2.x, bp2.cp2.y, bp2.to.x, bp2.to.y);
            }

            // Draw arrow
            let tA = C.link_arrow_location;
            g.beginFill(color_multi);
            let p1 = LineUtils.getPerpendicularPointFromBezierPoint(bp2.from, bp2.cp1, bp2.cp2, bp2.to, tA, 4);
            let p2 = LineUtils.getDirectionalPointFromBezierPoint(bp2.from, bp2.cp1, bp2.cp2, bp2.to, tA, 9);
            let p3 = LineUtils.getPerpendicularPointFromBezierPoint(bp2.from, bp2.cp1, bp2.cp2, bp2.to, tA, -4);
            g.moveTo(p1.x, p1.y);
            g.lineTo(p2.x, p2.y);
            g.lineTo(p3.x, p3.y);
            g.endFill();
            g.endStroke();
        }

        // Draw multiplex
        g.setStrokeStyle(thickness_multiplex);
        g.beginStroke(color);
        g.beginFill(color_multi);
        g.drawCircle(multiplex.x, multiplex.y, C.link_multi_radius);
        g.endFill();
        g.endStroke();
        line['multiplexBounds'] = { x: multiplex.x, y: multiplex.y, radius: C.link_multi_radius };

        // Draw move handler
        // if (isSelected) {
        //     let point = LineUtils.getBezierPoint(bp.from, bp.cp1, bp.cp2, bp.to, C.link_change_handle_location);
        //     let radius = C.link_change_handle_radius;
        //     g.setStrokeStyle(C.link_change_handle_border_thickness);
        //     g.beginStroke(C.link_change_handle_border_color);
        //     g.beginFill(C.link_change_handle_color);
        //     g.drawCircle(point.x, point.y, radius);
        //     g.endFill();
        //     g.endStroke();
        // }

        return g;
    }

    public linkMoveTargetHit(point: createjs.Point, start: limeds.ISegment, stop: limeds.ISegment) {
        let C = this.config;
        let bp = this.getBezierPoints(start, stop);
        let origin = LineUtils.getBezierPoint(bp.from, bp.cp1, bp.cp2, bp.to, C.link_change_handle_location);
        let radius = C.link_change_handle_radius + C.link_selected_thickness;
        let dist = Math.pow(origin.x - point.x, 2) + Math.pow(origin.y - point.y, 2)
        return {
            hit: dist <= Math.pow(radius, 2),
            from: bp.from
        };
    }

    public multiLinkMultiplexHit(point: createjs.Point, bounds: MultiplexBounds) {
        let C = this.config;
        let radius = bounds.radius
        let dist = Math.pow(bounds.x - point.x, 2) + Math.pow(bounds.y - point.y, 2)
        return {
            hit: dist <= Math.pow(radius, 2)
        };
    }

    /**
     * Draws a temporary dashed straigth line with an arrow head.
     * 
     * @param line the shape to use the grahpics object from
     * @param start the starting point
     * @param stop the end point
     */
    public tempLink(line: createjs.Shape, start: createjs.Point, stop: createjs.Point) {
        let g = line.graphics;
        let config = this.config;
        // line
        g.clear();
        g.setStrokeStyle(2);
        g.beginStroke(config.link_drawing_color);
        g.setStrokeDash([10, 5]);
        g.moveTo(start.x, start.y).lineTo(stop.x, stop.y);

        // arrow head
        let p = LineUtils.getDirectionalPointFromLinePoint(start, stop, stop, -10);
        let p1 = LineUtils.getPerpendicularPointFromLinePoint(start, stop, p, 4);
        let p2 = LineUtils.getPerpendicularPointFromLinePoint(start, stop, p, -4);
        g.beginFill(config.link_drawing_color);
        g.setStrokeDash();
        g.moveTo(stop.x, stop.y).lineTo(p1.x, p1.y).lineTo(p2.x, p2.y).closePath();
    }

    /**
     * Draws a complete segment from the given options in the ISegmentView argument
     */
    public segment(options: limeds.ISegmentView, readOnly: boolean, badges?: any): createjs.DisplayObject {
        let config = this.config;
        let width = config.segment_width;
        let height = config.segment_height;
        if (this.infoLayer) {
            height += config.segment_height / 3;
        }

        let segmentTemplate = this.getCachedSegment(options.color);
        let base = segmentTemplate.clone();
        //base['stroke'] = this.setupsSegmentGraphics(base as createjs.Shape, options.color);


        let group = new createjs.Container();
        group['baseNode'] = (function () {
            return base;
        })();
        group.addChild(base);

        // Connectors     
        let w = this.config.connector_size;
        let h = this.config.connector_size;
        let x = (width - w) / 2;
        let y = -h / 2;

        var n1 = this.getClonedConnectorAt(x, y, options.color, 'north', readOnly);
        var n2 = this.getClonedConnectorAt(x, y + height, options.color, 'south', readOnly);
        var n3 = this.getClonedConnectorAt(-w / 2, (height - h) / 2, options.color, 'west', readOnly);
        var n4 = this.getClonedConnectorAt(-w / 2 + width, (height - h) / 2, options.color, 'east', readOnly);

        group.addChild(n1);
        group.addChild(n2);
        group.addChild(n3);
        group.addChild(n4);


        // TextNode
        let title = new createjs.Text(options.label, config.segment_title_font, Util.getTextColor(options.color));
        let metrics: any = title.getMetrics();
        title.x = Math.round((width - metrics.width) / 2);
        title.y = Math.round(config.segment_title_offset_top);
        group.addChild(title);

        if (this.infoLayer) {
            // Badges
            if (badges) {
                let iconWidth = 16, borderOffset = 5, offsetX = 4;
                let font = "18px FontAwesome";
                let color = Util.shadeColor(this.config.segment_color, -0.50);

                let icon = 0xf29c;
                let badgeOptions = {
                    web: { icon: 0xf0ac, alignLeft: true, order: 0 },
                    schedule: { icon: 0xf017, alignLeft: true, order: 1 },
                    subscriptions: { icon: 0xf09e, alignLeft: true, order: 2 },
                    validation: { icon: 0xf071, alignLeft: false, order: 3 },
                    caching: { icon: 0xf1c0, alignLeft: false, order: 4 }
                };
                let badgesLeft = 0, badgesRight = 0;
                let hit = null;
                let sortedBadges = Object.keys(badges).sort((a, b) => badgeOptions[a].order - badgeOptions[b].order);
                for (let key of sortedBadges) {
                    if (!badges[key]) {
                        continue;
                    }
                    let o = badgeOptions[key];
                    let badge = new createjs.Text(String.fromCodePoint(badgeOptions[key].icon || icon), font, color);
                    badge['badgeType'] = key;
                    badge['badgeBounds'] = badge.getMetrics();

                    // No hit defined yet: create it, else use the cached one.
                    if (!hit) {
                        hit = new createjs.Shape();
                        hit.graphics.beginFill('green').drawRect(0, 0, badge.getMeasuredWidth(), badge.getMeasuredHeight());
                    }
                    badge.hitArea = hit;

                    // Positioning
                    badge.y = config.segment_title_offset_top + metrics.height + borderOffset;
                    let pos = o.alignLeft ? badgesLeft : badgesRight;
                    if (o.alignLeft) {
                        badge.x = config.segment_border_thickness + borderOffset + badgesLeft * (iconWidth + offsetX);
                        badgesLeft++;
                    }
                    else {
                        badge.x = config.segment_width - config.segment_border_thickness - borderOffset - iconWidth - badgesRight * (iconWidth + offsetX);
                        badgesRight++;
                    }

                    group.addChild(badge);
                }
            }
        }

        let offset = ((w - config.segment_border_thickness) / 2) + config.segment_border_thickness;
        group.cache(0 - offset, 0 - offset, width + 2 * offset, height + 2 * offset);

        group['limedsType'] = limeds.LimedsType.Segment;
        group.x = Math.round(options.x);
        group.y = Math.round(options.y);
        return group;
    }

    /**
     * Draw the selection brackets around the given segment. 
     */
    public drawSegmentSelection(segment: limeds.ISegment) {
        let config = this.config;
        let b = segment.displayObject.getBounds();
        let group = segment.displayObject as createjs.Container;
        let selection = new createjs.Shape;
        let g = selection.graphics;
        let offset = config.selection_brackets_offset;
        let size = config.selection_brackets_size;
        let t = config.selection_brackets_thickness;
        g.setStrokeStyle(config.selection_brackets_thickness);
        g.beginStroke(config.selection_brackets_color);
        g.moveTo(b.x + t, b.y + t + size).lineTo(b.x + t, b.y + t).lineTo(b.x + t + size, b.y + t);
        g.moveTo(b.x + b.width - t - size, b.y + t).lineTo(b.x + b.width - t, b.y + t).lineTo(b.x + b.width - t, b.y + t + size);
        g.moveTo(b.x + b.width - t, b.y + b.height - t - size).lineTo(b.x + b.width - t, b.y + b.height - t).lineTo(b.x + b.width - t - size, b.y + b.height - t);
        g.moveTo(b.x + t + size, b.y + b.height - t).lineTo(b.x + t, b.y + b.height - t).lineTo(b.x + t, b.y + b.height - t - size);
        group.addChild(selection);
        group.updateCache();
    }

    public toggleInfoLayer() {
        this.infoLayer = !this.infoLayer;
        this.segmentCache.clear();
    }

    private getClonedConnectorAt(x: number, y: number, color: string, location: 'north' | 'south' | 'west' | 'east', invisible: boolean): createjs.DisplayObject {
        let connTemplate = this.getCachedConnector(invisible ? null : color);
        let conn = connTemplate.clone();
        conn.x = x;
        conn.y = y;
        conn['location'] = location;
        conn['limedsConnector'] = true;
        return conn;
    }

    private getConnectorPoint(container: createjs.Container, location: 'north' | 'south' | 'east' | 'west'): createjs.Point {
        let p = new createjs.Point(0, 0);
        for (let i = 0; i < container.children.length; ++i) {
            let child = container.children[i];
            if (child['limedsConnector'] && child['location'] === location) {
                let x = child.x + this.config.connector_size / 2;
                let y = child.y + this.config.connector_size / 2;
                p.x = container.x + x;
                p.y = container.y + y;
                return p;
            }
        }
        return p;
    }

    private createConnectorGraphics(color: string): createjs.DisplayObject {
        let conn = new createjs.Shape();
        let w = this.config.connector_size;
        let h = this.config.connector_size;
        let border = this.config.connector_border_thickness;
        if (color !== 'read_only') {
            conn.graphics.beginFill(color);
            conn.graphics.setStrokeStyle(border, 'square').beginStroke(this.config.connector_border_color);
            conn.graphics.drawRect(0, 0, w, h);
        }
        conn.cache(0 - border, 0 - border, w + 2 * border, h + 2 * border);
        return conn;
    }

    private createSegmentGraphics(color: string): createjs.DisplayObject {
        let config = this.config;
        let width = config.segment_width;
        let height = config.segment_height;
        if (this.infoLayer) {
            height += config.segment_height / 3;
        }
        let base = new createjs.Shape();
        let border = config.connector_border_thickness;
        base['stroke'] = this.setupSegmentGraphics(base, color);
        base.graphics.drawRoundRect(0, 0, width, height, config.segment_corner_radius);
        base.cache(0 - border, 0 - border, width + 2 * border, height + 2 * border);
        return base;
    }

    private setupSegmentGraphics(node: createjs.Shape, color: string) {
        let g = node.graphics;
        let shadeLight = Util.shadeColor(color, 0.35);
        let shadeDark = Util.shadeColor(color, -0.05);
        let borderColor = Util.shadeColor(color, -0.125);
        g.beginLinearGradientFill([shadeLight, shadeDark], [0, 1], 0, 0, 0, 80);
        return g.setStrokeStyle(this.config.segment_border_thickness, 'round', 'round').beginStroke(borderColor).command;
    }

    private getBezierControlPoints(startDirection: 'north' | 'south' | 'east' | 'west', from: createjs.Point, to: createjs.Point): [createjs.Point, createjs.Point] {
        let cp1, cp2;
        let x = from.x;
        let y = from.y;
        let dx = to.x;
        let dy = to.y;
        switch (startDirection) {
            case 'north':
            case 'south':
                cp1 = new createjs.Point(x, ((dy - y) / 2) + y);
                cp2 = new createjs.Point(dx, ((dy - y) / 2) + y);
                break;
            case 'east':
            case 'west':
                cp1 = new createjs.Point(((dx - x) / 2) + x, y);
                cp2 = new createjs.Point(((dx - x) / 2) + x, dy);
                break;
        }
        return [cp1, cp2];
    }

    private getBezierPoints(from: limeds.ISegment, to: limeds.ISegment): BezierPoints {
        let startDirection = from.getView().getPositionTo(to.getView());
        let endDirection;
        switch (startDirection) {
            case 'north':
                endDirection = 'south';
                break;
            case 'south':
                endDirection = 'north';
                break;
            case 'west':
                endDirection = 'east';
                break;
            case 'east':
                endDirection = 'west';
                break;
        }
        let fromPoint = this.getConnectorPoint(from.getDisplayObject() as createjs.Container, startDirection);
        let toPoint = this.getConnectorPoint(to.getDisplayObject() as createjs.Container, endDirection);
        let cps = this.getBezierControlPoints(startDirection, fromPoint, toPoint);
        return {
            from: fromPoint,
            cp1: cps[0],
            cp2: cps[1],
            to: toPoint
        };
    }

    private getBezierPointsFrom(x: number, y: number, to: limeds.ISegment): BezierPoints {
        let fromPoint = new createjs.Point(x, y);
        let startDirection = LineUtils.getPositionToFrom(fromPoint, to.getView());
        let endDirection;
        switch (startDirection) {
            case 'north':
                endDirection = 'south';
                break;
            case 'south':
                endDirection = 'north';
                break;
            case 'west':
                endDirection = 'east';
                break;
            case 'east':
                endDirection = 'west';
                break;
        }
        let toPoint = this.getConnectorPoint(to.getDisplayObject() as createjs.Container, endDirection);
        let cps = this.getBezierControlPoints(startDirection, fromPoint, toPoint);
        return {
            from: fromPoint,
            cp1: cps[0],
            cp2: cps[1],
            to: toPoint
        };
    }

    private getBezierPointsTo(from: limeds.ISegment, x: number, y: number): BezierPoints {
        let toPoint = new createjs.Point(x, y);
        let startDirection = LineUtils.getPositionFromTo(from.getView(), toPoint);
        let endDirection;
        switch (startDirection) {
            case 'north':
                endDirection = 'south';
                break;
            case 'south':
                endDirection = 'north';
                break;
            case 'west':
                endDirection = 'east';
                break;
            case 'east':
                endDirection = 'west';
                break;
        }
        let fromPoint = this.getConnectorPoint(from.getDisplayObject() as createjs.Container, startDirection);
        let cps = this.getBezierControlPoints(startDirection, fromPoint, toPoint);
        return {
            from: fromPoint,
            cp1: cps[0],
            cp2: cps[1],
            to: toPoint
        };
    }

    private compensate(point: createjs.Point) {
        let config = this.config;
        let w = this.config.connector_size;
        let offset = ((w - config.segment_border_thickness) / 2) + config.segment_border_thickness;
        point.x += offset;
        point.y -= offset;
        return point;
    }



}

class LineUtils {

    /**
     * Gets a point for a given t on a Bezier curve.
     */
    static getBezierPoint(from, cp1, cp2, to, t) {
        var iT = 1 - t;
        var x = Math.pow(iT, 3) * from.x + 3 * Math.pow(iT, 2) * t * cp1.x + 3 * iT * Math.pow(t, 2) * cp2.x + Math.pow(t, 3) * to.x;
        var y = Math.pow(iT, 3) * from.y + 3 * Math.pow(iT, 2) * t * cp1.y + 3 * iT * Math.pow(t, 2) * cp2.y + Math.pow(t, 3) * to.y;
        return new createjs.Point(x, y);
    }

    static getLinePoint(start, stop, t): createjs.Point {
        let m = Math.atan2(start.y - stop.y, start.x - stop.x) + Math.PI;
        let length = Math.sqrt(Math.pow(stop.y - start.y, 2) + Math.pow(stop.x - start.x, 2)) * t;
        return new createjs.Point(Math.cos(m) * +length + start.x, Math.sin(m) * +length + start.y);
    }

    static getPerpendicularPointFromBezierPoint(from, cp1, cp2, to, t, length): createjs.Point {
        let origin = LineUtils.getBezierPoint(from, cp1, cp2, to, t);
        let tx = LineUtils.getTangent(from.x, cp1.x, cp2.x, to.x, t);
        let ty = LineUtils.getTangent(from.y, cp1.y, cp2.y, to.y, t);
        let m = Math.atan2(ty, tx) + (Math.PI / 2);
        return new createjs.Point(Math.cos(m) * -length + origin.x, Math.sin(m) * -length + origin.y);
    }

    static getDirectionalPointFromBezierPoint(from, cp1, cp2, to, t, length): createjs.Point {
        let origin = LineUtils.getBezierPoint(from, cp1, cp2, to, t);
        let tx = LineUtils.getTangent(from.x, cp1.x, cp2.x, to.x, t);
        let ty = LineUtils.getTangent(from.y, cp1.y, cp2.y, to.y, t);
        let m = Math.atan2(ty, tx) + Math.PI;
        return new createjs.Point(Math.cos(m) * -length + origin.x, Math.sin(m) * -length + origin.y);
    }

    static getDirectionalPointFromLinePoint(start, stop, point, length): createjs.Point {
        let m = Math.atan2(start.y - stop.y, start.x - stop.x) + Math.PI;
        return new createjs.Point(Math.cos(m) * +length + point.x, Math.sin(m) * +length + point.y);
    }

    static getPerpendicularPointFromLinePoint(start, stop, point, length): createjs.Point {
        let m = Math.atan2(start.y - stop.y, start.x - stop.x) + (Math.PI / 2);
        return new createjs.Point(Math.cos(m) * -length + point.x, Math.sin(m) * -length + point.y);
    }

    private static getTangent(a, b, c, d, t) {
        var adjustedT = 1 - t;
        return 3 * d * Math.pow(t, 2) -
            3 * c * Math.pow(t, 2) +
            6 * c * adjustedT * t -
            6 * b * adjustedT * t +
            3 * b * Math.pow(adjustedT, 2) -
            3 * a * Math.pow(adjustedT, 2);
    }

    static getPositionToFrom(start: createjs.Point, view: limeds.ISegmentView): ('north' | 'south' | 'east' | 'west') {
        let c1 = start;
        let c2 = view.getCenterPoint();
        let m = (c2.y - c1.y) / (c2.x - c1.x);

        let direction;
        if (isNaN(m)) {
            direction = c1.y < c2.y ? 'south' : 'north';
        }
        else if (m == 0) {
            direction = c1.x < c2.x ? 'east' : 'west';
        }
        else if (-1 < m && m < 1) {
            direction = c1.x < c2.x ? 'east' : 'west';
        }
        else if (m <= -1 || 1 <= m) {
            direction = c1.y < c2.y ? 'south' : 'north';
        }
        return direction;
    }

    static getPositionFromTo(view: limeds.ISegmentView, stop: createjs.Point): ('north' | 'south' | 'east' | 'west') {
        let c1 = view.getCenterPoint();
        let c2 = stop;
        let m = (c2.y - c1.y) / (c2.x - c1.x);

        let direction;
        if (isNaN(m)) {
            direction = c1.y < c2.y ? 'south' : 'north';
        }
        else if (m == 0) {
            direction = c1.x < c2.x ? 'east' : 'west';
        }
        else if (-1 < m && m < 1) {
            direction = c1.x < c2.x ? 'east' : 'west';
        }
        else if (m <= -1 || 1 <= m) {
            direction = c1.y < c2.y ? 'south' : 'north';
        }
        return direction;
    }
}

class Util {
    public static shadeColor(hex: string, lum: number): string {

        // validate hex string
        hex = String(hex).replace(/[^0-9a-f]/gi, '');
        if (hex.length < 6) {
            hex = hex[0] + hex[0] + hex[1] + hex[1] + hex[2] + hex[2];
        }
        lum = lum || 0;

        // convert to decimal and change luminosity
        var rgb = "#", c, i;
        for (i = 0; i < 3; i++) {
            c = parseInt(hex.substr(i * 2, 2), 16);
            c = Math.round(Math.min(Math.max(0, c + (c * lum)), 255)).toString(16);
            rgb += ("00" + c).substr(c.length);
        }

        return rgb;
    }

    public static getTextColor(hex: string) {
        // validate hex string
        hex = String(hex).replace(/[^0-9a-f]/gi, '');
        if (hex.length < 6) {
            hex = hex[0] + hex[0] + hex[1] + hex[1] + hex[2] + hex[2];
        }
        let total = 0;
        for (let i = 0; i < 3; i++) {
            total += parseInt(hex.substr(i * 2, 2), 16);
        }

        if (total < 382) {
            return '#ffffff';
        }
        else {
            return '#000000';
        }
    }
}