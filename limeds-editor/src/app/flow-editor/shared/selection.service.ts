import { Injectable } from '@angular/core';
import { Observable }  from 'rxjs/Observable';
import { Subject }  from 'rxjs/Subject';

import * as limeds from '../../shared/limeds';

@Injectable()
export class SelectionService {
    private selectionChangedSource = new Subject<Selected>();
    selectionChanged$: Observable<Selected> = this.selectionChangedSource.asObservable();

    private selectedId: string = null;
    private selectedType: limeds.LimedsType = limeds.LimedsType.None;

    /**
     * Change the current selection, and notify all subscribers
     */        
    changeSelection(id: string, type: limeds.LimedsType) {
        this.selectedId = id;
        this.selectedType = type;
        this.selectionChangedSource.next(this.getCurrentSelection());
    }
    
    /**
     * Clear the current selection
     */
    clearSelection() {
        this.selectedId = null;
        this.selectedType = limeds.LimedsType.None;
        this.selectionChangedSource.next(this.getCurrentSelection());
    }

    /**
     * Get the current selection
     */
    getCurrentSelection() {
        return { id: this.selectedId, type: this.selectedType };
    }
    
    /**
     * Returns whether a selection is active
     */
    isSelectionActive(): boolean {
        return !(this.selectedId === null && this.selectedType === limeds.LimedsType.None);
    }
}

export interface Selected {
    id: string;
    type: limeds.LimedsType;
} 