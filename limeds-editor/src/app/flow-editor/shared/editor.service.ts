import { Injectable } from '@angular/core';

import { CanvasService } from './canvas.service';
import { ConfigPanel } from '../../panels/components/config';
import { Registry } from './registry.service';
import { SelectionService } from './selection.service';
import { LimedsApi } from '../../shared/limeds-api.service';
import * as limeds from '../../shared/limeds';

@Injectable()
export class EditorService {
    private static counter: number = 0;

    constructor(
        private canvasService: CanvasService,
        private registry: Registry,
        private selection: SelectionService,
        private api: LimedsApi) { }

    init() {
        //this.canvasService.init(); 
    }

    public createSegment(id: string) {
        let loc = this.registry.getNewLocation();
        let coords = this.canvasService.screenToStage(loc[0], loc[1]);
        let label = this.registry.getLabelFromFQN(id);
        let view = new limeds.SegmentView(id, '${sliceVersion}', label, coords.x, coords.y, 120, 60, 'SLICE_LOCAL', '#8ED944');
        // make an empty model
        let segment = new limeds.Segment(view);
        this.registry.updateLastAdded(segment);
        this.registry.addSegment(segment);
        this.redraw();
    }

    /**
     * @param {string} id The id of the Segment to import
     * @param {string} label The label of the Segment to import
     * @param {string} versionExpression The version expression selected in the panel to import a Segment
     */
    public importSegment(id: string, label: string, versionExpression: string) {
        let loc = this.registry.getNewLocation();
        let coords = this.canvasService.screenToStage(loc[0], loc[1]);
        let view: limeds.ISegmentView = new limeds.SegmentView(id, versionExpression, label, coords.x, coords.y, 120, 60, 'IMPORTED', '#ffffff');
        // fetch model
        let segment = this.registry.findSegment(id);
        if (segment) {
            // segment already exists: overwriting
            // change version
            segment.getView().versionExpr = versionExpression;
            segment.getView().label = label;
            // update functionalTargets
            this.registry.listLinks(id)
                .map(id => this.registry.findLink(id).getView())
                .filter(link => link.toId === id || link.toIds.indexOf(id) > -1)
                .forEach(link => {
                    this.registry.findSegment(link.fromId).getModel().dependencies[link.fromVar].functionTargets[id] = versionExpression;
                });
            // Update model with newest found that matches version expression
            this.api.getSegment(id, versionExpression).subscribe(model => {
                segment.setModel(model);
                this.redraw();
            });
        }
        else {
            // segment is new: add like new
            this.api.getSegment(id, versionExpression).subscribe(
                model => {
                    let segment = new limeds.Segment(view, model);
                    this.registry.updateLastAdded(segment);
                    this.registry.addSegment(segment, true);
                    this.redraw();
                }
            )
        }
    }

    public instantiateFactory(factory: limeds.ISegmentModel, name: string, configuration: any[], dependencies: { [key: string]: limeds.Dependency }) {
        let loc = this.registry.getNewLocation();
        let coords = this.canvasService.screenToStage(loc[0], loc[1]);
        let instanceId = this.registry.getFQN(name);
        let view = new limeds.SegmentView(instanceId, '^'+factory.version, name, coords.x, coords.y, 120, 60, 'INSTANTIATED', '#ffaa00');
        let model: limeds.ISegmentModel = new limeds.SegmentModel(view.getId());
        for (let name in dependencies) {
            let dep = dependencies[name];
            model.dependencies[name] = {
                required: dep.required,
                collection: dep.collection,
                type: dep.type,
                functionTargets: {}
            };
        }

        model.templateInfo = <limeds.Template>{};
        model.templateInfo.type = 'FACTORY_INSTANCE';
        model.templateInfo.factoryId = factory.id;
        model.templateInfo.configuration = configuration;
        model.language = factory.language;
        model.version = factory.version;
        if (factory.documentation)
            model.documentation = factory.documentation;
        if (factory.httpDocumentation)
            model.httpDocumentation = factory.httpDocumentation;
        let segment = new limeds.Segment(view, model);
        this.registry.updateLastAdded(segment);
        this.registry.addSegment(segment, true);
        this.redraw();
    }

    public zoomIn() {
        this.canvasService.zoomIn();
    }

    public zoomOut() {
        this.canvasService.zoomOut();
    }

    public getZoomLevel() {
        return this.canvasService.getZoomLevel();
    }

    public getMouseLoc() {
        return this.canvasService.getMouseLoc();
    }

    public redraw() {
        this.canvasService.redrawRegistry();
    }

    public recacheSegment(segment: limeds.ISegment) {
        this.canvasService.drawSegment(segment);
    }

    public removeSelectedObject() {
        if (this.selection.isSelectionActive()) {
            let sel = this.selection.getCurrentSelection();
            switch (sel.type) {
                case limeds.LimedsType.Segment:
                    this.registry.delSegment(sel.id);
                    break;

                case limeds.LimedsType.Link:
                    this.registry.delLink(sel.id);
                    break;
            }
            this.selection.clearSelection();
        }
    }

    public clearSelection() {
        this.selection.clearSelection();
        this.canvasService.ctxMenu$.next({ show: false });
    }

    public toggleInfoLayer() {
        this.canvasService.toggleInfoLayer();
        this.redraw();
    }
}