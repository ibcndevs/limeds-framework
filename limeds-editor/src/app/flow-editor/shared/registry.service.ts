import { Inject, Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/combineLatest';

import { APP_CONFIG } from '../../shared/config';
import { LimedsApi } from '../../shared/limeds-api.service';
import * as limeds from '../../shared/limeds';

import { cloneDeep } from 'lodash';


@Injectable()
export class Registry {
    private slice: limeds.ISlice;
    private segments: Map<string, limeds.ISegment> = new Map<string, limeds.ISegment>();
    private links: Map<string, limeds.ILink> = new Map<string, limeds.ILink>();
    private displayObjects: Map<createjs.DisplayObject, string> = new Map<createjs.DisplayObject, string>();
    private scripts: Map<string, string> = new Map<string, string>();

    private lastAddedId: string = null;
    private lastAddedLoc: [number, number] = [100, 80];

    constructor(
        @Inject(APP_CONFIG) private config,
        private api: LimedsApi
    ) { }

    /**
     * Asynchronously load a slice in the registry, then use the callback function on the thisObject to call once finished.
     * This can be used to call a redraw on the canvas.
     * E.g. initWithSlice(slice, this.editor.redraw, this.editor);
     */
    public initWithSlice(slice: limeds.ISlice, callback: Function, thisObject: any): Observable<boolean> {
        return Observable.create(o => {
            this.slice = slice;
            let segments = new Map<string, limeds.ISegment>();

            if (slice.uiContext) {
                slice.uiContext.nodes.forEach(n => {
                    let view = new limeds.SegmentView(
                        n.id, n.versionExpr, n.label, n.x, n.y,
                        this.config.segment_width,
                        this.config.segment_height,
                        n.type,
                        n.color
                    );
                    segments.set(n.id, new limeds.Segment(view));
                });

                // Slice Local segments: fetch and set model
                slice.segments.forEach(model => {
                    segments.get(model.id).setModel(model);
                });

                // Segments from uiContext (all): find IMPORTED and fetch model from server
                // Add all segments to registry
                // let promises: Promise<limeds.ISegmentModel>[] = [];
                let obsArr: Observable<limeds.ISegmentModel>[] = [];
                segments.forEach(segment => {
                    if (segment.getView().type === 'IMPORTED') {
                        obsArr.push(this.api.getSegment(segment.getView().getId(), segment.getView().getVersionExpression()));
                    }
                    else {
                        obsArr.push(Observable.of(segment.model));
                    }
                });

                Observable.combineLatest(obsArr).subscribe(
                    models => {
                        models.forEach(model => segments.get(model.id).setModel(model));
                        segments.forEach(seg => this.addSegment(seg));

                        segments.clear();

                        slice.uiContext.links.forEach(l => {
                            let view = new limeds.LinkView(l.fromId, l.fromVar, l.toId, l.multi, l.toIds, l.t, l.x, l.y);
                            let link = new limeds.Link(view);
                            this.loadLink(link);
                        });

                        // CodeAttachments
                        for (let k in slice.codeAttachments) {
                            this.scripts.set(k, slice.codeAttachments[k]);
                        }

                        callback.apply(thisObject);
                        o.next(true);
                    },
                    error => {
                        console.log(error);
                        o.next(false)
                    }
                );
            }
            o.next(true);
        });
    }

    public getLoadedSlice() {
        return this.slice;
    }

    /**
     * Return the id to ISegment mapping.
     */
    public getSegments(): Map<string, limeds.ISegment> {
        return this.segments;
    }

    /**
    * Return the id to ILink mapping.
    */
    public getLinks(): Map<string, limeds.ILink> {
        return this.links;
    }

    /**
     * Return the id to createjs.DisplayObject mapping.
     */
    public getDisplayObjects(): Map<createjs.DisplayObject, string> {
        return this.displayObjects;
    }

    /**
     * Returns the code attachments for this slice.
     */
    public getScripts(): Map<string, string> {
        return this.scripts;
    }

    /**
     * Adds a Segment to the Registry by adding the view and the model and setting the codeAttachment.
     * imported won't ever add a codeAttachment
     */
    public addSegment(segment: limeds.ISegment, imported?: boolean) {
        // Set in segments map
        let id = segment.getView().getId();
        this.segments.set(id, segment);

        if (segment.getView().type === 'SLICE_LOCAL') {
            // Set scripts if not Java
            let lang = segment.getModel().language;
            if (!imported && 'java' !== lang.toLowerCase()) {
                let applyDoc = segment.getModel().documentation && segment.getModel().documentation['apply'];
                this.scripts.set(id, limeds.CodeTemplates.getFor(lang, 'single_component', applyDoc));
            }
        }
    }

    /**
     * Delete a segment from the Registry, by using its id. This will remove both the view and the model from the registry.
     * Any links connected to this segment will also be removed.
     * The bound codeAttachments will also be deleted.
     * 
     * @param id The unique id of this segment.
     */
    public delSegment(id: string) {
        let segment = this.findSegment(id);
        // Check all  links for incoming or outgoing connections
        this.links.forEach((link, linkId) => {
            let fromId = link.getView().fromId;
            let toId = link.getView().toId;
            let toIds = link.getView().toIds;
            // Outgoing or incoming link
            if (fromId === id || toId === id || toIds.indexOf(id) > -1) {
                this.delLink(link.getView().getId());
            }
        }, this);
        // Remove displayObject
        this.displayObjects.delete(segment.getDisplayObject());
        // remove segment       
        this.segments.delete(id);
        // remove bound codeAttachments!
        this.scripts.delete(id);
    }


    public updateSegment(segment: limeds.ISegment) {
        let id = segment.getView().getId();
        let segmentOriginal = this.findSegment(id);
        if (!segmentOriginal) {
            throw new Error('Segment with that id not found in registry: ' + segment.getView().getId());
        }
    }

    public loadLink(link: limeds.ILink) {
        let id = link.getView().getId();
        this.links.set(id, link);
    }

    /**
     * Resets the registry to a clean state
     */
    public reset() {
        this.segments.clear();
        this.links.clear();
        this.scripts.clear();
        this.displayObjects.clear();
        this.slice = null;
        this.lastAddedId = null;
        this.lastAddedLoc = [100, 80];
    }

    /**
     * Adds a new Link to the Registry by adding the view and the model. This link will be visually represented but will also be set as dependency on the model of the from-segment.
     * 
     * @param link - The link to add.
     * @param isOneToMany If true and dependency is already filled in, the functionTarget will be added to the existing ones. 
     */
    public addLink(link: limeds.ILink) {
        let id = link.getView().getId();
        this.links.set(id, link);
        let segment = this.findSegment(link.getView().fromId);

        if (segment.getModel()) {
            // First is dependency already specified?
            let dep: limeds.Dependency = segment.getModel().dependencies[link.getView().fromVar];
            if (!dep) {
                dep = {
                    required: true,
                    collection: false,
                    type: 'org.ibcn.limeds.FunctionalSegment',
                    functionTargets: {}
                };
                segment.getModel().dependencies[link.getView().fromVar] = dep;
            }

            if (link.getView().multi) {
                link.getView().toIds
                    .map(id => this.findSegment(id))
                    .forEach(toSegment => {
                        let versionExpr = toSegment.getView().getVersionExpression();
                        dep.functionTargets[toSegment.getId()] = versionExpr;
                    });
            } else {
                let toSegment = this.findSegment(link.getView().toId);
                let versionExpr = toSegment.getView().getVersionExpression();
                dep.functionTargets[link.getView().toId] = versionExpr;
            }
            dep.required = Object.keys(dep.functionTargets).length !== 0;
        }
    }

    public mergeLinkToMulti(links: limeds.ILink[], name: string) {
        let segment = this.findSegment(links[0].getView().fromId);
        let fromId = segment.getId();
        let toIds = [];
        links.forEach(link => {
            let v = link.getView();
            if (v.multi)
                v.toIds.forEach(id => toIds.push(id));
            else
                toIds.push(v.toId);
        });


        // Make new multi link
        let multiLinkView = new limeds.LinkView(fromId, name, "");
        let multiLink = new limeds.Link(multiLinkView);
        multiLink.getView().multi = true;
        multiLink.getView().toIds = toIds;
        multiLink.getView().t = this.config.link_multi_t;

        // Make new dependency
        let dep: limeds.Dependency = {
            required: true,
            collection: true,
            type: "org.ibcn.limeds.FunctionalSegment",
            functionTargets: {}
        }

        links.forEach(link => {
            // Add new functiontargets
            let versionExpr = this.findSegment(link.getView().toId).getView().getVersionExpression();
            dep.functionTargets[link.getView().toId] = versionExpr; // "${sliceVersion}";
            // Remove old dependencies
            delete segment.getModel().dependencies[link.getView().fromVar];
        });
        // Add new depedency
        segment.getModel().dependencies[name] = dep;

        // Remove old single links
        // this.findLinkBetween()
        links.forEach(link => this.links.delete(link.getView().getId()));

        // Add new multi link
        this.links.set(multiLink.getView().getId(), multiLink);
    }

    /**
     * Deletes a link form the Registry by using its unique id.
     * Only the link is deleted, that means the functionTarget on the 
     * dependency of the originating segment is removed, not the
     * dependency itself.
     * The Link object is also removed.
     * 
     * @param id The id of the LinkInterface that has to be deleted.
     */
    public delLink(id: string) {
        let link = this.findLink(id);
        // Remove functionTarget from dependency on from segment
        let segment = this.findSegment(link.getView().fromId);
        let dep = segment.getModel().dependencies[link.getView().fromVar];
        if (link.getView().multi) {
            dep.functionTargets = {};
        } else {
            delete dep.functionTargets[link.getView().toId];
        }
        dep.required = Object.keys(dep.functionTargets).length !== 0;
        // Remove DisplayObject
        this.displayObjects.delete(link.getDisplayObject());
        this.links.delete(id);
    }

    /**
     * Deletes the actua dependency.
     * The link itself is not deleted yet!!
     */
    public delDependency(link: limeds.ILink) {
        let segment = this.findSegment(link.getView().fromId);
        delete segment.getModel().dependencies[link.getView().fromVar];
    }


    /**
     * Finds the segment mapped to the given id.
     * 
     * @param id The id of the segment
     */
    public findSegment(id: string): limeds.ISegment {
        return this.segments.get(id);
    }

    /**
     * Finds the link from the given fromId of a segment to the given toId of a segment.
     * 
     * @param fromId The id of the segment that is the start of this link
     * @param toId The id of the segment that is the end of this link
     * @return The Link if found, null otherwise
     */
    public findLinkBetween(fromId: string, toId: string): limeds.ILink {
        return Array.from(this.links.values()).find(link => link.getView().fromId === fromId && link.getView().toId === toId);
    }

    /**
     * Finds the link from the given linkId
     * 
     * @param linkId The id of the link
     */
    public findLink(linkId: string): limeds.ILink {
        return this.links.get(linkId);
    }

    /**
     * Lists all the links that are attached to the given segment. The links will be incoming and outgoing.
     * 
     * @param id The id of the segment
     * @returns An array of all link ids
     */
    public listLinks(id: string): string[] {
        let model = this.findSegment(id).getModel();
        let linkIds = [];
        // List every link that is either leaving from or coming into the segment with the given id
        this.links.forEach((link, linkId) => {
            let view = link.getView();
            if (id === view.fromId || id === view.toId || view.toIds.indexOf(id) > -1) {
                linkIds.push(linkId);
            }
        });
        return linkIds;
    }

    /**
     * Disconnect the current displayObject if present before, connecting the given displayObject to the given ISliceElement. 
     * This also sets it in the display to id mapping.
     */
    public connectDisplayObject(displayObject: createjs.DisplayObject, element: limeds.ISliceElement) {
        let d = element.getDisplayObject();
        if (d) {
            d.stage && d.stage.removeChild(d);
            this.displayObjects.delete(d);
        }
        element.setDisplayObject(displayObject);
        this.displayObjects.set(displayObject, element.getView().getId());
    }

    /**
     * Sets the displayObject of the given ISliceElement to null.
     * This also removes it in the display to id mapping;
     * 
     * @return The released DisplayObject
     */
    public releaseDisplayObject(element: limeds.ISliceElement): createjs.DisplayObject {
        let d = element.getDisplayObject();
        d.stage && d.stage.removeChild(d);
        this.displayObjects.delete(d);
        element.setDisplayObject(null);
        return d;
    }

    /**
     * Serializes the current state of the registry as a slice object COPY.
     */
    public serialize(): any {
        let slice = cloneDeep(this.slice);

        // Clear all!
        slice.uiContext = {
            nodes: [],
            links: []
        };
        slice.segments = [];
        slice.codeAttachments = {};

        // Loop over all visuals nodes 
        this.segments.forEach((segment, id) => {
            // uiContext.nodes
            let view = segment.getView();
            slice.uiContext.nodes.push(view.toUiNode());

            // Write model to components if SLICE_LOCAL type
            if (view.type === 'SLICE_LOCAL' || view.type === 'INSTANTIATED') {
                let model: any = segment.getModel();
                slice.segments.push(Object.assign({},model));
            }
        });

        this.links.forEach((link, id) => {
            let view = link.getView();
            slice.uiContext.links.push(view.toUiLink());
        });

        this.scripts.forEach((val, key) => {
            slice.codeAttachments[key] = val;
        });
        return slice;
    }

    /**
     * Normalizes a given segment name, returning the fully qualified name.
     * Can only be used on segment components of the slice that is currently opened!
     */
    public getFQN(name: string) {
        let slice = this.slice.id;
        if (!name.startsWith(slice)) {
            name = slice + '.' + name;
        }
        return name;
    }

    /**
     * Returns a label from the fully qualified name.
     * If it starts with the id of the slice, it is subtracted from it.
     */
    public getLabelFromFQN(fqn: string) {
        let idx = fqn.lastIndexOf('.');
        if (idx > -1)
            return fqn.substring(fqn.lastIndexOf('.') + 1);
        else
            return fqn;
    }

    public updateLastAdded(segment: limeds.ISegment) {
        this.lastAddedId = segment.getId();
        this.lastAddedLoc = [this.lastAddedLoc[0] + 25, this.lastAddedLoc[1] + 50];
    }

    public resetLastAdded(id: string) {
        if (id === this.lastAddedId) {
            this.lastAddedLoc = [100, 100];
        }
    }

    public getNewLocation(): [number, number] {
        return this.lastAddedLoc;
    }

    /**
     * Rename a segment by cloning it, changing the clone, removing the original from 
     * the registry and adding the new one.
     * @param {limeds.ISegment} segment The original segment to rename
     * @param {string} newLabel The new label for this Segment
     */
    public renameSegment(segment: limeds.ISegment, newLabel: string) {
        switch (segment.getView().type) {
            // Rename the view, the id, and all functional targets to this segment.
            case 'SLICE_LOCAL':
            case 'INSTANTIATED':
                // Copy original
                let copy1 = cloneDeep(segment) as limeds.ISegment;
                let copyLinks1 = cloneDeep(this.listLinks(segment.getId()).map(id => this.findLink(id))) as limeds.ILink[];
                let oldId = segment.getId();
                let oldLabel = segment.getView().label;
                let newId = oldId.substring(0, oldId.lastIndexOf(oldLabel)) + newLabel;
                let oldScript = this.getScripts().get(oldId);
                // Delete original
                this.delSegment(oldId);
                // Build new
                copy1.getView().label = newLabel;
                copy1.getView().id = newId;
                copy1.getModel().id = newId;
                // Prepare links
                copyLinks1.forEach(link => {
                    if (link.getView().fromId === oldId) {
                        link.getView().fromId = newId
                    } else {
                        let idx = link.getView().toIds.indexOf(oldId);
                        if (link.getView().toId === oldId) {
                            link.getView().toId = newId
                        };
                        if (idx > -1) {
                            link.getView().toIds[idx] = newId;
                        };
                    }
                });
                // Add segment
                this.addSegment(copy1, false);
                // Add oldScript if there was any
                if (oldScript) {
                    this.getScripts().set(newId, oldScript);
                }
                // Add links
                copyLinks1.forEach(link => {
                    this.addLink(link);
                });
                break;

            case 'IMPORTED':
                // Copy original
                let copy2 = cloneDeep(segment) as limeds.ISegment;
                let copyLinks2 = cloneDeep(this.listLinks(segment.getId()).map(id => this.findLink(id))) as limeds.ILink[];
                // Delete original
                this.delSegment(segment.getId());
                // Build new
                copy2.getView().label = newLabel;
                // Add segment
                this.addSegment(copy2, true);
                // Add links
                copyLinks2.forEach(link => this.addLink(link));
                break;
        }
    }
}