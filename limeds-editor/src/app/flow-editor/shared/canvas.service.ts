import { Inject, Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';

import { APP_CONFIG } from '../../shared/config';
import { DrawService } from './draw.service';
import { Registry } from './registry.service';
import { SelectionService, Selected } from './selection.service';
import { Subscription } from 'rxjs/subscription';
import * as limeds from '../../shared/limeds';

@Injectable()
export class CanvasService {
    panelActions$: Subject<any> = new Subject<any>();
    editorTooltips$: Subject<any> = new Subject<any>();
    ctxMenu$: Subject<any> = new Subject<any>();

    private stage: createjs.Stage = undefined;
    private zoom: number = 1.0;
    private canvasEl: HTMLCanvasElement;
    private tempLine: any = null;

    private redraw: boolean = true;
    private redrawCheap: boolean = true;

    constructor(
        private registry: Registry,
        private drawService: DrawService,
        private selection: SelectionService,
        @Inject(APP_CONFIG) private C
    ) {
        this.selection.selectionChanged$.subscribe(change => this.onSelectionChanged(change));
    }

    private onSelectionChanged(change: Selected) {
        this.redrawRegistry();
    }

    public setStage(canvasEl: HTMLCanvasElement) {
        if (this.stage == undefined) {
            this.stage = new createjs.Stage("thecanvas");
        }
        this.stage.snapToPixelEnabled = true;
        this.canvasEl = canvasEl;
        // Scrollwheel zoom handler
        canvasEl.addEventListener('wheel', (e: any) => {
            if (e.deltaY > 0) {
                this.zoomOut();
            }
            else {
                this.zoomIn();
            }
        });
        this.stage.cursor = "pointer";
        this.stage.addEventListener('stagemousedown', (e: createjs.MouseEvent) => this.stageMouseHandler(e, canvasEl));

        createjs.Ticker.timingMode = createjs.Ticker.RAF_SYNCHED;
        createjs.Ticker.framerate = 48;
        createjs.Ticker.addEventListener('tick', this.tickerRedraw);

        this.stage.enableMouseOver(20);

        return this.stage;
    }

    /**
     * Resets the canvas service by unbinding the stage.
     */
    public reset() {
        this.stage.clear();
        this.stage = undefined;
    }

    public screenToStage(x: number, y: number): createjs.Point {
        return this.stage.globalToLocal(x, y);
    }

    public drawSegment(segment: limeds.ISegment) {
        let draw = this.drawService;
        let readOnly = segment.getView().type === 'IMPORTED';

        // badges
        let model = segment.getModel();
        let web = model.httpOperation && Object.keys(model.httpOperation).length > 0;
        let schedule = model.schedule && model.schedule !== '';
        let someInputValidated = false;//TODO model.inputDocumentation && model.inputDocumentation.some(inputDoc => inputDoc.validate);
        let validation = someInputValidated || false; //TODO (model.outputDocumentation && model.outputDocumentation.validate);
        let caching = model.cache && Object.keys(model.cache).length > 0;
        let subscriptions = model.subscriptions && model.subscriptions.length > 0;
        let badges = {
            web: web,
            schedule: schedule,
            validation: validation,
            caching: caching,
            subscriptions: subscriptions
        };

        // Base form
        let options = segment.getView();
        let group = draw.segment(options, readOnly, badges);

        // Add draggable handlers
        this.makeSegmentDraggable(group, segment, readOnly);

        // Update own displayObject
        this.registry.connectDisplayObject(group, segment);
    }


    // *********

    private drawLink(link: limeds.ILink, redraw?: boolean) {
        let line = redraw ? link.getDisplayObject() as createjs.Shape : new createjs.Shape();

        let fromSegment = this.registry.findSegment(link.getView().fromId);
        let toSegment = this.registry.findSegment(link.getView().toId);
        let selected = (this.selection.getCurrentSelection().id === link.getView().getId());

        let draw = this.drawService;
        if (!link.getView().multi)
            draw.link(line, fromSegment, toSegment, selected);
        else {
            let toSegments = link.getView().toIds.map(id => this.registry.findSegment(id));
            draw.multiLink(line, fromSegment, toSegments, link.getView().t, link.getView().x, link.getView().y, selected);
        }

        this.registry.connectDisplayObject(line, link);

        if (!redraw) {
            // Set limedsType
            line['limedsType'] = limeds.LimedsType.Link;
            if (!link.getView().multi)
                this.makeLinkSelectable(line);
            else
                this.makeMultiLinkSelectable(line)
            //this.stage.addChild(line);
        }
    }

    private redrawTempLink(line: createjs.Shape, from: createjs.Point, to: createjs.Point) {
        let draw = this.drawService;
        draw.tempLink(line, from, to);
        this.stage.removeChild(line);
        this.stage.addChild(line);
        this.redrawRegistry();
    }

    private makeLinkSelectable(node: createjs.DisplayObject) {
        let id = this.registry.getDisplayObjects().get(node);
        let link = this.registry.getLinks().get(id);
        if (!node.hasEventListener('mousedown')) {
            node.on('mousedown', (evt: createjs.MouseEvent) => {
                let moved = false;
                let selected = id === this.selection.getCurrentSelection().id;
                let mouse = this.stage.globalToLocal(evt.stageX, evt.stageY);
                this.createTempLine();
                let segmentFrom = this.registry.findSegment(link.getView().fromId);
                let segmentTo = this.registry.findSegment(link.getView().toId);
                let hitTest = this.drawService.linkMoveTargetHit(mouse, segmentFrom, segmentTo);
                node.on('pressmove', (moveEvent: createjs.MouseEvent) => {
                    if (!moved) {
                        let m2 = this.stage.globalToLocal(moveEvent.stageX, moveEvent.stageY);
                        moved = Math.abs(mouse.x - m2.x) > 2 || Math.abs(mouse.y - m2.y) > 2;
                    }
                    if (hitTest.hit) {
                        let to = this.stage.globalToLocal(moveEvent.stageX, moveEvent.stageY);
                        this.setTempLine(hitTest.from, to);
                        // hide main line
                        this.stage.removeChild(link.getDisplayObject());
                        this.redrawRegistry();
                    }

                });

                node.on('pressup', (moveEvent: createjs.MouseEvent) => {
                    this.clearTempLine();
                    if (!moved) {
                        // Normal click to select
                        this.selection.changeSelection(id, node['limedsType']);
                    }
                    else {
                        let loc = this.stage.globalToLocal(moveEvent.stageX, moveEvent.stageY);
                        let target = this.stage.getObjectsUnderPoint(loc.x, loc.y, 2);
                        if (hitTest.hit) {
                            let t = target[0];
                            let stopId = this.registry.getDisplayObjects().get(t);
                            while (t && !stopId) {
                                t = t.parent;
                                stopId = this.registry.getDisplayObjects().get(t);
                            }
                            if (stopId // a matching id is found in the registry
                                && stopId !== link.getView().fromId // it is not dropped on its own start
                                && node !== target[0] // it is not dropped on itself
                                && (target[0]['limedsType'] === limeds.LimedsType.Segment || target[0].parent['limedsType'] === limeds.LimedsType.Segment) // It is dropped on a segment
                            ) {

                                let createsCycle = this.registry.findLinkBetween(stopId, link.getView().fromId);
                                let alreadyExists = this.registry.findLinkBetween(link.getView().fromId, stopId);
                                if (!alreadyExists) {
                                    let info = {
                                        subject: 'link-drawn',
                                        fromId: link.getView().fromId,
                                        toId: stopId
                                    };
                                    if (!createsCycle) {
                                        info.subject = 'link-changed';
                                        info['linkId'] = link.getView().getId();
                                        info['fromVar'] = link.getView().fromVar;
                                    }
                                    // Prompt for variable name
                                    this.panelActions$.next(info);
                                }
                            }
                        }
                    }
                    this.redrawRegistry();
                    node.removeAllEventListeners('pressup');
                });
            });
        }
    }

    private makeMultiLinkSelectable(node: createjs.DisplayObject) {
        let id = this.registry.getDisplayObjects().get(node);
        let link = this.registry.getLinks().get(id);
        if (!node.hasEventListener('mousedown')) {
            node.on('mousedown', (evt: createjs.MouseEvent) => {
                let moved = false;
                let selected = id === this.selection.getCurrentSelection().id;
                let mouse = this.stage.globalToLocal(evt.stageX, evt.stageY);
                // this.createTempLine();
                let segmentFrom = this.registry.findSegment(link.getView().fromId);
                let segmentTo = this.registry.findSegment(link.getView().toId);
                let hitTest = this.drawService.multiLinkMultiplexHit(mouse, node['multiplexBounds']);
                node.on('pressmove', (moveEvent: createjs.MouseEvent) => {
                    if (!moved) {
                        let m2 = this.stage.globalToLocal(moveEvent.stageX, moveEvent.stageY);
                        moved = Math.abs(mouse.x - m2.x) > 2 || Math.abs(mouse.y - m2.y) > 2;
                    }
                    if (hitTest.hit) {
                        let to = this.stage.globalToLocal(moveEvent.stageX, moveEvent.stageY);
                        link.getView().t = -1;
                        link.getView().x = to.x;
                        link.getView().y = to.y;
                        this.redrawRegistry();
                    }

                });

                node.on('pressup', (moveEvent: createjs.MouseEvent) => {
                    if (!moved) {
                        // Normal click to select
                        if (evt.nativeEvent.button === 0) {
                            this.selection.changeSelection(id, node['limedsType']);
                        }
                        else if (evt.nativeEvent.button === 2) {
                            link.getView().t = this.C.link_multi_t;
                            this.redrawRegistry();
                        }
                    }
                    else {
                        this.redrawRegistry();
                    }
                    node.removeAllEventListeners('pressup');
                    node.removeAllEventListeners('pressmove');
                });


            });

        }
    }

    private makeSegmentDraggable(node: createjs.DisplayObject, segment: limeds.ISegment, readOnly?: boolean) {
        let offset = null;
        let exclude: createjs.Shape[] = [];
        let badges: createjs.Shape[] = [];
        (node as createjs.Container)
            .children
            .filter(c => c['limedsConnector'])
            .forEach((c) => {
                if (c['limedsConnector'])
                    exclude.push(c as createjs.Shape);
                if (c['badgeType'])
                    badges.push(c as createjs.Shape);
            });

        node.on('dblclick', (evt: createjs.MouseEvent) => {
            let readOnly = segment.getView().type === 'IMPORTED';
            let factoryInstance = segment.getModel().templateInfo && segment.getModel().templateInfo.type === 'FACTORY_INSTANCE';
            if (!readOnly && !factoryInstance) {
                // Open script panel
                let info = {
                    subject: 'script-panel'
                };
                this.panelActions$.next(info);
            }

        });

        node.on('mouseover', (evt: createjs.MouseEvent) => {
            if (evt.target['limedsConnector']) {
                this.stage.cursor = 'crosshair';
            }

            if (evt.target['badgeType']) {
                this.stage.cursor = 'help';
                let tooltip = {
                    content: '',
                    x: evt.nativeEvent.clientX,
                    y: evt.nativeEvent.clientY - evt.target.getBounds().height,
                    state: 'over'
                };
                let model = segment.getModel();
                switch (evt.target['badgeType']) {
                    case 'web':
                        tooltip.content = [model.httpOperation.method, model.httpOperation.path].join(' ');
                        break;
                    case 'schedule':
                        tooltip.content = model.schedule.slice(0, 1).toUpperCase() + model.schedule.slice(1);
                        break;
                    case 'subscriptions':
                        tooltip.content = ['Channels:', model.subscriptions.join(', ')].join(' ');
                        break;
                    case 'caching':
                        tooltip.content = [
                            'Cache(' + model.cache.size + ')',
                            'for',
                            model.cache.retentionDuration,
                            model.cache.retentionDurationUnit
                        ].join(' ');
                        break;
                    case 'validation':
                        let str = [];
                        let someInputValidated = false; //TODO model.inputDocumentation && model.inputDocumentation.some(item => item.validate);
                        let outputValidated =false; //TODO model.outputDocumentation && model.outputDocumentation.validate;
                        if (someInputValidated)
                            str.push('input');
                        if (someInputValidated && outputValidated)
                            str.push('&');
                        if (outputValidated)
                            str.push('output');
                        str.push('format validated');
                        tooltip.content = str.join(' ');
                        tooltip.content = tooltip.content.slice(0, 1).toUpperCase() + tooltip.content.slice(1);
                        break;
                }
                if (tooltip.content.length > 0) {
                    this.editorTooltips$.next(tooltip);
                }
            }
        });

        node.on('mouseout', (evt: createjs.MouseEvent) => {
            if (evt.target['limedsConnector']) {
                this.stage.cursor = 'pointer';
            }
            if (evt.target['badgeType']) {
                switch (evt.target['badgeType']) {
                    case 'web':
                    case 'schedule':
                    case 'subscriptions':
                    case 'caching':
                    case 'validation':
                    default:
                        this.stage.cursor = 'pointer';
                        this.editorTooltips$.next({
                            state: 'out'
                        })
                        break;
                }
            }
        });

        node.on('mousedown', (evt: createjs.MouseEvent) => {
            // Only left and right click registers
            if (evt.nativeEvent.button !== 0 && evt.nativeEvent.button !== 2) {
                return;
            }
            if (evt.nativeEvent.button === 2) {
                evt.preventDefault();
                evt.stopPropagation();
            }

            let mouseStill = true;
            let global = this.stage.localToGlobal(node.x, node.y);
            let groupId = this.registry.getDisplayObjects().get(node);
            node['offset'] = { 'x': Math.round(global.x - evt.stageX), 'y': Math.round(global.y - evt.stageY) };

            if (!readOnly && evt.nativeEvent.button === 0) {
                // if in exclude; stop
                for (let shape of exclude) {
                    let p = shape.globalToLocal(evt.stageX, evt.stageY);
                    let startId = this.registry.getDisplayObjects().get(node);
                    if (shape.hitTest(p.x, p.y)) {
                        this.createTempLine();
                        node.on('pressmove', (evt2: createjs.MouseEvent) => {
                            let from = this.stage.globalToLocal(evt.stageX, evt.stageY);
                            let to = this.stage.globalToLocal(evt2.stageX, evt2.stageY);
                            this.setTempLine(from, to);
                            this.redrawRegistryFor(null);
                        });
                        node.on('pressup', (evt3: createjs.MouseEvent) => {
                            this.clearTempLine();
                            let loc = this.stage.globalToLocal(evt3.stageX, evt3.stageY);
                            let target = this.stage.getObjectsUnderPoint(loc.x, loc.y, 2);
                            if (target.length !== 0 && node !== target[0].parent) {
                                let stopId = this.registry.getDisplayObjects().get(target[0].parent);
                                if (stopId) {
                                    // Prompt for variable name
                                    let info = {
                                        subject: 'link-drawn',
                                        fromId: startId,
                                        toId: stopId
                                    };
                                    this.panelActions$.next(info);
                                }
                                else {
                                    this.redrawRegistryFor(null);
                                }
                            }
                            else {
                                node.removeAllEventListeners('pressup');
                                node.removeAllEventListeners('pressmove');
                                this.redrawRegistryFor(null);
                            }
                        });

                        return;
                    }
                }
            }
            let segment = this.registry.getSegments().get(groupId);

            node.on('pressmove', (evt2: createjs.MouseEvent) => {
                if (mouseStill) {
                    let epsilon = 5;
                    mouseStill = Math.abs(evt.stageX - evt2.stageX) < epsilon && Math.abs(evt.stageY - evt2.stageY) < epsilon;
                    return;
                }
                if (evt.nativeEvent.button === 0) {

                    // Reset new location for new segments
                    this.registry.resetLastAdded(segment.getId());

                    // Translate display object
                    let displayObject = evt2.currentTarget;
                    let local = this.stage.globalToLocal(evt2.stageX + node['offset'].x, evt2.stageY + node['offset'].y);

                    // Move segment DisplayObject
                    segment.getView().x = Math.round(local.x);
                    segment.getView().y = Math.round(local.y);

                    // Update stage on each call
                    this.redrawRegistryFor(segment);
                }
                else if (evt.nativeEvent.button === 2) {
                    evt2.preventDefault();
                    evt2.stopPropagation();
                }
            });
            node.on('pressup', (evt3: createjs.MouseEvent) => {
                if (mouseStill) {
                    // Handle normal click
                    this.selection.changeSelection(groupId, node['limedsType']);


                    // On right click
                    if (evt.nativeEvent.button === 2) {
                        evt3.preventDefault();
                        evt3.stopPropagation();
                        this.ctxMenu$.next({
                            show: true,
                            x: evt3.nativeEvent.clientX,
                            y: evt3.nativeEvent.clientY
                        });
                    }
                }
                else {
                    if (evt.nativeEvent.button === 0) {
                        // Handle up from move
                        // Update segment with final location
                        let local = this.stage.globalToLocal(evt3.stageX + node['offset'].x, evt3.stageY + node['offset'].y);
                        segment.getView().x = Math.round(local.x);
                        segment.getView().y = Math.round(local.y);

                        // Disconnect the previous displayObject
                        this.registry.releaseDisplayObject(this.registry.findSegment(groupId));
                        this.redrawRegistry();
                    }
                }

                node.removeAllEventListeners('pressmove');
                node.removeAllEventListeners('pressup');
            });
        });

    }

    public toggleInfoLayer() {
        this.drawService.toggleInfoLayer();
    }

    /* Scaling and zooming */

    private scaleStage(position: createjs.Point) {
        let newPos = new createjs.Point(position.x * this.zoom, position.y * this.zoom);
        position = this.stage.localToGlobal(position.x, position.y);
        let diff = new createjs.Point(Math.round(position.x - newPos.x), Math.round(position.y - newPos.y));
        this.stage.setTransform(diff.x, diff.y, this.zoom, this.zoom);
        this.redrawRegistryFor(null);
    }

    public zoomIn() {
        let z = this.zoom + this.C.zoom_step;
        if (z <= this.C.zoom_max) {
            this.zoom = z;
            this.scaleStage(this.getMouseLoc());
        }
    }

    public zoomOut() {
        let z = this.zoom - this.C.zoom_step;
        if (z > this.C.zoom_min) {
            this.zoom = z;
            this.scaleStage(this.getMouseLoc());
        }
    }

    public resetZoom() {
        this.zoom = 1.0;
        this.scaleStage(this.getMouseLoc());
    }

    public getZoomLevel() {
        return this.zoom;
    }

    public getMouseLoc() {
        let m = { x: this.stage.mouseX, y: this.stage.mouseY };
        return this.stage.globalToLocal(m.x, m.y);
    }

    public drawSelected() {
        if (this.selection.isSelectionActive()) {
            let sel = this.selection.getCurrentSelection();

            switch (sel.type) {
                case limeds.LimedsType.Segment:
                    let segment = this.registry.findSegment(sel.id);
                    if (!segment) {
                        this.selection.clearSelection();
                        return;
                    }

                    this.drawService.drawSegmentSelection(segment);
                    break;

                case limeds.LimedsType.Link:
                    let link = this.registry.findLink(sel.id);
                    if (!link) {
                        this.selection.clearSelection();
                        return;
                    }
                    let line = link.getDisplayObject() as createjs.Shape;
                    let gg = line.graphics;
                    line['stroke'].width = this.C.link_selected_thickness;
                    break;

                default:
                    this.selection.clearSelection();
                    break;
            }
        }
    }

    private createTempLine() {
        this.tempLine = {
            line: new createjs.Shape()
        };
    }

    private setTempLine(from: createjs.Point, to: createjs.Point) {
        if (this.tempLine) {
            this.tempLine.from = from;
            this.tempLine.to = to;
        }
    }

    private clearTempLine() {
        if (this.tempLine && this.tempLine.line) {
            this.stage.removeChild(this.tempLine.line);
        }
        this.tempLine = null;
    }

    private drawTempLink() {
        if (this.tempLine !== null && this.tempLine.from && this.tempLine.to && this.tempLine.line) {
            let draw = this.drawService;
            draw.tempLink(this.tempLine.line, this.tempLine.from, this.tempLine.to);
            this.stage.removeChild(this.tempLine.line);
            this.stage.addChild(this.tempLine.line);
        }
    }

    private repaint() {
        let r = this.registry;
        r.getLinks().forEach((link, id) => {
            this.stage.addChild(link.getDisplayObject());
        });

        r.getSegments().forEach((segment, id) => {
            this.stage.addChild(segment.getDisplayObject());
        });
    }

    /**
     * The actual rendering method, this is a handler being called by the tick event.
     */
    private tickerRedraw = () => {
        if (this.redraw || this.redrawCheap) {
            // Skip this on cheap redraws
            if (!this.redrawCheap) {
                if (this.C.debug) { console.log('redraw') }

                // Remove all display children from the stage
                this.stage.removeAllChildren();

                // Redraw the complete registry
                let r = this.registry;

                r.getSegments().forEach((segment, id) => {
                    delete segment.displayObject;
                    this.drawSegment(segment);
                });

                r.getLinks().forEach((link, id) => {
                    delete link.displayObject;
                    this.drawLink(link);
                });
            } else if (this.C.debug) { console.log('cheap redraw') }

            this.repaint();

            this.drawSelected();
            this.drawTempLink();

            // Update the stage with the new drawing operations
            this.stage.update();

            // reset redraw booleans
            this.redraw = false;
            this.redrawCheap = false;
        }
    }

    /**
     * Remove all children from the stage and redraw based on the contents of the Registry service.
     */
    public redrawRegistry() {
        this.redraw = true;
        this.redrawCheap = false;
    }

    /**
     * Redraw all DisplayObjects connected to the given ISegment.
     * This includes attached links.
     * 
     * @param segment The segment to redraw together with the connected links.
     */
    public redrawRegistryFor(segment: limeds.ISegment) {
        // Already full redraw: cheap is false, no full yet: cheap is true
        this.redrawCheap = !this.redraw;

        if (this.redrawCheap && segment !== null) {
            // Redraw links
            this.registry.listLinks(segment.getView().getId())
                .map(id => this.registry.findLink(id), this)
                .forEach(link => this.drawLink(link, true));

            // Redraw segment
            let segmentView = segment.getView();
            let segmentDisplay = segment.getDisplayObject();
            segmentDisplay.setTransform(segmentView.x, segmentView.y);
        }

    }


    /********* HANDLERS  ***********/

    private stageMouseHandler = (e: createjs.MouseEvent, canvasEl: HTMLCanvasElement) => {
        let mouseStill = true;
        switch (e.nativeEvent.button) {
            case 0: // left click
                let m = this.getMouseLoc();
                let onCanvas = !this.stage.getObjectUnderPoint(m.x, m.y, 0);
                if (onCanvas) {
                    let bounds = this.stage.getBounds();
                    if (bounds) {
                        let offset = this.C.selection_brackets_offset * 2
                        this.stage.cache(bounds.x - offset, bounds.y - offset, bounds.width + 2 * offset, bounds.height + 2 * offset, 1);
                    }
                    let offset = {
                        x: this.stage.x - e.stageX,
                        y: this.stage.y - e.stageY
                    };

                    this.stage.addEventListener('stagemousemove', (e2: createjs.MouseEvent) => {
                        if (mouseStill) {
                            mouseStill = e.stageX === e2.stageX && e.stageY === e2.stageY;
                        }
                        this.stage.setTransform(Math.round(e2.stageX + offset.x), Math.round(e2.stageY + offset.y), this.zoom, this.zoom);
                        this.redrawRegistryFor(null);
                    });

                    this.stage.addEventListener('stagemouseup', (e3: createjs.MouseEvent) => {
                        // Handle normal click
                        if (mouseStill && onCanvas) {
                            if (this.selection.isSelectionActive()) {
                                this.selection.clearSelection();
                            }
                        }

                        onCanvas = false;
                        this.stage.removeAllEventListeners('stagemousemove');
                        this.stage.removeAllEventListeners('stagemouseup');
                        let translate = new createjs.Point(Math.round(e3.stageX + offset.x), Math.round(e3.stageY + offset.y));
                        this.stage.setTransform(0, 0, this.zoom, this.zoom);
                        this.stage.uncache();

                        // Translate all objects
                        let p = this.stage.globalToLocal(translate.x, translate.y);
                        this.registry.getSegments().forEach(segment => {
                            segment.getView().x += p.x;
                            segment.getView().y += p.y;
                        });
                        this.registry.getLinks().forEach(link => {
                            if (link.getView().multi && link.getView().t < 0) {
                                link.getView().x += p.x;
                                link.getView().y += p.y;
                            }
                        });

                        this.redrawRegistry();
                    });
                }
                break;

            case 1: // mousewheel click
                this.resetZoom();
                break;

            case 2: // right click

                break;

        }
    }
}