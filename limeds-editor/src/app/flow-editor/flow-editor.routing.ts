import { Routes, RouterModule } from '@angular/router';

import { FlowEditor } from './flow-editor.component';

import {CanDeactivateGuard} from '../shared/can-deactivate-guard.service';

const flowEditorRoutes: Routes = [
    { path: 'view/:id', component: FlowEditor, canDeactivate: [CanDeactivateGuard] },
    { path: 'view', redirectTo: '/slices', pathMatch: 'full'}, 
];

export const flowEditorRouting = RouterModule.forChild(flowEditorRoutes);