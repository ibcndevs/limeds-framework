import { NgModule } from '@angular/core';

import { PanelsModule } from '../panels/panels.module';
import { SharedModule } from '../shared/shared.module';

import { FlowEditor } from './flow-editor.component';
import { FlowCanvas } from './flow-canvas/flow-canvas.component';
import { Sidebar } from './sidebar/sidebar.component';
import { SliceInfo } from './slice-info/slice-info.component';
import { Statusbar } from './statusbar/statusbar.component';
import { Topbar } from '../shared/topbar/topbar.component';
import { ToolbarButton } from './sidebar/toolbar-btn/toolbar-btn.component';
import { MyTooltipDirective } from './sidebar/tooltip.directive';
import { SegmentContextMenu } from './segment-ctx-menu.component';

import { flowEditorRouting } from './flow-editor.routing';

import {
    CanvasService,
    DrawService,
    EditorService,
    Registry,
    SelectionService
}  from './shared';

@NgModule({
    imports: [
        SharedModule,
        PanelsModule,
        flowEditorRouting
    ],
    declarations: [
        FlowEditor,
        Sidebar,
        FlowCanvas,
        SliceInfo,
        Statusbar,
        ToolbarButton,
        MyTooltipDirective,
        SegmentContextMenu
    ],
    entryComponents: [
        SegmentContextMenu
    ],
    providers: [
        CanvasService,
        DrawService,
        EditorService,
        Registry,
        SelectionService
    ]
})
export class FlowEditorModule { }