import { Component, Input } from '@angular/core';

@Component({
    selector: 'toolbar-btn',
    templateUrl: './toolbar-btn.component.html',
    styleUrls: ['./toolbar-btn.component.theme-chrome.css']
})
export class ToolbarButton {
    @Input() disabled: boolean;
    @Input() large: boolean;    
    @Input() action: Function;
    
    doAction() {
        this.action();
    }
}