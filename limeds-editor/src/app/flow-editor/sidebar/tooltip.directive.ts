import { Directive, ElementRef, HostListener, Input, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/platform-browser';

@Directive({
    selector: '[myTooltip]'
})
export class MyTooltipDirective {
    @Input('myTooltip') text: string;

    private _defaultColor = 'red';
    private span: HTMLSpanElement;

    constructor(
        private elementRef: ElementRef,
        @Inject(DOCUMENT) private document) {
    }

    @HostListener('mouseenter')
    onMouseEnter() {
        let el = this.elementRef.nativeElement;
        this.span = this.document.createElement('span');
        // Set class first so that it becomes an non-inline object
        this.span.className = "my-tooltip";

        // Append to the body
        this.document.body.appendChild(this.span);

        // Set style attributes and text
        let offset = (el.firstChild.offsetHeight - this.span.offsetHeight) / 2;
        let top = this.getOffset(el).top + offset;
        let left = 75;
        this.span.style.top = top + 'px';
        this.span.style.left = left + 'px';
        this.span.innerHTML = this.text;
    }

    @HostListener('mouseleave')
    onMouseLeave() {
        this.span.remove();
    }

    private getOffset(el) {
        var _x = 0;
        var _y = 0;
        while (el && !isNaN(el.offsetLeft) && !isNaN(el.offsetTop)) {
            _x += el.offsetLeft - el.scrollLeft;
            _y += el.offsetTop - el.scrollTop;
            el = el.offsetParent;
        }
        return { top: _y, left: _x };
    }


}