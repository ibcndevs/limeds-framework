import { Component, EventEmitter, Inject, OnInit, Output, Renderer } from '@angular/core';

import { APP_CONFIG } from '../../shared/config';
import { EditorService } from '../shared/editor.service';
import { Registry } from '../shared/registry.service';
import { SelectionService } from '../shared/selection.service';
import * as limeds from '../../shared/limeds';

@Component({
    selector: 'sidebar',
    templateUrl: './sidebar.component.html',
    styleUrls: ['./sidebar.component.theme-chrome.css']
})
export class Sidebar implements OnInit {
    @Output('toolbar-btn') toolbarBtn = new EventEmitter<string>();

    infoLayer: boolean = false;
    empty: boolean = true;
    factoryInstance: boolean = false;
    link: boolean = false;
    readOnly: boolean = false;


    constructor(
        @Inject(APP_CONFIG) private config,
        private editor: EditorService,
        private registry: Registry,
        private selection: SelectionService,
        private renderer: Renderer) { }

    ngOnInit() {
        this.selection.selectionChanged$.subscribe(result => {
            switch (result.type) {
                case limeds.LimedsType.Segment:
                    let segment = this.registry.findSegment(result.id);
                    this.empty = false;
                    this.factoryInstance = segment.getModel().templateInfo && segment.getModel().templateInfo.type == "FACTORY_INSTANCE";
                    this.link = false
                    this.readOnly = segment.getView().type === 'IMPORTED';
                    break;

                case limeds.LimedsType.Link:
                    this.empty = false;
                    this.factoryInstance = false;
                    this.link = true;
                    this.readOnly = false;
                    break;

                case limeds.LimedsType.None:
                default:
                    this.empty = true;
                    this.factoryInstance = false;
                    this.link = false;
                    this.readOnly = false;
            }
        })
    }

    onImportSegment = () => {
        this.toolbarBtn.emit('import-segment');
    }

    onCreateSegment = () => {
        this.toolbarBtn.emit('create-segment');
    }

    onRemoveObject = () => {
        this.editor.removeSelectedObject();
    }

    onInfoLayer = () => {
        this.infoLayer = !this.infoLayer;
        this.toolbarBtn.emit('toggle-info-layer');
    }

    onTypeEditor = () => {
        this.toolbarBtn.emit('type-editor');
    }

    onGeneralConfig = () => {
        this.toolbarBtn.emit('config-panel');
    }

    onAssignEntryPoint = () => {
        this.toolbarBtn.emit('web-api');
    }

    onConfigureProperties = () => {
        this.toolbarBtn.emit('edit-configuration');
    }

    onConfigureSchemas = () => {
        this.toolbarBtn.emit('config-schemas');
    }

    onDocumentationView = () => {
        this.toolbarBtn.emit('view-doc');
    }

    onConfigureAspects = () => {
        this.toolbarBtn.emit('adv-options');
    }

    onScriptPanel = () => {
        this.toolbarBtn.emit('script-panel');
    }

    public onButton10 = () => {
        this.config.debug = !this.config.debug;
    }

    public onChangeColor = () => {
        this.toolbarBtn.emit('color');
    }

    blurAfterAction(evt: MouseEvent) {
        if (evt.eventPhase === evt.BUBBLING_PHASE) {
            let target = evt.target as HTMLElement;
            while (target != null && !(target instanceof HTMLButtonElement)) {
                target = target.parentElement;
            }
            if (target) {
                this.renderer.invokeElementMethod(target, 'blur');
            }
        }
    }
}