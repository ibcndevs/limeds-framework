import { Injectable, Compiler, ComponentRef, ComponentFactory, Injector, ViewContainerRef, Type, ComponentFactoryResolver} from '@angular/core';
import { Subject } from 'rxjs/Subject';

import {
    AddLinkPanel,
    AddPropertyPanel,
    AdvOptionsPanel,
    AreYouSurePanel,
    BasePanel,
    ColorPanel,
    ConfigPanel,
    DocumentationPanel,
    CreateSlicePanel,
    CreateSegmentPanel,
    EditConfigurationPanel,
    EditSlicePanel,
    FactoryInstancePanel,
    ImportSegmentPanel,
    MultiLinkPanel,
    PromptPanel,
    RawCodePanel,
    SaveAsPanel,
    ScriptPanel,
    SetupQueryPanel,
    TypePickerPanel,
    TyperefPanel,
    TypesPanel,
    VarRenamePanel,
    ViewErrorPanel,
    ViewDocumentationPanel,
    ViewInstallablePanel,
    WebApiPanel
} from './components';

import { PanelsModule } from './panels.module';

import { KeybindingService } from '../shared/keybinding.service';

export enum PanelType {
    ADD_LINK,
    ADV_OPTIONS,
    ARE_YOU_SURE,
    COLOR,
    CONFIG,
    CREATE_SLICE,
    CREATE_SEGMENT,
    DOCUMENTATION,
    EDIT_CONFIGURATION,
    EDIT_CONFIGURATION_ADD_PROPERTY,
    EDIT_SLICE,
    FACTORY_INSTANCE,
    IMPORT_SEGMENT,
    MULTI_LINK,
    PROMPT,
    RAW_CODE,
    SAVE_AS,
    SCRIPT,
    SETUP_QUERY,
    TYPE_PICKER,
    TYPEREF,
    TYPES,
    VAR_RENAME,
    VIEW_ERROR,
    VIEW_DOCUMENTATION,
    VIEW_INSTALLABLE,
    WEB_API
}

export enum PanelAction {
    CONFIRM,
    CANCEL
}

export class PanelResult {
    constructor(
        public type: PanelType,
        public action: PanelAction,
        public result: any
    ) { }
}

@Injectable()
export class PanelManager {
    private refMap: Map<PanelType, ComponentRef<any>> = new Map<PanelType, ComponentRef<any>>();
    private factoryMap: Map<PanelType, ComponentFactory<any>>;
    private keybindingMap: Map<PanelType, any> = new Map<PanelType, any>();
    private subjectMap: Map<PanelType, Subject<PanelResult>> = new Map<PanelType, Subject<PanelResult>>();


    constructor(
        private keybinding: KeybindingService,
        private injector: Injector,
        private resolver: ComponentFactoryResolver,
        private compiler: Compiler) {
        this.init();
    }

    /**
     * Creates a new panel.
     * 
     * @param panel The identifier of the panel as configured in the PanelManager
     * @param viewContainerRef The viewContainer to add this panel to in the DOM tree
     * @param saveKeybindings Keeps a copy of the current keybindings and restores them on destruction of the panel
     */
    createPanel(panel: PanelType, viewContainerRef: ViewContainerRef, saveKeybindings: boolean, config?: any): Subject<PanelResult> {
        // Set up subject for results
        let sub = new Subject<PanelResult>();
        this.subjectMap.set(panel, sub);

        let factory = this.factoryMap.get(panel);
        if (saveKeybindings) {
            this.keybindingMap.set(panel, this.keybinding.getKeymapCopy());
        }
        let ref = viewContainerRef.createComponent(factory, 0, viewContainerRef.injector);
        this.refMap.set(panel, ref);
        let p: BasePanel = ref.instance;
        if (config) {
            p.setConfig(config);
        }
        p.closeSubject$.subscribe(this.onCloseEvent);

        return sub;
    }

    /**
     * Destroys the specified panel and restorse keybindings if saveKeybindings was true.
     * 
     * @param panel The identifier of the panel to destroy
     */
    destroyPanel(panel: PanelType) {
        this.refMap.get(panel).destroy();
        if (this.keybindingMap.has(panel)) {
            this.keybinding.setKeymap(this.keybindingMap.get(panel));
            this.keybindingMap.delete(panel);
        }

        // Cleanup
        this.refMap.delete(panel);
    }

    private onCloseEvent = (panelResult: PanelResult) => {
        // Rebroadcast event to subscribers of subject
        this.subjectMap.get(panelResult.type).next(panelResult);
        // Destroy panel afterwards.
        this.destroyPanel(panelResult.type);
    }

    private init() {
        this.factoryMap = new Map<PanelType, ComponentFactory<any>>();
        // let resolver = this.compiler.compileModuleSync(PanelsModule).create(this.injector).componentFactoryResolver;
        // resolver.resolveComponentFactory(AddLinkPanel);

        // this.resolver.resolveComponentFactory(AddLinkPanel)

        this.factoryMap.set(PanelType.ADD_LINK, this.resolver.resolveComponentFactory(AddLinkPanel));
        this.factoryMap.set(PanelType.ADV_OPTIONS, this.resolver.resolveComponentFactory(AdvOptionsPanel));
        this.factoryMap.set(PanelType.ARE_YOU_SURE, this.resolver.resolveComponentFactory(AreYouSurePanel));
        this.factoryMap.set(PanelType.COLOR, this.resolver.resolveComponentFactory(ColorPanel));
        this.factoryMap.set(PanelType.CONFIG, this.resolver.resolveComponentFactory(ConfigPanel));
        this.factoryMap.set(PanelType.CREATE_SLICE, this.resolver.resolveComponentFactory(CreateSlicePanel));
        this.factoryMap.set(PanelType.CREATE_SEGMENT, this.resolver.resolveComponentFactory(CreateSegmentPanel));
        this.factoryMap.set(PanelType.DOCUMENTATION, this.resolver.resolveComponentFactory(DocumentationPanel));
        this.factoryMap.set(PanelType.EDIT_SLICE, this.resolver.resolveComponentFactory(EditSlicePanel));
        this.factoryMap.set(PanelType.EDIT_CONFIGURATION, this.resolver.resolveComponentFactory(EditConfigurationPanel));
        this.factoryMap.set(PanelType.EDIT_CONFIGURATION_ADD_PROPERTY, this.resolver.resolveComponentFactory(AddPropertyPanel));
        this.factoryMap.set(PanelType.FACTORY_INSTANCE, this.resolver.resolveComponentFactory(FactoryInstancePanel));
        this.factoryMap.set(PanelType.IMPORT_SEGMENT, this.resolver.resolveComponentFactory(ImportSegmentPanel));
        this.factoryMap.set(PanelType.MULTI_LINK, this.resolver.resolveComponentFactory(MultiLinkPanel));
        this.factoryMap.set(PanelType.PROMPT, this.resolver.resolveComponentFactory(PromptPanel));
        this.factoryMap.set(PanelType.RAW_CODE, this.resolver.resolveComponentFactory(RawCodePanel));
        this.factoryMap.set(PanelType.SAVE_AS, this.resolver.resolveComponentFactory(SaveAsPanel));
        this.factoryMap.set(PanelType.SCRIPT, this.resolver.resolveComponentFactory(ScriptPanel));
        this.factoryMap.set(PanelType.SETUP_QUERY, this.resolver.resolveComponentFactory(SetupQueryPanel));
        this.factoryMap.set(PanelType.TYPE_PICKER, this.resolver.resolveComponentFactory(TypePickerPanel));
        this.factoryMap.set(PanelType.TYPEREF, this.resolver.resolveComponentFactory(TyperefPanel));
        this.factoryMap.set(PanelType.TYPES, this.resolver.resolveComponentFactory(TypesPanel));
        this.factoryMap.set(PanelType.VAR_RENAME, this.resolver.resolveComponentFactory(VarRenamePanel));
        this.factoryMap.set(PanelType.VIEW_ERROR, this.resolver.resolveComponentFactory(ViewErrorPanel));
        this.factoryMap.set(PanelType.VIEW_DOCUMENTATION, this.resolver.resolveComponentFactory(ViewDocumentationPanel));
        this.factoryMap.set(PanelType.VIEW_INSTALLABLE, this.resolver.resolveComponentFactory(ViewInstallablePanel));
        this.factoryMap.set(PanelType.WEB_API, this.resolver.resolveComponentFactory(WebApiPanel));
    }
}