import { Component, OnInit } from '@angular/core';

import { BasePanel } from '../base-panel';
import { KeybindingService, Key, KeyCombo, Action } from '../../../shared/keybinding.service';
import { LimedsApi } from '../../../shared/limeds-api.service';
import { PanelType } from '../../panel-manager.service';
import * as limeds from '../../../shared/limeds';

@Component({
    selector: 'lds-error-panel',
    templateUrl: './view-error.panel.html',
    styleUrls: ['./view-error.panel.css']
})
export class ViewErrorPanel extends BasePanel implements OnInit {
    issue: limeds.Issue;

    constructor(
        private api: LimedsApi,
        private keybindings: KeybindingService) {
        super(PanelType.VIEW_ERROR);
    }

    ngOnInit() {
        this.keybindings.clearKeymap();
        this.keybindings.setKeyBinding(new KeyCombo(Key.KEY_ESC), new Action(this, this.cancelPanel));
    }

    onSubmit() {
        this.submitPanel({});
    }

    onCancel() {
        this.cancelPanel();
    }

    setConfig(config: any) {
        this.issue = config.issue;
    }

}