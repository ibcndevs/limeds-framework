import { Component, AfterViewInit, Input, OnInit, ViewContainerRef, ComponentFactoryResolver } from '@angular/core';
import { AbstractControl, FormGroup, FormControl, Validators } from '@angular/forms';

import { BasePanel } from '../base-panel';
import { PanelManager, PanelType, PanelAction } from '../../panel-manager.service';
import { TypePickerPanel } from '../type-picker';
import { VarRenamePanel } from '../var-rename';
import { KeybindingService, Key, KeyCombo, Action } from '../../../shared/keybinding.service';
import { Registry } from '../../../flow-editor/shared/registry.service';
import { Help } from '../../../shared/help';
import { LimedsApi } from '../../../shared/limeds-api.service';
import * as limeds from '../../../shared/limeds';

import * as ace from 'brace';
import 'brace/mode/json';
import 'brace/theme/dawn';
import 'brace/theme/solarized_light'

import * as beautify from 'js-beautify'
import { cloneDeep } from 'lodash';

interface View {
    index?: number,
    label: string,
    schema: any,
    preview: boolean,
    schemaResolved?: any
};

@Component({
    selector: 'lds-documentation-panel',
    templateUrl: './documentation.panel.html',
    styleUrls: ['./documentation.panel.css']
})
export class DocumentationPanel extends BasePanel implements OnInit, AfterViewInit {
    invalidJson: boolean = false;
    myForm = new FormGroup({});
    errorMsg = '';
    views: View[] = [];

    private web: boolean = false;
    private config: any;
    private dirty: boolean = false;;
    private selTab: string = 'output';
    private editorRegistry: { [key: string]: any } = {};

    private THEME_EDIT = 'ace/theme/dawn';
    private THEME_PREVIEW = 'ace/theme/solarized_light';

    constructor(
        private keybindings: KeybindingService,
        private registry: Registry,
        private api: LimedsApi,
        private resolver: ComponentFactoryResolver,
        private panelManager: PanelManager,
        private ref: ViewContainerRef) {
        super(PanelType.DOCUMENTATION);
    }

    ngOnInit() {
        this.keybindings.clearKeymap();
        this.keybindings.setKeyBinding(new KeyCombo(Key.KEY_ESC), new Action(this, this.cancelPanel));
    }

    ngAfterViewInit() {
        try {
            setTimeout(() => {
                this.openTab(this.views[this.views.length - 1].label);
            });

            // Init Ace input editors
            for (let i = 0; i < this.views.length; i++) {
                this.initAce(this.views[i].label, i);
            }

        } catch (err) {
            console.log(err);
        }
    }

    initAce(label: string, idx: number) {
        try {
            let editor = this.storeEditor(label, 'codeEditor[' + idx + ']');

            // Take schema of view with mapped label
            let schema = this.views.find(v => v.label === label).schema;
            let input = ('' === schema) ? '' : JSON.stringify(schema);
            editor.setValue(input.length > 0 ? beautify(input) : '');
            editor.clearSelection();
        } catch (err) {
            console.log(err);
        }
    }

    openTab(label: string) {
        this.selTab = label;
        this.rehookAutoFormatCommand(label);
    }

    isActive(id: string) {
        return {
            active: this.selTab === id
        };
    }

    togglePreview(view: View) {
        let editor = this.getEditor(view.label);
        if (!view.preview) {
            if ('' == editor.getValue()) {
                view.preview = true;
                editor.setTheme(this.THEME_PREVIEW);
                editor.setReadOnly(true);
            }
            else {
                view.schema = JSON.parse(editor.getValue());
                let sliceVersion = this.config.slice.version;
                let schema = editor.getValue().replace("${sliceVersion}", sliceVersion);
                this.api.resolveSchema(schema).subscribe(result => {
                    view.schemaResolved = result;
                    editor.setReadOnly(true);
                    editor.setTheme(this.THEME_PREVIEW);
                    editor.setValue(beautify(JSON.stringify(result)));
                    editor.clearSelection();
                    view.preview = true;
                });
            }
        }
        else {
            view.preview = false;
            editor.setValue(view.schema === '' ? '' : beautify(JSON.stringify(view.schema)));
            editor.clearSelection();
            editor.setTheme(this.THEME_EDIT);
            editor.setReadOnly(false);
        }
    }

    getPreviewButtonCss(view: View) {
        return { 'btn-default': !view.preview, 'btn-info': view.preview };
    }

    getPreviewCss(view: View) {
        return { 'fa-toggle-on': view.preview, 'fa-toggle-off': !view.preview };
    }

    addParam() {
        this.dirty = true;
        let idx = this.views.length;
        let id = 'param' + (idx);

        let view = {
            index: idx,
            label: id,
            schema: '',
            preview: false
        };

        this.views.push(view);

        setTimeout(() => {
            this.initAce(view.label, view.index);
            this.openTab(view.label);
            this.getEditor(view.label).focus();
        });
    }

    delParam(label: string) {
        this.dirty = true;
        let idx = this.views.findIndex(item => item.label === label);
        this.views.splice(idx, 1);

        setTimeout(() => {
            this.openTab(this.views[idx - 1].label);
            this.getEditor(this.views[idx - 1].label).focus();
        });
    }

    insertType(label: string) {
        // Save keybindings
        let bindings = this.keybindings.getKeymapCopy();
        // Fetch correct ace editor instance
        let editor = this.getEditor(label);
        // Launch panel
        let fact = this.resolver.resolveComponentFactory(TypePickerPanel);
        let ref = this.ref.createComponent(fact);
        let slice: limeds.ISlice = this.config.slice;
        ref.instance.setConfig({ sliceId: slice.id });
        try {
            ref.instance.subject$.subscribe(event => {
                // Restore keybindings
                this.keybindings.setKeymap(bindings);

                switch (event.action) {
                    case 'submit':
                        let typeRef = { '@typeRef': event.id };
                        let content: string = beautify(JSON.stringify(typeRef));
                        let row = editor.getCursorPosition().row + content.split('\n').length;
                        editor.insert(content);
                        editor.focus();
                        break;
                    case 'close':
                        break;
                }
                ref.destroy();
            });
        } catch (err) {
            console.log
        }
    }

    private reallySubmit() {
        try {
            this.config.applyDoc.in = [];
            for (let i = 1; i < this.views.length; i++) {
                let id = this.views[i].label;
                let idx = i - 1;
                this.errorMsg = 'Unable to save: the schema of parameter ' + (idx + 1) + ' is invalid JSON!';
                let input = this.getEditor(id).getValue();
                if (!this.config.applyDoc.in[idx]) {
                    this.config.applyDoc.in[idx] = {};
                }
                this.config.applyDoc.in[idx].schema = ('' === input) ? input : JSON.parse(input);
                this.config.applyDoc.in[idx].label = this.views[i].label;
            }
            let output = this.getEditor('output').getValue();
            if (!this.config.applyDoc.out) {
                this.config.applyDoc.out = {};
            }
            this.config.applyDoc.out.schema = ('' === output) ? output : JSON.parse(output);
            let result = {
                applyDoc: this.config.applyDoc
            }
            this.submitPanel(result);
        } catch (err) {
            this.invalidJson = true;
            console.error(err);
        }
    }

    onSubmit() {
        // If web enabled, and it wasnt originally
        if (this.dirty) {
            let options = {
                title: 'Input parameters changed',
                description: 'This will automatically change the arguments list of the script\'s apply() function. The script body, however, will remain intact and it is your own responsibility to refactor this if necessary.',
                question: 'Click \'Ok\' to proceed or \'Cancel\' to go back.'
            };
            this.panelManager.createPanel(PanelType.ARE_YOU_SURE, this.ref, true, options).subscribe(result => {
                switch (result.action) {
                    case PanelAction.CONFIRM:
                        setTimeout(() => {
                            this.reallySubmit();
                        });
                        break;
                    case PanelAction.CANCEL:
                        break;
                }
            });
        }
        else {
            this.reallySubmit();
        }
    }

    setConfig(config: any) {
        this.config = cloneDeep(config);
        if (!this.config.applyDoc) {
            this.config.applyDoc = {
                id: 'apply',
                in: [],
                out: ''
            };
        }

        this.web = this.config.web;

        this.initViews();
    }

    addButtonStyle() {
        return this.web && this.views.length === 2 ? 'noAdd' : 'canAdd';
    }

    private initViews() {
        // set views
        this.views.push({
            label: 'output',
            schema: this.config.applyDoc.out.schema || '',
            preview: false
        });

        for (let doc of this.config.applyDoc.in as limeds.NamedDoc[]) {
            this.views.push({
                label: doc.label,
                schema: doc.schema || '',
                preview: false
            });
        }
    }

    editParamName(event: MouseEvent, p) {
        event.preventDefault();
        event.stopPropagation();
        this.dirty = true;
        let fact = this.resolver.resolveComponentFactory(VarRenamePanel);
        let ref = this.ref.createComponent(fact);
        ref.instance.setConfig({ name: p.label, paramNames: this.views.map(item => item.label) });
        ref.instance.subject$.subscribe(event => {
            switch (event.action) {
                case 'submit':
                    let newName = event.name;
                    let oldName = p.label;
                    this.updateEditorMapping(oldName, newName);
                    p.label = newName;
                    this.openTab(p.label);
                    break;
                case 'close':

                    break;
            }
            ref.destroy();
        });
    }

    private autoFormat(editor: ace.Editor) {
        let pos = editor.getCursorPosition();
        let content = beautify(editor.getValue());
        editor.setValue(content);
        editor.clearSelection();
        editor.moveCursorToPosition(pos);
    }

    /**
     * Hooks the autoformatting hook to the given label Ace Editor, removing all other hooks.
     */
    private rehookAutoFormatCommand(label) {
        for (let key in this.editorRegistry) {
            let ed = this.editorRegistry[key];
            if (key === label) {
                ed.commands.addCommand({
                    name: 'autoFormat',
                    bindKey: { win: 'Ctrl-Shift-F', mac: 'Command-Shift-F' },
                    exec: editor => {
                        this.autoFormat(editor);
                    },
                    readOnly: false
                });
                ed.focus();
            }
            else {
                (ed.commands as any).removeCommand('autoFormat');
            }
        }
    }

    /**
     * Retrieve the AceEditor instance mapped to the given label.
     */
    private getEditor(label): ace.Editor {
        return this.editorRegistry[label];
    }

    /**
     * Save and init an empty AceEditor instance under the given label key.
     */
    private storeEditor(label, htmlId): ace.Editor {
        this.editorRegistry[label] = ace.edit(htmlId);
        let editor: ace.Editor = this.editorRegistry[label];
        editor.$blockScrolling = Infinity;
        editor.getSession().setMode('ace/mode/json');
        editor.setTheme('ace/theme/dawn');
        return editor;
    }

    /**
     * Move the mapping under oldLabel, to newLabel, whilest removing the oldLabel
     */
    private updateEditorMapping(oldLabel, newLabel) {
        this.editorRegistry[newLabel] = this.editorRegistry[oldLabel];
        delete this.editorRegistry[oldLabel]
    }
}