import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators, AbstractControl } from '@angular/forms';
import { DOCUMENT } from '@angular/platform-browser';

import { BasePanel } from '../base-panel';
import { KeybindingService, Key, KeyCombo, Action } from '../../../shared/keybinding.service';
import { LimedsApi } from '../../../shared/limeds-api.service';
import { Registry } from '../../../flow-editor/shared/registry.service';
import { PanelType } from '../../panel-manager.service';
import { Help } from '../../../shared/help';
import * as limeds from '../../../shared/limeds';

@Component({
    selector: 'lds-create-segment-panel',
    templateUrl: './create-segment.panel.html',
    styleUrls: ['./create-segment.panel.css']
})
export class CreateSegmentPanel extends BasePanel implements OnInit {
    model: any = {};
    form: FormGroup;

    private name: string;
    private edit: boolean = false;

    constructor(
        private api: LimedsApi,
        private keybindings: KeybindingService,
        private registry: Registry,
        @Inject(DOCUMENT) private document) {
        super(PanelType.CREATE_SEGMENT);
    }

    ngOnInit() {
        this.keybindings.clearKeymap();
        this.keybindings.setKeyBinding(new KeyCombo(Key.KEY_ESC), new Action(this, this.cancelPanel));
        let names = Array.from(this.registry.getSegments().keys());
        let el = this.document.querySelector('#inputName') as HTMLElement;
        el.focus();

        this.form = new FormGroup({
            name: new FormControl('', [
                Validators.required,
                Validators.pattern('[A-Z][^\\.\\s]+'),
                this.isUnique(names)
            ])
        });
    }

    onSubmit() {
        this.submitPanel({ name: this.form.controls['name'].value });
    }

    setConfig(config: any) {
        if (config && config.name) {
            setTimeout(() => this.form.controls['name'].setValue(config.name));
        }
        this.edit = config.edit;
    }

    private isUnique(names: string[]) {
        return (control: AbstractControl): ValidationResult => {
            if (this.edit) {
                return null;
            } else {
                let name = control.value;
                let found = names.map(val => this.registry.getLabelFromFQN(val)).some(val => {
                    return name && name === val;
                });
                return found ? { notUnique: found } : null;
            }
        }
    }
}

interface ValidationResult { [key: string]: any }
