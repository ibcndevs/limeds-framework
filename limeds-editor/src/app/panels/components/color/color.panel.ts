import { Component, Inject, Input, OnInit} from '@angular/core';
import { DOCUMENT } from '@angular/platform-browser';

import { BasePanel } from '../base-panel';
import { KeybindingService, Key, KeyCombo, Action} from '../../../shared/keybinding.service';
import { PanelManager, PanelType } from '../../panel-manager.service';
import * as limeds from '../../../shared/limeds';

@Component({
    selector: 'lds-color-panel',
    templateUrl: './color.panel.html',
    styleUrls: ['./color.panel.css']
})
export class ColorPanel extends BasePanel implements OnInit {
    original: string;
    color: string;

    private x: number = 0;
    private y: number = 0;

    private oldBindings;
    private parent: any;
    private picker: HTMLCanvasElement;
    private canvas: HTMLCanvasElement;
    private ctx: CanvasRenderingContext2D;

    private listener = (e: MouseEvent) => {
        this.x = e.offsetX;
        this.y = e.offsetY;
        this.color = this.getColor();
        let c = this.picker.getContext('2d');
        c.fillStyle = this.color;
        c.fillRect(0, 0, this.picker.width, this.picker.height);
    };


    constructor(
        private keybindings: KeybindingService,
        @Inject(DOCUMENT) private document) {
        super(PanelType.COLOR);
    }

    ngOnInit() {
        this.keybindings.clearKeymap();
        this.keybindings.setKeyBinding(new KeyCombo(Key.KEY_ESC), new Action(this, this.cancelPanel));
        this.setupCanvas();
    }

    setConfig(config: any) {
        this.original = config;
        this.color = config;
    }

    onSubmit() {
        this.submitPanel({ original: this.original, color: this.color });
    }

    onCancel() {
        this.cancelPanel();
    }

    private setupCanvas() {
        this.canvas = this.document.getElementById('colorCanvas') as HTMLCanvasElement;
        this.picker = this.document.getElementById('pickerCanvas') as HTMLCanvasElement;
        let c = this.picker.getContext('2d');
        c.fillStyle = this.original;
        c.fillRect(0, 0, this.picker.width, this.picker.height);
        this.canvas.width = 500;
        this.canvas.height = 200;
        this.ctx = this.canvas.getContext("2d");
        let ctx = this.ctx;
        let gradient = ctx.createLinearGradient(0, 0, this.canvas.width, 0);
        // Create color gradient
        gradient.addColorStop(0, "rgb(255,   0,   0)");
        gradient.addColorStop(0.15, "rgb(255,   0, 255)");
        gradient.addColorStop(0.33, "rgb(0,     0, 255)");
        gradient.addColorStop(0.49, "rgb(0,   255, 255)");
        gradient.addColorStop(0.67, "rgb(0,   255,   0)");
        gradient.addColorStop(0.84, "rgb(255, 255,   0)");
        gradient.addColorStop(1, "rgb(255,   0,   0)");

        ctx.fillStyle = gradient;
        ctx.fillRect(0, 0, ctx.canvas.width, this.canvas.height);

        // Create semi transparent gradient (white -> trans. -> black)
        gradient = ctx.createLinearGradient(0, 0, 0, this.canvas.height);
        gradient.addColorStop(0, "rgba(255, 255, 255, 1)");
        gradient.addColorStop(0.5, "rgba(255, 255, 255, 0)");
        gradient.addColorStop(0.5, "rgba(0,     0,   0, 0)");
        gradient.addColorStop(1, "rgba(0,     0,   0, 1)");

        ctx.fillStyle = gradient;
        ctx.fillRect(0, 0, ctx.canvas.width, this.canvas.height);
    }

    public onMouseDown(event: MouseEvent) {
        this.canvas.addEventListener('mousemove', this.listener);
        this.color = this.getColor();
    }

    public onMouseUp(event: MouseEvent) {
        this.canvas.removeEventListener('mousemove', this.listener);
    }
    
    public onClick(event: MouseEvent) {
        this.canvas.removeEventListener('mousemove', this.listener);
        this.listener(event);
        this.color = this.getColor();
    }

    private getColor() {
        let data = this.ctx.getImageData(this.x, this.y, 1, 1);
        return this.rgbToHex(data.data[0], data.data[1], data.data[2]);
    }

    private componentToHex(c: number) {
        var hex = c.toString(16);
        return hex.length == 1 ? "0" + hex : hex;
    }

    private rgbToHex(r: number, g: number, b: number) {
        return "#" + this.componentToHex(r) + this.componentToHex(g) + this.componentToHex(b);
    }


}