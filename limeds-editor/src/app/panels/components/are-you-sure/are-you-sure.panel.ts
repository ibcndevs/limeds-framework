import { Component, Inject, OnInit, ElementRef, ViewChild, AfterViewInit, Renderer } from '@angular/core';

import { BasePanel } from '../base-panel';
import { KeybindingService, Key, KeyCombo, Action } from '../../../shared/keybinding.service';
import { PanelManager, PanelType } from '../../panel-manager.service';
import * as limeds from '../../../shared/limeds';

export interface AreYouSurePanelConfig {
    /** Title of the panel. Default: 'Are you sure? */
    title?: string;
    /** Button style of the panenls. Default: 'OkCancel */
    buttons?: 'OkCancel' | 'YesNoCancel' | 'Ok';
    /** Description that is displayed */
    description?: string;
    /** Question that is displayed in the second paragraph */
    question?: string;
}

@Component({
    selector: 'lds-are-you-sure-panel',
    templateUrl: './are-you-sure.panel.html',
    styleUrls: ['./are-you-sure.panel.css']
})
export class AreYouSurePanel extends BasePanel implements OnInit, AfterViewInit {
    title: string = "Are you sure?";
    buttons: 'OkCancel' | 'YesNoCancel' | 'Ok' = 'OkCancel';
    description: string;
    question: string;
    
    @ViewChild('ok')
    private ok: ElementRef;

    constructor(
        private keybindings: KeybindingService,
        private renderer: Renderer
    ) {
        super(PanelType.ARE_YOU_SURE);
    }

    ngOnInit() {
        this.keybindings.clearKeymap();
        this.keybindings.setKeyBinding(new KeyCombo(Key.KEY_ESC), new Action(this, this.cancelPanel));
    }

    ngAfterViewInit() {
        this.renderer.invokeElementMethod(this.ok.nativeElement, 'focus');
    }

    setConfig(config: any) {
        if (config.title) {
            this.title = config.title;
        }
        if (config.buttons) {
            this.buttons = config.buttons;
        }
        this.description = config.description;
        this.question = config.question;
    }

    onSubmit() {
         this.submitPanel({choice: 'ok'});
    }

    onNo() {
        this.submitPanel({choice: 'no'});
    } 

    onCancel() {
        this.cancelPanel();
    }

}