import { Component, OnInit } from '@angular/core';

import { BasePanel } from '../base-panel';
import { KeybindingService, Key, KeyCombo, Action } from '../../../shared/keybinding.service';
import { LimedsApi } from '../../../shared/limeds-api.service';
import { PanelType } from '../../panel-manager.service';
import * as limeds from '../../../shared/limeds';

@Component({
    selector: 'lds-installable-panel',
    templateUrl: './view-installable.panel.html',
    styleUrls: ['./view-installable.panel.css']
})
export class ViewInstallablePanel extends BasePanel implements OnInit {
    installables: limeds.ISlice[];
    types: string[] = [];
    httpOps: { [key: string]: limeds.HttpOperation[] } = {};
    moduleId: string;
    moduleVersion: string;


    constructor(
        private api: LimedsApi,
        private keybindings: KeybindingService) {
        super(PanelType.VIEW_INSTALLABLE);
    }

    ngOnInit() {
        this.keybindings.clearKeymap();
        this.keybindings.setKeyBinding(new KeyCombo(Key.KEY_ESC), new Action(this, this.cancelPanel));
    }

    onSubmit() {
        this.submitPanel({});
    }

    onCancel() {
        this.cancelPanel();
    }

    setConfig(config: any) {
        this.moduleId = config.id;
        this.moduleVersion = config.version;
        this.api.getInstallable(config.id, config.version).subscribe(res => {
            this.installables = res;
            for (let i of res) {
                let ops = [];
                let types = [];
                if (i.segments) {
                    for (let s of i.segments) {
                        if (s.httpOperation) {
                            ops.push(s.httpOperation);
                        }
                    }
                    ops.sort((a, b) => {
                        if (a.path === b.path) {
                            return this.mapHttpOrder(a.method) - this.mapHttpOrder(b.method);
                        }
                        else {
                            let la = a.path.toLowerCase();
                            let lb = b.path.toLowerCase();
                            return (la < lb ? -1 : (la > lb ? 1 : 0));
                        }
                    });
                }

                this.httpOps[i.id + i.version] = ops;
                this.types[i.id + i.version] = i.types ? Object.keys(i.types) : [];
                this.types.sort((a, b) => {
                    let la = a.toLowerCase();
                    let lb = b.toLowerCase();
                    return (la < lb ? -1 : (la > lb ? 1 : 0));
                });
            }
        });
    }

    private mapHttpOrder(op: string): number {
        switch (op) {
            case 'HEAD':
                return 0;
            case 'GET':
                return 1;
            case 'POST':
                return 2;
            case 'PUST':
                return 3;
            case 'DELETE':
                return 4;
            case 'OPTIONS':
                return 5;
        }
    }
}