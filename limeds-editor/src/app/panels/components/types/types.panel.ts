import { Component, AfterViewInit, OnInit, ViewContainerRef, ComponentFactoryResolver} from '@angular/core';
import { AbstractControl, FormGroup, FormControl, Validators } from '@angular/forms';

import { BasePanel } from '../base-panel';
import { TyperefPanel } from '../typeref';
import { KeybindingService, Key, KeyCombo, Action} from '../../../shared/keybinding.service';
import { LimedsApi } from '../../../shared/limeds-api.service';
import { PanelType } from '../../panel-manager.service';
import { Registry } from '../../../flow-editor/shared/registry.service';
import { Help } from '../../../shared/help';
import { Subject } from 'rxjs/Subject';
import * as limeds from '../../../shared/limeds';

import { cloneDeep } from 'lodash';
import * as ace from 'brace';
import 'brace/mode/json';
import 'brace/theme/dawn';

import * as beautify from 'js-beautify';

@Component({
    selector: 'lds-types-panel',
    templateUrl: './types.panel.html',
    styleUrls: ['./types.panel.css'],
})
export class TypesPanel extends BasePanel implements OnInit, AfterViewInit {
    myForm = new FormGroup({});
    editor: any;
    types: { [key: string]: any };
    checked: any;

    private openTypeKey: string;
    private openTypeValue: any;
    private style: any;


    constructor(
        private keybindings: KeybindingService,
        private ref: ViewContainerRef,
        private registry: Registry,
        private resolver: ComponentFactoryResolver,
        private api: LimedsApi
    ) {
        super(PanelType.TYPES);
    }

    ngOnInit() {
        this.keybindings.clearKeymap();
        this.keybindings.setKeyBinding(new KeyCombo(Key.KEY_ESC), new Action(this, this.cancelPanel));

        this.myForm.addControl('typeSelect', new FormControl(''));

        (this.myForm.controls['typeSelect'] as FormControl).valueChanges.subscribe(value => {
            if (this.saveOpenEditorData())
                this.loadEditorData(value);
            else if (value !== this.checked.typeId) {
                this.myForm.controls['typeSelect'].setValue(this.checked.typeId);
            }
        });
    }

    ngAfterViewInit() {
        this.editor = ace.edit('typeEditor');
        this.editor.$blockScrolling = Infinity;
        this.editor.getSession().setMode('ace/mode/json');
        this.editor.setTheme('ace/theme/dawn');
        this.loadEditorData('');
    }

    private saveOpenEditorData() {
        if (this.openTypeKey) {
            // Save previous open type
            try {
                this.types[this.openTypeKey] = JSON.parse(this.editor.getValue());
                return true;
            } catch (err) {
                this.checked = {
                    valid: false,
                    msg: 'There are still JSON syntax errors, which make this json type unparsable. Please fix them before switching types.',
                    typeId: this.openTypeKey
                };
                return false;
            }
        } else {
            return true;
        }
    }

    private loadEditorData(typeId: string) {
        // New open type
        this.openTypeKey = typeId;
        let json = this.types[typeId];
        if (json) {
            this.openTypeValue = beautify(JSON.stringify(json));
            this.editor.setSession(new ace.EditSession(this.openTypeValue, 'ace/mode/json'));
            // this.editor.setValue(this.openTypeValue);
            this.editor.clearSelection();
            this.editor.moveCursorTo(0, 1);
            this.editor.setReadOnly(false);
            this.enableEditor();
            this.checked = null;
            this.editor.focus();
        } else {
            this.editor.setSession(new ace.EditSession('', 'ace/mode/json'));
            this.editor.clearSelection();
            this.editor.moveCursorTo(0, 0);
            this.disableEditor();
        }
    }

    private disableEditor() {
        this.editor.setReadOnly(true);
        this.style = this.editor.container.style;
        this.editor.container.style.pointerEvents = "none";
        this.editor.container.style.opacity = 0.5;
        this.editor.setStyle("disabled", true);
        this.editor.blur();
    }

    private enableEditor() {
        this.editor.setReadOnly(false);
        if (this.style) {
            this.editor.container.style = this.style;
            this.editor.setStyle("disabled", false);
        }
        this.editor.focus();
    }

    onSubmit() {
        // Save open editor
        if (this.openTypeKey) {
            // Save previous open type
            this.types[this.openTypeKey] = JSON.parse(this.editor.getValue());
        }

        // Check all for valid
        let checks = [];
        for (let key in this.types) {
            let json = this.types[key];
            checks.push(this.onValidateSchema(key));
        }
        Promise.all(checks).then(resultArr => {
            let err = resultArr.find(info => !info.valid);
            if (err) {
                this.myForm.controls['typeSelect'].setValue(err.typeId);
                this.saveOpenEditorData();
                this.loadEditorData(err.typeId);
                this.onValidateSchema(err.typeId);
            }
            else {
                // export changed typedefinitions map
                let result = {
                    typeDefinitions: this.types
                };
                this.submitPanel(result);
            }
        });
    }

    onCancel() {
        // don't export anythng
        this.cancelPanel();
    }

    setConfig(config: any) {
        // Take a copy of incoming types
        this.types = cloneDeep(config.typeDefinitions);
    }

    listTypeNames() {
        return Object.keys(this.types).sort((a, b) => {
            let la = a.toLowerCase();
            let lb = b.toLowerCase();
            return (la < lb ? -1 : (la > lb ? 1 : 0));
        });
    }

    getTypeAmount() {
        return Math.max(Object.keys(this.types).length, 2);
    }

    getLabel(typeId: string) {
        return typeId.split('_${')[0];
    }

    onAddType() {
        try {
            // Save keybindings
            let bindings = this.keybindings.getKeymapCopy();
            let fact = this.resolver.resolveComponentFactory(TyperefPanel);
            let ref = this.ref.createComponent(fact);
            ref.instance.subject$.subscribe((event: any) => {
                this.keybindings.setKeymap(bindings);
                if (event.action === 'submit') {
                    let name = event.name;
                    // Prefix
                    name = [this.registry.getFQN(name), '${sliceVersion}'].join('_');
                    if (!this.types[name])
                        this.types[name] = {};
                    this.myForm.controls['typeSelect'].setValue(name);
                    this.saveOpenEditorData();
                    this.loadEditorData(name);
                }
                else if (event.action === 'close') {

                }
                ref.destroy();
            });

        } catch (err) {

        }

    }

    onDelType() {
        let key = this.myForm.controls['typeSelect'].value;
        delete this.types[key];
        this.openTypeKey = null;
        this.openTypeValue = null;
        this.loadEditorData(null);
        this.checked = null;
    }

    onDismissValidationBox() {
        this.checked = null;
    }

    onValidateSchema(key?: string) {
        this.saveOpenEditorData();
        let myKey = key || this.openTypeKey;
        return this.api.validateSchema(this.types[myKey]).subscribe(
            result => {
                if (result.error) {
                    this.checked = {
                        valid: false,
                        msg: result.error,
                        typeId: myKey
                    };
                    return this.checked;
                }
                else {
                    this.checked = {
                        valid: true,
                        msg: 'Schema looks valid!',
                        typeId: myKey
                    }
                    return this.checked;
                }
            },
            error => console.log(error)
        );
    }
}