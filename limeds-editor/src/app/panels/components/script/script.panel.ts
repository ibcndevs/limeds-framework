import { Component, Input, OnInit, AfterViewInit } from '@angular/core';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';

import { BasePanel } from '../base-panel';
import { KeybindingService, Key, KeyCombo, Action } from '../../../shared/keybinding.service';
import { PanelManager, PanelType, PanelResult, PanelAction } from '../../panel-manager.service';
import { Registry } from '../../../flow-editor/shared/registry.service';
import { Help } from '../../../shared/help';
import * as limeds from '../../../shared/limeds';

import * as ace from 'brace';
import 'brace/mode/javascript';
import 'brace/theme/sqlserver';

import * as beautify from 'js-beautify'


interface Var {
    label: string;
    collection: boolean;
    segments?: string[];
    /* If true, this is used as a variable linking to the Segment. If false it is the segment itself (self-documentation) */
    asVar: boolean;
}

@Component({
    selector: 'lds-script-panel',
    templateUrl: './script.panel.html',
    styleUrls: ['./script.panel.css']
})
export class ScriptPanel extends BasePanel implements OnInit, AfterViewInit {
    private oldBindings;
    private config: any;
    private tabs: HTMLDivElement;
    private parent: HTMLDivElement;
    private buttons: HTMLDivElement;
    private resizeHandler: any;

    private ed: any;

    selTab: string = 'script';
    vars: Var[] = [];
    descriptions: Map<string, string> = new Map<string, string>();
    errorMsg: string;
    segment: string;

    documentation: Map<string, limeds.Documentation[]> = new Map<string, limeds.Documentation[]>();

    scriptLibDoc: SafeHtml = '';

    constructor(
        private registry: Registry,
        private keybindings: KeybindingService,
        private sanitizer: DomSanitizer) {
        super(PanelType.SCRIPT);
    }

    ngOnInit() {
        this.keybindings.clearKeymap();
        this.keybindings.setKeyBinding(new KeyCombo(Key.KEY_ESC), new Action(this, this.cancelPanel));
        this.keybindings.setKeyBinding(new KeyCombo(Key.KEY_S, false, true), new Action(this, this.killSaveAction));
        this.scriptLibDoc = this.sanitizer.bypassSecurityTrustHtml(require('./lib-documentation.html'));
    }

    ngAfterViewInit() {
        this.ed = ace.edit('code');
        this.ed.$blockScrolling = Infinity;
        this.ed.getSession().setMode('ace/mode/javascript');
        this.ed.setTheme('ace/theme/sqlserver');
        //this.config.script = limeds.CodeTemplates.replaceVars(this.config.script, this.config.inputDocs);
        this.ed.setValue(beautify(this.config.script));
        this.ed.clearSelection();
        this.ed.commands.addCommand({
            name: 'autoFormat',
            bindKey: { win: 'Ctrl-Shift-F', mac: 'Command-Shift-F' },
            exec: editor => {
                this.autoFormat(editor);
            },
            readOnly: false
        });
    }

    private killSaveAction() {
        // do nothing, but prevents save dialog of browser
    }

    private autoFormat(editor: ace.Editor) {
        let pos = editor.getCursorPosition();
        let content = beautify(editor.getValue());
        editor.setValue(content);
        editor.clearSelection();
        editor.moveCursorToPosition(pos);
    }

    setConfig(config: any) {
        this.config = config;
        this.segment = config.segmentLabel;
        for (let key in config.dependencies) {
            let val = config.dependencies[key];
            if (Object.keys(val.functionTargets).length === 0) {
                continue; // skip and go to next
            }
            if (val.collection) {
                let segments = Object.keys(val.functionTargets)
                    .map(id => this.registry.findSegment(id))
                    .map(segment => segment.getView().label);
                this.vars.push({ label: key, collection: val.collection, segments: segments, asVar: true });
            } else {
                this.vars.push({ label: key, collection: val.collection, asVar: true });
            }
            if (!val.collection) {
                // Populate documentation of var now:
                let target;
                for (let t in val.functionTargets) {
                    target = t;
                }
                let segment = this.registry.findSegment(target);
                let desc = segment.getModel().description;
                this.descriptions.set(key, desc);

                this.documentation.set(key, segment.getModel().documentation);
            }
        }
    }

    onSubmit() {
        let txt = this.ed.getValue();
        try {
            eval(txt);
            this.config.script = txt;
            this.submitPanel(this.config);
        } catch (err) {
            this.errorMsg = 'Cannot save while there is an error in you script!';
        }

    }

    openTab(id: string) {
        this.selTab = id;
    }

    isActive(id: string) {
        return { active: this.selTab === id };
    }

}