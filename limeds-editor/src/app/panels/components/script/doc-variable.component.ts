import { Component, Input, OnInit } from '@angular/core';

import { LimedsApi } from '../../../shared/limeds-api.service';
import * as limeds from '../../../shared/limeds';

@Component({
    selector: 'lds-doc-variable',
    templateUrl: './doc-variable.component.html'
})
export class DocVariableComponent implements OnInit {
    @Input('var')
    variable: { label: string, collection: boolean, asVar?:boolean };

    @Input('description')
    description: string

    @Input('documentation')
    docs: limeds.Documentation[];

    functions: limeds.Documentation[] = [];
    desc: string;

    constructor(
        private api: LimedsApi
    ) { }

    ngOnInit() {
        if (this.docs) {
            if (this.docs.length > 1) {
                this.functions = this.docs.filter(d => d.id !== 'apply').sort((a,b) => {
                    let la = a.id;
                    let lb = b.id;
                    return (la < lb ? -1 : (la > lb ? 1 : 0));
                });
            }
            else {
                this.functions = this.docs;
            }
        }
    }
}