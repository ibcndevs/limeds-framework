import { Subject } from 'rxjs/Subject';
import { PanelType, PanelAction, PanelResult } from '../panel-manager.service';

/**
 * This base class for Panel must be extended from. 
 * It will provide the closePanel() method to close the window.
 * 
 * This sends an event on the closeSubject$ stream, that is handled by the PanelManager.
 */
export abstract class BasePanel {
    private panelType: PanelType;
    
    /**
     * This is the Subject to listen to to close the panel.
     * It send the panelType. Normally the PanelManager is listening.
     */
    closeSubject$: Subject<PanelResult>;
    
    
    /**
     * Constructs a panel. Enter the panel type as argument.
     * 
     * @param panelType The correct Panel enum value
     */
    constructor(panelType: PanelType) {
        this.panelType = panelType;
        this.closeSubject$ = new Subject<PanelResult>();
    }
    
    /**
     * Signals the panel manager to close and cancel this panel. Sends an optional result object with it, 
     * the panel manager will send it further to the subscribers of the Subject at creation time.
     * 
     * @param result Optional result object.
     */
    cancelPanel(result?: any) {
        this.closeSubject$.next(new PanelResult(this.panelType, PanelAction.CANCEL, result || {}));
    }
    
    /**
     * Signals the panel manager to close and confirm this panel. Sends an optional result object with it, 
     * the panel manager will send it further to the subscribers of the Subject at creation time.
     * 
     * @param result Optional result object.
     */
    submitPanel(result?: any) {
        this.closeSubject$.next(new PanelResult(this.panelType, PanelAction.CONFIRM, result || {}));
    }
    
    abstract setConfig(config: any);
}