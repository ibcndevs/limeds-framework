import { Component, AfterViewInit, OnInit, ViewChild, ElementRef, Renderer} from '@angular/core';
import { AbstractControl, FormGroup, FormControl, Validators } from '@angular/forms';

import { BasePanel } from '../base-panel';
import { KeybindingService, Key, KeyCombo, Action} from '../../../shared/keybinding.service';
import { LimedsApi } from '../../../shared/limeds-api.service';
import { PanelType } from '../../panel-manager.service';
import { Help } from '../../../shared/help';
import { Subject } from 'rxjs/Subject';
import * as limeds from '../../../shared/limeds';

@Component({
    selector: 'lds-var-rename-panel',
    templateUrl: './var-rename.panel.html',
    styleUrls: ['./var-rename.panel.css']
})
export class VarRenamePanel extends BasePanel implements OnInit, AfterViewInit {
    model: any = {};
    myForm = new FormGroup({});
    subject$ = new Subject<VarRenameEvent>();
    private config: any;
    private name: string = '';
    private paramNames: string[] = [];

    @ViewChild('inputName') inputName: ElementRef;

    constructor(
        private keybindings: KeybindingService,
        private renderer: Renderer,
        private api: LimedsApi
    ) {
        super(PanelType.VAR_RENAME);
    }

    ngOnInit() {
        this.keybindings.clearKeymap();
        this.keybindings.setKeyBinding(new KeyCombo(Key.KEY_ESC), new Action(this, this.onCancel));

        this.myForm.addControl('name', new FormControl(this.name, [
            Validators.required,
            Validators.pattern('[a-z][a-zA-Z0-9\\-\\_]*',),
            (c: AbstractControl) =>  {
                let res = c.value !== this.name && (this.paramNames.indexOf(c.value) > 0 || 'output' === c.value);
                return res ? {'notUnique': res} : {};
            }
        ]));
    }

    ngAfterViewInit() {
        this.renderer.invokeElementMethod(this.inputName.nativeElement, 'focus');
        this.renderer.invokeElementMethod(this.inputName.nativeElement, 'select');
    }

    onSubmit() {
        if (this.myForm.valid) {
            this.subject$.next({ action: 'submit', name: this.myForm.controls['name'].value });
        }
    }

    onCancel() {
        this.subject$.next({ action: 'close', name: '' });
    }

    setConfig(config: any) {
        this.config = config;
        this.name = config.name;
        this.paramNames = config.paramNames;
    }

}

interface VarRenameEvent {
    action: 'close' | 'submit';
    name: string;
}