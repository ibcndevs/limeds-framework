import { Component, Input, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';

import { BasePanel } from '../base-panel';
import { KeybindingService, Key, KeyCombo, Action } from '../../../shared/keybinding.service';
import { PanelManager, PanelType } from '../../panel-manager.service';
import { Registry } from '../../../flow-editor/shared/registry.service';
import { Help } from '../../../shared/help/help.component';
import { LimedsApi } from '../../../shared/limeds-api.service'

import * as limeds from '../../../shared/limeds';

import { cloneDeep } from 'lodash';

@Component({
    selector: 'lds-config-panel',
    templateUrl: './config.panel.html',
    styleUrls: ['./config.panel.css']
})
export class ConfigPanel extends BasePanel implements OnInit {
    myForm: FormGroup = new FormGroup({});
    // canExport: boolean = true;
    canConvert: boolean = true;

    version: string;
    versionExpression: string;
    versions: string[];
    isSliceLocal: boolean;
    private oldBindings;
    private parent: any;
    private config: any;


    constructor(
        private keybindings: KeybindingService,
        private api: LimedsApi
    ) {
        super(PanelType.CONFIG);
    }

    ngOnInit() {
        this.keybindings.clearKeymap();
        this.keybindings.setKeyBinding(new KeyCombo(Key.KEY_ESC), new Action(this, this.cancelPanel));
        this.myForm.addControl('export', new FormControl());
        this.myForm.addControl('description', new FormControl());
    }

    onSubmit() {
        let templateInfo: limeds.Template;
        if (this.canConvert && this.myForm.get('factory').value) {
            // Existing template info, so just change type to factory 
            if (this.config.templateInfo) {
                templateInfo = cloneDeep(this.config.templateInfo);
                templateInfo.type = 'FACTORY';
            }
            // No template, add a new one with empty config
            else {
                templateInfo = {
                    type: 'FACTORY',
                    configuration: []
                };
            }
        }
        else {
            // Existing template info, with configuration, so just change type to configurable instance
            if (this.config.templateInfo && this.config.templateInfo.configuration.length > 0) {
                templateInfo = cloneDeep(this.config.templateInfo);
                templateInfo.type = 'CONFIGURABLE_INSTANCE';
            }
            // No template, which shouldn't happen, or configuration length === 0. Delete templatInfo
            else {
                templateInfo = null;
            }
        }

        let result = {
            export: this.myForm.get('export').value,
            description: this.myForm.get('description').value,
            templateInfo: templateInfo
        };
        this.submitPanel(result);
    }

    setConfig(config: any) {
        this.config = config;
        // this.canExport = config.canExport;
        let templateInfo: limeds.Template = config.templateInfo;
        this.canConvert = !templateInfo || templateInfo.type === 'FACTORY' || templateInfo.type === 'CONFIGURABLE_INSTANCE';
        if (this.canConvert) {
            this.myForm.addControl('factory', new FormControl(''));
        }
        this.version = config.version;
        this.versionExpression = config.versionExpressesion;
        this.isSliceLocal = (config.type as limeds.SegmentViewType) === 'SLICE_LOCAL';
        if (!this.isSliceLocal) {
            let factoryId = templateInfo.factoryId;
            this.api.listFactories().subscribe(models => {
                this.versions = models.filter(model => {
                    return model.id === factoryId;
                }).map(model => model.version);
            });
        }

        setTimeout(() => {
            this.myForm.controls['export'].setValue(config.export);
            this.myForm.controls['description'].setValue(config.description);
            if (this.canConvert) {
                this.myForm.controls['factory'].setValue(templateInfo && templateInfo.type === 'FACTORY');
            }
        });
    }
}