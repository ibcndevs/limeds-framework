import { Component, Inject, OnInit, ElementRef, ViewChild, AfterViewInit, Renderer } from '@angular/core';
import { FormGroup, FormControl, FormControlName } from '@angular/forms';
import { BasePanel } from '../base-panel';
import { KeybindingService, Key, KeyCombo, Action } from '../../../shared/keybinding.service';
import { PanelManager, PanelType } from '../../panel-manager.service';
import * as limeds from '../../../shared/limeds';

/**
 * Configuration object for PromptPanel
 */
export interface PromptPanelConfig {
    /** Title of the panel. Default: 'Prompt' */
    title?: string;
    /** Css fontawesome class for the icon. Default: 'fa-question-circle-o' */
    titleIcon?: string;
    /** Button text for Ok button. Default: 'Ok' */
    okText?: string;
    /** Button text for Cancel button. Default: 'Cancel' */
    cancelText?: string;
    /** Description text in the panel */
    description: string;
    /** Question text, 1 paragraph below description text. Default: none */
    question?: string;
    /** Label for the input box. Default: none */
    label?: string;
    /** Placeholder text for the input box. Default: '' */
    placeholder?: string;
    /** Initial value for the input box. Default: '' */
    value?: string;
    /** Custom validation function that checks whether to enable the Ok button. False disables the button. Default: checks for non-empty, not equals to initial value */
    validationFunction?: {(newValue:string, oldValue: string): boolean};
}

@Component({
    selector: 'lds-prompt-panel',
    templateUrl: './prompt.panel.html',
    styleUrls: ['./prompt.panel.css']
})
export class PromptPanel extends BasePanel implements OnInit, AfterViewInit {
    title: string = 'Prompt';
    titleIcon: string = 'fa-question-circle-o';
    okText: string = 'Ok';
    cancelText: string = 'Cancel';
    description: string;
    question: string;
    label: string;
    placeholder: string = '';
    value: string = '';
    myForm = new FormGroup({});

    private validationFunction: {(newValue:string, oldValue: string): boolean};

    @ViewChild('in')
    private input: ElementRef;

    constructor(
        private keybindings: KeybindingService,
        private renderer: Renderer
    ) {
        super(PanelType.PROMPT);
    }

    ngOnInit() {
        this.keybindings.clearKeymap();
        this.keybindings.setKeyBinding(new KeyCombo(Key.KEY_ESC), new Action(this, this.cancelPanel));
    }

    ngAfterViewInit() {
        this.renderer.invokeElementMethod(this.input.nativeElement, 'focus');
        if (this.value.length > 0) {
            this.renderer.invokeElementMethod(this.input.nativeElement, 'setSelectionRange', [0,this.value.length]);
        }
    }

    setConfig(config: any) {
        let c = config as PromptPanelConfig;
        if (c.title) {
            this.title = config.title;
        }
        if (config.titleIcon) {
            this.titleIcon = config.titleIcon;
        }
        if (config.okText) {
            this.okText = config.okText;
        }
        if (config.cancelText) {
            this.cancelText = config.cancelText;
        }
        if (config.placeholder) {
            this.placeholder = config.placeholder;
        }
        if (config.label) {
            this.label = config.label;
        }
        if (config.question) {
            this.question = config.question;
        }
        if (config.value) {
            this.value = config.value;
        }
        if (config.validationFunction) {
            this.validationFunction = config.validationFunction;
        }
        this.description = config.description;
        this.myForm.addControl('in', new FormControl(this.value));
    }

    isValid() {
        if (this.input) {
            let check = this.input && this.myForm.get('in').value && (this.myForm.get('in').value !== this.value);
            if (this.validationFunction) {
                check = this.validationFunction.call(this, this.myForm.get('in').value, this.value);
            }
            return check;
        }
        else {
            return false;
        }
    }

    onSubmit() {
        this.submitPanel({
            input: this.myForm.get('in').value
        });
    }

    onCancel() {
        this.cancelPanel();
    }

}