import { Component, Input, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { BasePanel } from '../base-panel';
import { KeybindingService, Key, KeyCombo, Action } from '../../../shared/keybinding.service';
import { LimedsApi } from '../../../shared/limeds-api.service';
import { PanelManager, PanelType } from '../../panel-manager.service';
import { Registry } from '../../../flow-editor/shared/registry.service';
import { Help } from '../../../shared/help';
import * as limeds from '../../../shared/limeds';

@Component({
    selector: 'lds-save-as-panel',
    templateUrl: './save-as.panel.html'
})
export class SaveAsPanel extends BasePanel implements OnInit {
    currentVersion: string;

    myForm = new FormGroup({});

    id: string;
    isMajor: boolean = false;
    isMinor: boolean = true;
    isMicro: boolean = false;
    error: boolean = false;

    private major: number;
    private minor: number;
    private micro: number;

    constructor(
        private keybindings: KeybindingService,
        private api: LimedsApi
    ) {
        super(PanelType.SAVE_AS);
    }

    ngOnInit() {
        this.keybindings.clearKeymap();
        this.keybindings.setKeyBinding(new KeyCombo(Key.KEY_ESC), new Action(this, this.cancelPanel));
        this.myForm.addControl('open', new FormControl(true));
        this.myForm.addControl('undeploy', new FormControl(true));
    }

    setConfig(config: any) {
        let version = config.currentVersion;
        [this.major, this.minor, this.micro] = version.split('.');
        this.currentVersion = version;
        this.id = config.id;
    }

    bumpMajor() {
        this.isMajor = true;
        this.isMinor = false;
        this.isMicro = false;
        this.error = false;
    }

    bumpMinor() {
        this.isMajor = false;
        this.isMinor = true;
        this.isMicro = false;
        this.error = false;
    }

    bumpMicro() {
        this.isMajor = false;
        this.isMinor = false;
        this.isMicro = true;
        this.error = false;
    }

    getNewVersion() {
        if (this.isMajor) {
            return [+this.major + 1, 0, 0].join('.');
        }
        else if (this.isMinor) {
            return [+this.major, +this.minor + 1, 0].join('.');
        }
        else if (this.isMicro) {
            return [+this.major, +this.minor, +this.micro + 1].join('.');
        }
        else {
            return '-';
        }
    }

    onSubmit() {
        let newVersion = this.getNewVersion();
        this.api.listSlices().subscribe(slices => {
            if (slices.map(slice => slice.id).some(id => id === newVersion)) {
                this.error = true;
            } else {
                this.submitPanel({
                    newVersion: newVersion,
                    open: this.myForm.controls['open'].value,
                    undeploy: this.myForm.controls['undeploy'].value
                });
            }
        });
    }
}