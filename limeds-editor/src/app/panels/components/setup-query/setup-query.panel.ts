import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormGroup, FormControl, Validators } from '@angular/forms';

import { BasePanel } from '../base-panel';
import { KeybindingService, Key, KeyCombo, Action } from '../../../shared/keybinding.service';
import { LimedsApi } from '../../../shared/limeds-api.service';
import { PanelType } from '../../panel-manager.service';
import { Help } from '../../../shared/help';
import * as limeds from '../../../shared/limeds';

@Component({
    selector: 'lds-setup-query-panel',
    templateUrl: './setup-query.panel.html',
    styleUrls: ['./setup-query.panel.css']
})
export class SetupQueryPanel extends BasePanel implements OnInit {
    props: Property[] = [];
    pathInfo: string[];
    myForm = new FormGroup({});


    constructor(
        private api: LimedsApi,
        private keybindings: KeybindingService) {
        super(PanelType.SETUP_QUERY);
    }

    ngOnInit() {
        this.keybindings.clearKeymap();
        this.keybindings.setKeyBinding(new KeyCombo(Key.KEY_ESC), new Action(this, this.cancelPanel));

        this.myForm.addControl('name', new FormControl('', Validators.required));
        this.myForm.addControl('type', new FormControl('String', Validators.required));
        this.myForm.addControl('required', new FormControl(false));
        this.myForm.addControl('description', new FormControl(''));
    }

    onSubmit() {
        let q = this.quote;
        // construct schema
        let schema;
        if (this.props.length === 0) {
            schema = '{}';
        } else {
            schema = '{';
            let parts: string[];
            for (let p of this.props) {
                parts = [];
                parts.push(p.type);
                if (!p.required) {
                    parts.push('*')
                }
                if (p.description && p.description.length > 0) {
                    parts.push('(' + p.description + ')');
                }
                schema += q(p.name) + ': ' + q(parts.join(' ')) + ',';
            }
            schema = schema.substring(0, schema.length - 1) + '}';
        }
        try {
            schema = JSON.parse(schema)
        } catch (err) {
            console.log(err);
        }

        let result = {
            httpDoc: {
                querySchema: schema,
                pathInfo: this.pathInfo
            }
        };

        // send it back
        this.submitPanel(result);
    }

    onCancel() {
        this.cancelPanel();
    }

    onDelete() {
        this.submitPanel({ httpDoc: null });
    }

    setConfig(config: any) {
        let httpDoc = config.httpDoc as limeds.HttpDoc;
        this.pathInfo = httpDoc && httpDoc.pathInfo ? httpDoc.pathInfo : [];
        this.props = httpDoc && httpDoc.querySchema ? this.parseHttpDoc(httpDoc.querySchema) : [];
    }

    cannotAddProp() {
        let name = this.myForm.get('name').value;
        return !name || this.props.some(item => item.name === name);
    }

    addProp() {
        let name = this.myForm.get('name').value;
        let type = this.myForm.get('type').value;
        let required = this.myForm.get('required').value;
        let description = this.myForm.get('description').value;
        let found = this.props.find(item => item.name === name);
        if (!found) {
            this.props.push({
                name: name,
                type: type,
                required: required,
                description: description
            });
        }
        this.myForm.get('name').setValue('');
        this.myForm.get('type').setValue('String');
        this.myForm.get('required').setValue(false);
        this.myForm.get('description').setValue('');
    }

    removeProp(prop: Property) {
        let idx = this.props.findIndex(item => item.name === prop.name);
        this.props.splice(idx, 1);
    }

    private parseHttpDoc(schema: any): Property[] {
        let props: Property[] = [];
        for (let key in schema) {
            let parts = schema[key].split(' ');
            let type = parts[0];
            let required = parts.length === 1 || parts[1] !== '*';
            let description;
            if (parts.length === 1) {
                description = '';
            }
            else {
                if (required) {
                    description = parts[1].substring(1, parts[1].length - 1);
                }
                else {
                    description = parts.length >= 3 ? parts[2].substring(1, parts[2].length - 1) : '';
                }
            }
            props.push({
                name: key,
                type: type,
                required: required,
                description: description
            });
        }
        return props;
    }

    private quote(str: string): string {
        return '\"' + str + '\"';
    }

    getType(type: string) {
        switch (type) {
            case 'String':
                return 'string';
            case 'boolean':
                return 'boolean';
            case 'IntNumber':
                return 'integer number';
            case 'FloatNumber':
                return 'float number';
        }
    }
}

export interface Property {
    name: string;
    type: 'boolean' | 'IntNumber' | 'FloatNumber' | 'String';
    required: boolean;
    description: string;
}