import { Component, Input, OnInit } from '@angular/core';

import { BasePanel } from '../base-panel';
import { KeybindingService, Key, KeyCombo, Action } from '../../../shared/keybinding.service';
import { PanelManager, PanelType } from '../../panel-manager.service';
import { Registry } from '../../../flow-editor/shared/registry.service';
import * as limeds from '../../../shared/limeds';

@Component({
    selector: 'lds-raw-code-panel',
    templateUrl: './raw-code.panel.html',
    styleUrls: ['./raw-code.panel.css']
})
export class RawCodePanel extends BasePanel implements OnInit {
    slice: limeds.ISlice;
    title: string = 'Raw code';

    private oldBindings;
    private parent: any;

    constructor(
        private registry: Registry,
        private keybindings: KeybindingService) {
        super(PanelType.RAW_CODE);
    }

    ngOnInit() {
        this.keybindings.clearKeymap();
        this.keybindings.setKeyBinding(new KeyCombo(Key.KEY_ESC), new Action(this, this.cancelPanel));
    }

    setConfig(config: any) {
        if (config && config.code) {
            this.slice = config.code;
        }
        else {
            this.slice = this.registry.serialize();
        }

        if (config && config.title) {
            this.title = config.title;
        }
    }
}