import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormGroup, FormControl, FormArray, Validators } from '@angular/forms';

import { BasePanel } from '../base-panel';
import { KeybindingService, Key, KeyCombo, Action } from '../../../shared/keybinding.service';
import { LimedsApi } from '../../../shared/limeds-api.service';
import { Registry } from '../../../flow-editor/shared/registry.service';
import { PanelType } from '../../panel-manager.service';
import { Help } from '../../../shared/help';
import * as limeds from '../../../shared/limeds';

@Component({
    selector: 'lds-factory-instance-panel',
    templateUrl: './factory-instance.panel.html'
})
export class FactoryInstancePanel extends BasePanel implements OnInit {
    vm: any = {
        configEntries: []
    };

    myForm: FormGroup = new FormGroup({});
    private config: { factory: limeds.ISegmentModel };

    constructor(
        private api: LimedsApi,
        private keybindings: KeybindingService,
        private registry: Registry) {
        super(PanelType.FACTORY_INSTANCE);
    }

    ngOnInit() {
        this.keybindings.clearKeymap();
        this.keybindings.setKeyBinding(new KeyCombo(Key.KEY_ESC), new Action(this, this.cancelPanel));
        let names = Array.from(this.registry.getSegments().keys());
        this.myForm.addControl('name', new FormControl('', [
            Validators.required,
            Validators.pattern('[A-Z][^\\.\\s]+'),
            this.isUnique(names)
        ]));
    }

    onSubmit() {
        let result = {
            name: this.myForm.controls['name'].value,
            dependencies: this.vm.dependencies,
            configEntries: []
        };

        this.vm.configEntries.forEach(entry => {
            entry.value = this.myForm.controls[entry.name].value + '';
            if (entry.simpleType === 'array') {
                entry.value = (entry.value as string).split(',').map(part => part.trim()).join(',');
            }
            delete entry.simpleType;
            result.configEntries.push(entry);
        });

        this.submitPanel(result);
    }

    setConfig(config: any) {
        this.config = config;
        this.initTemplate(this.config.factory.templateInfo);
        this.initDependencies(this.config.factory.templateInfo, this.config.factory.dependencies);
    }

    private initTemplate(template: limeds.Template) {
        if (template.type === 'FACTORY') {
            this.vm.configEntries = template.configuration
                .filter(c => !c.name.startsWith('$.'))
                .map(entry => {
                    let result: any = {};
                    if (entry.possibleValues && entry.possibleValues.length !== 0) {
                        result.simpleType = 'select';
                        result.possibleValues = entry.possibleValues;
                    }
                    else {
                        switch (entry.type) {
                            case "int":
                            case "short":
                            case "long":
                            case "float":
                            case "double":
                                result.simpleType = 'number';
                                break;
                            case "boolean":
                                result.simpleType = 'checkbox';
                                break;
                            case "java.lang.String[]":
                                result.simpleType = 'array';
                                break;
                            case "java.lang.String":
                            default:
                                result.simpleType = 'text';
                                break;
                        }
                    }
                    result.name = entry.name;
                    result.value = entry.value
                    result.type = entry.type;
                    if (entry.description) {
                        result.description = entry.description
                    }

                    // Add to formGroup
                    this.myForm.addControl(result.name, new FormControl(result.value, Validators.required))

                    return result;
                });
        }
    }

    private initDependencies(template: limeds.Template, dependencies: { [key: string]: limeds.Dependency }) {
        if (template.type === 'FACTORY') {
            let deps = template.configuration
                .filter(c => c.name.startsWith('$.dependencies.'))
                .filter(c => {
                    let depName = c.name.substring('$.dependencies.'.length);
                    return !!dependencies[depName];
                })
                .map(c => {
                    let depName = c.name.substring('$.dependencies.'.length);
                    let dep = dependencies[depName];
                    dep['id'] = depName;
                    return dep;
                });

            this.vm.dependencies = {};
            deps.forEach(d => {
                let id = d['id'];
                delete d['id'];
                this.vm.dependencies[id] = d;
            });
        }
    }

    isValid() {
        for (let key in this.myForm.controls) {
            let c = this.myForm.controls[key];
            if (c.errors) {
                return false;
            }
        }
        return true;
    }

    private isUnique(names: string[]) {
        return (control: AbstractControl): ValidationResult => {
            let name = control.value;
            let found = names.map(val => this.registry.getLabelFromFQN(val)).some(val => {
                return name && name === val;
            });
            return found ? { notUnique: found } : null;
        }
    }
}

interface ValidationResult { [key: string]: any }