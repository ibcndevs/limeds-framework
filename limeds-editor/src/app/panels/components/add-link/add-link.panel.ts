import { Component, Inject, OnInit, AfterViewInit, ElementRef, Renderer, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { BasePanel } from '../base-panel';
import { Help } from '../../../shared/help';
import { KeybindingService, Key, KeyCombo, Action } from '../../../shared/keybinding.service';
import { LimedsApi } from '../../../shared/limeds-api.service';
import { PanelType } from '../../panel-manager.service';
import * as limeds from '../../../shared/limeds';

@Component({
    selector: 'lds-add-link-panel',
    templateUrl: './add-link.panel.html',
    styleUrls: ['./add-link.panel.css']
})
export class AddLinkPanel extends BasePanel implements OnInit, AfterViewInit {
    vm: any = {};

    @ViewChild('customVar') customVar: ElementRef;

    form: FormGroup = new FormGroup({
        radioVar: new FormControl(
            { value: 'custom', disabled: this.vm.isFactoryInstance },
            Validators.required),
        customVarName: new FormControl(
            { value: '', disabled: this.vm.isFactoryInstance },
            Validators.pattern('[a-z][^\\.\\s]+')),
        chosenVarName: new FormControl()
    });

    constructor(
        private api: LimedsApi,
        private keybindings: KeybindingService,
        private renderer: Renderer) {
        super(PanelType.ADD_LINK);
    }

    ngOnInit() {
        this.keybindings.clearKeymap();
        this.keybindings.setKeyBinding(new KeyCombo(Key.KEY_ESC), new Action(this, this.cancelPanel));
    }

    ngAfterViewInit() {
        setTimeout(() => {
            if (this.customVar) {
                this.renderer.invokeElementMethod(this.customVar.nativeElement, 'focus');
            }
        });
    }

    onSubmit() {
        let input = this.form.value;
        let custom = input.radioVar === 'custom';
        let result = custom ? input.customVarName : input.chosenVarName;
        this.submitPanel({ varName: result });
    }

    onCancel() {
        this.cancelPanel();
    }

    setConfig(config: any) {
        this.vm.cycle = config.cycle;
        this.vm.factoryNoVars = !this.vm.cycle;
        if (!this.vm.cycle) {
            let deps: Object = config.dependencies;
            let vars = [];
            for (let k in deps) {
                let value = deps[k];
                vars.push({ name: k, targets: Object.keys(value.functionTargets).length });
            }
            let label = config.toLabel as string;
            this.vm = {
                isFactoryInstance: config.isFactoryInstance,
                vars: vars,
                factoryNoVars: config.isFactoryInstance && vars.length === 0
            };
            let input = {
                radioVar: 'custom',
                customVarName: label.charAt(0).toLowerCase() + label.slice(1),
                chosenVarName: ''
            };
            // setTimeout(() => {
            this.form.controls['radioVar'].setValue(input.radioVar);
            this.form.controls['customVarName'].setValue(input.customVarName);
            this.form.controls['chosenVarName'].setValue(input.chosenVarName);
            // });
        }
    }

    useCustomVar() {
        this.form.controls['radioVar'].setValue('custom');
    }

    useSelectedVar() {
        this.form.controls['radioVar'].setValue('selected');
    }

    isValid() {
        let custom = this.form.controls['radioVar'].value === 'custom';

        if (custom) {
            let ctrl = this.form.controls['customVarName'];
            return !ctrl.errors && ctrl.value != '';
        }
        else {
            let ctrl = this.form.controls['chosenVarName'];
            return !ctrl.errors && ctrl.value != '';
        }
    }
}