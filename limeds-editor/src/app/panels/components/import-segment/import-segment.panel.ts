import { Component, Inject, OnInit, Pipe, PipeTransform, AfterViewInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/debounceTime';

import { BasePanel } from '../base-panel';
import { KeybindingService, Key, KeyCombo, Action } from '../../../shared/keybinding.service';
import { LimedsApi } from '../../../shared/limeds-api.service';
import { Registry } from '../../../flow-editor/shared/registry.service';
import { PanelType } from '../../panel-manager.service';
import { Help } from '../../../shared/help';
import * as limeds from '../../../shared/limeds';

interface SegmentObject {
    id: string;
    factory: boolean;
};

@Component({
    selector: 'lds-import-segment-panel',
    templateUrl: './import-segment.panel.html',
    styleUrls: ['./import-segment.panel.css']
})
export class ImportSegmentPanel extends BasePanel implements OnInit, AfterViewInit {
    myForm = new FormGroup({
        segment: new FormControl('', Validators.required),
        version: new FormControl('', Validators.required)
    });

    segments: { [key: string]: string[] } = {};
    segmentObjects: SegmentObject[] = [];

    private sliceId: string;
    private searchTerm$ = new Subject<string>();

    private factories: Map<string, limeds.ISegmentModel> = new Map<string, limeds.ISegmentModel>();

    items: Observable<SegmentObject[]> = this.searchTerm$
        .debounceTime(200)
        .distinctUntilChanged()
        .switchMap(term => this.findMatches(term));

    constructor(
        private api: LimedsApi,
        private registry: Registry,
        private keybindings: KeybindingService) {
        super(PanelType.IMPORT_SEGMENT);
    }

    ngOnInit() {
        this.keybindings.clearKeymap();
        this.keybindings.setKeyBinding(new KeyCombo(Key.KEY_ESC), new Action(this, this.cancelPanel));



        // List all segments
        let fetchComponents = this.api.listSegments().subscribe(results => {
            results.forEach(segment => {
                // If segment is not from this slice
                if (segment.id.substring(0, segment.id.lastIndexOf('.')) !== this.sliceId) {
                    let record = this.segments[segment.id];
                    if (!record) {
                        this.segments[segment.id] = [segment.version];
                        this.segmentObjects.push({ id: segment.id, factory: false });
                    }
                    else {
                        record.push(segment.version);
                    }
                }
            });
            this.segmentObjects.sort((a, b) => {
                let la = a.id;
                let lb = b.id;
                return (la < lb ? -1 : (la > lb ? 1 : 0));
            });
        });
        let fetchFactories = this.api.listFactories().subscribe(results => {
            results.forEach(segment => {
                let record = this.segments[segment.id];
                if (!record) {
                    this.segments[segment.id] = [segment.version];
                    this.segmentObjects.push({ id: segment.id, factory: true });
                }
                else {
                    record.push(segment.version);
                }
                this.factories.set(segment.id + segment.version, segment);
            });
            this.segmentObjects.sort((a, b) => {
                let la = a.id;
                let lb = b.id;
                return (la < lb ? -1 : (la > lb ? 1 : 0));
            });
        });


        Promise.all([fetchComponents, fetchFactories]).then(() => {
            // Sort all versions
            for (let key in this.segments) {
                let versions = this.segments[key];
                versions.sort((a, b) => {
                    let [aMajor, aMinor, aMicro] = a.split('.').map(s => parseInt(s));
                    let [bMajor, bMinor, bMicro] = b.split('.').map(s => parseInt(s));
                    let major = bMajor - aMajor;
                    let minor = bMinor - aMinor;
                    let micro = bMicro - aMicro;
                    if (major === 0) {
                        if (minor === 0) {
                            return micro;
                        }
                        else {
                            return minor;
                        }
                    } else {
                        return major;
                    }
                });
            }
        });

        this.myForm.controls['segment'].valueChanges.subscribe(val => this.myForm.controls['version'].setValue('latest'));
    }

    ngAfterViewInit() {
        this.searchTerm$.next('');
        document.getElementById('searchFilter').focus();
    }

    onSubmit() {
        let segment = this.myForm.controls['segment'].value;
        let version = this.myForm.controls['version'].value;
        if ('latest' === version) {
            version = this.segments[segment.id][0];
        }
        let result: any = {
            segment: segment,
            version: '^' + version
        };
        if (segment.factory) {
            result.factory = this.factories.get(segment.id + version);
        }
        this.submitPanel(result);
    }

    setConfig(config: any) {
        this.sliceId = config.sliceId;
    }

    getStyle(segment: SegmentObject) {
        return {
            segment: !segment.factory,
            factory: segment.factory
        };
    }

    search(query: string) {
        this.searchTerm$.next(query);
    }

    private findMatches(term: string): Observable<SegmentObject[]> {
        let str = term.toLowerCase();
        return Observable.of(this.segmentObjects.filter(obj => str === '' || obj.id.toLowerCase().indexOf(str) > -1));
    }

}