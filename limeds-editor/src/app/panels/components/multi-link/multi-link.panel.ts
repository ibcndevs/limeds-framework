import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormGroup, FormControl, Validators } from '@angular/forms';

import { BasePanel } from '../base-panel';
import { KeybindingService, Key, KeyCombo, Action } from '../../../shared/keybinding.service';
import { PanelType } from '../../panel-manager.service';
import { Registry } from '../../../flow-editor/shared/registry.service';
import { Help } from '../../../shared/help';
import * as limeds from '../../../shared/limeds';

import { cloneDeep } from 'lodash';

@Component({
    selector: 'lds-multi-link',
    templateUrl: './multi-link.panel.html'
})
export class MultiLinkPanel extends BasePanel implements OnInit {

    // TODO: report the merges back, so they can be done on the registry, or ignored


    myForm = new FormGroup({});

    vars = [];
    status = {
        multi: false,
        single: false
    };

    private fromId: string;
    private newDependencies: { [key: string]: limeds.Dependency };
    private newLinks: limeds.ILink[];

    constructor(
        private keybindings: KeybindingService,
        private registry: Registry
    ) {
        super(PanelType.MULTI_LINK);
    }

    ngOnInit() {
        this.keybindings.clearKeymap();
        this.keybindings.setKeyBinding(new KeyCombo(Key.KEY_ESC), new Action(this, this.cancelPanel));
        this.myForm.addControl('variables', new FormControl());
        this.myForm.addControl('merge', new FormControl({ value: '', disabled: !this.status.multi }));
        this.myForm.addControl('rename', new FormControl({ value: '', disabled: !this.status.single }, Validators.pattern('[a-z][^\\.\\s]*')));
        this.myForm.controls['variables'].valueChanges.subscribe(changes => {
            this.status.multi = changes.length > 1;
            this.status.single = changes.length === 1;

            if (this.status.multi) {
                this.myForm.get('merge').enable();
                this.myForm.get('rename').disable();
            } else if (this.status.single) {
                this.myForm.get('merge').disable();
                this.myForm.get('rename').enable();
            } else {
                this.myForm.get('merge').disable();
                this.myForm.get('rename').disable();
            }

            if (this.status.single) {
                this.myForm.controls['rename'].setValue(changes);
            }
        });
    }

    onCancel() {
        this.cancelPanel();
    }

    setConfig(config: any) {
        let segment = config.segment as limeds.ISegment;
        this.fromId = segment.getId();
        this.newDependencies = segment.model.dependencies;
        this.newLinks = Array.from(this.registry.getLinks().values());
        this.refreshVars();
    }

    onMerge() {
        let name = this.myForm.controls['merge'].value;
        if (name && name.length > 0) {
            let selected: string[] = this.myForm.controls['variables'].value;
            selected.forEach(variable => {
                // Remove old singular dependency in variables 
                let idx = this.vars.findIndex(val => val.key === variable);
                this.vars.splice(idx, 1);
            });

            // Merge selected links            
            this.registry.mergeLinkToMulti(this.newLinks.filter(link => selected.indexOf(link.getView().fromVar) > -1), name);

            this.refreshVars();

            // All targets  are put in dep variable, now assign it a key and add to vars
            //this.vars.push({ key: name, value: this.newDependencies[name] });
            this.myForm.controls['merge'].setValue('');
        }
    }

    onRename() {
        let name = this.myForm.controls['rename'].value;
        if (name && name.length > 0) {
            let selected = this.myForm.controls['variables'].value[0];
            let link = this.newLinks.filter(link => selected === link.getView().fromVar)[0];
            link = this.registry.findLink(link.getView().getId());
            link.getView().fromVar = name;
            let tmp = this.newDependencies[selected];
            this.newDependencies[name] = tmp;
            delete this.newDependencies[selected];

            this.refreshVars();
        }
    }

    onRemove() {
        let selected = this.myForm.controls['variables'].value[0];
        let link = this.newLinks.filter(link => selected === link.getView().fromVar)[0];
        if (!link) {
            delete this.newDependencies[selected];
        }
        else {
            link = this.registry.findLink(link.getView().getId());
            this.registry.delLink(link.getView().getId());
            this.registry.delDependency(link);
        }

        this.refreshVars();
    }

    getTargetAmount(myVar) {
        return Object.keys(myVar.value.functionTargets).length;
    }

    toggleRequired() {
        let selected = this.myForm.controls['variables'].value && this.myForm.controls['variables'].value[0];
        if (selected) {
            this.newDependencies[selected].required = !this.newDependencies[selected].required;
        }
    }

    isRequired() {
        let selected = this.myForm.controls['variables'].value && this.myForm.controls['variables'].value[0];
        return selected && this.newDependencies[selected].required;
    }

    private refreshVars() {
        this.vars = [];
        this.newLinks = Array.from(this.registry.getLinks().values());
        for (let key in this.newDependencies) {
            this.vars.push({ key: key, value: this.newDependencies[key] });
        }
        this.vars.sort((a, b) => a.key.toLowerCase() - b.key.toLowerCase());
    }

}