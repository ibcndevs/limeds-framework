import { Component, AfterViewInit, OnInit, ViewChild, ElementRef, Renderer} from '@angular/core';
import { AbstractControl, FormGroup, FormControl, Validators } from '@angular/forms';

import { BasePanel } from '../base-panel';
import { KeybindingService, Key, KeyCombo, Action} from '../../../shared/keybinding.service';
import { LimedsApi } from '../../../shared/limeds-api.service';
import { PanelType } from '../../panel-manager.service';
import { Help } from '../../../shared/help';
import { Subject } from 'rxjs/Subject';
import * as limeds from '../../../shared/limeds';

@Component({
    selector: 'lds-typeref-panel',
    templateUrl: './typeref.panel.html',
    styleUrls: ['./typeref.panel.css']
})
export class TyperefPanel extends BasePanel implements OnInit, AfterViewInit {
    model: any = {};
    myForm = new FormGroup({});
    subject$ = new Subject<TyperefEvent>();

    @ViewChild('inputName') inputName: ElementRef;

    constructor(
        private keybindings: KeybindingService,
        private renderer: Renderer,
        private api: LimedsApi
    ) {
        super(PanelType.TYPEREF);
    }

    ngOnInit() {
        this.keybindings.clearKeymap();
        this.keybindings.setKeyBinding(new KeyCombo(Key.KEY_ESC), new Action(this, this.onCancel));

        this.myForm.addControl('name', new FormControl('', [
            Validators.required,
            Validators.pattern('[A-Z][a-zA-Z0-9\\-\\_]*')
        ]));
    }

    ngAfterViewInit() {
        this.renderer.invokeElementMethod(this.inputName.nativeElement, 'focus');
    }

    onSubmit() {
        if (this.myForm.valid) {
            this.subject$.next({ action: 'submit', name: this.myForm.controls['name'].value });
        }
    }

    onCancel() {
        this.subject$.next({ action: 'close', name: '' });
    }

    setConfig(config: any) {

    }


}

interface TyperefEvent {
    action: 'close' | 'submit';
    name: string;
}