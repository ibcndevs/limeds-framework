import { Component, OnInit, AfterViewInit, ViewContainerRef, ComponentFactoryResolver } from '@angular/core';
import { AbstractControl, FormGroup, FormControl, Validators } from '@angular/forms';

import { BasePanel } from '../base-panel';
import { AreYouSurePanel, AreYouSurePanelConfig, PanelAction, PromptPanel, PromptPanelConfig, PanelResult, PanelType } from '../../../panels';
import { KeybindingService, Key, KeyCombo, Action } from '../../../shared/keybinding.service';
import { LimedsApi } from '../../../shared/limeds-api.service';
import { Help } from '../../../shared/help';
import * as limeds from '../../../shared/limeds';

import * as ace from 'brace';
import 'brace/mode/javascript';
import 'brace/theme/sqlserver';

import * as beautify from 'js-beautify';

@Component({
    selector: 'lds-adv-options-panel',
    templateUrl: './adv-options.panel.html',
    styleUrls: ['./adv-options.panel.css']
})
export class AdvOptionsPanel extends BasePanel implements OnInit, AfterViewInit {
    vm: any = {};
    myForm = new FormGroup({});


    customAspects: string[] = [];
    selectedCustomAspect: string = this.customAspects.length > 0 ? this.customAspects[0] : null;

    private errorId = '';
    private editor: ace.Editor;
    private inputs = new Map<string, string>();

    constructor(private api: LimedsApi,
        private keybindings: KeybindingService,
        private ref: ViewContainerRef,
        private resolver: ComponentFactoryResolver) {
        super(PanelType.ADV_OPTIONS);
    }

    ngOnInit() {
        this.keybindings.clearKeymap();
        this.keybindings.setKeyBinding(new KeyCombo(Key.KEY_ESC), new Action(this, this.cancelPanel));
        this.myForm.addControl('cacheSet', new FormControl(false));
        this.myForm.addControl('size', new FormControl(100));
        this.myForm.addControl('retentionDuration', new FormControl(5));
        this.myForm.addControl('retentionDurationUnit', new FormControl('MINUTES'));
        this.myForm.addControl('validationInputSet', new FormControl(false));
        this.myForm.addControl('validationOutputSet', new FormControl(false));
    }

    ngAfterViewInit() {
        this.initEditor();
        this.loadInput(this.selectedCustomAspect);
    }

    addCustomAspect() {
        let config: PromptPanelConfig = {
            title: 'New custom aspect',
            description: 'An aspect name needs to be all lower case, contain no spaces, no numbers, and is namespaced (like Java package names).',
            question: 'Please enter a unique name for your custom aspect.',
            label: 'Custom aspect name',
            placeholder: 'my.custom.aspect',
            validationFunction: (val, oldval) => {
                let regex = new RegExp(/^([^A-Z0-9\s])+$/g);
                return val !== null && val.length > 2 && regex.test(val);
            },
            okText: 'Create aspect'
        };

        let fact = this.resolver.resolveComponentFactory(PromptPanel);
        let ref = this.ref.createComponent(fact);
        ref.instance.setConfig(config);
        ref.instance.closeSubject$.subscribe(result => {
            switch (result.action) {
                case PanelAction.CONFIRM:
                    let id = result.result.input;
                    this.customAspects.push(id);
                    this.inputs.set(id, '{}');
                    this.selectCustomAspect(id);
                    setTimeout(() => this.loadInput(id));
                    break;
            }
            ref.destroy();
        });
    }

    selectCustomAspect(id: string) {
        let oldId = this.selectedCustomAspect;
        this.inputs.set(oldId, this.editor.getValue());
        this.selectedCustomAspect = id;
        this.loadInput(id);
    }

    deleteCustomAspect(id: string) {
        this.selectedCustomAspect = null;
        this.inputs.delete(id);
        this.customAspects.splice(this.customAspects.indexOf(id), 1);
    }

    private autoFormat(editor: ace.Editor) {
        let pos = editor.getCursorPosition();
        let content = beautify(editor.getValue());
        editor.setValue(content);
        editor.clearSelection();
        editor.moveCursorToPosition(pos);
    }

    private initEditor() {
        this.editor = ace.edit('code');
        this.editor.$blockScrolling = Infinity;
        this.editor.getSession().setMode('ace/mode/javascript');
        this.editor.setTheme('ace/theme/sqlserver');
        this.editor.commands.addCommand({
            name: 'autoFormat',
            bindKey: { win: 'Ctrl-Shift-F', mac: 'Command-Shift-F' },
            exec: editor => {
                this.autoFormat(editor);
            },
            readOnly: false
        });
    }

    private loadInput(id: string) {
        if (id) {
            this.editor.setValue(beautify(this.inputs.get(id)));
            this.editor.clearSelection();
        }
    }

    onSubmit() {
        try {
            let controls = this.myForm.controls;
            let result: any = {};
            // caching
            if (!controls['cacheSet'].value) {
                delete result['limeds.aspects.cache'];
            }
            else {
                result['limeds.aspects.cache'] = {
                    size: controls['size'].value,
                    retentionDuration: controls['retentionDuration'].value,
                    retentionDurationUnit: controls['retentionDurationUnit'].value
                };
            }
            //validation
            if (!controls['validationInputSet'].value && !controls['validationOutputSet'].value) {
                delete result['limeds.aspects.validator'];
            }
            else {
                result['limeds.aspects.validator'] = {
                    validateInput: controls['validationInputSet'].value,
                    validateOutput: controls['validationOutputSet'].value,
                };
            }

            // custom aspects
            this.selectCustomAspect(null);
            this.customAspects.forEach(id => {
                this.errorId = id;
                let strobj = this.inputs.get(id).trim();
                if (!strobj.startsWith('{')) {
                    throw new SyntaxError('Aspect configuration object must be a JSON object, not a primitive or array!');
                }
                let json = JSON.parse(strobj);
                result[id] = json;
            });
            // No errors, than submit, else do nothing
            this.submitPanel(result);
        } catch (err) {
            let config: AreYouSurePanelConfig = {
                title: err.name+ ' for \'' +this.errorId+'\' config object',
                description: err.message,
                question: 'Aspect configuration must be a valid JSON object.',
                buttons: 'Ok'
            };
            let fact = this.resolver.resolveComponentFactory(AreYouSurePanel);
            let ref = this.ref.createComponent(fact);
            ref.instance.setConfig(config);
            ref.instance.closeSubject$.subscribe(result => {
                ref.destroy();
                this.selectCustomAspect(this.errorId);
            });
        }
    }

    onCancel() {
        this.cancelPanel();
    }

    setConfig(config: any) {
        setTimeout(() => {
            Object.keys(config).forEach(id => {
                if ('limeds.aspects.cache' === id) {
                    // cache
                    let cacheConfig = config[id];
                    let edit = cacheConfig && cacheConfig.size && cacheConfig.retentionDuration && cacheConfig.retentionDurationUnit;
                    this.myForm.get('cacheSet').setValue(edit);
                    this.myForm.get('size').setValue(edit ? cacheConfig.size : 100);
                    this.myForm.get('retentionDuration').setValue(edit ? cacheConfig.retentionDuration : 5);
                    this.myForm.get('retentionDurationUnit').setValue(edit ? cacheConfig.retentionDurationUnit : 'MINUTES');
                }
                else if ('limeds.aspects.validator' === id) {
                    // validation
                    let validationConfig = config[id] || {};
                    this.myForm.get('validationInputSet').setValue(validationConfig.validateInput || false);
                    this.myForm.get('validationOutputSet').setValue(validationConfig.validateOutput || false);
                }
                else {
                    this.inputs.set(id, JSON.stringify(config[id]));
                    this.customAspects.push(id);
                }
            });
            this.selectedCustomAspect = this.customAspects.length > 0 ? this.customAspects[0] : null;
        });
    }

}