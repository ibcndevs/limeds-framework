import { Component, OnInit, ViewContainerRef, ComponentFactoryResolver, } from '@angular/core';
import { AbstractControl, FormGroup, FormControl, Validators } from '@angular/forms';

import { AddPropertyPanel } from './add-property.panel';
import { BasePanel } from '../base-panel';
import { KeybindingService, Key, KeyCombo, Action} from '../../../shared/keybinding.service';
import { PanelType } from '../../panel-manager.service';
import { Help } from '../../../shared/help';
import * as limeds from '../../../shared/limeds';

import { cloneDeep } from 'lodash';

@Component({
    selector: 'lds-edit-configuration-panel',
    templateUrl: './edit-configuration.panel.html',
    styleUrls: ['./edit-configuration.panel.css']
})
export class EditConfigurationPanel extends BasePanel implements OnInit {
    sliceName: string = '';
    hasTemplate: boolean;
    config: any = {};
    vm: any = {
        configEntries: []
    };
    myForm = new FormGroup({});

    constructor(
        private resolver: ComponentFactoryResolver,
        private ref: ViewContainerRef,
        private keybindings: KeybindingService) {
        super(PanelType.EDIT_CONFIGURATION);
    }

    ngOnInit() {
        this.keybindings.clearKeymap();
        this.keybindings.setKeyBinding(new KeyCombo(Key.KEY_ESC), new Action(this, this.cancelPanel));
    }

    onSubmit() {
        let result = {
            dependencies: this.vm.dependencies,
            configEntries: []
        };

        this.vm.configEntries.forEach(entry => {
            if ('array' === entry.simpleType) {
                let amount = entry.possibleValues.length;
                let results = [];
                for (let i = 0; i < amount; i++) {
                    let cc = this.myForm.get(entry.name + i);
                    if (cc && '' !== cc.value) {
                        results.push(cc.value.trim());
                    }
                }
                entry.value = results.join(',');
                delete entry.possibleValues;
            } else {
                entry.value = this.myForm.controls[entry.name].value;
                switch (entry.type) {
                    case 'boolean':
                        entry.value = !!entry.value;
                        break;
                    case 'double':
                        entry.value = parseFloat(entry.value);
                        break;
                }
            }
            delete entry.simpleType;
            result.configEntries.push(entry);

        });

        this.submitPanel(result);
    }

    onCancel() {
        this.cancelPanel();
    }

    setConfig(config: any) {
        this.config = cloneDeep(config);
        this.initTemplate(this.config.segment.getModel().templateInfo);
        this.initDependencies(this.config.segment.getModel().templateInfo, this.config.segment.getModel().dependencies);
    }

    hasNoTemplate() {
        return !this.config.segment.getModel().templateInfo;
    }

    isConfigurableInstance() {
        return this.config.segment.getModel().templateInfo && this.config.segment.getModel().templateInfo.type === 'CONFIGURABLE_INSTANCE';
    }

    isFactoryInstance() {
        return this.config.segment.getModel().templateInfo && this.config.segment.getModel().templateInfo.type === 'FACTORY_INSTANCE';
    }

    isFactory() {
        return this.config.segment.getModel().templateInfo && this.config.segment.getModel().templateInfo.type === 'FACTORY';
    }

    onAddProperty() {
        try {
            let bindings = this.keybindings.getKeymapCopy();
            let fact = this.resolver.resolveComponentFactory(AddPropertyPanel);
            let ref  = this.ref.createComponent(fact);
            // Send existing entries
            ref.instance.setConfig({
                entries: this.vm.configEntries
            });

            ref.instance.subject$.subscribe((event: any) => {
                this.keybindings.setKeymap(bindings);

                if (event.action === 'submit') {
                    let prop: any = {};
                    // Add new property
                    if (event.possibleValues && event.possibleValues.length !== 0) {
                        prop.simpleType = 'select';
                        prop.possibleValues = event.possibleValues;
                    }
                    else {
                        prop.simpleType = this.getSimpleType(event.type);
                    }
                    prop.name = event.name;
                    prop.type = event.type;
                    let validators;
                    switch (prop.type) {
                        case 'boolean':
                        case 'java.lang.String[]':
                            validators = [];
                            break;
                        case 'double':
                            validators = [Validators.required, Validators.pattern('\\d+(\\.\\d+)?')];
                            break;
                        case 'long':
                        case 'java.lang.String':
                        default:
                            validators = [Validators.required]
                    }

                    if ('java.lang.String[]' === event.type) {
                        prop.possibleValues = [''];
                        for (let i = 0; i < prop.possibleValues.length; i++) {
                            this.myForm.addControl(prop.name + i, new FormControl(prop.possibleValues[i]));
                        }
                    }
                    else {
                        // Add to formGroup
                        this.myForm.addControl(prop.name, new FormControl('', validators));
                    }

                    this.vm.configEntries.push(prop);
                    if (!this.config.segment.getModel().templateInfo) {
                        this.config.segment.getModel().templateInfo = {
                            type: 'CONFIGURABLE_INSTANCE',
                            configuration: []
                        };
                    }
                }
                else if (event.action === 'close') {
                    // do nothing
                }
                ref.destroy();
            });
        } catch (err) {
            console.error(err);
        }
    }

    onRemoveProperty(propName: string) {
        for (let idx in this.vm.configEntries) {
            let entry = this.vm.configEntries[idx];
            if (propName === entry.name) {
                this.vm.configEntries.splice(idx, 1);
                this.myForm.removeControl(propName);
                break;
            }
        }
    }

    isValid() {
        let ok = true;
        for (let key in this.myForm.controls) {
            let c = this.myForm.controls[key];
            ok = ok && !c.errors;
        }
        return ok;
    }

    private initTemplate(template: limeds.Template) {
        if (template && (template.type === 'FACTORY' || template.type === 'FACTORY_INSTANCE' || template.type === 'CONFIGURABLE_INSTANCE')) {
            this.vm.configEntries = template.configuration
                .filter(c => !c.name.startsWith('$.'))
                .map(entry => {
                    let result: any = {};
                    if (entry.possibleValues && entry.possibleValues.length !== 0) {
                        result.simpleType = 'select';
                        result.possibleValues = entry.possibleValues;
                    }
                    else {
                        result.simpleType = this.getSimpleType(entry.type);
                    }
                    result.name = entry.name;
                    result.value = entry.value
                    result.type = entry.type;
                    if (entry.description) {
                        result.description = entry.description;
                    }

                    // array
                    if ('array' === result.simpleType) {
                        result.possibleValues = entry.value ? entry.value.split(',').map(e => e.trim()) : [''];
                        for (let i = 0; i < result.possibleValues.length; i++) {
                            this.myForm.addControl(result.name + i, new FormControl(result.possibleValues[i]));
                        }
                    }
                    else {
                        // Add to formGroup
                        this.myForm.addControl(result.name, new FormControl(result.value, Validators.required));
                    }

                    return result;
                });
        }
        else {
            this.vm.configEntries = [];
        }
    }

    private initDependencies(template: limeds.Template, dependencies: { [key: string]: limeds.Dependency }) {
        this.hasTemplate = !!template;
        if (this.hasTemplate && (template.type === 'FACTORY' || template.type === 'FACTORY_INSTANCE' || template.type === 'CONFIGURABLE_INSTANCE')) {
            let deps = template.configuration
                .filter(c => c.name.startsWith('$.dependencies.'))
                .filter(c => {
                    let depName = c.name.substring('$.dependencies.'.length);
                    return !!dependencies[depName];
                })
                .map(c => {
                    let depName = c.name.substring('$.dependencies.'.length);
                    let dep = dependencies[depName];
                    dep['id'] = depName;
                    return dep;
                });

            this.vm.dependencies = {};
            deps.forEach(d => {
                let id = d['id'];
                delete d['id'];
                this.vm.dependencies[id] = d;
            });
        }
    }

    private getSimpleType(type: string) {
        let simpleType: string;
        switch (type) {
            case "java.lang.String":
            default:
            case "double":
                simpleType = 'text';
                break;
            case "long":
                simpleType = 'number';
                break;
            case "boolean":
                simpleType = 'checkbox';
                break;
            case "java.lang.String[]":
                simpleType = 'array';
                break;
        }
        return simpleType;
    }

    addEntry(configProp: limeds.ConfigProperty, idx: number) {
        configProp.possibleValues.push('');
        this.myForm.addControl(configProp.name + idx, new FormControl(''));
    }

}