import { Component, OnInit} from '@angular/core';
import { AbstractControl, FormGroup, FormControl, Validators } from '@angular/forms';
import { Subject } from 'rxjs/Subject';

import { BasePanel } from '../base-panel';
import { KeybindingService, Key, KeyCombo, Action} from '../../../shared/keybinding.service';
import { PanelType } from '../../panel-manager.service';
import { Help } from '../../../shared/help';
import * as limeds from '../../../shared/limeds';

@Component({
    selector: 'lds-add-property-panel',
    templateUrl: './add-property.panel.html'
})
export class AddPropertyPanel extends BasePanel implements OnInit {
    myForm = new FormGroup({});
    subject$ = new Subject<AddPropertyEvent>();

    private names: string[];

    constructor(
        private keybindings: KeybindingService) {
        super(PanelType.EDIT_CONFIGURATION_ADD_PROPERTY);
    }

    ngOnInit() {
        this.keybindings.clearKeymap();
        this.keybindings.setKeyBinding(new KeyCombo(Key.KEY_ESC), new Action(this, this.onCancel));

        this.myForm.addControl('name', new FormControl('', [
            Validators.required,
            Validators.pattern('[a-z][a-zA-Z0-9]*'),
            this.isUnique(this.names)
        ]));
        this.myForm.addControl('type', new FormControl('', Validators.required));
        this.myForm.addControl('restrictValues', new FormControl(''));
        this.myForm.addControl('possibleValues', new FormControl(''));
    }

    onSubmit() {
        if (this.myForm.valid) {
            let restrictValues = this.myForm.controls['restrictValues'].value;
            let values = restrictValues ? this.myForm.controls['possibleValues'].value.split(' ') : [];
            this.subject$.next({
                action: 'submit',
                name: this.myForm.controls['name'].value,
                type: this.myForm.controls['type'].value,
                possibleValues: values
            });
        }
    }

    onCancel() {
        this.subject$.next({ action: 'close', name: '', type: 'java.lang.String', possibleValues: [] });
    }

    setConfig(config: any) {
        this.names = config.entries.map(item => item.name);
    }

    isValid() {
        return !this.myForm.controls['name'].errors
            && !this.myForm.controls['type'].errors
            && (
                (this.myForm.controls['restrictValues'].value
                    && this.myForm.controls['possibleValues'].value.trim().length > 0 && this.myForm.controls['possibleValues'].value.split(' '))
                || !(this.myForm.controls['restrictValues'].value)
            );
    }

    private isUnique(names: string[]) {
        return (control: AbstractControl): ValidationResult => {
            let name = control.value;
            let found = names.some(val => {
                return name && name === val;
            });
            return found ? { notUnique: found } : null;
        }
    }

}

interface AddPropertyEvent {
    action: 'close' | 'submit';
    name: string;
    type: 'int' | 'java.lang.String' | 'boolean';
    possibleValues: string[];
}

interface ValidationResult { [key: string]: any }