import { Component, OnInit} from '@angular/core';
import { AbstractControl, FormGroup, FormControl, Validators } from '@angular/forms';

import { BasePanel } from '../base-panel';
import { KeybindingService, Key, KeyCombo, Action} from '../../../shared/keybinding.service';
import { LimedsApi } from '../../../shared/limeds-api.service';
import { PanelType } from '../../panel-manager.service';
import { Help } from '../../../shared/help';
import * as limeds from '../../../shared/limeds';

@Component({
    selector: 'lds-edit-slice-panel',
    templateUrl: './edit-slice.panel.html',
    styleUrls: ['./edit-slice.panel.css']
})
export class EditSlicePanel extends BasePanel implements OnInit {
    sliceName: string = '';
    original: any = {};
    myForm = new FormGroup({});

    constructor(
        private api: LimedsApi,
        private keybindings: KeybindingService) {
        super(PanelType.EDIT_SLICE);
    }

    ngOnInit() {
        this.keybindings.clearKeymap();
        this.keybindings.setKeyBinding(new KeyCombo(Key.KEY_ESC), new Action(this, this.cancelPanel));

        this.myForm.addControl('description', new FormControl(''));
        this.myForm.addControl('author', new FormControl('', Validators.required));
    }

    onSubmit() {
        let controls = this.myForm.controls;
        let result = {
            original: this.original,
            changes: {
                description: controls['description'].value,
                author: controls['author'].value
            }
        };
        this.submitPanel(result);
    }

    onCancel() {
        this.cancelPanel();
    }

    setConfig(config: any) {
        this.original = config;
        this.sliceName = config.id;
        setTimeout(() => {
            this.myForm.controls['description'].setValue(config.description);
            this.myForm.controls['author'].setValue(config.author);
        });
    }

    isValid() {
        for (let key in this.myForm.controls) {
            let c = this.myForm.controls[key];
            if (c.errors) {
                return false;
            }
        }
        return true;
    }
}