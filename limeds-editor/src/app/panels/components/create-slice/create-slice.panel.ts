import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormGroup, FormControl, Validators } from '@angular/forms';
import { Observable } from 'rxjs/Observable';

import { BasePanel } from '../base-panel';
import { KeybindingService, Key, KeyCombo, Action } from '../../../shared/keybinding.service';
import { LimedsApi } from '../../../shared/limeds-api.service';
import { PanelType } from '../../panel-manager.service';
import { Help } from '../../../shared/help';
import * as limeds from '../../../shared/limeds';

@Component({
    selector: 'lds-create-slice-panel',
    templateUrl: './create-slice.panel.html',
    styleUrls: ['./create-slice.panel.css']
})
export class CreateSlicePanel extends BasePanel implements OnInit {
    model: any = {};
    myForm = new FormGroup({});

    constructor(
        private keybindings: KeybindingService,
        private api: LimedsApi
    ) {
        super(PanelType.CREATE_SLICE);
    }

    ngOnInit() {
        this.keybindings.clearKeymap();
        this.keybindings.setKeyBinding(new KeyCombo(Key.KEY_ESC), new Action(this, this.cancelPanel));

        this.myForm.addControl('name', new FormControl('', [
            Validators.required,
            Validators.pattern('([a-z][a-z0-9\\-\\_]*\\.)*[a-z][a-z0-9\\-\\_]*')],
            this.isUnique.bind(this)));
        this.myForm.addControl('description', new FormControl(''));
        this.myForm.addControl('author', new FormControl('', Validators.required));
    }

    onSubmit() {
        let controls = this.myForm.controls;
        let result = {
            name: controls['name'].value,
            description: controls['description'].value,
            author: controls['author'].value
        };
        this.submitPanel(result);
    }

    onCancel() {
        this.cancelPanel();
    }

    setConfig(config: any) {

    }

    isValid() {
        for (let key in this.myForm.controls) {
            let c = this.myForm.controls[key];
            if (c.errors) {
                return false;
            }
        }
        return true;
    }

    private isUnique(control: AbstractControl): Promise<ValidationResult> {
        return new Promise(resolve => {
            this.api.listSlices().subscribe(slices => {
                let found = slices.map(slice => slice.id).some(val => val === control.value);
                resolve(found ? { notUnique: found } : null);
            });
        });
    }

}
interface ValidationResult { [key: string]: any }