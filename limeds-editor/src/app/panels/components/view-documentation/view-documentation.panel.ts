import { Component, Inject, OnInit} from '@angular/core';

import { BasePanel } from '../base-panel';
import { KeybindingService, Key, KeyCombo, Action } from '../../../shared/keybinding.service';
import { PanelManager, PanelType } from '../../panel-manager.service';
import * as limeds from '../../../shared/limeds';


@Component({
    selector: 'lds-view-doc-panel',
    templateUrl: './view-documentation.panel.html',
    styleUrls: ['./view-documentation.panel.css']
})
export class ViewDocumentationPanel extends BasePanel implements OnInit {
    title: string = "View Documentation";
    description: string;
    documentation: any;
    variable: any;
    
    constructor(
        private keybindings: KeybindingService
    ) {
        super(PanelType.VIEW_DOCUMENTATION);
    }

    ngOnInit() {
        this.keybindings.clearKeymap();
        this.keybindings.setKeyBinding(new KeyCombo(Key.KEY_ESC), new Action(this, this.cancelPanel));
    }

    setConfig(config: any) {
        if (config.title) {
            this.title = config.title;
        }
        if (config.description) {
            this.description = config.description;
        }
        if (config.documentation) {
            this.documentation = config.documentation;
        }
        if (config.var) {
            this.variable = config.var;
        }
    }

    onCancel() {
        this.cancelPanel();
    }

    isEmpty(obj: any) {
        if (!obj) {
            return true;
        } else {
            return Object.keys(obj).length === 0 ;
        }
    }

}