import { Component, AfterViewInit, OnInit, ViewChild, ElementRef, Renderer } from '@angular/core';
import { AbstractControl, FormGroup, FormControl, Validators } from '@angular/forms';

import { BasePanel } from '../base-panel';
import { KeybindingService, Key, KeyCombo, Action } from '../../../shared/keybinding.service';
import { LimedsApi } from '../../../shared/limeds-api.service';
import { PanelType } from '../../panel-manager.service';
import { Help } from '../../../shared/help';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import * as limeds from '../../../shared/limeds';

import * as ace from 'brace';
import 'brace/mode/json';
import 'brace/theme/solarized_light';

import * as beautify from 'js-beautify'

interface Types {
    [key: string]: any;
}

interface TypeDescriptor {
    package: boolean;
    name: string;
    label: string;
}

@Component({
    selector: 'lds-type-picker.panel',
    templateUrl: './type-picker.panel.html',
    styleUrls: ['./type-picker.panel.css']
})
export class TypePickerPanel extends BasePanel implements OnInit, AfterViewInit {
    model: any = {};
    myForm = new FormGroup({});
    subject$ = new Subject<TypePickerEvent>();
    searchTerm$ = new Subject<string>();
    registry: { [key: string]: { [key: string]: any } } = {};

    items: Observable<TypeDescriptor[]> = this.searchTerm$
        .debounceTime(200)
        .distinctUntilChanged()
        .switchMap(term => this.findMatches(term));

    private config: any;
    private editor: ace.Editor;
    private types: TypeDescriptor[] = [];


    private THEME_PREVIEW = 'ace/theme/solarized_light';

    @ViewChild('inputName')
    inputName: ElementRef;

    @ViewChild('typeCode')
    private typeCodeDiv: ElementRef;

    constructor(
        private keybindings: KeybindingService,
        private renderer: Renderer,
        private api: LimedsApi
    ) {
        super(PanelType.TYPE_PICKER);
    }

    ngOnInit() {
        this.keybindings.clearKeymap();
        this.keybindings.setKeyBinding(new KeyCombo(Key.KEY_ESC), new Action(this, this.onCancel));

        this.api.listTypes().subscribe((result: Types) => {
            let used = [];
            let usedTypes = [];
            // Go through all types and build types list as well as registry
            for (let typeId in result) {
                let fqn = typeId.split('_')[0];
                let version = typeId.split('_')[1];
                let idx = fqn.lastIndexOf('.');
                let pack = typeId.substring(0, idx);
                let typeName = fqn.substring(idx + 1);
                let schema = result[typeId];
                let newPackage = !(used.indexOf(pack) > -1);

                // Add package to array, if not in there yet
                if (newPackage) {
                    used.push(pack);
                    this.types.push({
                        package: true,
                        name: typeName, // to allow displaying the package when searching for typename only
                        label: pack
                    });
                }
                // Add type to list
                let newType = !(usedTypes.indexOf(fqn) > -1);
                if (newType) {
                    usedTypes.push(fqn);
                    this.types.push({
                        package: false,
                        name: fqn,
                        label: typeName
                    });
                    // Add entry to hidden package type name, to make the package appear in type only searches
                    this.types.find(o => o.label === pack).name += ';' + typeName;
                }

                // Add schema to registry
                if (!this.registry[fqn]) {
                    this.registry[fqn] = {};
                };
                this.registry[fqn][version] = schema;
            }

            // Select first type
            setTimeout(() => {
                let i = 0;
                let t = this.types[i];
                while (t.package) {
                    t = this.types[++i];
                }
                this.searchTerm$.next('');
                this.myForm.get('types').setValue(t.name);
            });
        });

        this.myForm.addControl('types', new FormControl());
        this.myForm.addControl('version', new FormControl());

        this.myForm.get('types').valueChanges.subscribe(fqn => {
            this.myForm.get('version').setValue(this.listVersions(fqn)[0]);
        });

        this.myForm.get('version').valueChanges.subscribe(version => {
            let fqn = this.myForm.get('types').value;
            this.initEditor(fqn, version);
        });
    }

    ngAfterViewInit() {
        this.editor = ace.edit(this.typeCodeDiv.nativeElement)
        let editor = this.editor;
        editor.$blockScrolling = Infinity;
        editor.getSession().setMode('ace/mode/json');
        editor.setTheme(this.THEME_PREVIEW);
        editor.setReadOnly(true);
        editor.getSession().setUseWrapMode(true);
    }

    onSubmit() {
        let fqn = this.myForm.get('types').value as string;
        let version = this.myForm.get('version').value
        let idx = fqn.lastIndexOf('.');
        let sliceId = this.config.sliceId;
        let pack = fqn.substring(0, idx);
        let isLocal = sliceId === pack;
        version = isLocal ? '${sliceVersion}' : '^' + version;
        this.subject$.next({
            action: 'submit',
            id: [fqn, version].join('_'),
            fqn: fqn,
            name: fqn.substring(idx + 1),
            version: version
        });
    }

    onCancel() {
        this.subject$.next({ action: 'close', name: '' });
    }

    setConfig(config: any) {
        this.config = config;
    }

    getOptionCss(isPackage: boolean) {
        return {
            'option-package': isPackage,
            'option-type': !isPackage
        };
    }

    /**
     * List all versions of a specific type.
     * 
     * @param fqn The fully qualified name of a type
     */
    listVersions(fqn: string): string[] {
        if (!fqn) {
            return [];
        }
        let versions = Object.keys(this.registry[fqn]);
        versions.sort((a, b) => {
            let [aMajor, aMinor, aMicro] = a.split('.').map(s => parseInt(s));
            let [bMajor, bMinor, bMicro] = b.split('.').map(s => parseInt(s));
            let major = bMajor - aMajor;
            let minor = bMinor - aMinor;
            let micro = bMicro - aMicro;
            if (major === 0) {
                if (minor === 0) {
                    return micro;
                }
                else {
                    return minor;
                }
            } else {
                return major;
            }
        });
        let latest = versions[0];
        versions.unshift(latest);
        return versions;
    }

    /**
     * Get the schema of a version of type.
     * 
     */
    getSchema(fqn: string, version: string): any {
        return this.registry[fqn][version];
    }

    search(query: string) {
        this.searchTerm$.next(query);

    }

    private initEditor(fqn: string, version: string) {
        let schema = this.registry[fqn][version];
        let content = beautify(JSON.stringify(schema))
        this.editor.setReadOnly(false);
        this.editor.setValue(content);
        this.editor.clearSelection();
        this.editor.setReadOnly(true);
    }

    private findMatches(term: string): Observable<TypeDescriptor[]> {
        let str = term.toLowerCase();
        return Observable.of(this.types.filter(obj => {
            return str === ''
                || obj.name.toLowerCase().indexOf(str) > -1
                || obj.label.toLowerCase().indexOf(str) > -1;
        }));
    }


}

interface TypePickerEvent {
    action: 'close' | 'submit';
    id?: string,
    fqn?: string;
    name?: string,
    version?: string;
}