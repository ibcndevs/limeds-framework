import { Component, Input, OnInit, ViewContainerRef } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { BasePanel } from '../base-panel';
import { KeybindingService, Key, KeyCombo, Action } from '../../../shared/keybinding.service';
import { PanelManager, PanelType, PanelAction, PanelResult } from '../../panel-manager.service';
import { Registry } from '../../../flow-editor/shared/registry.service';
import { Help } from '../../../shared/help';
import * as limeds from '../../../shared/limeds';

import { cloneDeep } from 'lodash';

@Component({
    selector: 'lds-web-api-panel',
    templateUrl: './web-api.panel.html',
    styleUrls: ['./web-api.panel.css']
})
export class WebApiPanel extends BasePanel implements OnInit {
    model: any = {};
    myForm = new FormGroup({});
    httpDocumentation: limeds.HttpDoc;


    methods: any[] = [
        {
            label: 'GET',
            value: 'GET'
        },
        {
            label: 'POST',
            value: 'POST'
        }, {
            label: 'PUT',
            value: 'PUT'
        }, {
            label: 'DELETE',
            value: 'DELETE'
        }];



    private pattern = new RegExp('^(every|delay)\\s(\\d+)\\s(days|hours|minutes|seconds|milliseconds)(\\soffset\\s(\\d+)\\s(days|hours|minutes|seconds|milliseconds))?$', 'g');
    private original: any;

    constructor(
        private keybindings: KeybindingService,
        private panelManager: PanelManager,
        private registry: Registry,
        private ref: ViewContainerRef) {
        super(PanelType.WEB_API);
    }

    ngOnInit() {
        this.keybindings.clearKeymap();
        this.keybindings.setKeyBinding(new KeyCombo(Key.KEY_ESC), new Action(this, this.cancelPanel));
        this.myForm.addControl('web', new FormControl());
        this.myForm.addControl('method', new FormControl());
        this.myForm.addControl('path', new FormControl('', Validators.pattern('\/\\S*')));
        this.myForm.addControl('authMode', new FormControl());
        this.myForm.addControl('authorityRequired', new FormControl());
        this.myForm.addControl('loadBalancing', new FormControl());
        this.myForm.addControl('scheduled', new FormControl());
        this.myForm.addControl('scheduleCommand', new FormControl());
        this.myForm.addControl('scheduleAmount', new FormControl());
        this.myForm.addControl('scheduleAmountUnit', new FormControl());
        this.myForm.addControl('scheduleOffset', new FormControl());
        this.myForm.addControl('scheduleOffsetUnit', new FormControl());
        this.myForm.addControl('event', new FormControl());
        this.myForm.addControl('eventChannels', new FormControl());
    }

    setConfig(config: any) {
        if (!config) {
            config = {};
        }
        this.original = config;
        if (config.httpDocumentation) {
            this.httpDocumentation = cloneDeep(config.httpDocumentation);
        }
        let web = Object.keys(config.web).length > 0;
        let schedule = config.schedule || '';
        let event = config.subscriptions && config.subscriptions.length > 0;

        // parse schedule
        let match, command, amount, amountUnit, offset, offsetUnit;
        if (schedule.length > 0) {
            match = this.pattern.exec(schedule);
            command = match[1];
            amount = match[2];
            amountUnit = match[3];
            offset = match[5] || '';
            offsetUnit = match[6] || ''
        }

        setTimeout(() => {
            this.myForm.controls['web'].setValue(web || false);
            this.myForm.controls['method'].setValue(config.web.method || 'GET');
            this.myForm.controls['path'].setValue(config.web.path || '');
            this.myForm.controls['authMode'].setValue(config.web.authMode || 'NONE');
            let authReq = config.web.authorityRequired ? config.web.authorityRequired.join(' ') : '';
            this.myForm.controls['authorityRequired'].setValue(authReq);
            let control = this.myForm.get('authorityRequired');
            if (this.myForm.controls['authMode'].value === 'NONE' || this.myForm.controls['authMode'].value === 'AUTHENTICATE_ONLY') {
                control.disable()
            }
            else {
                control.enable();
            }
            this.myForm.controls['loadBalancing'].setValue(config.web.loadBalancing || 'defaults.roundrobin');
            this.myForm.controls['loadBalancing'].disable();

            this.myForm.controls['scheduled'].setValue(schedule || false);
            this.myForm.controls['scheduleCommand'].setValue(command || '');
            this.myForm.controls['scheduleAmount'].setValue(amount || '');
            this.myForm.controls['scheduleAmountUnit'].setValue(amountUnit || '');
            this.myForm.controls['scheduleOffset'].setValue(offset || '');
            this.myForm.controls['scheduleOffsetUnit'].setValue(offsetUnit || '');

            this.myForm.controls['event'].setValue(event || false);
            this.myForm.controls['eventChannels'].setValue(config.subscriptions && config.subscriptions.join(' ') || '');

            this.myForm.controls['authMode'].valueChanges.subscribe(change => {
                let control = this.myForm.get('authorityRequired');
                if (change === 'NONE' || change === 'AUTHENTICATE_ONLY')
                    control.disable();
                else
                    control.enable();
            });
        });
    }

    isValid() {
        let c = this.myForm.controls;
        let web = c['web'].value;
        let schedule = c['scheduled'].value;
        let event = c['event'].value;

        let ok = true;

        if (web) {
            ok = ok && c['path'].value !== '' && !c['path'].errors;
        }

        if (schedule) {
            ok = ok && c['scheduleCommand'].value !== '' && c['scheduleAmount'].value >= 0 && c['scheduleAmountUnit'].value !== '';
            if (c['scheduleOffset'].value > 0 || c['scheduleOffsetUnit'].value !== '') {
                ok = ok && c['scheduleOffset'].value > 0 && c['scheduleOffsetUnit'].value !== '';
            }
        }

        return ok;
    }

    private reallySubmit() {
        let controls = this.myForm.controls;
        let web = controls['web'].value;
        let subscriptions = controls['event'].value && controls['eventChannels'].value.length > 0;
        let schedule = controls['scheduled'].value;
        let mySchedule = [controls['scheduleCommand'].value,
        controls['scheduleAmount'].value,
        controls['scheduleAmountUnit'].value].join(' ');
        let myOffset = (controls['scheduleOffset'].value > 0) && (controls['scheduleOffsetUnit'].value !== '') ?
            [' offset', controls['scheduleOffset'].value, controls['scheduleOffsetUnit'].value].join(' ') : '';

        let authorityRequired = [];
        if (controls['authMode'].value === 'AUTHORIZE_CONJUNCTIVE' || controls['authMode'].value === 'AUTHORIZE_DISJUNCTIVE') {
            let str = controls['authorityRequired'].value as string;
            authorityRequired = str.trim().replace(/\s+/g, ' ').split(' ');
        }
        let result = {
            web: web ? {
                method: controls['method'].value,
                path: controls['path'].value,
                authMode: controls['authMode'].value,
                authorityRequired: authorityRequired,
                loadBalancing: controls['loadBalancing'].value,
            } : {},
            schedule: schedule ? mySchedule + myOffset : '',
            subscriptions: subscriptions ? controls['eventChannels'].value.split(' ') : []
        };
        if (this.httpDocumentation !== null) {
            result['httpDocumentation'] = this.httpDocumentation;
        }
        this.submitPanel(result);
    }

    onSubmit() {
        // If web enabled, and it wasnt originally
        if (this.myForm.get('web').value && !(Object.keys(this.original.web).length > 0)) {
            let options = {
                title: 'Web endpoint enabled',
                description: 'Enabling a web endpoint will remove all input parameters, except the first one, which will now act as the incoming http request body.',
                question: 'Are you sure this is what you want?'
            };
            this.panelManager.createPanel(PanelType.ARE_YOU_SURE, this.ref, true, options).subscribe(result => {
                switch (result.action) {
                    case PanelAction.CONFIRM:
                        // Remove all but first inputDoc, if any
                        let id = this.original.id;
                        let applyDoc = this.registry.findSegment(id).getModel().documentation && this.registry.findSegment(id).getModel().documentation['apply'];
                        if (applyDoc && applyDoc.in && applyDoc.in.length > 0) {
                            while (applyDoc.in.length !== 1) {
                                applyDoc.in.pop();
                            }
                        }
                        setTimeout(() => {
                            this.reallySubmit();
                        });
                        break;
                    case PanelAction.CANCEL:
                        break;
                }
            });
        }
        else {
            this.reallySubmit();
        }
    }

    setupQuery() {
        this.panelManager.createPanel(PanelType.SETUP_QUERY, this.ref, true, { httpDoc: this.httpDocumentation }).subscribe(result => {
            switch (result.action) {
                case PanelAction.CONFIRM:
                    let httpDoc = result.result.httpDoc;
                    this.httpDocumentation = httpDoc;
                    break;
                case PanelAction.CANCEL:

                    break;
            }
        });
    }
}