import { Component, Input, OnInit, ElementRef } from '@angular/core';

@Component({
    selector: 'lds-panel-container',
    templateUrl: './container.panel.html',
    styleUrls: ['./container.panel.css']
})
export class PanelContainer implements OnInit {
    @Input() minWidth: string = '80%';
    @Input() minHeight: string = '80%';
    @Input() maxWidth: string;
    @Input() maxHeight: string;
    @Input() width: string = '';
    @Input() height: string = '';
    
    constructor(private ref: ElementRef) { }
    
    ngOnInit() {
        let el = this.ref.nativeElement.querySelector('div.my-panel-item') as HTMLElement;
        el.style.minWidth = this.minWidth;
        el.style.minHeight = this.minHeight;

        if (this.maxWidth) {
            el.style.maxWidth = this.maxWidth;
        }
        if (this.maxHeight) {
            el.style.maxHeight = this.maxHeight;
        }

        if (this.width) {
            el.style.width = this.width;
        }
        if (this.height) {
            el.style.height = this.height;
        }
    }
}