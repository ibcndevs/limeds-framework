import { NgModule, ModuleWithProviders} from  '@angular/core';

import { PanelManager,
    AddLinkPanel,
    AddPropertyPanel,
    AdvOptionsPanel,
    AreYouSurePanel,
    BasePanel,
    ColorPanel,
    ConfigPanel,
    DocumentationPanel,
    DocVariableComponent,
    CreateSlicePanel,
    CreateSegmentPanel,
    EditConfigurationPanel,
    EditSlicePanel,
    FactoryInstancePanel,
    ImportSegmentPanel,
    MultiLinkPanel,
    PromptPanel,
    RawCodePanel,
    SaveAsPanel,
    ScriptPanel,
    SetupQueryPanel,
    TypePickerPanel,
    TyperefPanel,
    TypesPanel,
    VarRenamePanel,
    ViewErrorPanel,
    ViewDocumentationPanel,
    ViewInstallablePanel,
    WebApiPanel
} from './index';

import { PanelContainer } from './container';

import { SharedModule } from'../shared/shared.module';


@NgModule({
    imports: [
        SharedModule
    ],
    declarations: [
        AddLinkPanel,
        AddPropertyPanel,
        AdvOptionsPanel,
        AreYouSurePanel,
        ColorPanel,
        ConfigPanel,
        DocumentationPanel,
        DocVariableComponent,
        CreateSlicePanel,
        CreateSegmentPanel,
        EditConfigurationPanel,
        EditSlicePanel,
        FactoryInstancePanel,
        ImportSegmentPanel,
        MultiLinkPanel,
        PromptPanel,
        RawCodePanel,
        SaveAsPanel,
        ScriptPanel,
        SetupQueryPanel,
        TypePickerPanel,
        TyperefPanel,
        TypesPanel,
        VarRenamePanel,
        WebApiPanel, 
        ViewErrorPanel,
        ViewDocumentationPanel,
        ViewInstallablePanel,
        PanelContainer
    ],
    entryComponents: [
        AddLinkPanel,
        AddPropertyPanel,
        AdvOptionsPanel,
        AreYouSurePanel,
        ColorPanel,
        ConfigPanel,
        DocumentationPanel,
        DocVariableComponent,
        CreateSlicePanel,
        CreateSegmentPanel,
        EditConfigurationPanel,
        EditSlicePanel,
        FactoryInstancePanel,
        ImportSegmentPanel,
        MultiLinkPanel,
        PromptPanel,
        RawCodePanel,
        SaveAsPanel,
        ScriptPanel,
        SetupQueryPanel,
        TypePickerPanel,
        TyperefPanel,
        TypesPanel,
        VarRenamePanel,
        WebApiPanel, 
        ViewErrorPanel,
        ViewDocumentationPanel,
        ViewInstallablePanel,
        PanelContainer
    ]
})
export class PanelsModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: PanelsModule,
            providers: [
                PanelManager
            ]
        };
    }
}