import { NgModule } from '@angular/core';

import { SharedModule } from '../shared/shared.module';
import { PanelsModule } from '../panels/panels.module';

import { slicesRouting } from './slices.routing';
import { Slices } from './slices.component';
import { LoginComponent } from '../login/login.component';

@NgModule({
    imports: [
        SharedModule,
        slicesRouting
    ],
    declarations: [
        Slices
    ]
})
export class SlicesModule { }