import { Routes, RouterModule } from '@angular/router';

import { Slices } from './slices.component';
import { LoginComponent } from '../login/login.component';

const slicesRoutes: Routes = [
    { path: '', redirectTo: '/slices', pathMatch: 'full'}, 
    { path: 'slices', component: Slices }
];

export const slicesRouting = RouterModule.forChild(slicesRoutes);