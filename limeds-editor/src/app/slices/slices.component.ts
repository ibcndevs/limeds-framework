import { Component, OnInit, HostListener, ViewContainerRef } from '@angular/core';
import { Router, Route } from '@angular/router';

import { KeybindingService } from '../shared/keybinding.service';
import { LimedsApi } from '../shared/limeds-api.service';
import { PanelManager, PanelType, PanelResult, PanelAction, PromptPanelConfig } from '../panels';
import { Topbar } from '../shared/topbar/topbar.component';
import * as limeds from '../shared/limeds';

import { cloneDeep } from 'lodash';

@Component({
    templateUrl: './slices.component.html',
    styleUrls: ['./slices.component.css']
})
export class Slices implements OnInit {
    slices: limeds.ISlice[];
    errorMessage;
    releaseVersion: string;
    coreVersion: string;

    private activeId: string = '';
    private activeVersion: string = '';


    constructor(
        private api: LimedsApi,
        private keybindings: KeybindingService,
        private router: Router,
        private panelManager: PanelManager,
        private viewContainerRef: ViewContainerRef) {
    }

    ngOnInit() {
        this.keybindings.addEventSource(window);
        this.listSlices();
        this.api.getHostInfo().subscribe(info => {
            this.releaseVersion = info.releaseVersion;
            let coreVersion = info.modules.find(item => 'org.ibcn.limeds.core' === item.id).version;
            this.coreVersion = coreVersion.substr(0, coreVersion.lastIndexOf('.'));
        });
    }



    openSlice(flow: limeds.ISlice) {
        let id = limeds.Utils.encodeId(flow.id);
        let version = limeds.Utils.encodeVersion(flow.version);
        this.router.navigate(['/view', id, { version: version }]);
    }

    createSlice() {
        let sub = this.panelManager.createPanel(PanelType.CREATE_SLICE, this.viewContainerRef, true);
        sub.subscribe((result) => this.onCreatePanelClose(result));
    }

    removeSlice(slice: limeds.ISlice) {
        this.api.removeSlice(slice.id, slice.version).subscribe(slice => this.listSlices(), (error) => console.error(error));
    }

    editSlice(slice: limeds.ISlice) {
        let sub = this.panelManager.createPanel(PanelType.EDIT_SLICE, this.viewContainerRef, true, slice);
        sub.subscribe((result) => this.onEditPanelClose(result));
    }

    copySlice(slice: limeds.ISlice) {
        let config: PromptPanelConfig = {
            description: 'This will make a copy of the ' + slice.id + ' slice. It will copy the description and owner, but these can be changed afterwards through the edit panel, if so desired.',
            question: 'Please enter a new slice name, using lower case input.',
            label: 'New slice name',
            placeholder: 'org.ibcn.product.module',
            okText: 'Copy',
            title: 'Copy Slice',
            titleIcon: 'fa-clone',
            value: slice.id,
            validationFunction: (newValue) => {
                let regex = new RegExp(/^([a-z][a-z0-9\-\_]*\.)*[a-z][a-z0-9\-\_]*$/g);
                let duplicate = this.slices.map(slice => slice.id).some(id => id === newValue);
                return regex.test(newValue) && !duplicate;
            }
        }
        let sub = this.panelManager.createPanel(PanelType.PROMPT, this.viewContainerRef, true, config);
        sub.subscribe(result => this.onCopyPanelClose(result, slice))
    }

    deploySlice(slice: limeds.ISlice) {
        this.api.deploySlice(slice.id, slice.version, true).subscribe(slice => this.listSlices(), error => console.log(error));
    }

    undeploySlice(slice: limeds.ISlice) {
        this.api.undeploySlice(slice.id, slice.version, true).subscribe(slice => this.listSlices(), error => console.log(error));
    }

    onMouseOver(flow: limeds.ISlice) {
        this.activeId = flow.id;
        this.activeVersion = flow.version;
    }

    onMouseOut() {
        this.activeId = '';
        this.activeVersion = '';
    }

    openWebsite() {
        window.open('http://limeds.intec.ugent.be/');
    }

    openWiki() {
        window.open('https://bitbucket.org/ibcndevs/limeds-framework/wiki/Home');
    }

    openSwagger() {
        window.open('/swagger/');
    }

    openDebugView() {
        window.open('/editor/errors');
    }

    openConfigurationEditor() {
        this.router.navigate(['/configurations']);
    }

    openInstallables() {
        this.router.navigate(['/install']);
    }

    private listSlices() {
        this.api.listSlices().subscribe(
            slices => {
                this.slices = [];
                this.slices = slices;
            },
            error => console.error(error)
        );
    }

    private onCreatePanelClose(data: PanelResult) {
        switch (data.action) {
            case PanelAction.CONFIRM:
                let res = data.result;
                let slice = new limeds.Slice(res.name, res.author, res.description);
                this.api.createSlice(slice).subscribe(
                    slices => this.listSlices(),
                    error => console.log(error)
                );
                break;

            case PanelAction.CANCEL:
            default:
            // do nothing
        }
    }

    private onEditPanelClose(data: PanelResult) {
        switch (data.action) {
            case PanelAction.CONFIRM:
                // update flow
                let changes = data.result.changes;
                var updatedSlice = Object.assign(data.result.original, changes);
                this.api.updateSlice(updatedSlice, updatedSlice.id, updatedSlice.version).subscribe(
                    slices => this.listSlices(),
                    error => console.log(error)
                );
                break;

            case PanelAction.CANCEL:
            default:
            // do nothing
        }
    }

    private onCopyPanelClose(data: PanelResult, originalSlice: limeds.ISlice) {
        switch (data.action) {
            case PanelAction.CONFIRM:
                let id = data.result.input;
                let copy = cloneDeep(originalSlice) as limeds.ISlice;
                let originalId = copy.id;
                // let regex = new RegExp('^'+originalId, 'g');
                let regex = new RegExp('^' + originalId + '((?=\\.[A-Z][^\\.]*$)|$)', 'g');
                let str = JSON.stringify(copy, (k: string, v: any) => {
                    switch (k) {
                        case 'id':
                        case 'fromId':
                        case 'toId':
                            return this.replaceIfMatch(v, id, regex);
                        // return v.replace(regex, id);
                        case 'toIds':
                            return (v as string[]).map(item => this.replaceIfMatch(item, id, regex));
                        // return (v as string[]).map(item => item.replace(regex, id));
                        case 'codeAttachments':
                        case 'functionTargets':
                            Object.keys(v).forEach(key => {
                                let newKey = this.replaceIfMatch(key, id, regex);
                                // let newKey = key.replace(regex, id);
                                if (newKey !== key) {
                                    v[newKey] = v[key];
                                    delete v[key];
                                }
                            });
                            return v;
                        case 'status':
                            return 'undeployed';
                        default:
                            return v;
                    }
                });
                let slice = JSON.parse(str);
                this.api.createSlice(slice).subscribe(result => {
                    this.listSlices();
                });
                break;
            case PanelAction.CANCEL:
        }
    }

    /**
     * Search in haystack using regex. The regex is used to match Ánd to replace found match with newNeedle.
     */
    private replaceIfMatch(haystack: string, newNeedle: string, regex: RegExp): string {
        if (regex.test(haystack)) {
            return haystack.replace(regex, newNeedle);
        }
        return haystack;
    }

}