import { AfterContentInit, Component, ContentChildren, EventEmitter, Output, QueryList } from '@angular/core';
import { ContextMenuItem } from './menu-item.component';

import { Subject } from 'rxjs/Subject';

@Component({
    selector: 'lds-ctx-menu',
    templateUrl: './context-menu.component.html',
    styleUrls: ['./context-menu.component.css']
})
export class ContextMenu implements AfterContentInit{
    menuAction$ = new Subject<string>();
    @ContentChildren(ContextMenuItem) items: QueryList<ContextMenuItem>;

    constructor() { }

    ngAfterContentInit() {
        this.items.forEach(child => {
            child.emitter.subscribe(value => {
                this.menuAction$.next(value);
            });
        });
    }
}