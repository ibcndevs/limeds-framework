import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
    selector: 'lds-ctx-menu-item',
    template: `
        <div class="lds-ctx-menu-item" (click)="emitAction()">
            <span class="logo">
            	<i class="fa {{faLogoClass}}"></i>
            </span>
            <span class="title">
                {{title}}
            </span>
        </div>
    `
})
export class ContextMenuItem {
    @Input() title: string;
    @Input('fa-logo-class') faLogoClass: string;
    @Input() action: string;
    @Output() emitter = new EventEmitter<string>();

    emitAction() {
        this.emitter.emit(this.action);
    }
}