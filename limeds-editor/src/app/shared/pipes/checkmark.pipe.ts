import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Pipe({ name: 'checkmark'})
export class CheckmarkPipe implements PipeTransform {
    constructor(
        private sanitizer: DomSanitizer
    ) { }

    transform(check: boolean) {
        return check ? this.sanitizer.bypassSecurityTrustHtml('&#x2714;') : this.sanitizer.bypassSecurityTrustHtml('&#x2718;');
    }
}