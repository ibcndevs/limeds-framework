import { Pipe, PipeTransform } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { LimedsApi } from '../limeds-api.service';

/**
 * This pipe outputs an Observable, it must be followed by an AsyncPipe. 
 * To output as json, follow it up with a JsonPipe.
 */
@Pipe({
    name: 'resolve'
})
export class ResolvePipe implements PipeTransform {
    constructor(private api: LimedsApi) { }

    transform(value: any): Observable<string> {
        if (!value) {
            return Observable.of('no schema defined');
        }
        let schema = JSON.stringify(value);
        if (schema.length === 0) {
            return Observable.of('no schema defined');
        }
        return this.api.resolveSchema(schema);
    }
}