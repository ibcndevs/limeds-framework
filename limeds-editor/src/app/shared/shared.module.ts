import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';

import { ContextMenu } from './context-menu/context-menu.component';
import { ContextMenuItem } from './context-menu/menu-item.component';
import { Help } from './help/help.component';
import { Topbar } from './topbar/topbar.component';

import { APP_CONFIG, CONFIG } from './config';
import { KeybindingService } from './keybinding.service';
import { LimedsApi } from './limeds-api.service';

import {
    CheckmarkPipe,
    ResolvePipe
} from './pipes';

import { BsDropdownModule } from 'ng2-bootstrap/dropdown';
import { TabsModule } from 'ng2-bootstrap/tabs';
import { TooltipModule, } from 'ng2-bootstrap/tooltip';

@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        BsDropdownModule.forRoot(),
        TooltipModule.forRoot(),
        TabsModule.forRoot()
    ],
    declarations: [
        ContextMenu,
        ContextMenuItem,
        Help,
        Topbar,
        CheckmarkPipe,
        ResolvePipe
    ],
    exports: [
        CommonModule,
        ReactiveFormsModule,
        BsDropdownModule,
        TooltipModule,
        TabsModule,
        ContextMenu,
        ContextMenuItem,
        Help,
        CheckmarkPipe,
        ResolvePipe,
        Topbar
    ]
})
export class SharedModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: SharedModule,
            providers: [
                { provide: APP_CONFIG, useValue: CONFIG },
                KeybindingService,
                LimedsApi
            ]
        };
    }
}