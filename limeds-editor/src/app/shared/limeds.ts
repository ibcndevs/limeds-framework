import { JS_TEMPLATE } from './code-templates';

/**
 * A complete slice
 */
export interface ISlice {
    id: string;
    description: string;
    author: string;
    status?: string;
    version: string;
    segments: ISegmentModel[];
    uiContext?: UiContext;
    codeAttachments: any;
    typeDefinitions?: any;
    types?: any;
}

export class Slice implements ISlice {
    id: string;
    description: string;
    author: string;
    status: string;
    version: string;
    segments: ISegmentModel[];
    uiContext: UiContext;
    codeAttachments: any;
    typeDefinitions: any;
    types?: any;

    constructor(id: string, author: string, description?: string) {
        this.id = id;
        this.description = description || '';
        this.author = author;
        this.status = 'undeployed';
        this.version = '0.0.0';
        this.segments = [];
        this.uiContext = {
            nodes: [],
            links: []
        };
        this.codeAttachments = {};
        this.typeDefinitions = {};
    }
}

/**
 * The new ui context.
 */
export interface UiContext {
    nodes: UiNode[];
    links: UiLink[];
}

export interface ISliceElement {
    getView(): ILinkView | ISegmentView;
    getDisplayObject(): createjs.DisplayObject;

    setView(view: ILinkView | ISegmentView): void;
    setDisplayObject(displayObject: createjs.DisplayObject): void;
}

export interface ILink extends ISliceElement {
    view: ILinkView;
    displayObject?: createjs.DisplayObject;

    getView(): ILinkView;
    getDisplayObject(): createjs.DisplayObject;

    setView(view: ILinkView): void;
    setDisplayObject(displayObject: createjs.DisplayObject): void;
}

export class Link implements ILink {
    public view;
    public displayObject;

    constructor(view: ILinkView) {
        this.view = view;
    }

    public getView(): ILinkView {
        return this.view;
    }


    public getDisplayObject(): createjs.DisplayObject {
        return this.displayObject;
    }

    public setView(view: ILinkView) {
        this.view = view;
    }

    public setDisplayObject(displayObject: createjs.DisplayObject) {
        this.displayObject = displayObject;
    }
}

export interface UiLink {
    fromId: string;
    fromVar: string;
    toId: string;
    toIds: string[];
    multi: boolean;
    x: number;
    y: number;
    t: number;
}

export interface ILinkView extends UiLink {
    fromId: string;
    fromVar: string;
    toId: string;
    toIds: string[];
    multi: boolean;
    x: number;
    y: number;
    t: number;

    getId(): string;
    toUiLink(): UiLink;
}

export class LinkView implements ILinkView {
    public fromId: string;
    public fromVar: string;
    public toId: string;
    public toIds: string[];
    public multi: boolean;
    public t: number;
    public x: number;
    public y: number;

    constructor(fromId: string, fromVar: string, toId: string, multi?: boolean, toIds?: string[], t?: number, x?: number, y?: number) {
        this.fromId = fromId;
        this.fromVar = fromVar;
        this.toId = toId;
        this.toIds = toIds ? toIds : [];
        this.multi = multi ? multi : false;
        this.t = t || 0.65;
        this.x = x || 0;
        this.y = y || 0;
    }

    public getId() {
        if (this.multi) {
            return this.fromId + '#' + this.fromVar + '|' + this.toIds.join('-');
        } else {
            return this.fromId + '#' + this.fromVar + '|' + this.toId;
        }
    }

    public toUiLink() {
        return {
            fromId: this.fromId,
            fromVar: this.fromVar,
            toId: this.toId,
            toIds: this.toIds,
            multi: this.multi,
            t: this.t,
            x: this.x,
            y: this.y
        };
    }
}

export interface ISegment extends ISliceElement {
    view: ISegmentView;
    model: ISegmentModel;
    displayObject?: createjs.DisplayObject;

    getId(): string;
    getView(): ISegmentView;
    getModel(): ISegmentModel;
    getDisplayObject(): createjs.DisplayObject;

    setView(view: ISegmentView): void;
    setModel(model: ISegmentModel): void;
    setDisplayObject(displayObject: createjs.DisplayObject): void;
}

export class Segment implements ISegment {
    public view;
    public model;
    public displayObject;

    constructor(view: ISegmentView, model?: ISegmentModel) {
        this.view = view;
        this.model = model || new SegmentModel(view.getId());
    }

    public getId(): string {
        return this.view.id;
    }

    public getView(): ISegmentView {
        return this.view;
    }

    public getModel(): ISegmentModel {
        return this.model;
    }

    public getDisplayObject(): createjs.DisplayObject {
        return this.displayObject;
    }

    public setView(view: ISegmentView) {
        this.view = view;
    }

    public setModel(model: ISegmentModel) {
        this.model = model;
    }

    public setDisplayObject(displayObject: createjs.DisplayObject) {
        this.displayObject = displayObject;
    }
}

export type SegmentViewType = 'SLICE_LOCAL' | 'IMPORTED' | 'INSTANTIATED';

export interface UiNode {
    id: string;
    versionExpr: string;
    label: string;
    type: SegmentViewType;
    x: number;
    y: number;
    color: string;
}

export interface ISegmentView extends UiNode {
    width: number;
    height: number;
    getId(): string;
    getVersionExpression(): string;

    getCenterPoint(): createjs.Point;
    getPositionTo(view: ISegmentView): 'north' | 'south' | 'east' | 'west';
    toUiNode(): UiNode;
}

export class SegmentView implements ISegmentView {
    public id: string;
    public versionExpr: string;
    public label: string;
    public type: SegmentViewType;
    public x: number;
    public y: number;
    public width: number;
    public height: number;
    public color: string;

    constructor(id: string, versionExpr: string, label: string, x: number, y: number, width: number, height: number, type?: SegmentViewType, color?: string) {
        this.id = id;
        this.versionExpr = versionExpr;
        this.label = label;
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.type = type;
        this.color = color;
    }

    public getId() {
        return this.id;
    }

    public getVersionExpression() {
        return this.versionExpr;
    }

    public getCenterPoint() {
        return new createjs.Point(this.x + (this.width / 2), this.y + (this.height / 2));
    }

    public getPositionTo(view: SegmentView): ('north' | 'south' | 'east' | 'west') {
        let c1 = this.getCenterPoint();
        let c2 = view.getCenterPoint();
        let left1 = this.x;
        let right1 = this.x + this.width;
        let left2 = view.x;
        let right2 = view.x + view.width;
        let m = (c2.y - c1.y) / (c2.x - c1.x);
        let e = 10;

        let direction;
        if (isNaN(m)) {
            direction = c1.y < c2.y ? 'south' : 'north';
        }
        else if (-1 < m && m < 1 && (right1 + e < left2 || left1 > right2 + e)) {
            direction = c1.x < c2.x ? 'east' : 'west';
        }
        else if (m <= -1 || 1 <= m) {
            direction = c1.y < c2.y ? 'south' : 'north';
        }
        else {
            direction = c1.y < c2.y ? 'south' : 'north';
        }
        return direction;
    }

    public toUiNode() {
        return {
            id: this.id,
            versionExpr: this.versionExpr,
            label: this.label,
            type: this.type,
            x: this.x,
            y: this.y,
            color: this.color
        };
    }
}

export interface ISegmentModel {
    id: string;
    version: string;
    description?: string;
    export: boolean;
    language: Language;
    tags: string[];
    serviceProperties: string[];
    interfaces: string[];
    subscriptions: string[];
    templateInfo?: Template;
    documentation?: Documentation[];
    httpDocumentation?: HttpDoc;
    httpOperation?: HttpOperation;
    schedule?: string;
    dependencies: { [key: string]: Dependency };
    cache?: Cache;
    httpAssertions: HttpAssertion[];
    configureAspects?: any;
}

export class SegmentModel implements ISegmentModel {
    id: string;
    version: string;
    description: string;
    export: boolean;
    language: Language;
    tags: string[];
    serviceProperties: string[];
    interfaces: string[];
    subscriptions: string[]
    templateInfo: Template;
    httpOperation: HttpOperation;
    schedule: string;
    dependencies: { [key: string]: Dependency };
    cache: Cache;
    httpAssertions: HttpAssertion[];

    constructor(id: string) {
        this.id = id;
        this.version = '0.0.0';
        this.export = false;
        this.language = 'javascript';
        this.tags = [];
        this.serviceProperties = [];
        this.interfaces = [
            'org.ibcn.limeds.FunctionalSegment'
        ];
        this.subscriptions = [];
        this.httpOperation = <any>{};
        this.dependencies = {};
        this.httpAssertions = [];
    }
}

export interface Template {
    type: 'FACTORY' | 'FACTORY_INSTANCE' | 'CONFIGURABLE_INSTANCE' | 'OTHER' | 'OTHER_FACTORY';
    name?: string;
    factoryId?: string;
    configuration: ConfigProperty[];
}

export interface Documentation {
    id: string;
    description?: string;
    in?: NamedDoc[];
    out?: UnnamedDoc;
}

export interface NamedDoc {
    label: string;
    schema: any;
}

export interface UnnamedDoc {
    schema: any
}

export interface HttpDoc {
    querySchema: any;
    pathInfo: string[]
}

export interface ConfigProperty {
    name: string;
    type: string;
    value: string;
    possibleValues?: string[];
    description?: string;
}

export interface HttpOperation {
    method: 'GET' | 'POST' | 'PUT' | 'DELETE';
    path: string;
    supportedQueries?: string[];
    authorityRequired: string[];
    authMode: 'NONE' | 'AUTHENTICATE_ONLY' | 'AUTHORIZE_CONJUNCTIVE' | 'AUTHORIZE_DISJUNCTIVE';
    loadBalancing: string;
}

export interface Dependency {
    required: boolean;
    collection: boolean;
    type: string;
    functionTargets: { [key: string]: string };
}

export interface Cache {
    size: number;
    retentionDuration: number;
    retentionDurationUnit: string;
}

export interface HttpAssertion {
    target: string;
    method: 'HEAD' | 'GET' | 'PUT' | 'POST' | 'DELETE' | 'OPTIONS';
    request: any;
    responseValidatorScript: string;
}

export enum LimedsType {
    Segment,
    Link,
    None
}

export interface Configurations {
    [key: string]: Template
}

export interface Installable {
    id: string;
    version: string;
    segments?: string[];
    types?: string[];
    installed: boolean;
}

export interface IssueMap {
    [key: string]: Issue[];
}

export interface Issue {
    timestamp: number;
    source: string;
    type: string;
    message: string;
    location: string;
    trace: Trace[];
}

export interface Trace {
    segmentId: string;
    location: string;
}

export interface HostInfo {
    releaseVersion: string;
    modules: ModuleInfo[];
}

export interface ModuleInfo {
    id: string;
    version: string;
    status: 'active' | 'installed' | 'resolved' | 'processing';
}

export class Utils {
    private static DOT_REPLACEMENT = '~';
    private static REPLACE_PATTERN = /\~/g;

    static encodeId(id: string): string {
        return encodeURIComponent(id.replace(/\./g, Utils.DOT_REPLACEMENT));
    }

    static decodeId(id: string): string {
        return decodeURIComponent(id).replace(Utils.REPLACE_PATTERN, '.');
    }

    static encodeVersion(version: string): string {
        return encodeURIComponent(version.replace(/\./g, Utils.DOT_REPLACEMENT));
    }

    static decodeVersion(version: string): string {
        return decodeURIComponent(version).replace(Utils.REPLACE_PATTERN, '.');
    }
}

export type Language = 'javascript';
export type CodeTemplateType = 'single_component';

export class CodeTemplates {
    private static initialized = false;
    private static templates: Map<Language, Map<CodeTemplateType, string>> = new Map<Language, Map<CodeTemplateType, string>>();

    private static languages: Language[] = ['javascript'];
    private static types: CodeTemplateType[] = ['single_component'];

    private static inputPattern = /^(\s*var\s+apply\s*=\s*function\s*\(\s*).*(\s*\)\s*?\{?\s*)$/gm;
    private static varReplacePattern = /^(\s*var\s+apply\s*=\s*function\s*\(\s*).*?((?:, httpContext)?\s*\)\s*?\{?\s*)$/gm;

    private static init() {
        for (let lang of this.languages) {
            let mapping = new Map<CodeTemplateType, string>();
            for (let type of this.types) {
                mapping.set(type, JS_TEMPLATE);
            }
            this.templates.set(lang, mapping);
        }
        this.initialized = true;
    }

    /**
     * Replace the input of apply(input), with all documenten input paramters.
     * 
     * @param {string} template The template to replace vars in
     * @param {Documentation=} applyDoc The documentation of apply, it can be null, or omitted.
     */
    public static replaceVars(template: string, applyDoc: Documentation): string {
        if (!applyDoc || !applyDoc.in || applyDoc.in.length === 0) {
            return template;
        }
        let vars = applyDoc.in
            .map(a => a.label).join(', ');
        return template.replace(this.varReplacePattern, '$1' + vars + '$2');
    }

    /**
     * Return the CodeTemplate for the given language and type.
     * Variables of the form ${{varName}} are replaced in the template.
     * 
     * NOTICE: inspect each template carefully to make sure replacementes are correct!!
     * 
     * @param language The language of the scripts
     * @param type The type of template
     * @param config The inputDocumentation, to replace input with any parameters that are renamed or added.
     */
    static getFor(language: Language, type: CodeTemplateType, applyDoc: Documentation): string {
        if (!this.initialized) {
            this.init();
        }
        let script = CodeTemplates.templates.get(language.toLowerCase() as Language).get(type);
        return this.replaceVars(script, applyDoc);
    }

    /**
     * Functions to easily change the arguments from (input) to (input, httpContext).
     */
    static toggleHttpArgs(script: string, httpOn: boolean): string {
        if (httpOn) {
            return script.replace(this.inputPattern, '$1input, httpContext$2');
        }
        else {
            let repl = script.replace(this.inputPattern, '$1input) {');
            return repl;
        }
    }
}