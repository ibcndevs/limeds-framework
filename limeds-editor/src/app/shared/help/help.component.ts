import { Component, HostListener, ElementRef, Input } from '@angular/core';

@Component({
    selector: 'lds-help',
    templateUrl: './help.component.html',
    styleUrls: ['./help.component.css']
})
export class Help {
    private offset = { x: 4, y: -4 };

    @Input('logoClass')
    logoClass: string = 'fa-question-circle';

    @Input('htmlContent')
    htmlContent: string;

    @Input('pre')
    pre: boolean = false;

    constructor(private ref: ElementRef) { }

    @HostListener('mouseover')
    onMouseOver() {
        let logo = (this.ref.nativeElement as HTMLElement).querySelector('.lds-help-logo') as HTMLElement;
        let item = (this.ref.nativeElement as HTMLElement).querySelector('.lds-help-item') as HTMLElement;
        let panelContainer = (this.ref.nativeElement as HTMLElement).querySelector('lds-panel-container') as HTMLElement;
        item.style.display = 'block';
        item.style.left = (this.getOffsetLeft(logo) + logo.offsetWidth + this.offset.x) + 'px';
        let top = (this.getOffsetTop(logo) - item.offsetHeight);
        item.style.top = top <= 150 ? Math.max(this.getOffsetTop(logo) + this.offset.y, top) + 'px' : top + 'px';
    }

    @HostListener('mouseout')
    onMouseOut() {
        let el = (this.ref.nativeElement as HTMLElement).querySelector('.lds-help-item') as HTMLElement;
        el.style.display = 'none';
    }

    private getOffsetTop(el: HTMLElement) {
        var offsetTop = 0;
        do {
            if (!isNaN(el.offsetTop)) {
                offsetTop += el.offsetTop;
            }
        } while (el = el.offsetParent as HTMLElement);
        return offsetTop;
    }

    private getOffsetLeft(el: HTMLElement) {
        var offsetLeft = 0;
        do {
            if (!isNaN(el.offsetLeft)) {
                offsetLeft += el.offsetLeft;
            }
        } while (el = el.offsetParent as HTMLElement);
        return offsetLeft;
    }

    getCss() {
        return { pre: this.pre };
    }
}