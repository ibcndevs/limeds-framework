import { InjectionToken } from '@angular/core';

export const APP_CONFIG = new InjectionToken<Config>('app.config');

export interface Config {
    debug: boolean;
    connector_border_color: string;
    connector_border_thickness: number;
    connector_size: number;
    segment_border_thickness: number;
    segment_color: string;
    segment_corner_radius: number;
    segment_height: number;
    segment_title_color: string;
    segment_title_font: string;
    segment_title_offset_top: number;
    segment_width: number;
    link_arrow_location: number;
    link_change_handle_border_thickness: number;
    link_change_handle_border_color: string;
    link_change_handle_color: string;
    link_change_handle_location: number;
    link_change_handle_radius: number;
    link_color: string;
    link_drawing_color: string;
    link_selected_color: string;
    link_selected_thickness: number;
    link_thickness: number;
    link_multi_color: string;
    link_multi_radius: number;
    link_multi_t: number;
    link_multi_thickness: number;
    link_multi_multiplex_thickness: number;
    selection_brackets_color: string;
    selection_brackets_offset: number;
    selection_brackets_size: number;
    selection_brackets_thickness: number;
    zoom_max: number;
    zoom_min: number;
    zoom_step: number;
}

export const CONFIG: Config = {
    debug: false,
    connector_border_color: '#eee',
    connector_border_thickness: 2,
    connector_size: 12,
    segment_border_thickness: 4,
    segment_color: '#8ED944',
    segment_corner_radius: 4,
    segment_height: 38,
    segment_title_color: "black",
    segment_title_font: "13px Arial",
    segment_title_offset_top: 10,
    segment_width: 150,
    link_arrow_location: 0.5,
    link_change_handle_border_thickness: 1,
    link_change_handle_border_color: 'blue',
    link_change_handle_color: 'orange',
    link_change_handle_location: 0.62,
    link_change_handle_radius: 6,
    link_color: '#666666',
    link_drawing_color: '#999',
    link_selected_color: 'blue',
    link_selected_thickness: 5,
    link_thickness: 2,
    link_multi_color: '#7c7c7c',
    link_multi_radius: 7,
    link_multi_t: 0.65,
    link_multi_thickness: 1.5,
    link_multi_multiplex_thickness: 3,
    selection_brackets_color: 'blue',
    selection_brackets_offset: 1,
    selection_brackets_size: 8,
    selection_brackets_thickness: 1,
    zoom_max: (0.1 * 30) + 0.001,
    zoom_min: 0.1 * 3,
    zoom_step: 0.1,
}