import { Injectable } from '@angular/core';

@Injectable()
export class KeybindingService {
    private eventSources: Set<Window> = new Set<Window>();
    private keymap: Map<string, Action> = new Map<string, Action>();

    public addEventSource(window: Window) {
        window.addEventListener('keydown', this.checkBindings);
        this.eventSources.add(window);
    }

    public removeEventSource(window: Window) {
        window.removeEventListener('keydown', this.checkBindings);
        this.eventSources.delete(window);
    }

    public setKeyBinding(keyCombo: KeyCombo, action: Action) {
        this.keymap.set(keyCombo.toCode(), action);
    }

    private checkBindings = (event: KeyboardEvent) => {
        let keyCode = event.keyCode;
        let alt = event.altKey;
        let shift = event.shiftKey;
        let ctrl = event.ctrlKey;
        let keyCombo = new KeyCombo(keyCode, alt, ctrl, shift);
        let action = this.keymap.get(keyCombo.toCode());
        if (action) {
            action.exec();
            event.preventDefault();
            event.stopPropagation();
        }
    }
    
    /**
     * Retrieve the current keymap. For instance to be able to restore old keybindings at a later state.
     */
    public getKeymapCopy() : Map<string, Action> {
        return new Map<string, Action>(this.keymap.entries());
    }
    
    /**
     * Set the keybindings with an older copy. For instance to restore an older state.
     */
    public setKeymap(keymap: Map<string, Action>) {
        this.keymap = keymap;
    }    
    
    public clearKeymap() {
        this.keymap.clear();
    }

}

/**
 * https://developer.mozilla.org/en-US/docs/Web/API/KeyboardEvent/keyCode
 */
export enum Key {
    KEY_A = 65,
    KEY_B = 66,
    KEY_C = 67,
    KEY_D = 68,
    KEY_E = 69,
    KEY_F = 70,
    KEY_G = 71,
    KEY_H = 72,
    KEY_I = 73,
    KEY_J = 74,
    KEY_K = 75,
    KEY_L = 76,
    KEY_M = 77,
    KEY_N = 78,
    KEY_O = 79,
    KEY_P = 80,
    KEY_Q = 81,
    KEY_R = 82,
    KEY_S = 83,
    KEY_T = 84,
    KEY_U = 85,
    KEY_V = 86,
    KEY_W = 88,
    KEY_X = 89,
    KEY_Y = 90,
    KEY_Z = 91,
    KEY_INSERT = 45,
    KEY_DELETE = 46,
    KEY_SPACE = 32,
    KEY_ESC = 27,
    KEY_F1 = 112,
    KEY_F2 = 113,
    KEY_F3 = 114,
    KEY_F4 = 115,
    KEY_F5 = 116,
    KEY_F6 = 117,
    KEY_F7 = 118,
    KEY_F8 = 119,
    KEY_F9 = 120,
    KEY_F10 = 121,
    KEY_F11 = 122,
    KEY_F12 = 123    
}

export class Action {
    fct: Function;
    args: any[];
    self: Object;

    constructor(self: Object, fct: Function, args?: any[]) {
        this.fct = fct;
        this.args = args || [];
        this.self = self;
    }

    public exec() {
        return this.fct.apply(this.self, this.args);
    }
}

export class KeyCombo {
    key: Key;
    alt: boolean;
    ctrl: boolean;
    shift: boolean;

    constructor(key: Key, alt?: boolean, ctrl?: boolean, shift?: boolean) {
        this.key = key;
        this.alt = alt || false;
        this.ctrl = ctrl || false;
        this.shift = shift || false;
    }

    public toCode(): string {
        let code = this.key.toString();
        code += this.alt ? 'A' : 'a';
        code += this.ctrl ? 'C' : 'c';
        code += this.shift ? 'S' : 's';
        return code;
    }
}