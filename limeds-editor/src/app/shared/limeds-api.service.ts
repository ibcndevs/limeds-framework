import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';

import { environment } from '../../environments/environment';
import * as limeds from './limeds';

@Injectable()
export class LimedsApi {
    private DEV_PORT: string = "8080";
    private HOST: string = null;
    private PORT: string = null;
    private CONTEXT = "__context";
    private auth = { withCredentials: true };

    constructor(
        private http: Http,
        private router: Router
    ) {
        this.PORT = this.getPort();
    }

    private getPort(): string {
        if (!environment.production) {
            console.log('development mode: using port ' + this.DEV_PORT);
            return this.DEV_PORT;
        }
        else {
            return window.location.port;
        }
    }


    initHost(): Observable<string> {
        if (this.HOST === null) {
            let base = window.location.protocol + '//' + window.location.hostname + ':' + this.PORT;
            let url = [base, this.CONTEXT].join('/')
            return this.http.get(url)
                .map(body => body.json())
                .map(cfg => {
                    this.HOST = base + (cfg.LIMEDS_API_ROOT === '/' ? '/_limeds' : cfg.LIMEDS_API_ROOT + '/_limeds');
                    return this.HOST;
                }).catch(err => {
                    this.HOST = [base, '_limeds'].join('/');
                    return this.HOST;
                });
        }
        else {
            return Observable.of(this.HOST);
        }
    }

    /**
     * Login with login name and password. This sets a cookie token, so it is best to refresh the page after login.
     */
    login(login, pass) {
        return this.initHost()
            .map(host => this.url(host, '..', 'auth', 'tokens'))
            .switchMap(url => this.http.post(url, { username: login, password: pass }))
            .map(this.extractData)
            .catch(err => this.handleError(err));
    }

    /**
     * Return all slices from the server as an Observable.
     */
    listSlices(): Observable<limeds.ISlice[]> {
        return this.initHost()
            .map(host => this.url(host, 'slices'))
            .switchMap(url => this.http.get(url, this.auth))
            .map(this.extractData)
            .catch(err => this.handleError(err));
    }

    /**
     * Get all details of 1 slice from the server.
     * 
     * @param id The id of this slice
     * @param version The version of this slice, if omitted the latest version will be returned
     */
    getSlice(id: string, version?: string): Observable<limeds.ISlice> {
        return this.initHost()
            .map(host => this.url(host, 'slices', id, version || 'latest'))
            .switchMap(url => this.http.get(url, this.auth))
            .map(this.extractData)
            .catch(err => this.handleError(err));
    }

    /**
     * Creates a new slice.
     * 
     * @param slice The new slice object
     */
    createSlice(slice: limeds.ISlice): Observable<limeds.ISlice> {
        return this.initHost()
            .map(host => this.url(host, 'slices'))
            .switchMap(url => this.http.post(url, slice, this.auth))
            .map(this.extractData)
            .catch(err => this.handleError(err));
    }

    /**
     * Updates an existing slice.
     * 
     * @param slice The new slice object
     * @param id The id of the slice to update
     * @param version The version of the slice to update. If omitted the latest version will be updated.
     */
    updateSlice(slice: limeds.ISlice, id: string, version?: string): Observable<limeds.ISlice> {
        return this.initHost()
            .map(host => this.url(host, 'slices', id, version || 'latest'))
            .switchMap(url => this.http.put(url, slice, this.auth))
            .map(this.extractData)
            .catch(err => this.handleError(err));
    }

    /**
     * Removes an existing slice.
     * 
     * @param id The id of this slice
     * @param version The version of this slice, if omitted the latest version will be deleted
     */
    removeSlice(id: string, version?: string): Observable<limeds.ISlice> {
        return this.initHost()
            .map(host => this.url(host, 'slices', id, version || 'latest'))
            .switchMap(url => this.http.delete(url, this.auth))
            .map(this.extractData)
            .catch(err => this.handleError(err));
    }

    /**
     * Deploys this version of the given slice
     * 
     * @param id The id of this slice
     * @param version The version of this slice, if omitted the latest version will be returned
     * @param sync The method will return only when the action was completed on the server.
     */
    deploySlice(id: string, version?: string, sync?: boolean): Observable<limeds.ISlice> {
        return this.initHost()
            .map(host => {
                let url = this.url(host, 'slices', id, version || 'latest', 'deploy');
                if (sync) {
                    url += '?sync=true';
                }
                return url;
            })
            .switchMap(url => this.http.get(url, this.auth))
            .map(this.extractData)
            .catch(err => this.handleError(err));
    }

    /**
     * Undeploys this version of the given slice
     * 
     * @param id The id of this slice
     * @param version The version of this slice, if omitted the latest version will be returned
     * @param sync The method will return only when the action was completed on the server.
     */
    undeploySlice(id: string, version?: string, sync?: boolean): Observable<limeds.ISlice> {
        return this.initHost()
            .map(host => {
                let url = this.url(host, 'slices', id, version || 'latest', 'undeploy');
                if (sync) {
                    url += '?sync=true';
                }
                return url;
            })
            .switchMap(url => this.http.get(url, this.auth))
            .map(this.extractData)
            .catch(err => this.handleError(err));
    }

    /**
     * Lists all the current http endpoints that are up
     */
    listHttpEndpoints(): Observable<limeds.HttpOperation[]> {
        return this.initHost()
            .map(host => this.url(host, 'http'))
            .switchMap(url => this.http.get(url, this.auth))
            .map(this.extractData)
            .catch(err => this.handleError(err));
    }

    /**
     * Lists all the current factory segments
     */
    listFactories(): Observable<limeds.ISegmentModel[]> {
        return this.initHost()
            .map(host => this.url(host, 'factories'))
            .switchMap(url => this.http.get(url, this.auth))
            .map(this.extractData)
            .catch(err => this.handleError(err));
    }

    /**
     * Return all segments from the server as an Observable.
     * 
     * @param all Shows all segments (the private ones - not exported- too).
     */
    listSegments(all?: boolean): Observable<limeds.ISegmentModel[]> {
        return this.initHost()
            .map(host => {
                let url = this.url(host, 'segments');
                if (all) {
                    url += '?showPrivate=true';
                }
                return url;
            })
            .switchMap(url => this.http.get(url, this.auth))
            .map(res => this.extractData(res))
            .catch(err => this.handleError(err));
    }

    listConfigs(): Observable<limeds.Configurations> {
        return this.initHost()
            .map(host => this.url(host, 'config'))
            .switchMap(url => this.http.get(url, this.auth))
            .map(this.extractData)
            .catch(err => this.handleError(err));
    }

    updateConfig(id: string, configuration: limeds.ConfigProperty[]): Observable<limeds.Template> {
        return this.initHost()
            .map(host => this.url(host, 'config', id))
            .switchMap(url => this.http.put(url, configuration, this.auth))
            .map(this.extractData)
            .catch(err => this.handleError(err));
    }

    deleteConfig(id: string): Observable<boolean> {
        return this.initHost()
            .map(host => this.url(host, 'config', id))
            .switchMap(url => this.http.delete(url, this.auth))
            .map(this.extractData)
            .catch(err => this.handleError(err));
    }

    instantiateFactory(factoryId: string, configuration: limeds.ConfigProperty[]): Observable<string> {
        return this.initHost()
            .map(host => this.url(host, 'config', factoryId))
            .switchMap(url => this.http.post(url, configuration, this.auth))
            .map(this.extractData)
            .catch(err => this.handleError(err));
    }

    /**
     * Get all details of 1 factory.
     * 
     * @param id The id of this factory
     * @param version The version of this factory, if omitted the latest version will be returned
     */
    getFactory(id: string, version?: string): Observable<limeds.ISegmentModel> {
        return this.initHost()
            .map(host => this.url(host, 'factories', id, version || 'latest'))
            .switchMap(url => this.http.get(url, this.auth))
            .map(this.extractData)
            .catch(err => this.handleError(err));
    }

    /**
     * Get all details of 1 segment.
     * 
     * @param id The id of this segment
     * @param version The version of this segment, if omitted the latest version will be returned
     */
    getSegment(id: string, version: string): Observable<limeds.ISegmentModel> {
        return this.initHost()
            .map(host => this.url(host, 'segments', id, version))
            .switchMap(url => this.http.get(url, this.auth))
            .map(this.extractData)
            .catch(err => this.handleError(err));
    }

    /**
     * Get all details version info of the host system
     */
    getHostInfo(): Observable<limeds.HostInfo> {
        return this.initHost()
            .map(host => this.url(host, 'hostinfo'))
            .switchMap(url => this.http.get(url, this.auth))
            .map(this.extractData)
            .catch(err => this.handleError(err));
    }

    /**
     * Get an overview of all locally available types.
     */
    listTypes() {
        return this.initHost()
            .map(host => this.url(host, 'types'))
            .switchMap(url => this.http.get(url, this.auth))
            .map(this.extractData)
            .catch(err => this.handleError(err));
    }

    /**
     * Return the requested type, if the resolve flag is present, other typeRefs are resolved.
     * 
     * @param typeId The id of the the type in question
     * @param resolve Whether to resolve recurring typeRefs
     */
    getType(typeId: string, resolve?: boolean) {
        return this.initHost()
            .map(host => {
                let url = this.url(host, 'types', typeId);
                url = resolve ? url + '?resolve' : url;
                return url;
            })
            .switchMap(url => this.http.get(url, this.auth))
            .map(this.extractData)
            .catch(err => this.handleError(err));
    }

    /**
     * Validate a given LimeDS json schema.
     */
    validateSchema(schema: any) {
        return this.initHost()
            .map(host => this.url(host, 'types', 'schemavalidator'))
            .switchMap(url => this.http.post(url, schema, this.auth))
            .map(this.extractData)
            .catch(err => this.handleError(err));
    }

    /**
    * Resolve a JSON Schema
    */
    resolveSchema(schema: any) {
        return this.initHost()
            .map(host => this.url(host, 'types', 'resolver'))
            .switchMap(url => this.http.post(url, schema, this.auth))
            .map(this.extractData)
            .catch(err => this.handleError(err));
    }

    /**
     * List all installable bundles from the default repostiory.
     * @param refresh When true, the repo index will be refreshed
     */
    listInstallables(refresh?: boolean): Observable<limeds.Installable[]> {
        return this.initHost()
            .map(host => {
                let part = refresh ? 'installables?refresh=true' : 'installables';
                return this.url(host, part);
            })
            .switchMap(url => this.http.get(url, this.auth))
            .map(this.extractData)
            .catch(err => this.handleError(err));
    }

    /**
     * Get more details about one installable (version)
     */
    getInstallable(id: string, version: string): Observable<limeds.ISlice[]> {
        return this.initHost()
            .map(host => this.url(host, 'installables', id, version))
            .switchMap(url => this.http.get(url, this.auth))
            .map(this.extractData)
            .catch(err => this.handleError(err));
    }

    /**
     * Install one particular version of an installable
     */
    installInstallable(id: string, version: string) {
        return this.initHost()
            .map(host => this.url(host, 'installables', id, version))
            .switchMap(url => this.http.put(url, this.auth))
            .map(this.extractData)
            .catch(err => this.handleError(err));
    }

    /**
     * Uninstall one particular version of an installable
     */
    uninstallInstallable(id: string, version: string) {
        return this.initHost()
            .map(host => this.url(host, 'installables', id, version))
            .switchMap(url => this.http.delete(url, this.auth))
            .map(this.extractData)
            .catch(err => this.handleError(err));
    }

    /**
     * List all issues.
     */
    listAllIssues(): Observable<limeds.IssueMap> {
        return this.initHost()
            .map(host => this.url(host, 'issues'))
            .switchMap(url => this.http.get(url, this.auth))
            .map(this.extractData)
            .catch(err => this.handleError(err));
    }

    /**
    * List the latest x issues.
    */
    listLatestIssues(amount: number): Observable<limeds.IssueMap> {
        return this.initHost()
            .map(host => this.url(host, 'issues?limit='+amount))
            .switchMap(url => this.http.get(url, this.auth))
            .map(this.extractData)
            .catch(err => this.handleError(err));
    }

    /**
     * Clear all issues permanently
     */
    clearIssues(): Observable<any> {
        return this.initHost()
            .map(host => this.url(host, 'issues'))
            .switchMap(url => this.http.delete(url))
            .map(this.extractData)
            .catch(err => this.handleError(err));
    }

    private extractData(res: Response) {
        if (res.status < 200 || res.status >= 300) {
            throw new Error('Bad response status: ' + res.status);
        }
        try {
            let body = res.json();
            return body;
        } catch (err) {
            return '';
        }
    }

    private url(...args: string[]) {
        return args.join('/');
    }

    private redirectToLogin() {
        this.router.navigate(['/login']);
    }

    private handleError(error: any) {
        if (error.status === 401) {
            // Auth error: redirect to login
            this.redirectToLogin();
            return Observable.throw(error.statusText);
        }
        else {
            let errMsg = error.message || 'Server error';
            return Observable.throw(error);
        }
    }

}