import { Routes, RouterModule } from '@angular/router';

import { ConfigurationEditor } from './configuration-editor.component';

const configRoutes: Routes = [
    { path: 'configurations', component: ConfigurationEditor }
];

export const configEditorRouting = RouterModule.forChild(configRoutes);