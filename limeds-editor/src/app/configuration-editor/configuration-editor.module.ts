import { NgModule } from '@angular/core';

import { SharedModule }  from '../shared/shared.module';

import { configEditorRouting } from './configuration-editor.routing';
import { ConfigurationEditor } from './configuration-editor.component';

@NgModule({
    imports: [
        SharedModule,
        configEditorRouting
    ],
    declarations: [
        ConfigurationEditor
    ]
})
export class ConfigurationEditorModule { }