import { Component, OnInit, ElementRef, ViewContainerRef, ViewChild, Renderer, ComponentFactoryResolver } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';

import { AddPropertyPanel } from '../panels';

import { KeybindingService } from '../shared/keybinding.service';
import { LimedsApi } from '../shared/limeds-api.service';
import * as limeds from '../shared/limeds';

import { cloneDeep } from 'lodash';

@Component({
    selector: 'lds-configuration-editor',
    templateUrl: './configuration-editor.component.html',
    styleUrls: ['./configuration-editor.component.css']
})
export class ConfigurationEditor implements OnInit {
    configs: limeds.Configurations = {};
    myForm = new FormGroup({
        configSelect: new FormControl('')
    });
    myForm2 = new FormGroup({});
    vm: any = {
        configEntries: []
    };
    selectedConfig: limeds.Template;
    carbonCopy: limeds.ConfigProperty[];
    description: string;

    @ViewChild('selected')
    private selected: ElementRef;

    constructor(
        private router: Router,
        private api: LimedsApi,
        private ref: ViewContainerRef,
        private keybindings: KeybindingService
    ) { }

    ngOnInit() {
        this.api.listConfigs().subscribe(this.reload);
        this.myForm.get('configSelect').valueChanges.subscribe(value => {
            if (this.saveOpenEditorData()) {
                this.loadEditorData(value);
            }
        })
    }

    private reload = (configs: limeds.Configurations) => {
        this.configs = configs;
    }

    private saveOpenEditorData() {
        return true;
    }

    private loadEditorData(configId: string) {
        this.description = null;
        this.selectedConfig = this.configs[configId];
        this.carbonCopy = cloneDeep(this.selectedConfig.configuration);
        this.initTemplate(this.configs[configId]);
        if (this.selectedConfig['sliceConfig']) {
            switch (this.selectedConfig.type) {
                case 'FACTORY':
                    this.api.getFactory(configId).subscribe(model => this.description = model.description);
                    break;
                case 'FACTORY_INSTANCE':
                    this.api.getFactory(this.selectedConfig.factoryId).subscribe(model => this.description = model.description);
                    break;
                case 'CONFIGURABLE_INSTANCE':
                    this.api.getSegment(configId, 'latest').subscribe(model => this.description = model.description);
                    break;
            }
        }
    }

    listConfigNames() {
        return Object.keys(this.configs).sort((a, b) => {
            let strA = this.configs[a].factoryId ? [this.configs[a].factoryId, a].join('_') : a;
            let strB = this.configs[b].factoryId ? [this.configs[b].factoryId, b].join('_') : b;
            let lcA = strA.toLowerCase();
            let lcB = strB.toLowerCase();

            return (lcA < lcB ? -1 : (lcA > lcB ? 1 : 0));
        });
    }

    getNameAmount() {
        return Object.keys(this.configs).length || 10;
    }

    getLabel(name: string) {
        let c = this.configs[name];
        return name.length >= 40 ? name.slice(0, 37) + '...' : name;
    }

    getPropertyLabel(property: limeds.ConfigProperty) {
        return property.name;
    }

    openWebsite() {
        window.open('http://limeds.intec.ugent.be/');
    }

    openWiki() {
        window.open('https://bitbucket.org/ibcndevs/limeds-framework/wiki/Home');
    }

    openSwagger() {
        window.open('/swagger/');
    }

    openSlices() {
        this.router.navigate(['/slices']);
    }

    private initTemplate(template: limeds.Template) {
        if (template && (template.type === 'FACTORY' || template.type === 'FACTORY_INSTANCE' || template.type === 'CONFIGURABLE_INSTANCE')) {
            for (let c in this.myForm2.controls) {
                this.myForm2.removeControl(c);
            }
            this.vm.configEntries = template.configuration
                .map(entry => {
                    let result: any = {};
                    if (entry.possibleValues && entry.possibleValues.length !== 0) {
                        result.simpleType = 'select';
                        result.possibleValues = entry.possibleValues;
                    }
                    else {
                        result.simpleType = this.getSimpleType(entry.type);
                    }
                    result.name = entry.name;
                    result.value = entry.value
                    result.type = entry.type;
                    if (entry.description) {
                        result.description = entry.description;
                    }

                    // array
                    if ('array' === result.simpleType) {
                        result.possibleValues = entry.value ? entry.value.split(',').map(item => item.trim()) : [''];
                        for (let i = 0; i < result.possibleValues.length; i++) {
                            this.myForm2.addControl(result.name + i, new FormControl(result.possibleValues[i]));
                        }
                    }
                    else if ('checkbox' === result.simpleType) {
                        this.myForm2.addControl(result.name, new FormControl(this.strToBool(result.value)));
                    }
                    else {
                        this.myForm2.addControl(result.name, new FormControl(result.value));
                    }

                    return result;
                }).
                sort((a, b) => {
                    return (a.name < b.name ? -1 : (a.name > b.name ? 1 : 0));
                });
        }
        else {
            this.vm.configEntries = [];
        }
    }

    private strToBool(str) {
        switch (str.toLowerCase()) {
            case "false":
            case "no":
            case "0":
            case "":
                return false;
            default:
                return true;
        }
    }

    private getSimpleType(type: string) {
        let simpleType: string;
        switch (type) {
            case "java.lang.String":
            default:
            case "double":
                simpleType = 'text';
                break;
            case "long":
                simpleType = 'number';
                break;
            case "boolean":
            case "java.lang.Boolean":
                simpleType = 'checkbox';
                break;
            case "java.lang.String[]":
                simpleType = 'array';
                break;
        }
        return simpleType;
    }

    isValid() {
        return true;
    }

    getOptionCss(key: string) {
        let obj = this.configs[key];
        let css: any = {
            'factory': 'FACTORY' === obj.type,
            'factory-instance': 'FACTORY_INSTANCE' === obj.type,
            'configurable-instance': 'CONFIGURABLE_INSTANCE' === obj.type
        };
        return css;
    }

    cancel() {
        this.selectedConfig.configuration = this.carbonCopy;
        this.initTemplate(this.selectedConfig);
    }

    reset() {
        let id = this.myForm.get('configSelect').value;
        this.api.deleteConfig(id)
            .switchMap(() => this.api.listConfigs())
            .do(this.reload)
            .subscribe(result => this.setSelected(id));
    }

    delete() {
        let id = this.myForm.get('configSelect').value;
        let parentId = this.selectedConfig.factoryId;
        this.api.deleteConfig(id)
            .switchMap(() => this.api.listConfigs())
            .do(this.reload)
            .subscribe(result => this.setSelected(parentId));
    }

    save() {
        let configuration: limeds.ConfigProperty[] = cloneDeep(this.vm.configEntries);
        for (let c of configuration) {
            if ('array' === c['simpleType']) {
                let amount = c.possibleValues.length;
                let results = [];
                for (let i = 0; i < amount; i++) {
                    let cc = this.myForm2.get(c.name + i);
                    if (cc && '' !== cc.value) {
                        results.push(cc.value.trim());
                    }
                }
                c.value = results.join(',');
                delete c.possibleValues;
            }
            else {
                let control = this.myForm2.controls[c.name];
                if (control) {
                    c.value = control.value + '';
                }
            }
        }

        let id = this.myForm.get('configSelect').value;

        this.api.updateConfig(id, configuration)
            .switchMap(() => this.api.listConfigs())
            .do(this.reload)
            .subscribe(() => this.setSelected(id));
    }

    instantiate() {
        let factory = this.selectedConfig;
        let factoryId = this.myForm.get('configSelect').value;

        let configuration: limeds.ConfigProperty[] = cloneDeep(this.vm.configEntries);
        for (let c of configuration) {
            if ('array' === c['simpleType']) {
                let amount = c.possibleValues.length;
                let results = [];
                for (let i = 0; i < amount; i++) {
                    let cc = this.myForm2.get(c.name + i);
                    if (cc && '' !== cc.value) {
                        results.push(cc.value.trim());
                    }
                }
                c.value = results.join(',');
                delete c.possibleValues;
            }
            else {
                let control = this.myForm2.controls[c.name];
                if (control) {
                    c.value = control.value + '';
                }
            }
        }

        let configProp = configuration.find(e => '$.id' === e.name);

        this.api.instantiateFactory(factoryId, configuration)
            .subscribe(result => {
                let id = configProp ? configProp.value : result;
                this.api.listConfigs().do(this.reload).subscribe(() => this.setSelected(id));
            });
    }

    addEntry(configProp: limeds.ConfigProperty, idx: number) {
        configProp.possibleValues.push('');
        this.myForm2.addControl(configProp.name + idx, new FormControl(''));
    }

    private setSelected(id: string) {
        this.myForm.get('configSelect').setValue(id);
    }
}