import { NgModule } from '@angular/core';

import { SharedModule } from '../shared/shared.module';
// import { PanelsModule } from '../panels/panels.module';

import { loginRouting } from './login.routing';
import { LoginComponent } from './login.component';

@NgModule({
    imports: [
        SharedModule,
        loginRouting
    ],
    declarations: [
        LoginComponent
    ]
})
export class LoginModule { }