import { Component } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { LimedsApi } from '../shared/limeds-api.service';

@Component({
    selector: 'lds-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent {

    constructor(
        private api: LimedsApi,
        private router: Router
    ) { }

    myForm = new FormGroup({
        'login': new FormControl('', Validators.required),
        'pass': new FormControl('', Validators.required)
    });

    login() {
        let login = this.myForm.get('login').value;
        let pass = this.myForm.get('pass').value;
        this.api.login(login, pass).subscribe(
            result => {
                this.router.navigate(['/']);
            },
            err => console.log(err)
        );
    }

}