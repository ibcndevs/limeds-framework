import { Routes, RouterModule } from '@angular/router';

import { ErrorsComponent } from './errors.component';

const errorsRoutes: Routes = [
    { path: 'errors', component: ErrorsComponent }
];

export const errorsRouting = RouterModule.forChild(errorsRoutes);