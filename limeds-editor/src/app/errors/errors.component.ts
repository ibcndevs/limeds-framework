import { Component, OnInit, ViewContainerRef, ComponentFactoryResolver, LOCALE_ID, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { Form, FormGroup, FormControl, FormControlName } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';

import { KeybindingService } from '../shared/keybinding.service';
import { LimedsApi } from '../shared/limeds-api.service';
import { PanelManager, PanelType, PanelAction, PanelResult } from '../panels';

import * as limeds from '../shared/limeds';

@Component({
    selector: 'lds-errors',
    templateUrl: './errors.component.html',
    styleUrls: ['./errors.component.css']
})
export class ErrorsComponent implements OnInit {
    filteredIssues: limeds.Issue[] = [];
    isLimited: boolean = true;
    myForm = new FormGroup({ filter: new FormControl('') });

    private issues: limeds.Issue[] = [];
    private datePipe;


    constructor(
        private keybindings: KeybindingService,
        private api: LimedsApi,
        private panelManager: PanelManager,
        private ref: ViewContainerRef,
        private resolver: ComponentFactoryResolver,
        private router: Router,
        @Inject(LOCALE_ID) private locale: string
    ) { }


    ngOnInit() {
        this.datePipe = new DatePipe(this.locale);
        this.keybindings.addEventSource(window);
        this.myForm.get('filter').valueChanges
            .debounceTime(300)
            .distinctUntilChanged()
            .subscribe(this.filter.bind(this));
        this.reload();
    }

    openIssue(issue: limeds.Issue) {
        let sub = this.panelManager.createPanel(PanelType.VIEW_ERROR, this.ref, true, { issue: issue }).subscribe(result => {
            switch (result.action) {
                case PanelAction.CONFIRM:
                case PanelAction.CANCEL:
                default:
                // do nothing
            }
        });
    }

    openWebsite() {
        window.open('http://limeds.intec.ugent.be/');
    }

    openWiki() {
        window.open('https://bitbucket.org/ibcndevs/limeds-framework/wiki/Home');
    }

    openSwagger() {
        window.open('/swagger/');
    }

    openDebugView() {
        window.open('/editor/errors');
    }

    openConfigurationEditor() {
        this.router.navigate(['/configurations']);
    }

    openInstallables() {
        this.router.navigate(['/install']);
    }

    openSlices() {
        this.router.navigate(['/slices']);
    }

    hardRefresh() {
        this.reload(true);
    }

    toggleFull() {
        this.isLimited = false;
        this.reload(true);
    }

    toggleLimit() {
        this.isLimited = true;
        this.reload(true);
    }

    clearIssues() {
        this.api.clearIssues().subscribe(result => this.reload(true));
    }

    private filter(term?: string) {
        this.filteredIssues = !term ? this.issues : this.issues.filter(item => {
            for (let key in item) {
                let str = key === 'timestamp' ? this.datePipe.transform(item[key], 'HH:mm:ss') : item[key];
                if (str.indexOf(term) > -1) {
                    return true;
                }
            }
            return false;
        });
    }

    private reload(refresh?: boolean) {
        if (refresh) {
            this.issues = [];
        }
        let obs = this.isLimited ? this.api.listLatestIssues(15) : this.api.listAllIssues();
        obs.subscribe(issueMap => {
            for (let key in issueMap) {
                let issueArray: any = issueMap[key];
                for (let issue of issueArray) {
                    this.issues.push(issue);
                }
            }
            this.issues.sort((a, b) => {
                if (a.timestamp === b.timestamp) {
                    let la = a.source.toLowerCase();
                    let lb = b.source.toLowerCase();
                    return (la < lb ? -1 : ( la > lb ? 1 : 0));
                }
                else {
                    return b.timestamp - a.timestamp;
                }
            });
        });
        this.filter();
    }

    private sortInstallables = (a, b) => {
        if (a.id.toLowerCase() === b.id.toLowerCase()) {
            return this.sortSemVer(a.version, b.version);
        }
        else {
            let la = a.id.toLowerCase();
            let lb = b.id.toLowerCase();
            return (la < lb ? -1 : (la > lb ? 1 : 0));
        }
    }

    private sortSemVer = (a: string, b: string) => {
        let [aMajor, aMinor, aPatch, aQual] = a.split('.');
        let [bMajor, bMinor, bPatch, bQual] = b.split('.');
        if (aMajor === bMajor) {
            if (aMinor === bMinor) {
                if (aPatch === bPatch) {
                    if (bQual && aQual) {
                        let la = aQual.toLowerCase();
                        let lb = bQual.toLowerCase();
                        return (lb < la ? -1 : (lb > la ? 1 : 0));
                    }
                    else if (aQual) {
                        return 1;
                    } else {
                        return -1;
                    }
                }
                else {
                    return parseInt(bPatch) - parseInt(aPatch);
                }
            }
            else {
                return parseInt(bMinor) - parseInt(aMinor);
            }
        }
        else {
            return parseInt(bMajor) - parseInt(aMajor);
        }
    }
}