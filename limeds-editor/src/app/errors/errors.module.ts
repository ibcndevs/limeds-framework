import { NgModule } from '@angular/core';

import { SharedModule }  from '../shared/shared.module';

import { errorsRouting } from './errors.routing';
import { ErrorsComponent } from './errors.component';

@NgModule({
    imports: [
        SharedModule,
        errorsRouting
    ],
    declarations: [
        ErrorsComponent
    ]
})
export class ErrorsModule { }