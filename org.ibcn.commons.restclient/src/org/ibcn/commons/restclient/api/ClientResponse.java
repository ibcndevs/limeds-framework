/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.commons.restclient.api;

/**
 * Interface defining a HTTP response that was retrieved by the client from the
 * target server.
 * 
 * @author wkerckho
 *
 * @param <T>
 *            The type of the response body
 */
public interface ClientResponse<T> {

	/**
	 * Gets the HTTP status code of the response.
	 * 
	 * @return An integer status code
	 */
	int status();

	/**
	 * Gets the HTTP status message of the response (e.g. FORBIDDEN).
	 * 
	 * @return A status message String
	 */
	String statusText();

	/**
	 * Gets the HTTP headers of the response.
	 * 
	 * @return A Headers instance
	 */
	Headers getHeaders();

	/**
	 * Gets the body contained in the response.
	 * 
	 * @return A Void, String, JsonNode or InputStream instance, depending on
	 *         the type parameter T.
	 */
	T getBody();

}
