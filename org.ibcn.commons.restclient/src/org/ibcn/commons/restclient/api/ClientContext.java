/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.commons.restclient.api;

import java.io.InputStream;

/**
 * Interface defining a ClientContext, used to specify the details of this REST
 * call, including the required HTTP headers, additional query parameters, the
 * HTTP method to call and an optional message body.
 * 
 * @author wkerckho
 *
 */
public interface ClientContext {

	/**
	 * Appends a query parameter to the target URL.
	 * 
	 * @param name
	 *            The name of the query parameter
	 * @param value
	 *            The value of the query parameter
	 * @return The current ClientContext instance
	 */
	ClientContext withQuery(String name, Object value);

	/**
	 * Sets a HTTP header for this session.
	 * 
	 * @param name
	 *            The name of the header
	 * @param value
	 *            The value of the header
	 * @return The current ClientContext instance
	 */
	ClientContext withHeader(String name, Object value);

	/**
	 * Configures the session to perform a HTTP HEAD request.
	 * 
	 * @return A ClientReceiver instance
	 * @throws Exception
	 */
	ClientReceiver head() throws Exception;

	/**
	 * Configures the session to perform a HTTP GET request.
	 * 
	 * @return A ClientReceiver instance
	 * @throws Exception
	 */
	ClientReceiver get() throws Exception;

	/**
	 * Configures the session to perform a HTTP POST request with a JSON message
	 * as argument (the session automatically sets the Content-Type HTTP header
	 * to application/json).
	 * 
	 * @param body
	 *            A String instance that represents the JSON message to use as
	 *            the request body
	 * @return A ClientReceiver instance
	 * @throws Exception
	 */
	ClientReceiver postJson(String body) throws Exception;

	/**
	 * Configures the session to perform a HTTP POST request.
	 * 
	 * @param body
	 *            A String instance to use as the request body
	 * @return A ClientReceiver instance
	 * @throws Exception
	 */
	ClientReceiver post(String body) throws Exception;

	/**
	 * Configures the session to perform a HTTP POST request.
	 * 
	 * @param body
	 *            A byte array to use as the request body
	 * @return A ClientReceiver instance
	 * @throws Exception
	 */
	ClientReceiver post(byte[] body) throws Exception;

	/**
	 * Configures the session to perform a HTTP POST request.
	 * 
	 * @param body
	 *            A binary inputstream to use as the request body
	 * @return A ClientReceiver instance
	 * @throws Exception
	 */
	ClientReceiver post(InputStream body) throws Exception;

	/**
	 * Configures the session to perform a HTTP PUT request with a JSON message
	 * as argument (the session automatically sets the Content-Type HTTP header
	 * to application/json).
	 * 
	 * @param body
	 *            A JsonNode instance to use as the request body
	 * @return A ClientReceiver instance
	 * @throws Exception
	 */
	ClientReceiver putJson(String body) throws Exception;

	/**
	 * Configures the session to perform a HTTP PUT request.
	 * 
	 * @param body
	 *            A String instance to use as the request body
	 * @return A ClientReceiver instance
	 * @throws Exception
	 */
	ClientReceiver put(String body) throws Exception;

	/**
	 * Configures the session to perform a HTTP PUT request.
	 * 
	 * @param body
	 *            A byte array to use as the request body
	 * @return A ClientReceiver instance
	 * @throws Exception
	 */
	ClientReceiver put(byte[] body) throws Exception;

	/**
	 * Configures the session to perform a HTTP PUT request.
	 * 
	 * @param body
	 *            A binary inputstream to use as the request body
	 * @return A ClientReceiver instance
	 * @throws Exception
	 */
	ClientReceiver put(InputStream body) throws Exception;

	/**
	 * Configures the session to perform a HTTP DELETE request.
	 * 
	 * @return A ClientReceiver instance
	 * @throws Exception
	 */
	ClientReceiver delete() throws Exception;

}
