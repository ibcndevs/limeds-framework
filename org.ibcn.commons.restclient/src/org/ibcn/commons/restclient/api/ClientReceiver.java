/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.commons.restclient.api;

import java.io.InputStream;
import java.util.function.Function;

/**
 * Interface defining the receiving part of the client, actually performs the
 * request as configured by the ClientContext.
 * 
 * @author wkerckho
 *
 */
public interface ClientReceiver {

	/**
	 * Execute the HTTP request and ignore the response.
	 * 
	 * @throws Exception
	 */
	void returnNoResult() throws Exception;

	/**
	 * Execute the HTTP request and return a ClientResponse instance without a
	 * body (only containing status info and response headers).
	 * 
	 * @return A ClientResponse instance
	 * @throws Exception
	 */
	ClientResponse<Void> returnNoBody() throws Exception;

	/**
	 * Execute the HTTP request and return a ClientResponse instance with a
	 * String as the returned body.
	 * 
	 * @return A ClientResponse instance
	 * @throws Exception
	 */
	ClientResponse<String> returnString() throws Exception;

	/**
	 * Execute the HTTP request and return a ClientResponse instance with a
	 * String as the returned body, specifying the decoding charset.
	 * 
	 * @param charset
	 *            The charset to decode the response
	 * @return A ClientResponse instance
	 * @throws Exception
	 */
	ClientResponse<String> returnString(String charset) throws Exception;

	/**
	 * Execute the HTTP request and return a ClientResponse instance with the
	 * parameterized type as the returned body.
	 * 
	 * @param deserializer
	 *            The deserializer function to apply to convert the HTTP
	 *            response body bytes into the desired type.
	 * @return A ClientResponse instance
	 * @throws Exception
	 */
	<T> ClientResponse<T> returnObject(Function<InputStream, T> deserializer) throws Exception;

	/**
	 * Execute the HTTP request and return a ClientResponse instance with binary
	 * data as the returned body. The binary data is represented as an
	 * InputStream.
	 * 
	 * @return A ClientResponse instance
	 * @throws Exception
	 */
	ClientResponse<InputStream> returnBinary() throws Exception;

}
