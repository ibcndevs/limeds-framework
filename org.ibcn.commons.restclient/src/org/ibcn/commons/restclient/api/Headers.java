/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.commons.restclient.api;

import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * An utility class used to represent a collection of HTTP headers as a
 * multimap.
 * 
 * @author wkerckho
 *
 */
public class Headers extends HashMap<String, List<String>> {

	private static final long serialVersionUID = -7730719233270414579L;

	/**
	 * Gets the first value present for the specified header name.
	 * 
	 * @param key
	 *            The name of the header
	 * @return The first value for this header
	 */
	public String getFirst(Object key) {
		List<String> list = get(key);
		if (list != null && list.size() > 0) {
			return list.get(0);
		}
		return null;
	}

	public String getValueAsString(Object key) {
		return get(key).stream().collect(Collectors.joining(","));
	}

	@Override
	public boolean containsKey(Object key) {
		return keySet().stream().anyMatch(mapKey -> mapKey.toLowerCase().equals(("" + key).toLowerCase()));
	}

	@Override
	public List<String> get(Object key) {
		Optional<List<String>> result = entrySet().stream()
				.filter(e -> e.getKey().toLowerCase().equals(("" + key).toLowerCase())).map(e -> e.getValue())
				.findAny();
		return result.isPresent() ? result.get() : null;
	}

}
