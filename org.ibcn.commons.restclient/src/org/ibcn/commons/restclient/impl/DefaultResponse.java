/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.commons.restclient.impl;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.zip.GZIPInputStream;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.ibcn.commons.restclient.api.ClientResponse;
import org.ibcn.commons.restclient.api.Headers;

public class DefaultResponse<T> implements ClientResponse<T> {

	private int status;
	private String statusText;
	private Headers headers = new Headers();
	private T body;

	public DefaultResponse(HttpResponse response, Class<?> responseClass) {
		this(response, responseClass, null, null);
	}

	public DefaultResponse(HttpResponse response, Class<?> responseClass, Function<InputStream, T> deserializer) {
		this(response, responseClass, null, deserializer);
	}

	public DefaultResponse(HttpResponse response, Class<?> responseClass, String charset) {
		this(response, responseClass, charset, null);
	}

	@SuppressWarnings("unchecked")
	public DefaultResponse(HttpResponse response, Class<?> responseClass, String charset,
			Function<InputStream, T> deserializer) {
		status = response.getStatusLine().getStatusCode();
		statusText = response.getStatusLine().getReasonPhrase();

		for (Header header : response.getAllHeaders()) {
			String headerName = header.getName().toLowerCase();
			List<String> list = headers.get(headerName);
			if (list == null)
				list = new ArrayList<String>();
			list.add(header.getValue());
			headers.put(headerName, list);
		}

		HttpEntity responseEntity = response.getEntity();
		if (responseEntity != null) {
			// charset == null --> Auto figure out charset
			if (charset == null) {
				charset = "UTF-8";
				Header contentType = responseEntity.getContentType();
				if (contentType != null) {
					String responseCharset = ResponseUtils.getCharsetFromContentType(contentType.getValue());
					if (responseCharset != null && !responseCharset.trim().equals("")) {
						charset = responseCharset;
					}
				}
			}

			try {
				byte[] rawBody;
				try {
					InputStream responseInputStream = responseEntity.getContent();
					if (ResponseUtils.isGzipped(responseEntity.getContentEncoding())) {
						responseInputStream = new GZIPInputStream(responseEntity.getContent());
					}
					rawBody = ResponseUtils.getBytes(responseInputStream);
				} catch (IOException e2) {
					throw new RuntimeException(e2);
				}

				if (Object.class.equals(responseClass) && deserializer != null) {
					this.body = (T) deserializer.apply(new ByteArrayInputStream(rawBody));
				} else if (String.class.equals(responseClass)) {
					this.body = (T) new String(rawBody, charset);
				} else if (InputStream.class.equals(responseClass)) {
					this.body = (T) new ByteArrayInputStream(rawBody);
				} else if (Void.class.equals(responseClass)) {
					this.body = null;
				} else {
					throw new Exception(
							"Unknown result type. Only String, Object (with specified deserializer), InputStream and Void are supported.");
				}
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		}

		try {
			EntityUtils.consume(responseEntity);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}

	}

	@Override
	public int status() {
		return status;
	}

	@Override
	public String statusText() {
		return statusText;
	}

	@Override
	public Headers getHeaders() {
		return headers;
	}

	@Override
	public T getBody() {
		return body;
	}

}
