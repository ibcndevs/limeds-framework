/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.commons.restclient.impl;

import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.net.URLEncoder;
import java.security.Permission;
import java.security.PermissionCollection;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHeaders;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpHead;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.entity.InputStreamEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.message.BasicHeader;
import org.ibcn.commons.restclient.api.Client;
import org.ibcn.commons.restclient.api.ClientContext;
import org.ibcn.commons.restclient.api.ClientReceiver;
import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Component
public class DefaultClient implements Client {

	private static final String APPLICATION_JSON = "application/json";
	private static final String TEXT_PLAIN = "text/plain";
	private static final String APPLICATION_OCTET_STREAM = "application/octet-stream";

	protected static Logger LOGGER = LoggerFactory.getLogger(DefaultClient.class);

	private HttpClient httpClient;

	static {
		removeCryptographyRestrictions();
	}

	public DefaultClient() {
		int timeout = Integer.parseInt(System.getProperty("org.ibcn.commons.restclient.timeout", "8000"));
		LOGGER.info("Setting up HttpClient with timeout " + timeout + " ms");

		RequestConfig config = RequestConfig.custom().setSocketTimeout(timeout).setConnectTimeout(timeout).build();
		httpClient = HttpClients.custom().setConnectionManager(new PoolingHttpClientConnectionManager())
				.setDefaultRequestConfig(config).build();
	}

	@Override
	public ClientContext target(String url) {
		return new ClientContext() {

			private LinkedHashMap<String, Object> queryParams = new LinkedHashMap<>();
			private LinkedList<Header> headers = new LinkedList<>();

			@Override
			public ClientContext withQuery(String name, Object value) {
				try {
					queryParams.put(name, URLEncoder.encode((String) value, "UTF-8"));
				} catch (UnsupportedEncodingException e) {
					// TODO
					e.printStackTrace();
				}
				return this;
			}

			@Override
			public ClientContext withHeader(String name, Object value) {
				headers.add(new BasicHeader(name, value.toString()));
				return this;
			}

			private String generateQuery() throws Exception {
				String query = queryParams.size() > 0 ? "?" : "";
				query += queryParams.entrySet().stream().map(e -> e.getKey() + "=" + e.getValue())
						.collect(Collectors.joining("&"));
				return query;
			}

			@Override
			public ClientReceiver head() throws Exception {
				HttpHead head = new HttpHead(url + generateQuery());
				head.setHeaders(headers.toArray(new Header[] {}));
				return new DefaultClientReceiver(head, httpClient);
			}

			@Override
			public ClientReceiver get() throws Exception {
				HttpGet get = new HttpGet(url + generateQuery());
				get.setHeaders(headers.toArray(new Header[] {}));
				return new DefaultClientReceiver(get, httpClient);
			}

			private ClientReceiver post(HttpEntity entity) throws Exception {
				HttpPost post = new HttpPost(url + generateQuery());
				post.setHeaders(headers.toArray(new Header[] {}));
				post.setEntity(entity);
				return new DefaultClientReceiver(post, httpClient);
			}

			private ClientReceiver put(HttpEntity entity) throws Exception {
				HttpPut put = new HttpPut(url + generateQuery());
				put.setHeaders(headers.toArray(new Header[] {}));
				put.setEntity(entity);
				return new DefaultClientReceiver(put, httpClient);
			}

			@Override
			public ClientReceiver delete() throws Exception {
				HttpDelete delete = new HttpDelete(url + generateQuery());
				delete.setHeaders(headers.toArray(new Header[] {}));
				return new DefaultClientReceiver(delete, httpClient);
			}

			private boolean isContentTypeSpecified() {
				return headers.stream().anyMatch(h -> HttpHeaders.CONTENT_TYPE.equals(h.getName()));
			}

			@Override
			public ClientReceiver postJson(String body) throws Exception {
				if (!isContentTypeSpecified()) {
					withHeader(HttpHeaders.CONTENT_TYPE, APPLICATION_JSON);
				}
				return post(new StringEntity(body));
			}

			@Override
			public ClientReceiver post(String body) throws Exception {
				if (!isContentTypeSpecified()) {
					withHeader(HttpHeaders.CONTENT_TYPE, TEXT_PLAIN);
				}
				return post(new StringEntity(body));
			}

			@Override
			public ClientReceiver post(byte[] body) throws Exception {
				if (!isContentTypeSpecified()) {
					withHeader(HttpHeaders.CONTENT_TYPE, APPLICATION_OCTET_STREAM);
				}
				return post(new ByteArrayEntity(body));
			}

			@Override
			public ClientReceiver putJson(String body) throws Exception {
				if (!isContentTypeSpecified()) {
					withHeader(HttpHeaders.CONTENT_TYPE, APPLICATION_JSON);
				}
				return put(new StringEntity(body.toString()));
			}

			@Override
			public ClientReceiver put(String body) throws Exception {
				if (!isContentTypeSpecified()) {
					withHeader(HttpHeaders.CONTENT_TYPE, TEXT_PLAIN);
				}
				return put(new StringEntity(body));
			}

			@Override
			public ClientReceiver put(byte[] body) throws Exception {
				if (!isContentTypeSpecified()) {
					withHeader(HttpHeaders.CONTENT_TYPE, APPLICATION_OCTET_STREAM);
				}
				return put(new ByteArrayEntity(body));
			}

			@Override
			public ClientReceiver post(InputStream body) throws Exception {
				if (!isContentTypeSpecified()) {
					withHeader(HttpHeaders.CONTENT_TYPE, APPLICATION_OCTET_STREAM);
				}
				return post(new InputStreamEntity(body));
			}

			@Override
			public ClientReceiver put(InputStream body) throws Exception {
				if (!isContentTypeSpecified()) {
					withHeader(HttpHeaders.CONTENT_TYPE, APPLICATION_OCTET_STREAM);
				}
				return put(new InputStreamEntity(body));
			}

		};
	}

	/* Remove the Cryptography restrictions put in by USA */

	private static void removeCryptographyRestrictions() {
		if (!isRestrictedCryptography()) {
			LOGGER.info("Cryptography restrictions removal not needed");
			return;
		}
		try {
			/*
			 * Do the following, but with reflection to bypass access checks:
			 *
			 * JceSecurity.isRestricted = false;
			 * JceSecurity.defaultPolicy.perms.clear();
			 * JceSecurity.defaultPolicy.add(CryptoAllPermission.INSTANCE);
			 */

			final Class<?> jceSecurity = ClassLoader.getSystemClassLoader().loadClass("javax.crypto.JceSecurity");
			final Class<?> cryptoPermissions = ClassLoader.getSystemClassLoader()
					.loadClass("javax.crypto.CryptoPermissions");
			final Class<?> cryptoAllPermission = ClassLoader.getSystemClassLoader()
					.loadClass("javax.crypto.CryptoAllPermission");

			final Field isRestrictedField = jceSecurity.getDeclaredField("isRestricted");
			isRestrictedField.setAccessible(true);
			
			// Set final off
			Field modifiersField = Field.class.getDeclaredField("modifiers");
			modifiersField.setAccessible(true);
			modifiersField.setInt(isRestrictedField, isRestrictedField.getModifiers() & ~Modifier.FINAL);

			isRestrictedField.set(null, false);

			final Field defaultPolicyField = jceSecurity.getDeclaredField("defaultPolicy");
			defaultPolicyField.setAccessible(true);
			final PermissionCollection defaultPolicy = (PermissionCollection) defaultPolicyField.get(null);

			final Field perms = cryptoPermissions.getDeclaredField("perms");
			perms.setAccessible(true);
			((Map<?, ?>) perms.get(defaultPolicy)).clear();

			final Field instance = cryptoAllPermission.getDeclaredField("INSTANCE");
			instance.setAccessible(true);
			defaultPolicy.add((Permission) instance.get(null));

			LOGGER.info("Successfully removed cryptography restrictions");
		} catch (final Exception e) {
			LOGGER.warn("Failed to remove cryptography restrictions");
		}
	}

	private static boolean isRestrictedCryptography() {
		// This simply matches the Oracle JRE, but not OpenJDK.
		return "Java(TM) SE Runtime Environment".equals(System.getProperty("java.runtime.name"));
	}
}
