/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.commons.restclient.impl;

import java.io.InputStream;
import java.util.function.Function;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpUriRequest;
import org.ibcn.commons.restclient.api.ClientReceiver;
import org.ibcn.commons.restclient.api.ClientResponse;

public class DefaultClientReceiver implements ClientReceiver {

	private HttpUriRequest request;
	private HttpClient httpClient;

	public DefaultClientReceiver(HttpUriRequest request, HttpClient httpClient) {
		this.request = request;
		this.httpClient = httpClient;
	}

	@Override
	public ClientResponse<String> returnString() throws Exception {
		HttpResponse httpResponse = httpClient.execute(request);
		return new DefaultResponse<String>(httpResponse, String.class);
	}

	@Override
	public ClientResponse<String> returnString(String charset) throws Exception {
		HttpResponse httpResponse = httpClient.execute(request);
		return new DefaultResponse<String>(httpResponse, String.class, charset);
	}

	@Override
	public ClientResponse<InputStream> returnBinary() throws Exception {
		HttpResponse httpResponse = httpClient.execute(request);
		return new DefaultResponse<InputStream>(httpResponse, InputStream.class);
	}

	@Override
	public ClientResponse<Void> returnNoBody() throws Exception {
		HttpResponse httpResponse = httpClient.execute(request);
		return new DefaultResponse<Void>(httpResponse, Void.class);
	}

	@Override
	public void returnNoResult() throws Exception {
		returnBinary();
	}

	@Override
	public <T> ClientResponse<T> returnObject(Function<InputStream, T> deserializer) throws Exception {
		HttpResponse httpResponse = httpClient.execute(request);
		return new DefaultResponse<T>(httpResponse, Object.class, deserializer);
	}

}
