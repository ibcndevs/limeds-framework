/*******************************************************************************
 * Copyright 2017 Ghent University - imec, IDLab, Department of Information Technology
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.ibcn.limeds.cluster.provider;

import java.io.InputStream;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ibcn.commons.restclient.api.Client;
import org.ibcn.commons.restclient.api.ClientContext;
import org.ibcn.commons.restclient.api.ClientResponse;
import org.ibcn.limeds.SegmentContext;
import org.ibcn.limeds.annotations.HttpMethod;
import org.ibcn.limeds.annotations.LoadBalancingStrategy;
import org.ibcn.limeds.cluster.ClusterNode;
import org.ibcn.limeds.cluster.LoadBalancingController;
import org.ibcn.limeds.cluster.LoadBalancingStrategyProvider;
import org.ibcn.limeds.descriptors.HttpOperationDescriptor;
import org.ibcn.limeds.util.Errors;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.component.annotations.ReferencePolicy;
import org.osgi.service.metatype.annotations.Designate;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

import com.google.common.collect.Iterators;
import com.google.common.collect.Sets;
import com.google.common.io.ByteStreams;

/**
 * This annotation defines the cluster config object (see OSGi Metatype
 * specification)
 * 
 * @author wkerckho
 *
 */
@ObjectClassDefinition(name = "LimeDS Default Cluster Implementation Config")
@interface ClusterConfig {
	String[] hosts();
}

/**
 * This class provides the default implementation for the LimeDS load balancing
 * controller.
 * 
 * @author wkerckho
 *
 */
@Component(configurationPolicy = ConfigurationPolicy.REQUIRE)
@Designate(ocd = ClusterConfig.class)
public class LoadBalancingControllerImpl implements LoadBalancingController {

	@Reference(cardinality = ReferenceCardinality.MANDATORY, policy = ReferencePolicy.DYNAMIC)
	private Client restClient;
	private Map<String, LoadBalancingStrategyProvider> strategies = new ConcurrentHashMap<>();

	private Set<ClusterNode> cluster;

	@Activate
	public void start(ClusterConfig clusterConfig) {
		cluster = Arrays.stream(clusterConfig.hosts()).map(ClusterNode::new).collect(Collectors.toSet());

		// Add default strategies
		strategies.put(LoadBalancingStrategy.ROUND_ROBIN, new LoadBalancingStrategyProvider() {

			private Iterator<ClusterNode> clusterIter = Iterators.cycle(cluster.toArray(new ClusterNode[] {}));

			@Override
			public Set<ClusterNode> getTargetHosts(SegmentContext context) {
				return Sets.newHashSet(clusterIter.next());
			}

			@Override
			public String getStrategyId() {
				return LoadBalancingStrategy.ROUND_ROBIN;
			}
		});

		strategies.put(LoadBalancingStrategy.DUPLICATE, new LoadBalancingStrategyProvider() {

			@Override
			public Set<ClusterNode> getTargetHosts(SegmentContext context) {
				return cluster;
			}

			@Override
			public String getStrategyId() {
				return LoadBalancingStrategy.DUPLICATE;
			}
		});
	}

	@Deactivate
	public void stop() {
		strategies.clear();
		cluster = null;
	}

	@Reference(policy = ReferencePolicy.DYNAMIC, cardinality = ReferenceCardinality.MULTIPLE)
	public void bindStrategyProvider(LoadBalancingStrategyProvider provider) {
		strategies.put(provider.getStrategyId(), provider);
	}

	public void unbindStrategyProvider(LoadBalancingStrategyProvider provider) {
		strategies.remove(provider.getStrategyId());
	}

	@Override
	public boolean isTarget(HttpServletRequest request) {
		return request.getParameter("lb-target") != null && "true".equals(request.getParameter("lb-target"));
	}

	@Override
	public boolean handle(HttpServletRequest request, HttpServletResponse response, SegmentContext context)
			throws Exception {
		// Lookup the load balancing strategyF
		HttpOperationDescriptor httpOp = context.getDescriptor().getHttpOperation();
		LoadBalancingStrategyProvider strategyProvider = strategies.get(httpOp.getLoadBalancing());
		if (strategyProvider != null) {
			// Get targets based on the configured strategy
			Set<ClusterNode> targets = strategyProvider.getTargetHosts(context);
			if (targets.size() == 1) {
				/*
				 * The request is redirected in case of a single target
				 */
				ClusterNode target = targets.stream().findFirst().get();
				if (!target.isLocalNode()) {
					response.setStatus(HttpServletResponse.SC_TEMPORARY_REDIRECT);
					response.setHeader("Location",
							request.getScheme() + "://" + target + getRequestedPath(request) + "?lb-target=true");
					return true;
				} else {
					return false;
				}
			} else {
				/*
				 * The request is duplicated for each target in case of multiple
				 * targets (this takes longer!)
				 */
				List<ClientResponse<InputStream>> responses = targets.stream()
						.map(Errors.wrapFunction(node -> doRemoteCall(node, request))).collect(Collectors.toList());
				// Find potential problems
				if (responses.stream().anyMatch(clientResponse -> clientResponse.status() != 200)) {
					throw new Exception("Following load balancing targets returned an error: " + responses.stream()
							.filter(clientResponse -> clientResponse.status() != 200).collect(Collectors.toList()));
				} else {
					// Return one of the responses
					responses.stream().findAny().ifPresent(Errors.wrapConsumer(clientResponse -> {
						// Set headers
						clientResponse.getHeaders().keySet().stream().forEach(headerName -> response
								.setHeader(headerName, clientResponse.getHeaders().getValueAsString(headerName)));

						// Write body
						ByteStreams.copy(clientResponse.getBody(), response.getOutputStream());

						// Commit
						response.setStatus(200);
					}));
					return true;
				}
			}
		} else {
			throw new Exception("Requested load balancing strategy " + httpOp.getLoadBalancing() + " cannot be found!");
		}
	}

	private ClientResponse<InputStream> doRemoteCall(ClusterNode node, HttpServletRequest request) throws Exception {
		ClientContext httpContext = restClient.target(request.getScheme() + "://" + node + getRequestedPath(request));
		// Copy all headers
		Collections.list(request.getHeaderNames())
				.forEach(headerName -> httpContext.withHeader(headerName, request.getHeader(headerName)));

		// Copy all query parameters
		Collections.list(request.getParameterNames())
				.forEach(queryName -> httpContext.withQuery(queryName, request.getParameter(queryName)));

		// Add lb indication
		httpContext.withQuery("lb-target", true);

		// Set method and optional body
		HttpMethod method = HttpMethod.valueOf(request.getMethod().toUpperCase());
		switch (method) {
		case GET:
			return httpContext.get().returnBinary();
		case PUT:
			return httpContext.put(request.getInputStream()).returnBinary();
		case POST:
			return httpContext.post(request.getInputStream()).returnBinary();
		case DELETE:
			return httpContext.delete().returnBinary();
		default:
			throw new Exception("Unsupported HTTP method!");
		}
	}

	private String getRequestedPath(HttpServletRequest request) {
		if (request.getPathInfo() == null) {
			return request.getServletPath();
		} else {
			return request.getPathInfo();
		}
	}

}
